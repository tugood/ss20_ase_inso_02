import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-link-account',
  templateUrl: './link-account.component.html',
  styleUrls: ['./link-account.component.scss']
})
export class LinkAccountComponent implements OnInit {

  redirectURI = 'https://shoepping.tugood.team/tugood-interaction';

  constructor() {
  }

  ngOnInit(): void {
    localStorage.removeItem('access_token');
  }

  directToKeycloak() {
    window.location.href = 'https://auth.dev.tugood.team/auth/realms/tugood/protocol/openid-connect/auth' +
      '?client_id=shoepping' +
      '&redirect_uri=' + this.redirectURI +
      '&scope=openid' +
      '&response_type=code' +
      '&response_mode=query' +
      '&nonce=2qufjctm1p1';
  }
}
