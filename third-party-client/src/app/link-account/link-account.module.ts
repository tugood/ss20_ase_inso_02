import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from '../material.module';
import {RouterModule, Routes} from '@angular/router';
import {LinkAccountComponent} from './link-account.component';

export const linkAccountRoutes: Routes = [
  {path: '', pathMatch: 'full', component: LinkAccountComponent},
];

@NgModule({
  declarations: [LinkAccountComponent],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule
  ]
})
export class LinkAccountModule {
}
