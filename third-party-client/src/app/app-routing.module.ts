import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {linkAccountRoutes} from './link-account/link-account.module';
import {tugoodInteractionRoutes} from './tugood-interaction/tugood-interaction.module';


const routes: Routes = [
  ...linkAccountRoutes,
  ...tugoodInteractionRoutes
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
