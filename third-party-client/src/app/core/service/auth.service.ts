import {Injectable} from '@angular/core';
import {mergeMap} from 'rxjs/operators';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {ActivatedRoute} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isLoggedIn: boolean;
  clientSecret = 'cb496f1c-0a34-49b9-bfdf-14bfc37b7d46';
  redirectURI = 'https://shoepping.tugood.team/tugood-interaction';
  tugoodKarmaURI = 'https://dev.tugood.team';
  authorizationCode: string;
  refreshToken: string;

  constructor(private http: HttpClient) {
    this.isLoggedIn = false;
  }

  authenticateWithTugood(activatedRoute: ActivatedRoute) {
    return activatedRoute.queryParams.pipe(mergeMap(params => {
      this.authorizationCode = params.code;
      const tokenRequestBody = new HttpParams()
        .set('grant_type', 'authorization_code')
        .set('client_id', 'shoepping')
        .set('client_secret', this.clientSecret)
        .set('redirect_uri', this.redirectURI)
        .set('code', this.authorizationCode);

      return this.http.post('https://auth.dev.tugood.team/auth/realms/tugood/protocol/openid-connect/token',
        tokenRequestBody.toString(), {
          headers: new HttpHeaders()
            .set('Content-Type', 'application/x-www-form-urlencoded')
        });
    })).pipe(mergeMap(value => {
      const refreshTokenString = 'refresh_token';
      this.refreshToken = value[refreshTokenString];

      const accessTokenRequestBody = new HttpParams()
        .set('client_id', 'shoepping')
        .set('grant_type', 'refresh_token')
        .set('refresh_token', this.refreshToken)
        .set('client_secret', this.clientSecret);

      return this.http.post('https://auth.dev.tugood.team/auth/realms/tugood/protocol/openid-connect/token',
        accessTokenRequestBody.toString(), {
          headers: new HttpHeaders()
            .set('Content-Type', 'application/x-www-form-urlencoded')
        });
    }));
  }

  getRefreshToken() {
    return this.refreshToken;
  }

  getTugoodKarmaURI() {
    return this.tugoodKarmaURI;
  }

  isAlreadyLoggedIn() {
    return localStorage.getItem('access_token') != null;
  }
}
