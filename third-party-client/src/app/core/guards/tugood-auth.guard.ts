import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from '../service/auth.service';

@Injectable({
  providedIn: 'root'
})
export class TugoodAuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.isAlreadyAuthenticated(state.url);
  }

  isAlreadyAuthenticated(url: string): boolean {
    console.log(this.authService.isAlreadyLoggedIn());
    if (url.includes('code=') || this.authService.isAlreadyLoggedIn()) {
      return true;
    }

    this.router.navigate(['/']);
    return false;
  }
}
