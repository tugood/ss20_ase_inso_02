import {TestBed} from '@angular/core/testing';

import {TugoodAuthGuard} from './tugood-auth.guard';

describe('TugoodAuthGuard', () => {
  let guard: TugoodAuthGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(TugoodAuthGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
