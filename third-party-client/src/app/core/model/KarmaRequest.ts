export interface KarmaRequest {
  requestId: string;
  value: number;
}
