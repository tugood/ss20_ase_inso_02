import {Component, OnInit} from '@angular/core';

declare const SwaggerUIBundle: any;

@Component({
  selector: 'app-swagger-ui',
  templateUrl: './swagger-ui.component.html',
  styleUrls: ['./swagger-ui.component.scss']
})
export class SwaggerUiComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
    const ui = SwaggerUIBundle({
      dom_id: '#swagger-ui',
      layout: 'BaseLayout',
      presets: [
        SwaggerUIBundle.presets.apis,
        SwaggerUIBundle.SwaggerUIStandalonePreset
      ],
      url: 'https://dev.tugood.team/karma-swagger/api-docs.yaml',
      docExpansion: 'none',
      operationsSorter: 'alpha',
      requestInterceptor(request) {
        request.headers.Authorization = 'Bearer ' + localStorage.getItem('access_token');
        return request;
      }
    });
  }

}
