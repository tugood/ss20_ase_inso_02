import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SpendKarmaErrorDialogComponent} from './spend-karma-error-dialog.component';

describe('SpendKarmaErrorDialogComponent', () => {
  let component: SpendKarmaErrorDialogComponent;
  let fixture: ComponentFixture<SpendKarmaErrorDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SpendKarmaErrorDialogComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpendKarmaErrorDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
