import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-spend-karma-error-dialog',
  templateUrl: './spend-karma-error-dialog.component.html',
  styleUrls: ['./spend-karma-error-dialog.component.scss']
})
export class SpendKarmaErrorDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<SpendKarmaErrorDialogComponent>) {
  }

  ngOnInit(): void {
  }

  close() {
    this.dialogRef.close();
  }
}
