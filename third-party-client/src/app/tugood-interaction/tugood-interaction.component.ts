import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../core/service/auth.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {v4 as uuid} from 'uuid';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {SpendKarmaErrorDialogComponent} from './spend-karma-error-dialog/spend-karma-error-dialog.component';

@Component({
  selector: 'app-tugood-interaction',
  templateUrl: './tugood-interaction.component.html',
  styleUrls: ['./tugood-interaction.component.scss']
})
export class TugoodInteractionComponent implements OnInit {

  currentKarma: number;
  accessToken: string;
  karmaForm: FormGroup;
  tugoodKarmaEndpoint: string;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private authService: AuthService,
              private activatedRoute: ActivatedRoute,
              private http: HttpClient,
              private matDialog: MatDialog) {
    this.karmaForm = this.formBuilder.group({
      karmaToSpend: ['', Validators.min(0)]
    });
    this.currentKarma = -1;
    this.tugoodKarmaEndpoint = this.authService.getTugoodKarmaURI() + '/karma';
  }

  ngOnInit(): void {
    if (localStorage.getItem('access_token') === null) {
      this.authService.authenticateWithTugood(this.activatedRoute).subscribe(value => {
        const accessTokenString = 'access_token';
        this.accessToken = value[accessTokenString];
        localStorage.setItem('access_token', this.accessToken);
        window.location.href = 'https://shoepping.tugood.team/tugood-interaction';
      });
    } else {
      this.accessToken = localStorage.getItem('access_token');
    }
  }

  getKarma() {
    this.http.get(this.tugoodKarmaEndpoint, {
      headers: new HttpHeaders()
        .set('Authorization', 'Bearer ' + this.accessToken)
        .set('Content-Type', 'application/json')
    }).subscribe(karma => {
      console.log(karma);
      const karmaPointField = 'karmaPoints';
      this.currentKarma = karma[karmaPointField];
    });
  }

  spendKarma() {
    const karmaRequest = {
      requestId: uuid(),
      value: this.karmaForm.value.karmaToSpend
    };
    this.http.post(this.tugoodKarmaEndpoint, karmaRequest, {
      headers: new HttpHeaders()
        .set('Authorization', 'Bearer ' + this.accessToken)
        .set('Content-Type', 'application/json')
    }).subscribe(karma => {
        const karmaPointField = 'karmaPoints';
        this.currentKarma = karma[karmaPointField];
      },
      error => {
        if (error.status === 406) {
          this.openKarmaSpendErrorDialog();
        }
      });
  }

  openKarmaSpendErrorDialog() {
    const dialogConfig = new MatDialogConfig();
    this.matDialog.open(SpendKarmaErrorDialogComponent, dialogConfig);
  }
}
