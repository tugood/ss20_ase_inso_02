import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TugoodInteractionComponent} from './tugood-interaction.component';
import {RouterModule, Routes} from '@angular/router';
import {MaterialModule} from '../material.module';
import {HowToUseComponent} from './how-to-use/how-to-use.component';
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {TugoodAuthGuard} from '../core/guards/tugood-auth.guard';
import {SwaggerUiComponent} from './swagger-ui/swagger-ui.component';
import {SpendKarmaErrorDialogComponent} from './spend-karma-error-dialog/spend-karma-error-dialog.component';

export const tugoodInteractionRoutes: Routes = [
  {
    path: 'tugood-interaction',
    pathMatch: 'full',
    component: TugoodInteractionComponent,
    canActivate: [TugoodAuthGuard]
  },
  {path: 'documentation', pathMatch: 'full', component: HowToUseComponent}
];

@NgModule({
  declarations: [TugoodInteractionComponent, HowToUseComponent, SwaggerUiComponent, SpendKarmaErrorDialogComponent],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    MatInputModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule
  ]
})
export class TugoodInteractionModule {
}
