import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TugoodInteractionComponent} from './tugood-interaction.component';

describe('TugoodInteractionComponent', () => {
  let component: TugoodInteractionComponent;
  let fixture: ComponentFixture<TugoodInteractionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TugoodInteractionComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TugoodInteractionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
