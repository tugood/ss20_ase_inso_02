# TUgood App

<img src="assets/logo/tg.svg" alt="TUgood Logo" class="center" width="300">

[[_TOC_]]

## Project description
The project TUgood is aimed at providing a simple way to match up people who want to
deliver a short service ("Helper") to people in need of said services ("Requester"). Such services
are short-timed and can be done by a single person. An obvious example for
this would be the delivery of groceries or mowing a lawn. Especially for the delivery of
groceries an apparent need has surfaced during the outbreak of Covid-19 in Europe in 2020.
Even though this was one of the initiating factors for this project idea, the project is not
necessarily restricted to this situation but is meant as a product which can solve problems
further on.

## Architecture

# User Service

Responsible for:
*   registering users
*   managing account data

This service manages the user data in combination with Keycloak.

# Request Matching Service

Responsible for:
*   creating help requests
*   give an error in case the limit of help request creations has been exceeded
*   matching helpers to requests according to our heuristics
*   allowing helper to accept a request
*   allowing helpers to deny a request
*   checking if the helpers have not exceeded the limit for accepting requests
*   managing help requests, i.e. cancel them within the required time period
*   save the completion status for a match, i.e. helper finished vs. requester finished
*   save user ratings for the match
*   check if the request was denied by all helpers and send a notification
*   create a notification request for the requester if the task is expired
*   show accepted requests to users
*   show finished requests in the history section
*   show currently matched requests to users


# Notification Service

Responsible for:
*   send notifications for:
    *   request was accepted
    *   request was cancelled
    *   task was finished
    *   administrative messages
    *   etc.


# Karma Service

Responsible for:
*   calculating karma points for certain events, like:
    *   finished tasks
    *   canceled tasks
    *   ratings

Additionally, third party vendors should be able to request karma points and get the confirmation that
enough karma points are available to complete a transaction within their app. Then the Third party vendor
wants to “spend” the karma points. This can be achieved together with an OAuth Server.

# Abuse Management Service

Responsible for:
*   creating abuse reports for users
*   give the administrator the necessary data for handling abuse cases (i.e. warning or blocking users)
*   notify the user if he/she has been reported as an abuser


# Additional Services

## API Gateway

Responsible for:
*   functioning as reverse proxy
*   token introspection

We used the Kong API Gateway in this project.

## OAuth Server

Responsible for:
*   authorizing a third party app to access the "Karma Service" to do some transactions with karma points

In this project, we used Keycloak as identity provider.


## DNS Server

Responsible for:
*   Service Registration
*   Service Discovery

In this project, we used Consul for this.

<img src="assets/architecture/microservice_architecture.png" alt="microservice architecture" class="center" width="600">

## Implementation details

### Backend
For each service we use a domain-driven development project structure. All backend services are written with the Spring Boot
Framework and Kotlin. Furthermore, we strived to make our project completely reactived. Hence, we used Webflux.

#### Project structure
The generalized project structure for the backend looks like this:
```
application/                -> business logic implementation (services)
+--data/                    -> data transfer classes
domain/                     -> core package
+-- domain-entity/          -> package for the managed domain
    +-- model/              -> persistent model classes
    +-- service             -> domain business abstraction
    |   +-- mapper/         -> mapper for the data transfer classes
infrastructure/             -> technical details package
+-- configuration/          -> configuration for several infrastructure components
+-- persistance/            -> repository implementation
+-- web/                    -> web layer package
    +-- error/              -> web layer exceptions
    +-- model/              -> request/response models for web layer
    |   +-- request/        -> request model objects
    |   +-- response/       -> response model objects
    +-- controller/         -> OpenAPI definition and implementation of the REST interfaces
```

### Frontend
The frontend is written in Angular. We used an mobile-first approach and created PWA. Thus, it is possible to install the app on
smartphones and send push notifications to its users.
