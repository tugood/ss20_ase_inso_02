package com.ase.tugood.requestservice.infrastructure.web.rest.controller

import com.ase.tugood.requestservice.application.data.request.HelpRequestDTO
import com.ase.tugood.requestservice.domain.request.service.IHelpRequestService
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono


@RestController
@Profile("generatedata")
@RequestMapping("/generate/requests")
class TestDataController(@Autowired private val requestService: IHelpRequestService) {


    val logger: Logger = LoggerFactory.getLogger(javaClass)


    @RequestMapping(
        value = [""],
        produces = ["application/json"],
        method = [RequestMethod.POST])
    fun testRequestCreate(
        @RequestBody requests: List<HelpRequestDTO>
    ): Mono<List<String>> {

        return Flux.fromIterable(requests)
            .flatMap { helpRequest ->
                requestService.addNewHelpRequest(helpRequest, UserInfo(id = helpRequest.requester!!.id, email_verified = true))
                    .map { if (it.id == null) "undefined" else it.id }
            }
            .map { it!! }
            .collectList()
    }
}
