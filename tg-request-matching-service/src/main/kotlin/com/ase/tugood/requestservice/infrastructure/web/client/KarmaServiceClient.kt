package com.ase.tugood.requestservice.infrastructure.web.client

import com.ase.tugood.requestservice.application.data.karma.KarmaChangeDTO
import com.ase.tugood.requestservice.application.data.karma.KarmaRequestDTO
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.ClientResponse
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono


@Component
class KarmaServiceClient {


    val logger: Logger = LoggerFactory.getLogger(javaClass)

    @Autowired
    private val webClientBuilder: WebClient.Builder? = null

    @Autowired
    private val discoveryClientController: DiscoveryClientController? = null

    fun changeKarma(karmaRequestDTO: KarmaRequestDTO): Mono<List<KarmaChangeDTO>> {
        logger.info("Change karma of user: id={}", karmaRequestDTO.userId)

        if (discoveryClientController == null) {
            throw RuntimeException("Cannot initialize karma service client")
        }

        return discoveryClientController
            .karmaService()
            .flatMap { url ->
                webClientBuilder!!.build()
                    .post().uri { builder ->
                        url.resolve(builder.path("/internal/karma").build())
                    }
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(BodyInserters.fromValue(karmaRequestDTO))
                    .exchange()
            }
            .flatMap { t: ClientResponse? ->
                t?.bodyToMono(object : ParameterizedTypeReference<List<KarmaChangeDTO>>() {})
            }
            .switchIfEmpty(Mono.empty())
    }

}
