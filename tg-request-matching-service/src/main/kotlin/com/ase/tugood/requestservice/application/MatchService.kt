package com.ase.tugood.requestservice.application

import com.ase.tugood.requestservice.application.data.karma.InternalKarmaChangeType
import com.ase.tugood.requestservice.application.data.karma.KarmaRequestDTO
import com.ase.tugood.requestservice.application.data.match.MatchDTO
import com.ase.tugood.requestservice.application.data.match.MatchStatus
import com.ase.tugood.requestservice.application.data.match.RatingDTO
import com.ase.tugood.requestservice.domain.karma.service.IKarmaService
import com.ase.tugood.requestservice.domain.match.model.Match
import com.ase.tugood.requestservice.domain.match.service.IMatchService
import com.ase.tugood.requestservice.domain.match.service.mapper.MatchMapper
import com.ase.tugood.requestservice.domain.request.service.IHelpRequestService
import com.ase.tugood.requestservice.domain.user.service.IUserService
import com.ase.tugood.requestservice.infrastructure.persistence.MatchRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class MatchService(@Autowired private val matchRepository: MatchRepository,
                   @Autowired private val matchMapper: MatchMapper,
                   @Autowired private val helpRequestService: IHelpRequestService,
                   @Autowired private val userService: IUserService,
                   @Autowired private val karmaService: IKarmaService
) : IMatchService {
    private val logger = LoggerFactory.getLogger(javaClass)

    override fun getUsersAndHelpRequest(match: Match): Mono<MatchDTO> {
        val requester = userService.getUserForId(match.requesterId!!)
        val helpRequest = helpRequestService.getHelpRequestByIdAndSetRequester(match.helpRequestId!!, requester)
        val helper = userService.getUserForId(match.helperId!!)
        return matchMapper.toDto(match, helpRequest, helper)
            .onErrorMap { cause ->
                logger.error("cannot get the users and help request for match with id {}: {}", match.id, cause.message); throw cause
            }
            .log("got users and help request for match with id ${match.id}")
    }

    override fun changeKarmaOnCancelMatch(userId: String, matchDTO: MatchDTO) {
        val karmaRequestDTO = KarmaRequestDTO(
            userId = userId,
            type = InternalKarmaChangeType.CANCEL_REQUEST,
            matchingScore = matchDTO.matchingScore
        )

        karmaService.createKarma(karmaRequestDTO)
            .log("Created karma change for cancel of match with id ${matchDTO.id}")
            .subscribe()
    }

    override fun changeKarmaOnFinishMatch(userId: String, matchDTO: MatchDTO, ratingDTO: RatingDTO?, helperId: String?) {

        val karmaRequestDTO = KarmaRequestDTO(
            userId = userId,
            type = InternalKarmaChangeType.FINISH_REQUEST,
            ratingDTO = ratingDTO,
            helperId = helperId,
            matchingScore = matchDTO.matchingScore
        )

        karmaService.createKarma(karmaRequestDTO)
            .log("Created karma change for finish of match with id ${matchDTO.id}")
            .subscribe()
    }

    override fun changeMatchStatus(id: String, match: Match, status: MatchStatus): Mono<MatchDTO> {
        match.status = status
        return matchRepository.save(match)
            .flatMap { getUsersAndHelpRequest(match) }
            .onErrorMap { cause ->
                logger.error("cannot change status of match: {}", cause.message); throw cause
            }
            .log("changed status of match $id to $status")
    }

    override fun getMatchById(id: String): Mono<MatchDTO> {
        return matchRepository
            .findById(id)
            .flatMap {
                getUsersAndHelpRequest(it)
            }
    }
}
