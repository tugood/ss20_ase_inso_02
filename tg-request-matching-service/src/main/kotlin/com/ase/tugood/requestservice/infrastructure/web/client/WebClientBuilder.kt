package com.ase.tugood.requestservice.infrastructure.web.client

import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient

@Component
class WebClientBuilder {

    @Bean
    fun loadWebClientBuilder(): WebClient.Builder {
        return WebClient.builder()
    }

}
