package com.ase.tugood.requestservice.application

import com.ase.tugood.requestservice.application.data.karma.KarmaChangeDTO
import com.ase.tugood.requestservice.application.data.karma.KarmaRequestDTO
import com.ase.tugood.requestservice.domain.karma.service.IKarmaService
import com.ase.tugood.requestservice.infrastructure.web.client.KarmaServiceClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class KarmaService(@Autowired private val karmaServiceClient: KarmaServiceClient) : IKarmaService {

    @Override
    override fun createKarma(karmaRequestDTO: KarmaRequestDTO): Mono<List<KarmaChangeDTO>> {
        return karmaServiceClient.changeKarma(karmaRequestDTO)
    }

}
