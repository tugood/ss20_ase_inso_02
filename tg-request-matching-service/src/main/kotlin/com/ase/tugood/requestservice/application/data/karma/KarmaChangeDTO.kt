package com.ase.tugood.requestservice.application.data.karma

import java.time.Instant

data class KarmaChangeDTO(
    val id: String,
    val value: Int,
    val timestamp: Instant,
    val type: KarmaChangeType
)
