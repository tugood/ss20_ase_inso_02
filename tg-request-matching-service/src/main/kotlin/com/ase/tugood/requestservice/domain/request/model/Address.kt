package com.ase.tugood.requestservice.domain.request.model

import org.springframework.data.mongodb.core.geo.GeoJsonPoint
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed
import org.springframework.data.mongodb.core.mapping.Field
import java.io.Serializable

data class Address(

    @Field("streetName")
    var streetName: String? = null,

    @Field("streetNumber")
    var streetNumber: String? = null,

    @Field("postalCode")
    var postalCode: String? = null,

    @Field("city")
    var city: String? = null,

    @Field("country")
    var country: String? = null,

    @GeoSpatialIndexed(name = "location_2dsphere", type = GeoSpatialIndexType.GEO_2DSPHERE)
    @Field("location")
    var location: GeoJsonPoint? = null
) : Serializable

