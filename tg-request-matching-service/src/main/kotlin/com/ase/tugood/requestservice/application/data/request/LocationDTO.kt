package com.ase.tugood.requestservice.application.data.request

import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotNull

data class LocationDTO(
    @NotNull
    @JsonProperty("longitude")
    var longitude: Double = Double.NaN,

    @NotNull
    @JsonProperty("latitude")
    var latitude: Double = Double.NaN
)
