package com.ase.tugood.requestservice.domain.match.service

import com.ase.tugood.requestservice.application.data.match.MatchDTO
import com.ase.tugood.requestservice.application.data.match.RatingDTO
import com.ase.tugood.requestservice.application.data.request.MatchRequestDTO
import com.ase.tugood.requestservice.application.data.request.RateLimitDTO
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface IHelperMatchService {

    /**
     * Takes a match request from a helper and searches for suitable matches in the help requests
     * @param matchRequestDTO the request which contains the location of the helper
     * @param userInfo the user information of the Helper
     * @return multiple suitable matches for the Helper
     */
    fun createMatches(matchRequestDTO: MatchRequestDTO, userInfo: UserInfo): Flux<MatchDTO>

    /**
     * The aim of this function is to return the number of remaining tasks for this specific user.
     * @return the number of tasks remaining, current and max
     */
    fun getRemainingTasks(userInfo: UserInfo): Mono<RateLimitDTO>

    /**
     * Finds all Matches that a helper can accept
     * @param userInfo the user information of the helper
     * @return all matches, which can be accepted or rejected
     */
    fun getAcceptableMatches(userInfo: UserInfo): Flux<MatchDTO>

    /**
     * The aim of this function is to update the data of a given match in the data storage.
     * @param id the id of the match to be updated
     * @param userInfo the user information of the helper
     * @return the new match as reactive type
     */
    fun finishMatch(id: String, userInfo: UserInfo, ratingDTO: RatingDTO?): Mono<MatchDTO>


    /**
     * The aim of this function is to update the data of a given match in the data storage.
     * @param id the id of the match to be updated
     * @param userInfo the user information of the Helper
     * @return the new match as reactive type
     */
    fun cancelMatch(id: String, userInfo: UserInfo): Mono<MatchDTO>

    /**
     * Accept a match
     * @param id match id
     * @param userInfo logged in user
     */
    fun acceptMatch(id: String, userInfo: UserInfo): Mono<MatchDTO>

    /**
     * Reject a match
     * @param id match id
     * @param userInfo logged in user
     */
    fun rejectMatch(id: String, userInfo: UserInfo): Mono<MatchDTO>

    /**
     * Finds all finished or cancelled matches
     * @param userInfo the user information of the helper
     * @return all past matches, which were finished or cancelled
     */
    fun getHistoricMatches(userInfo: UserInfo): Flux<MatchDTO>

    /**
     * Finds all accepted matches
     * @param userInfo the user information of the helper
     * @return all current matches, which are in work
     */
    fun getActiveMatches(userInfo: UserInfo): Flux<MatchDTO>

}
