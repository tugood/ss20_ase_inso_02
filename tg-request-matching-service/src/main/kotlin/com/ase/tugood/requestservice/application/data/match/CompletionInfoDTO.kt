package com.ase.tugood.requestservice.application.data.match

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.format.annotation.DateTimeFormat
import java.time.Instant

/**
 * The completion information dto is part of a
 * matching. Every completed matching has exactly
 * one completion info object.
 */
data class CompletionInfoDTO(

    @field:DateTimeFormat
    @JsonProperty("requesterFinishedOn")
    var requesterFinishedOn: Instant? = null,

    @JsonProperty("helperRating")
    var helperRating: RatingDTO? = null,

    @field:DateTimeFormat
    @JsonProperty("helperFinishedOn")
    var helperFinishedOn: Instant? = null,

    @JsonProperty("requesterRating")
    var requesterRating: RatingDTO? = null
)
