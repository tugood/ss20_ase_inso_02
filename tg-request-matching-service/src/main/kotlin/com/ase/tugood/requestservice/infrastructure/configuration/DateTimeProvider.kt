package com.ase.tugood.requestservice.infrastructure.configuration

import org.springframework.stereotype.Component
import java.time.Instant

@Component
class DateTimeProvider {
    fun now(): Instant = Instant.now()
}
