package com.ase.tugood.requestservice.domain.match.service.mapper

import com.ase.tugood.requestservice.application.data.match.RatingDTO
import com.ase.tugood.requestservice.domain.match.model.Rating
import com.ase.tugood.requestservice.infrastructure.mapper.EntityMapper
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import org.springframework.stereotype.Component

@Component
class RatingMapper() : EntityMapper<RatingDTO, Rating> {
    override fun toEntity(dto: RatingDTO, userInfo: UserInfo): Rating {
        return merge(Rating(), dto, userInfo)
    }

    override fun merge(entity: Rating, dto: RatingDTO, userInfo: UserInfo): Rating {
        return entity.apply {
            this.rating = dto.rating ?: this.rating
            this.comment = dto.comment ?: this.comment
        }
    }

    override fun toDto(entity: Rating): RatingDTO {
        return RatingDTO().apply {
            this.rating = entity.rating
            this.comment = entity.comment
        }
    }

}
