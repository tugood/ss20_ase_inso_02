package com.ase.tugood.requestservice.application

import com.ase.tugood.requestservice.application.data.match.MatchStatus
import com.ase.tugood.requestservice.application.data.notification.ServiceNotificationDTO
import com.ase.tugood.requestservice.infrastructure.persistence.HelpRequestRepository
import com.ase.tugood.requestservice.infrastructure.persistence.MatchRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import reactor.kotlin.extra.bool.not
import java.time.Instant

@Component
class ExpirationCheckService(@Autowired private val helpRequestRepository: HelpRequestRepository,
                             @Autowired private val matchRepository: MatchRepository,
                            @Autowired private val notificationService: NotificationService) {

    val logger = LoggerFactory.getLogger(javaClass)

    // every day at 1am
    @Scheduled(cron = "0 0 1 * * ?")
    fun helpRequestExpirationCheck() {
        logger.info("Checking for expired help requests")

        helpRequestRepository
            .findAllByTimeFrameToBeforeAndExpirationDateNull(Instant.now())
            .filterWhen {
                matchRepository
                    .existsByStatusInAndHelpRequestId(
                        setOf(MatchStatus.FINISHED_BY_HELPER, MatchStatus.FINISHED_BY_REQUESTER),
                        it.id!!).not()
            }
            .doOnNext {
                // send notification
                notificationService.notify(it.requesterId!!,
                    Pair("notification.request.expired.title", null),
                    Pair("notification.request.expired.body", arrayOf(it.description)),
                    ServiceNotificationDTO.NotificationType.PLAIN_OLD).subscribe()

                // set expiration date and save again
                helpRequestRepository.save(it.apply { expirationDate = Instant.now() }).subscribe()
            }
            .subscribe()
    }
}
