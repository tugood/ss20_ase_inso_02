package com.ase.tugood.requestservice.application.data.request

import com.fasterxml.jackson.annotation.JsonProperty

data class MatchRequestDTO(
    @JsonProperty("location")
    var location: LocationDTO? = null
)
