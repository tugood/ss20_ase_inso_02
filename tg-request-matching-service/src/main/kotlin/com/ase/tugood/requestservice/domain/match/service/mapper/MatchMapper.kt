package com.ase.tugood.requestservice.domain.match.service.mapper

import com.ase.tugood.requestservice.application.data.user.UserDTO
import com.ase.tugood.requestservice.application.data.match.MatchDTO
import com.ase.tugood.requestservice.application.data.request.HelpRequestDTO
import com.ase.tugood.requestservice.domain.match.model.CompletionInfo
import com.ase.tugood.requestservice.domain.match.model.Match
import com.ase.tugood.requestservice.infrastructure.configuration.DateTimeProvider
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono

/**
 * Mapper for the entity {@link Match} and its DTO {@link MatchDTO}.
 */
@Component
class MatchMapper(@Autowired val dateTimeProvider: DateTimeProvider, val completionMapper: CompletionMapper) {

    fun toEntity(dto: MatchDTO, userInfo: UserInfo): Match {
        return merge(Match(), dto, userInfo)
    }

    fun merge(entity: Match, dto: MatchDTO, userInfo: UserInfo): Match {
        val now = dateTimeProvider.now()
        return entity.apply {
            this.id = dto.id ?: this.id
            this.status = dto.status
            this.matchDate = dto.matchDate ?: this.matchDate
            this.matchingScore = dto.matchingScore ?: this.matchingScore
            this.helperId = dto.helper?.id ?: this.helperId
            this.completionInfo = if (dto.completionInfoDTO != null)
                completionMapper.merge(this.completionInfo ?: CompletionInfo(),
                    dto.completionInfoDTO!!, userInfo) else null
            this.helpRequestId = dto.helpRequestDTO?.id ?: this.helpRequestId
            this.requesterId = dto.helpRequestDTO?.requester?.id ?: this.requesterId
            this.createdDate = createdDate ?: now
            this.lastModifiedDate = now
        }
    }

    fun toDto(entity: Match, helpRequest: Mono<HelpRequestDTO>, helper: Mono<UserDTO>): Mono<MatchDTO> {

        val match = MatchDTO().apply {
            this.id = entity.id
            this.status = entity.status
            this.matchDate = entity.matchDate
            this.matchingScore = entity.matchingScore
            this.completionInfoDTO = if (entity.completionInfo != null) completionMapper.toDto(entity.completionInfo!!) else null
            this.createdDate = entity.createdDate
            this.lastModifiedDate = entity.lastModifiedDate
            this.helpRequestDTO = HelpRequestDTO().apply {
                this.id = entity.helpRequestId
            }
            this.helper = UserDTO().apply {
                this.id = entity.helperId!!
            }
        }

        return helpRequest.flatMap { helpRequestDTO ->
            match.helpRequestDTO = helpRequestDTO
            helper.map { userDTO ->
                match.helper = userDTO
                match
            }.switchIfEmpty(Mono.just(match))
        }.switchIfEmpty(Mono.just(match))
    }
}
