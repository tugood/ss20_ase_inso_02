package com.ase.tugood.requestservice.infrastructure.web.rest.controller

import com.ase.tugood.requestservice.application.HelpRequestService
import com.ase.tugood.requestservice.application.data.match.MatchDTO
import com.ase.tugood.requestservice.application.data.request.HelpRequestDTO
import com.ase.tugood.requestservice.application.data.request.RateLimitDTO
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.swagger.v3.oas.annotations.ExternalDocumentation
import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.info.Contact
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.servers.Server
import io.swagger.v3.oas.annotations.servers.ServerVariable
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import java.util.*


@RestController
@RequestMapping("/api/requests")
@OpenAPIDefinition(
    info = Info(title = "TUgood Request Matching Service",
        description = "This is the API for the Request Matching Service",
        contact = Contact(email = "e01634247@student.tuwien.ac.at"),
        version = "1.0.0"),
    externalDocs = ExternalDocumentation(description = "Project Repo", url = "https://reset.inso.tuwien.ac.at/repo/2020ss-ASE-group/ss20_ase_inso_02/-/tree/master/tg-request-matching-service"),
    servers = [
        Server(url = "http://127.0.0.1:{port}", description = "local development setup", variables = [ServerVariable(name = "port", defaultValue = "8084")]),
        Server(url = "http://dev.tugood.team:{port}", description = "digital ocean development setup", variables = [ServerVariable(name = "port", defaultValue = "8000")])
    ]
)
class HelpRequestController(@Autowired private val helpRequestService: HelpRequestService) {

    @Operation(
        description = "Post a new help request",
        method = "addNewHelpRequest",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "Help request created", content = [Content(schema = Schema(implementation = HelpRequestDTO::class))])])
    @RequestMapping(
        produces = [APPLICATION_JSON_VALUE],
        consumes = [APPLICATION_JSON_VALUE],
        method = [RequestMethod.POST])
    fun addNewHelpRequest(
        @RequestHeader("X-Userinfo") userInfo: String,
        @RequestBody helpRequest: HelpRequestDTO
    ): Mono<ResponseEntity<HelpRequestDTO>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        return helpRequestService
            .addNewHelpRequest(helpRequest, info)
            .map { request -> ResponseEntity.ok(request) }
    }

    @Operation(
        description = "Get the remaining number of help requests",
        method = "getRemainingHelpRequests",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "Help request created", content = [Content(schema = Schema(implementation = HelpRequestDTO::class))])])
    @RequestMapping(
        value = ["/remaining"],
        produces = [APPLICATION_JSON_VALUE],
        method = [RequestMethod.GET])
    fun getRemainingHelpRequests(
        @RequestHeader("X-Userinfo") userInfo: String
    ): Mono<ResponseEntity<RateLimitDTO>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        return helpRequestService
            .getRemainingHelpRequests(info)
            .map { request -> ResponseEntity.ok(request) }
    }

    @Operation(
        description = "Update a help request",
        method = "updateHelpRequest",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "Help request updated", content = [Content(schema = Schema(implementation = HelpRequestDTO::class))])])
    @RequestMapping(
        value = ["/{id}"],
        produces = [APPLICATION_JSON_VALUE],
        method = [RequestMethod.PUT])
    fun updateHelpRequest(
        @RequestHeader("X-Userinfo") userInfo: String,
        @PathVariable("id") id: String,
        @RequestBody helpRequest: HelpRequestDTO
    ): Mono<ResponseEntity<HelpRequestDTO>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        return helpRequestService
            .updateHelpRequest(id, helpRequest, info)
            .map { request -> ResponseEntity.ok(request) }
    }

    @Operation(
        description = "Retrieve a help request by id",
        method = "getHelpRequestById",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "Help request found", content = [Content(schema = Schema(implementation = HelpRequestDTO::class))])])
    @RequestMapping(
        value = ["/{id}"],
        produces = [APPLICATION_JSON_VALUE],
        method = [RequestMethod.GET])
    fun getHelpRequestById(
        @RequestHeader("X-Userinfo") userInfo: String,
        @PathVariable("id") id: String
    ): Mono<ResponseEntity<HelpRequestDTO>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        return helpRequestService
            .getHelpRequestByIdAndRequester(id, info)
            .map { request -> ResponseEntity.ok(request) }
    }

    @Operation(
        description = "Retrieve all help requests",
        method = "getHelpRequests",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "Help requests for users were found", content = [Content(schema = Schema(implementation = HelpRequestDTO::class))])])
    @RequestMapping(
        produces = [APPLICATION_JSON_VALUE],
        method = [RequestMethod.GET])
    fun getHelpRequests(
        @RequestHeader("X-Userinfo") userInfo: String
    ): Mono<ResponseEntity<List<HelpRequestDTO>>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        return helpRequestService
            .getHelpRequestsByRequester(info)
            .collectList()
            .map { requests -> ResponseEntity.ok(requests) }
    }

    @Operation(
        description = "Delete a help request",
        method = "deleteHelpRequest",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "HelpRequest deleted", content = [Content(schema = Schema(implementation = MatchDTO::class))])])
    @RequestMapping(
        value = ["/{id}/delete"],
        produces = [APPLICATION_JSON_VALUE],
        method = [RequestMethod.DELETE])
    fun deleteHelpRequest(
        @RequestHeader("X-Userinfo") userInfo: String,
        @PathVariable("id") helpRequestID: String
    ): Mono<Void> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        return helpRequestService
            .deleteHelpRequestById(helpRequestID, info)
    }
}
