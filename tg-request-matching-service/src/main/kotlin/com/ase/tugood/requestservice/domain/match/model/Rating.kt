package com.ase.tugood.requestservice.domain.match.model

import org.springframework.data.mongodb.core.mapping.Field
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.Size

data class Rating(
    @field:Min(value = 0)
    @field:Max(value = 5)
    @Field("rating")
    var rating: Int? = null,

    @field:Size(min = 0, max = 2500)
    @Field("comment")
    var comment: String? = null
)
