package com.ase.tugood.requestservice.infrastructure.calculation

import org.springframework.data.mongodb.core.geo.GeoJsonPoint
import kotlin.math.*

const val earthRadiusInKilometers = 6372.8

/**
 * Uses the haversine formula to calculate the distance from a GeoJsonPoint to another
 * point which is specified by longitude and latitude
 *
 * @return distance in kilometers
 */
fun GeoJsonPoint.distanceTo(otherLat: Double, otherLong: Double): Double {
    val thisLong = x
    val thisLat = y

    val latDistance = Math.toRadians(otherLat - thisLat)
    val longDistance = Math.toRadians(otherLong - thisLong)
    val thisLatRadians = Math.toRadians(thisLat)
    val otherLatRadians = Math.toRadians(otherLat)

    val a = sin(latDistance / 2).pow(2.0).plus(sin(longDistance / 2).pow(2.0).times(cos(thisLatRadians)).times(cos(otherLatRadians)))
    val c = asin(sqrt(a)).times(2)
    return earthRadiusInKilometers * c;
}
