package com.ase.tugood.requestservice.application.data.request

data class RateLimitDTO(
    var remaining: Number? = null,
    var current: Number? = null,
    var max: Number? = null
)
