package com.ase.tugood.requestservice.infrastructure.web.rest.error

import org.springframework.http.HttpStatus

open class TUGoodException(message: String, var status: HttpStatus, type: String, cause: Throwable?) : RuntimeException(message, cause) {
    var error: Error = Error(message = message, code = status.value(), type = type)

    init {
        if (cause != null)
            if (cause is TUGoodException) {
                error.cause.add(cause.error)
            }
    }
}
