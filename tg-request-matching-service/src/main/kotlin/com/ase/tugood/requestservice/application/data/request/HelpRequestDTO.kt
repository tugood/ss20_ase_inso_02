package com.ase.tugood.requestservice.application.data.request

import com.ase.tugood.requestservice.application.data.user.UserDTO
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.Instant
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

data class HelpRequestDTO(

    @JsonProperty("id")
    var id: String? = null,

    @NotNull
    @JsonProperty("category")
    var category: Category? = null,

    @NotNull
    @JsonProperty("address")
    var address: AddressDTO? = null,

    @JsonProperty("isAsap")
    var isAsap: Boolean? = false,

    @NotNull
    @JsonProperty("timeFrame")
    var timeFrame: TimeFrameDTO? = null,

    @field:Size(max = 2500)
    @JsonProperty("description")
    var description: String? = null,

    @JsonProperty("requester")
    var requester: UserDTO? = null,

    @JsonIgnore
    var expirationDate: Instant? = null,

    @JsonIgnore
    var createdDate: Instant? = null,

    @JsonIgnore
    var lastModifiedDate: Instant? = null
)
