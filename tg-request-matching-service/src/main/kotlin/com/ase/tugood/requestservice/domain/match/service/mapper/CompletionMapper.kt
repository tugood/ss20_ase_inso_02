package com.ase.tugood.requestservice.domain.match.service.mapper

import com.ase.tugood.requestservice.application.data.match.CompletionInfoDTO
import com.ase.tugood.requestservice.domain.match.model.CompletionInfo
import com.ase.tugood.requestservice.domain.match.model.Rating
import com.ase.tugood.requestservice.infrastructure.mapper.EntityMapper
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class CompletionMapper(@Autowired val ratingMapper: RatingMapper) : EntityMapper<CompletionInfoDTO, CompletionInfo> {
    override fun toEntity(dto: CompletionInfoDTO, userInfo: UserInfo): CompletionInfo {
        return merge(CompletionInfo(), dto, userInfo)
    }

    override fun merge(entity: CompletionInfo, dto: CompletionInfoDTO, userInfo: UserInfo): CompletionInfo {
        return entity.apply {
            if (dto.requesterRating != null) {
                this.requesterRating = ratingMapper.merge(this.requesterRating ?: Rating(),
                    dto.requesterRating!!, userInfo)
            }
            if (dto.helperRating != null) {
                this.helperRating = ratingMapper.merge(this.helperRating ?: Rating(),
                    dto.helperRating!!, userInfo)
            }
            this.requesterFinishedOn = dto.requesterFinishedOn ?: this.requesterFinishedOn
            this.helperFinishedOn = dto.helperFinishedOn ?: this.helperFinishedOn
        }
    }

    override fun toDto(entity: CompletionInfo): CompletionInfoDTO {
        return CompletionInfoDTO().apply {
            this.requesterRating = if (entity.requesterRating != null) ratingMapper.toDto(entity.requesterRating!!) else null
            this.helperRating = if (entity.helperRating != null) ratingMapper.toDto(entity.helperRating!!) else null
            this.helperFinishedOn = entity.helperFinishedOn
            this.requesterFinishedOn = entity.requesterFinishedOn
        }
    }

}
