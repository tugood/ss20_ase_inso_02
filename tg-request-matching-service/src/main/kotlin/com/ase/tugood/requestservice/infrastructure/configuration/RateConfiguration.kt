package com.ase.tugood.requestservice.infrastructure.configuration

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource


@Configuration
@PropertySource("classpath:application.yml")
class RateConfiguration {

    @Value("\${rateLimit.maxCreationLimit}")
    lateinit var maxCreationLimit: Number

    @Value("\${rateLimit.maxMatchLimit}")
    lateinit var maxMatchLimit: Number
}
