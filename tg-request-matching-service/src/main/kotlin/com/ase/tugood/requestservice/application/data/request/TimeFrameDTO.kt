package com.ase.tugood.requestservice.application.data.request

import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.format.annotation.DateTimeFormat
import java.time.Instant
import javax.validation.constraints.NotNull


data class TimeFrameDTO(
    @field:DateTimeFormat
    @NotNull
    @JsonProperty("from")
    var from: Instant? = null,

    @field:DateTimeFormat
    @NotNull
    @JsonProperty("to")
    var to: Instant? = null
)

