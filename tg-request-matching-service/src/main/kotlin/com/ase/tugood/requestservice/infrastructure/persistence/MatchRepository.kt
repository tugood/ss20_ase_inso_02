package com.ase.tugood.requestservice.infrastructure.persistence

import com.ase.tugood.requestservice.application.data.match.MatchStatus
import com.ase.tugood.requestservice.domain.match.model.Match
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono


@Repository
interface MatchRepository : ReactiveMongoRepository<Match, String> {

    fun findAllByHelperId(helperId: String): Flux<Match>

    fun findAllByHelperIdAndStatusIn(helperId: String, status: Set<MatchStatus>): Flux<Match>

    fun findAllByRequesterIdAndStatusIn(requesterId: String, status: Set<MatchStatus>): Flux<Match>

    fun findByIdAndHelperId(id: String, helperId: String): Mono<Match>

    fun findByIdAndRequesterId(id: String, requesterId: String): Mono<Match>

    fun findByIdAndHelperIdAndStatusIn(id: String, helperId: String, status: Set<MatchStatus>): Mono<Match>

    fun findByIdAndRequesterIdAndStatusIn(id: String, requesterId: String, status: Set<MatchStatus>): Mono<Match>

    fun existsByHelpRequestIdAndHelperId(helpRequestId: String, helperId: String): Mono<Boolean>

    fun findAllByHelpRequestId(helpRequestId: String): Flux<Match>

    fun findAllByHelperIdAndStatusInAndHelpRequestIdIn(helperId: String, status: Set<MatchStatus>, helpRequestId: Set<String>): Flux<Match>

    fun existsByHelpRequestId(helpRequestId: String): Mono<Boolean>

    fun existsByStatusInAndHelpRequestId(status: Set<MatchStatus>, helpRequestId: String): Mono<Boolean>
}
