package com.ase.tugood.requestservice.application

import com.ase.tugood.requestservice.application.data.match.CompletionInfoDTO
import com.ase.tugood.requestservice.application.data.match.MatchDTO
import com.ase.tugood.requestservice.application.data.match.MatchStatus
import com.ase.tugood.requestservice.application.data.match.RatingDTO
import com.ase.tugood.requestservice.domain.match.service.IMatchService
import com.ase.tugood.requestservice.domain.match.service.IRequesterMatchService
import com.ase.tugood.requestservice.domain.match.service.mapper.MatchMapper
import com.ase.tugood.requestservice.domain.request.service.IHelpRequestService
import com.ase.tugood.requestservice.domain.user.service.IUserService
import com.ase.tugood.requestservice.infrastructure.persistence.MatchRepository
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Instant

@Service
class RequesterMatchService(@Autowired private val matchRepository: MatchRepository,
                            @Autowired private val matchMapper: MatchMapper,
                            @Autowired private val helpRequestService: IHelpRequestService,
                            @Autowired private val userService: IUserService,
                            @Autowired private val matchService: IMatchService
) : IRequesterMatchService {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun finishMatch(id: String, userInfo: UserInfo, ratingDTO: RatingDTO): Mono<MatchDTO> {
        val requester = userService.getUserForId(userInfo.id)
        return matchRepository
            .findByIdAndRequesterId(id, userInfo.id)
            .filter { it.status == MatchStatus.FINISHED_BY_HELPER }
            .flatMap { match ->
                matchRepository.save(matchMapper.merge(match, MatchDTO(status = MatchStatus.FINISHED_BY_REQUESTER).apply {
                    completionInfoDTO = CompletionInfoDTO(requesterFinishedOn = Instant.now(), requesterRating = ratingDTO)
                }
                    , userInfo))
            }
            .flatMap { match ->
                val helpRequest = helpRequestService.getHelpRequestByIdAndSetRequester(match.helpRequestId!!, requester)
                val helper = if (ratingDTO.rating != null) userService.createRating(match.helperId!!, ratingDTO) else
                    userService.getUserForId(match.helperId!!)
                matchMapper.toDto(match, helpRequest, helper)
            }
            .doOnNext { matchDTO ->
                matchService.changeKarmaOnFinishMatch(userInfo.id, matchDTO, ratingDTO, matchDTO.helper?.id)
            }
            .onErrorMap { cause ->
                logger.error("cannot finish match: {}", cause.message); throw cause
            }
            .log("finished match with id: $id and rating $ratingDTO")
    }

    override fun cancelMatch(id: String, userInfo: UserInfo): Mono<MatchDTO> {
        return matchRepository
            .findByIdAndRequesterIdAndStatusIn(id, userInfo.id, setOf(MatchStatus.ACCEPTED))
            .flatMap { match ->
                matchService.changeMatchStatus(id, match, MatchStatus.CANCELLED_BY_REQUESTER)
            }.doOnNext { matchDTO ->
                matchService.changeKarmaOnCancelMatch(userInfo.id, matchDTO)
            }
            .onErrorMap { cause ->
                logger.error("cannot cancel match: {}", cause.message); throw cause
            }
            .log("cancelled match with id: $id")
    }


    override fun getFinishableMatches(userInfo: UserInfo): Flux<MatchDTO> {
        return matchRepository
            .findAllByRequesterIdAndStatusIn(userInfo.id, setOf(MatchStatus.FINISHED_BY_HELPER))
            .flatMap { match ->
                matchService.getUsersAndHelpRequest(match)
            }
            .onErrorMap { cause ->
                logger.error("cannot get finishable matches: {}", cause.message); throw cause
            }
            .log("got finishable matches for helper: $userInfo.id")
    }

    override fun getHistoricMatches(userInfo: UserInfo): Flux<MatchDTO> {
        return matchRepository.findAllByRequesterIdAndStatusIn(userInfo.id, MatchStatus.historicStates)
            .flatMap { match ->
                matchService.getUsersAndHelpRequest(match)
            }
    }

    override fun getActiveMatches(userInfo: UserInfo): Flux<MatchDTO> {
        return matchRepository.findAllByRequesterIdAndStatusIn(userInfo.id, MatchStatus.activeStates)
            .flatMap { match ->
                matchService.getUsersAndHelpRequest(match)
            }
    }
}
