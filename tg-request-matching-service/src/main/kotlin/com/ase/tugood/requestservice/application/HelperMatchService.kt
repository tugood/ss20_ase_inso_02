package com.ase.tugood.requestservice.application

import com.ase.tugood.requestservice.application.data.match.CompletionInfoDTO
import com.ase.tugood.requestservice.application.data.match.MatchDTO
import com.ase.tugood.requestservice.application.data.match.MatchStatus
import com.ase.tugood.requestservice.application.data.match.RatingDTO
import com.ase.tugood.requestservice.application.data.notification.ServiceNotificationDTO
import com.ase.tugood.requestservice.application.data.request.MatchRequestDTO
import com.ase.tugood.requestservice.application.data.request.RateLimitDTO
import com.ase.tugood.requestservice.domain.match.model.Match
import com.ase.tugood.requestservice.domain.match.service.IHelperMatchService
import com.ase.tugood.requestservice.domain.match.service.IMatchService
import com.ase.tugood.requestservice.domain.match.service.mapper.MatchMapper
import com.ase.tugood.requestservice.domain.notification.service.INotificationService
import com.ase.tugood.requestservice.domain.request.model.HelpRequest
import com.ase.tugood.requestservice.domain.request.service.IHelpRequestService
import com.ase.tugood.requestservice.domain.user.service.IUserService
import com.ase.tugood.requestservice.infrastructure.calculation.distanceTo
import com.ase.tugood.requestservice.infrastructure.calculation.mapSigmoidZeroToOne
import com.ase.tugood.requestservice.infrastructure.configuration.RateConfiguration
import com.ase.tugood.requestservice.infrastructure.configuration.ScoringConfiguration
import com.ase.tugood.requestservice.infrastructure.persistence.HelpRequestRepository
import com.ase.tugood.requestservice.infrastructure.persistence.MatchRepository
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import com.ase.tugood.requestservice.infrastructure.web.rest.error.TUGoodException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.extra.bool.not
import reactor.kotlin.extra.math.sum
import java.time.Instant

@Service
class HelperMatchService(@Autowired private val matchRepository: MatchRepository,
                         @Autowired private val helpRequestRepository: HelpRequestRepository,
                         @Autowired private val matchMapper: MatchMapper,
                         @Autowired private val helpRequestService: IHelpRequestService,
                         @Autowired private val userService: IUserService,
                         @Autowired private val matchService: IMatchService,
                         @Autowired private val notificationService: INotificationService,
                         @Autowired private val rateConfiguration: RateConfiguration,
                         @Autowired private val scoringConfiguration: ScoringConfiguration
) : IHelperMatchService {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun createMatches(matchRequestDTO: MatchRequestDTO, userInfo: UserInfo): Flux<MatchDTO> {
        val helper = userService.getUserForId(userInfo.id)

        val toInsert = helper
            .map { it.distance?.times(1000) ?: 10000L }
            .log("distance")
            .flatMapMany {
                helpRequestRepository
                    .findByAddressLocationNearWithMaxDistance(matchRequestDTO.location!!.longitude,
                        matchRequestDTO.location!!.latitude,
                        it)
            }
            .filter {
                it.requesterId != userInfo.id
            }
            .filterExpiredHelpRequests()
            .filterWhen { matchableHelpRequest ->
                matchRepository
                    .existsByHelpRequestIdAndHelperId(matchableHelpRequest.id!!, userInfo.id)
                    .not()
            }
            .switchIfEmpty(Flux.empty())
            .take(250)
            .flatMap { request ->
                score(request, matchRequestDTO, userInfo).map { score ->
                    Pair(request, score)
                }
            }
            .filter {
                it.second >= scoringConfiguration.lowestViableScore.toLong()
            }
            .map {
                Match(
                    status = MatchStatus.CREATED,
                    matchDate = Instant.now(),
                    matchingScore = it.second,
                    helpRequestId = it.first.id,
                    helperId = userInfo.id,
                    requesterId = it.first.requesterId
                )
            }

        return getRemainingTasks(userInfo)
            .filter {
                it.remaining == 0L
            }
            .flatMap {
                Mono.error<TUGoodException>(TUGoodException(message = "No Remaining Tasks", status = HttpStatus.TOO_MANY_REQUESTS, type = "RateLimit", cause = null))
            }.thenMany {
                matchRepository.insert(toInsert)
                    .flatMap { match ->
                        val helpRequest = helpRequestService.getHelpRequestByIdAndSetRequester(match.helpRequestId!!,
                            userService.getUserForId(match.requesterId!!))

                        matchMapper.toDto(match, helpRequest, helper)
                    }
                    .onErrorMap { cause ->
                        logger.error("cannot create matches: {}", cause.message); throw cause
                    }
                    .log("matches for ${userInfo.id} created")
                    .subscribe(it)
            }
    }

    private fun score(helpRequest: HelpRequest, matchRequestDTO: MatchRequestDTO, helper: UserInfo): Mono<Long> {
        /* The distance gives the general base on which further scoring is evaluated. It takes the maximum distance
         * (30 km) and compares it to the actual distance of the match. Lower distance leads to better scores
         */
        fun distanceScore(): Mono<Long> {
            val requestLocation = helpRequest.address?.location
            val helperLocation = matchRequestDTO.location
            val maxDistance = scoringConfiguration.maxDistanceKm.toDouble()
            return if (helperLocation != null && requestLocation != null) {
                val kmDistance = minOf(requestLocation.distanceTo(helperLocation.latitude, helperLocation.longitude),
                    maxDistance)
                val inverseDistance = maxDistance - kmDistance
                return Mono.just(inverseDistance.mapSigmoidZeroToOne(0.0, maxDistance)
                    .times(scoringConfiguration.maxBaseScore.toLong())
                    .toLong())
            } else Mono.just(scoringConfiguration.minScore.toLong())
        }

        // every active task of the requester takes off 10 points of the score
        fun modifyByOpenRequests(base: Long, openRequests: Long): Long =
            base - openRequests.times(10)

        // 5 star ratings add 10 points, 4 star ratings add 5 points, 2 star ratings take off 5 points, 1 star ratings take off
        fun modifyByRequesterRating(base: Long, requesterRating: Double): Long =
            base + (requesterRating - 3.0).times(5.0).toLong()

        // every cancellation done by the requester leads to a deduction of 1 point
        fun modifyByRequesterCancellations(base: Long, requesterCancellations: Long): Long =
            base - requesterCancellations

        // other matched helpers take off a point if they have a better rating
        fun reductionByOtherHelperRating(base: Long, otherUserRating: Double, ownRating: Double): Long =
            if (otherUserRating > ownRating) -1 else 0

        // other matches take off further 5 points off the score of other matches for determining a clearer "winner"
        fun reductionByOtherMatchScore(base: Long, otherScore: Long): Long =
            if (otherScore > base) -5 else 0

        return distanceScore()
            .flatMap { baseScore ->
                matchRepository.findAllByRequesterIdAndStatusIn(helpRequest.requesterId!!, MatchStatus.activeStates)
                    .count()
                    .map {
                        modifyByOpenRequests(baseScore, it)
                    }
            }
            .flatMap { baseScore ->
                matchRepository.findAllByRequesterIdAndStatusIn(helpRequest.requesterId!!, setOf(MatchStatus.CANCELLED_BY_REQUESTER))
                    .count()
                    .map { modifyByRequesterCancellations(baseScore, it) }
            }
            .flatMap { baseScore ->
                userService.getUserForId(helpRequest.requesterId!!)
                    .map { requester ->
                        modifyByRequesterRating(baseScore, requester.avgRating ?: 3.0)
                    }
            }
            .flatMap { baseScore ->
                matchRepository.findAllByHelpRequestId(helpRequest.id!!)
                    .flatMap { otherMatch ->
                        userService.getUserForId(otherMatch.helperId!!)
                            .flatMap { otherUser ->
                                userService.getUserForId(helper.id)
                                    .map { thisUser ->
                                        Pair(otherUser.avgRating ?: 3.0, thisUser.avgRating ?: 3.0)
                                    }
                            }
                            .map { reductionByOtherHelperRating(baseScore, it.first, it.second) }
                            .map { currentReduction ->
                                currentReduction + reductionByOtherMatchScore(baseScore, otherMatch.matchingScore!!)
                            }
                    }
                    .switchIfEmpty(Flux.just(0L))
                    .sum()
                    .map { totalReductions ->
                        baseScore + totalReductions
                    }

            }
            .map {
                maxOf(it, scoringConfiguration.minScore.toLong())
            }
    }

    override fun getRemainingTasks(userInfo: UserInfo): Mono<RateLimitDTO> {
        return getActiveMatches(userInfo)
            .filter {
                it.status != MatchStatus.FINISHED_BY_HELPER
            }
            .count()
            .flatMap { entries ->
                Mono.just(RateLimitDTO().apply {
                    this.max = rateConfiguration.maxMatchLimit.toLong()
                    this.current = entries
                    this.remaining = java.lang.Long.max(0, (rateConfiguration.maxCreationLimit.toLong() - entries))
                })
            }
            .log("calculated remaining tasks")
    }

    override fun getAcceptableMatches(userInfo: UserInfo): Flux<MatchDTO> {
        return getRemainingTasks(userInfo)
            .filter {
                it.remaining == 0L
            }
            .flatMap {
                Mono.error<TUGoodException>(TUGoodException(message = "No Remaining Tasks", status = HttpStatus.TOO_MANY_REQUESTS, type = "RateLimit", cause = null))
            }.thenMany {
                helpRequestRepository.findAllByTimeFrameToAfter(Instant.now())
                    .map { helpRequest -> helpRequest.id!! }
                    .collectList()
                    .flatMapMany {
                        matchRepository.findAllByHelperIdAndStatusInAndHelpRequestIdIn(
                            userInfo.id,
                            setOf(MatchStatus.CREATED),
                            it.toSet()
                        )
                    }.filterWhen { match ->
                        matchRepository.findAllByHelpRequestId(match.helpRequestId!!).all { otherMatches ->
                            when (otherMatches.status) {
                                MatchStatus.CREATED -> true
                                MatchStatus.REJECTED -> true
                                MatchStatus.CANCELLED_BY_REQUESTER -> true
                                MatchStatus.CANCELLED_BY_HELPER -> true
                                MatchStatus.FINISHED_BY_HELPER -> false
                                MatchStatus.FINISHED_BY_REQUESTER -> false
                                MatchStatus.ACCEPTED -> false
                                MatchStatus.EXPIRED -> false
                            }
                        }
                    }
                    .flatMap {
                        matchService.getUsersAndHelpRequest(it)
                    }
                    .subscribe(it)
            }
    }

    override fun finishMatch(id: String, userInfo: UserInfo, ratingDTO: RatingDTO?): Mono<MatchDTO> {
        val helper = userService.getUserForId(userInfo.id)
        return matchRepository
            .findByIdAndHelperIdAndStatusIn(id, userInfo.id, setOf(MatchStatus.ACCEPTED))
            .flatMap { match ->
                matchRepository.save(matchMapper.merge(match,
                    MatchDTO(status = MatchStatus.FINISHED_BY_HELPER,
                        completionInfoDTO = CompletionInfoDTO(
                            helperFinishedOn = Instant.now(),
                            helperRating = ratingDTO))
                    , userInfo))
            }
            .flatMap { match ->
                val requester = if (ratingDTO?.rating != null) userService.createRating(match.requesterId!!, ratingDTO) else
                    userService.getUserForId(match.requesterId!!)
                val helpRequest = helpRequestService.getHelpRequestByIdAndSetRequester(match.helpRequestId!!, requester)
                matchMapper.toDto(match, helpRequest, helper)
            }
            .doOnNext { matchDTO ->
                matchService.changeKarmaOnFinishMatch(userInfo.id, matchDTO, ratingDTO, helperId = null)
            }
            .onErrorMap { cause ->
                logger.error("cannot finish match: {}", cause.message); throw cause
            }
            .log("finished match with id: $id and rating $ratingDTO")

    }

    override fun cancelMatch(id: String, userInfo: UserInfo): Mono<MatchDTO> {
        return matchRepository
            .findByIdAndHelperIdAndStatusIn(id, userInfo.id, setOf(MatchStatus.ACCEPTED))
            .flatMap { match ->
                matchService.changeMatchStatus(id, match, MatchStatus.CANCELLED_BY_HELPER)
            }.doOnNext { matchDTO ->
                matchService.changeKarmaOnCancelMatch(userInfo.id, matchDTO)
            }
            .onErrorMap { cause ->
                logger.error("cannot cancel match: {}", cause.message); throw cause
            }
            .log("cancelled match with id: $id")
    }

    override fun acceptMatch(id: String, userInfo: UserInfo): Mono<MatchDTO> {
        return getRemainingTasks(userInfo)
            .filter {
                it.remaining == 0L
            }
            .flatMap {
                Mono.error<TUGoodException>(TUGoodException(message = "No Remaining Tasks", status = HttpStatus.TOO_MANY_REQUESTS, type = "RateLimit", cause = null))
            }.then(
                matchRepository
                    .findByIdAndHelperIdAndStatusIn(id, userInfo.id, setOf(MatchStatus.CREATED))
                    .flatMap { match ->
                        matchService.changeMatchStatus(id, match, MatchStatus.ACCEPTED)
                    }.doOnNext { matchDTO ->
                        notificationService.notify(matchDTO.helpRequestDTO?.requester?.id!!,
                            Pair("notification.match.accepted.title", null),
                            Pair("notification.match.accepted.body", arrayOf(userInfo.name)),
                            ServiceNotificationDTO.NotificationType.PUSH).ignoreElement().subscribe()
                    }
            )
    }

    override fun rejectMatch(id: String, userInfo: UserInfo): Mono<MatchDTO> {
        return matchRepository
            .findByIdAndHelperIdAndStatusIn(id, userInfo.id, setOf(MatchStatus.CREATED))
            .flatMap { match ->
                matchService.changeMatchStatus(id, match, MatchStatus.REJECTED)
            }
    }

    override fun getHistoricMatches(userInfo: UserInfo): Flux<MatchDTO> {
        return matchRepository.findAllByHelperIdAndStatusIn(userInfo.id, MatchStatus.historicStates)
            .flatMap { match ->
                matchService.getUsersAndHelpRequest(match)
            }
    }

    override fun getActiveMatches(userInfo: UserInfo): Flux<MatchDTO> {
        return matchRepository.findAllByHelperIdAndStatusIn(userInfo.id, MatchStatus.activeStates)
            .flatMap { match ->
                matchService.getUsersAndHelpRequest(match)
            }
    }
}

fun Flux<HelpRequest>.filterExpiredHelpRequests(): Flux<HelpRequest> {
    return this.filter {
        it.timeFrame?.to?.isAfter(Instant.now()) ?: false
    }
}

