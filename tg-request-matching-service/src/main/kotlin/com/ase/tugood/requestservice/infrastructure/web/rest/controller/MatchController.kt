package com.ase.tugood.requestservice.infrastructure.web.rest.controller

import com.ase.tugood.requestservice.application.data.match.MatchDTO
import com.ase.tugood.requestservice.application.data.match.RatingDTO
import com.ase.tugood.requestservice.application.data.request.HelpRequestDTO
import com.ase.tugood.requestservice.application.data.request.MatchRequestDTO
import com.ase.tugood.requestservice.application.data.request.RateLimitDTO
import com.ase.tugood.requestservice.domain.match.service.IHelperMatchService
import com.ase.tugood.requestservice.domain.match.service.IMatchService
import com.ase.tugood.requestservice.domain.match.service.IRequesterMatchService
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.swagger.v3.oas.annotations.ExternalDocumentation
import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.info.Contact
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.servers.Server
import io.swagger.v3.oas.annotations.servers.ServerVariable
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import java.util.*


@RestController
@RequestMapping("/api/requests")
@OpenAPIDefinition(
    info = Info(title = "TUgood Request Matching Service",
        description = "This is the API for the Request Matching Service",
        contact = Contact(email = "silvio.vasiljevic@tuwien.ac.at"),
        version = "1.0.0"),
    externalDocs = ExternalDocumentation(description = "Project Repo", url = "https://reset.inso.tuwien.ac.at/repo/2020ss-ASE-group/ss20_ase_inso_02/-/tree/master/tg-request-matching-service"),
    servers = [
        Server(url = "http://127.0.0.1:{port}", description = "local development setup", variables = [ServerVariable(name = "port", defaultValue = "8084")]),
        Server(url = "http://dev.tugood.team:{port}", description = "digital ocean development setup", variables = [ServerVariable(name = "port", defaultValue = "8000")])
    ]
)
class MatchController(@Autowired private val helperMatchService: IHelperMatchService,
                      @Autowired private val requesterMatchService: IRequesterMatchService,
                      @Autowired private val matchService: IMatchService) {
    val logger: Logger = LoggerFactory.getLogger(javaClass)

    @Operation(
        description = "Create and Get acceptable matches",
        method = "createMatches",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "201", description = "Matches created", content = [Content(schema = Schema(implementation = MatchDTO::class))])])
    @RequestMapping(
        value = ["/matches"],
        produces = ["application/json"],
        consumes = ["application/json"],
        method = [RequestMethod.PUT])
    fun createMatches(
        @RequestHeader("X-Userinfo") userInfo: String,
        @RequestBody matchRequestDTO: MatchRequestDTO
    ): Mono<ResponseEntity<List<MatchDTO>>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        logger.info("got $matchRequestDTO from $info")

        return helperMatchService
            .createMatches(matchRequestDTO, info)
            .collectList()
            .map { matches -> ResponseEntity.status(HttpStatus.CREATED).body(matches) }
    }

    @Operation(
        description = "Get the remaining number of tasks",
        method = "getRemainingTasks",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "201", description = "Help request created", content = [Content(schema = Schema(implementation = HelpRequestDTO::class))])])
    @RequestMapping(
        value = ["/tasks/remaining"],
        produces = [MediaType.APPLICATION_JSON_VALUE],
        method = [RequestMethod.GET])
    fun getRemainingTasks(
        @RequestHeader("X-Userinfo") userInfo: String
    ): Mono<ResponseEntity<RateLimitDTO>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        return helperMatchService
            .getRemainingTasks(info)
            .map { request -> ResponseEntity.ok(request) }
    }

    @Operation(
        description = "Accept a match",
        method = "acceptMatch",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "Match accepted", content = [Content(schema = Schema(implementation = MatchDTO::class))])])
    @RequestMapping(
        value = ["/matches/{id}/accept"],
        produces = ["application/json"],
        method = [RequestMethod.PUT])
    fun acceptMatch(
        @RequestHeader("X-Userinfo") userInfo: String,
        @PathVariable("id") id: String
    ): Mono<ResponseEntity<MatchDTO>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        return helperMatchService.acceptMatch(id, info)
            .map { ResponseEntity.ok(it) }
    }

    @Operation(
        description = "Reject a match",
        method = "rejectMatch",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "Match rejected", content = [Content(schema = Schema(implementation = MatchDTO::class))])])
    @RequestMapping(
        value = ["/matches/{id}/reject"],
        produces = ["application/json"],
        method = [RequestMethod.PUT])
    fun rejectMatch(
        @RequestHeader("X-Userinfo") userInfo: String,
        @PathVariable("id") id: String
    ): Mono<ResponseEntity<MatchDTO>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        return helperMatchService.rejectMatch(id, info)
            .map { ResponseEntity.ok(it) }
    }


    @Operation(
        description = "Retrieve acceptable matches",
        method = "getAcceptableMatches",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "Matches retrieved", content = [Content(schema = Schema(implementation = MatchDTO::class))])])
    @RequestMapping(
        value = ["/matches/acceptable"],
        produces = [MediaType.APPLICATION_JSON_VALUE],
        method = [RequestMethod.GET])
    fun getAcceptableMatches(
        @RequestHeader("X-Userinfo") userInfo: String
    ): Mono<ResponseEntity<List<MatchDTO>>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        return helperMatchService.getAcceptableMatches(info)
            .collectList()
            .map { ResponseEntity.ok(it) }
    }

    @Operation(
        description = "Retrieve distinct subset of tasks",
        method = "getTasksSubset",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "Tasks retrieved", content = [Content(schema = Schema(implementation = MatchDTO::class))])])
    @RequestMapping(
        value = ["/tasks/{user:requester|helper}/{state:historic|active|finishable}"],
        produces = [MediaType.APPLICATION_JSON_VALUE],
        method = [RequestMethod.GET])
    fun getTasksSubset(
        @RequestHeader("X-Userinfo") userInfo: String,
        @PathVariable("user") user: String,
        @PathVariable("state") state: String
    ): Mono<ResponseEntity<List<MatchDTO>>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        return when (user) {
            "requester" -> when (state) {
                "historic" -> requesterMatchService.getHistoricMatches(info)
                "active" -> requesterMatchService.getActiveMatches(info)
                "finishable" -> requesterMatchService.getFinishableMatches(info)
                else -> throw RuntimeException("unreachable code reached.")
            }
            "helper" -> when (state) {
                "historic" -> helperMatchService.getHistoricMatches(info)
                "active" -> helperMatchService.getActiveMatches(info)
                else -> throw RuntimeException("unreachable code reached.")
            }
            else -> throw RuntimeException("unreachable code reached.")
        }
            .collectList()
            .map { ResponseEntity.ok(it) }
    }

    @Operation(
        description = "Retrieve a match by id",
        method = "getMatchById",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "Match found", content = [Content(schema = Schema(implementation = MatchDTO::class))])])
    @RequestMapping(
        value = ["/matches/{id}"],
        produces = [MediaType.APPLICATION_JSON_VALUE],
        method = [RequestMethod.GET])
    fun getMatchById(
        @RequestHeader("X-Userinfo") userInfo: String,
        @PathVariable("id") id: String
    ): Mono<ResponseEntity<MatchDTO>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        return matchService
            .getMatchById(id)
            .map { match -> ResponseEntity.ok(match) }
    }

    @Operation(
        description = "finish a task",
        method = "finishTask",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "Task finished", content = [Content(schema = Schema(implementation = MatchDTO::class))])])
    @RequestMapping(
        value = ["/tasks/{user:requester|helper}/{id}/finish"],
        produces = [MediaType.APPLICATION_JSON_VALUE],
        method = [RequestMethod.PUT])
    fun finishTask(
        @RequestHeader("X-Userinfo") userInfo: String,
        @PathVariable("user") user: String,
        @PathVariable("id") matchID: String,
        @RequestBody(required = false) ratingDTO: RatingDTO?
    ): Mono<ResponseEntity<MatchDTO>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        return when (user) {
            "requester" -> requesterMatchService.finishMatch(matchID, info, ratingDTO!!)
            "helper" -> helperMatchService.finishMatch(matchID, info, ratingDTO)
            else -> throw RuntimeException("Cannot finish for $user")
        }
            .map { ResponseEntity.ok(it) }
    }

    @Operation(
        description = "cancel a task",
        method = "cancelTask",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "Task cancelled", content = [Content(schema = Schema(implementation = MatchDTO::class))])])
    @RequestMapping(
        value = ["/tasks/{user:requester|helper}/{id}/cancel"],
        produces = [MediaType.APPLICATION_JSON_VALUE],
        method = [RequestMethod.PUT])
    fun cancelTask(
        @RequestHeader("X-Userinfo") userInfo: String,
        @PathVariable("user") user: String,
        @PathVariable("id") matchID: String
    ): Mono<ResponseEntity<MatchDTO>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        return when (user) {
            "requester" -> requesterMatchService.cancelMatch(matchID, info)
            "helper" -> helperMatchService.cancelMatch(matchID, info)
            else -> throw RuntimeException("Cannot cancel for $user")
        }
            .map { ResponseEntity.ok(it) }
    }
}
