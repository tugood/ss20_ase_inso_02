package com.ase.tugood.requestservice.domain.request.service.mapper

import com.ase.tugood.requestservice.application.data.user.UserDTO
import com.ase.tugood.requestservice.application.data.request.AddressDTO
import com.ase.tugood.requestservice.application.data.request.HelpRequestDTO
import com.ase.tugood.requestservice.application.data.request.LocationDTO
import com.ase.tugood.requestservice.application.data.request.TimeFrameDTO
import com.ase.tugood.requestservice.domain.request.model.Address
import com.ase.tugood.requestservice.domain.request.model.HelpRequest
import com.ase.tugood.requestservice.domain.request.model.TimeFrame
import com.ase.tugood.requestservice.infrastructure.configuration.DateTimeProvider
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.mongodb.core.geo.GeoJsonPoint
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono

/**
 * Mapper for the entity {@link HelpRequest} and its DTO {@link HelpRequestDTO}.
 */
@Component
class HelpRequestMapper(@Autowired var dateTimeProvider: DateTimeProvider) {

    fun toEntity(dto: HelpRequestDTO, userInfo: UserInfo): HelpRequest {
        return merge(HelpRequest(), dto, userInfo)
    }

    fun merge(entity: HelpRequest, dto: HelpRequestDTO, userInfo: UserInfo): HelpRequest {
        val now = dateTimeProvider.now()

        return entity.apply {
            id = dto.id
            category = dto.category
            address = addressDTOToAddress(dto.address)
            isAsap = dto.isAsap
            timeFrame = timeFrameDTOToTimeFrame(dto.timeFrame)
            description = dto.description
            expirationDate = dto.expirationDate
            createdDate = now
            lastModifiedDate = now
            requesterId = userInfo.id
        }
    }

    fun toDto(entity: HelpRequest, requester: Mono<UserDTO>): Mono<HelpRequestDTO> {
        val helpRequest = HelpRequestDTO().apply {
            this.id = entity.id
            this.category = entity.category
            this.address = addressToAddressDTO(entity.address)
            this.isAsap = entity.isAsap
            this.timeFrame = timeFrameToTimeFrameDTO(entity.timeFrame)
            this.description = entity.description
            this.expirationDate = entity.expirationDate
            this.createdDate = entity.createdDate
            this.lastModifiedDate = entity.lastModifiedDate
            this.requester = UserDTO().apply {
                this.id = entity.requesterId!!
            }
        }

        return requester.map { requesterDTO ->
            helpRequest.apply {
                this.requester = requesterDTO
            }
        }.switchIfEmpty(Mono.just(helpRequest))
    }

    protected fun addressDTOToAddress(addressDTO: AddressDTO?): Address? {
        if (addressDTO == null) {
            return null;
        }

        return Address().apply {
            this.city = addressDTO.city
            this.country = addressDTO.country
            this.location = locationDTOToLocation(addressDTO.location)
            this.postalCode = addressDTO.postalCode
            this.streetName = addressDTO.streetName
            this.streetNumber = addressDTO.streetNumber
        }
    }

    protected fun addressToAddressDTO(address: Address?): AddressDTO? {
        if (address == null) {
            return null;
        }

        return AddressDTO().apply {
            this.city = address.city
            this.country = address.country
            this.location = locationToLocationDTO(address.location)
            this.postalCode = address.postalCode
            this.streetName = address.streetName
            this.streetNumber = address.streetNumber
        }
    }

    protected fun locationDTOToLocation(locationDTO: LocationDTO?): GeoJsonPoint? {
        if (locationDTO == null) {
            return null
        }

        return GeoJsonPoint(locationDTO.longitude, locationDTO.latitude)
    }

    protected fun locationToLocationDTO(geoJsonPoint: GeoJsonPoint?): LocationDTO? {
        if (geoJsonPoint == null) {
            return null
        }

        val locationDTO = LocationDTO()
        locationDTO.apply {
            longitude = geoJsonPoint.x
            latitude = geoJsonPoint.y
        }

        return locationDTO
    }

    protected fun timeFrameDTOToTimeFrame(timeFrameDTO: TimeFrameDTO?): TimeFrame? {
        if (timeFrameDTO == null) {
            return null
        }
        val timeFrame = TimeFrame()
        timeFrame.from = timeFrameDTO.from
        timeFrame.to = timeFrameDTO.to
        return timeFrame
    }

    protected fun timeFrameToTimeFrameDTO(timeFrame: TimeFrame?): TimeFrameDTO? {
        if (timeFrame == null) {
            return null
        }
        val timeFrameDTO = TimeFrameDTO()
        timeFrameDTO.from = timeFrame.from
        timeFrameDTO.to = timeFrame.to
        return timeFrameDTO
    }
}
