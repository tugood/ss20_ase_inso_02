package com.ase.tugood.requestservice.domain.user.service

import com.ase.tugood.requestservice.application.data.match.RatingDTO
import com.ase.tugood.requestservice.application.data.user.UserDTO
import reactor.core.publisher.Mono

interface IUserService {

    /**
     * The aim of this function is the retrieve the user DTO by user id from tg-user-service.
     *
     * @param userId the id of the user to be retrieved
     * @return a data stream holding the retrieved user DTO
     */
    fun getUserForId(userId: String): Mono<UserDTO>

    /**
     * The aim of this function is to create a rating for the specified user in tg-user-service.
     *
     * @param userId the id of the user we want to create a rating for
     * @param ratingDTO the rating DTO holding the actual rating to be created
     * @return a data stream holding the user DTO a rating was stored for
     */
    fun createRating(userId: String, ratingDTO: RatingDTO): Mono<UserDTO>
}
