package com.ase.tugood.requestservice.infrastructure.persistence

import com.ase.tugood.requestservice.domain.request.model.HelpRequest
import org.springframework.data.mongodb.repository.Query
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Instant

@Repository
interface HelpRequestRepository : ReactiveMongoRepository<HelpRequest, String> {

    @Query(value = """{"address.location":
       { ${"$"}near : {
            ${"$"}geometry : {
               type : "Point" ,
               coordinates : [ ?0, ?1]
            },
            ${"$"}maxDistance : ?2
          }
       }}""")
    fun findByAddressLocationNearWithMaxDistance(longitude: Double, latitude: Double, maxDistanceInMeters: Long): Flux<HelpRequest>

    fun findByIdAndRequesterId(id: String, requesterId: String): Mono<HelpRequest>

    fun findAllByTimeFrameToBeforeAndExpirationDateNull(expireCheckDate: Instant): Flux<HelpRequest>

    fun findAllByTimeFrameToAfter(to: Instant): Flux<HelpRequest>

    fun findAllByRequesterId(requesterId: String): Flux<HelpRequest>

    fun deleteByIdAndRequesterId(id: String, requesterId: String): Mono<Void>
}
