package com.ase.tugood.requestservice.application

import com.ase.tugood.requestservice.application.data.user.UserDTO
import com.ase.tugood.requestservice.application.data.match.RatingDTO
import com.ase.tugood.requestservice.domain.user.service.IUserService
import com.ase.tugood.requestservice.infrastructure.web.client.UserServiceClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class UserService(@Autowired private val userServiceClient: UserServiceClient) : IUserService {

    override fun getUserForId(userId: String): Mono<UserDTO> {
        return userServiceClient
            .getUserData(userId)
    }


    override fun createRating(userId: String, ratingDTO: RatingDTO): Mono<UserDTO> {
        return userServiceClient
            .createRating(userId, ratingDTO)
    }
}
