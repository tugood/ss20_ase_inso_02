package com.ase.tugood.requestservice.application.data.match

import com.ase.tugood.requestservice.application.data.user.UserDTO
import com.ase.tugood.requestservice.application.data.request.HelpRequestDTO
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.format.annotation.DateTimeFormat
import java.time.Instant
import javax.validation.constraints.NotNull

data class MatchDTO(

    @JsonProperty("id")
    var id: String? = null,

    @JsonProperty("status")
    var status: MatchStatus = MatchStatus.CREATED,

    @field:DateTimeFormat
    @JsonProperty("matchDate")
    var matchDate: Instant? = null,

    @NotNull
    @JsonProperty("matchingScore")
    var matchingScore: Long? = null,

    @JsonProperty("completionInfo")
    var completionInfoDTO: CompletionInfoDTO? = null,

    @NotNull
    @JsonProperty("helpRequest")
    var helpRequestDTO: HelpRequestDTO? = null,

    @NotNull
    @JsonProperty("helper")
    var helper: UserDTO? = null,

    @JsonIgnore
    var createdDate: Instant? = null,

    @JsonIgnore
    var lastModifiedDate: Instant? = null
)
