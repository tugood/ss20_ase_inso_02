package com.ase.tugood.requestservice.application.data.karma

import com.ase.tugood.requestservice.application.data.match.RatingDTO

data class KarmaRequestDTO(
    val userId: String,
    val type: InternalKarmaChangeType,
    val helperId: String? = null,
    val ratingDTO: RatingDTO? = null,
    val matchingScore: Long? = null
)
