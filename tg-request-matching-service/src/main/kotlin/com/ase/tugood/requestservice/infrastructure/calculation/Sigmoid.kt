package com.ase.tugood.requestservice.infrastructure.calculation

import kotlin.math.abs

fun normalizedSigmoid(x: Double, k: Double): Double = (x - k.times(x)) / (k - abs(x).times(k).times(2) + 1)

fun Double.mapSigmoidZeroToOne(min: Double, max: Double): Double {
    return (normalizedSigmoid(this.mapLinear(min, max) * 2 - 1, 0.5) + 1) / 2
}

fun Double.mapLinear(min: Double, max: Double) = (this - min) / (max - min)
