package com.ase.tugood.requestservice.infrastructure.configuration

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource

@Configuration
@PropertySource("classpath:application.yml")
class ScoringConfiguration {

    @Value("\${scoring.maxDistanceKm}")
    lateinit var maxDistanceKm: Number

    @Value("\${scoring.maxBaseScore}")
    lateinit var maxBaseScore: Number

    @Value("\${scoring.maxScore}")
    lateinit var maxScore: Number

    @Value("\${scoring.minScore}")
    lateinit var minScore: Number

    @Value("\${scoring.lowestViableScore}")
    lateinit var lowestViableScore: Number
}
