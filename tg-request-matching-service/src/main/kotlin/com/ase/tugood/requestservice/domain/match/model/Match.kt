package com.ase.tugood.requestservice.domain.match.model

import com.ase.tugood.requestservice.application.data.match.MatchStatus
import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import org.springframework.format.annotation.DateTimeFormat
import java.time.Instant
import javax.validation.constraints.NotNull

@Document(collection = "tugood_match")
data class Match(

    @Id
    var id: String? = null,

    @Field("status")
    var status: MatchStatus = MatchStatus.CREATED,

    @field:DateTimeFormat
    @Field("matchDate")
    var matchDate: Instant? = null,

    @NotNull
    @Field("matchingScore")
    var matchingScore: Long? = null,

    @JsonProperty("completionInfo")
    var completionInfo: CompletionInfo? = null,

    @Field("helpRequestId")
    var helpRequestId: String? = null,

    @NotNull
    @JsonProperty("helperId")
    var helperId: String? = null,

    @NotNull
    @JsonProperty("requesterId")
    var requesterId: String? = null,

    @CreatedDate
    @Field("createdDate")
    var createdDate: Instant? = Instant.now(),

    @LastModifiedDate
    @Field("lastModifiedDate")
    var lastModifiedDate: Instant? = Instant.now()
)
