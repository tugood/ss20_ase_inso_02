package com.ase.tugood.requestservice.domain.match.model

import org.springframework.data.mongodb.core.mapping.Field
import org.springframework.format.annotation.DateTimeFormat
import java.time.Instant

data class CompletionInfo(

    @field:DateTimeFormat
    @Field("requesterFinishedOn")
    var requesterFinishedOn: Instant? = null,

    @Field("helperRating")
    var helperRating: Rating? = null,

    @field:DateTimeFormat
    @Field("helperFinishedOn")
    var helperFinishedOn: Instant? = null,

    @Field("requesterRating")
    var requesterRating: Rating? = null
)
