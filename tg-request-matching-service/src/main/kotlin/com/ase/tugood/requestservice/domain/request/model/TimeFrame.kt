package com.ase.tugood.requestservice.domain.request.model

import org.springframework.data.mongodb.core.mapping.Field
import org.springframework.format.annotation.DateTimeFormat
import java.io.Serializable
import java.time.Instant
import javax.validation.constraints.NotNull


data class TimeFrame(
    @field:DateTimeFormat
    @NotNull
    @Field("from")
    var from: Instant? = null,

    @field:DateTimeFormat
    @NotNull
    @Field("to")
    var to: Instant? = null
) : Serializable

