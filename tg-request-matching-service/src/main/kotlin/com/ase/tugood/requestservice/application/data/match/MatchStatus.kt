package com.ase.tugood.requestservice.application.data.match

import com.fasterxml.jackson.annotation.JsonProperty

enum class MatchStatus(val matchStatus: String) {
    @JsonProperty("CREATED")
    CREATED("CREATED"),

    @JsonProperty("REJECTED")
    REJECTED("REJECTED"),

    @JsonProperty("ACCEPTED")
    ACCEPTED("ACCEPTED"),

    @JsonProperty("EXPIRED")
    EXPIRED("EXPIRED"),

    @JsonProperty("CANCELLED_BY_HELPER")
    CANCELLED_BY_HELPER("CANCELLED_BY_HELPER"),

    @JsonProperty("CANCELLED_BY_REQUESTER")
    CANCELLED_BY_REQUESTER("CANCELLED_BY_REQUESTER"),

    @JsonProperty("FINISHED_BY_HELPER")
    FINISHED_BY_HELPER("FINISHED_BY_HELPER"),

    @JsonProperty("FINISHED_BY_REQUESTER")
    FINISHED_BY_REQUESTER("FINISHED_BY_REQUESTER");

    companion object {
        val historicStates = setOf(CANCELLED_BY_REQUESTER, CANCELLED_BY_HELPER, FINISHED_BY_REQUESTER)
        val activeStates = setOf(ACCEPTED, FINISHED_BY_HELPER)
    }
}

/**
 * STATE DIAGRAM:
 *                        REJECTED
 *                      /
 * NO MATCH -- CREATED  -- (EXPIRED)    CANCELLED_BY_HELPER
 *                      \            /
 *                        ACCEPTED  -- FINISHED_BY_HELPER -- FINISHED_BY_REQUESTER
 *                                  \
 *                                   CANCELLED_BY_REQUESTER
 */
