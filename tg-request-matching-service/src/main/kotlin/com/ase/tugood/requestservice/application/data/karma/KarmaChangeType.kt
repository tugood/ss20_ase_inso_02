package com.ase.tugood.requestservice.application.data.karma

enum class KarmaChangeType {
    THIRD_PARTY_TRANSACTION,
    INTERNAL_CANCEL_REQUEST,
    INTERNAL_FINISH_REQUEST,
    INTERNAL_COMPLETE_RATING
}
