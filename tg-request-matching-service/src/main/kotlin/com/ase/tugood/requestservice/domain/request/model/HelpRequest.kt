package com.ase.tugood.requestservice.domain.request.model

import com.ase.tugood.requestservice.application.data.request.Category
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import java.io.Serializable
import java.time.Instant
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size


@Document(collection = "tugood_help_request")
data class HelpRequest @JvmOverloads constructor(
    @Id
    var id: String? = null,

    @NotNull
    @Field("category")
    var category: Category? = null,

    @NotNull
    @Field("address")
    var address: Address? = null,

    @Field("isAsap")
    var isAsap: Boolean? = false,

    @NotNull
    @Field("timeFrame")
    var timeFrame: TimeFrame? = null,

    @field:Size(max = 2500)
    @Field("description")
    var description: String? = null,

    @Field("requesterId")
    var requesterId: String? = null,

    @NotNull
    @Field("expirationDate")
    var expirationDate: Instant? = null,

    @CreatedDate
    @Field("createdDate")
    var createdDate: Instant? = Instant.now(),

    @LastModifiedDate
    @Field("lastModifiedDate")
    var lastModifiedDate: Instant? = Instant.now()
) : Serializable {

    companion object {
        private const val serialVersionUID = 1L
    }
}
