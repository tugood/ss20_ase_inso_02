package com.ase.tugood.requestservice.domain.match.service

import com.ase.tugood.requestservice.application.data.match.MatchDTO
import com.ase.tugood.requestservice.application.data.match.RatingDTO
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface IRequesterMatchService {

    /**
     * Get tasks which were finished by helpers, which the requester can confirmed finished
     * @param userInfo the user information of the requester
     * @return all matches which were finished by helpers
     */
    fun getFinishableMatches(userInfo: UserInfo): Flux<MatchDTO>

    /**
     * The aim of this function is to update the data of a given match in the data storage.
     * @param id the id of the match to be updated
     * @param userInfo the user information of the requester
     * @return the new match as reactive type
     */
    fun finishMatch(id: String, userInfo: UserInfo, ratingDTO: RatingDTO): Mono<MatchDTO>


    /**
     * The aim of this function is to update the data of a given match in the data storage.
     * @param id the id of the match to be updated
     * @param userInfo the user information of the requester
     * @return the new match as reactive type
     */
    fun cancelMatch(id: String, userInfo: UserInfo): Mono<MatchDTO>

    /**
     * Finds all finished or cancelled matches
     * @param userInfo the user information of the requester
     * @return all past matches, which were finished or cancelled
     */
    fun getHistoricMatches(userInfo: UserInfo): Flux<MatchDTO>

    /**
     * Finds all accepted matches
     * @param userInfo the user information of the requester
     * @return all current matches, which are in work
     */
    fun getActiveMatches(userInfo: UserInfo): Flux<MatchDTO>

}
