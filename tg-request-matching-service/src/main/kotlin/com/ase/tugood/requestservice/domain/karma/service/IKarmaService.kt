package com.ase.tugood.requestservice.domain.karma.service

import com.ase.tugood.requestservice.application.data.karma.KarmaChangeDTO
import com.ase.tugood.requestservice.application.data.karma.KarmaRequestDTO
import reactor.core.publisher.Mono

interface IKarmaService {
    /**
     * The aim of this function is to request new karma change entries.
     *
     * @param karmaRequestDTO the karma request specifying the karma to be awarded or spent
     * @return a data stream that represents a list of karma change DTO which have been created
     */
    fun createKarma(karmaRequestDTO: KarmaRequestDTO): Mono<List<KarmaChangeDTO>>
}
