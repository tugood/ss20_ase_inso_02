package com.ase.tugood.requestservice.application.data.karma

enum class InternalKarmaChangeType {
    CANCEL_REQUEST,
    FINISH_REQUEST
}
