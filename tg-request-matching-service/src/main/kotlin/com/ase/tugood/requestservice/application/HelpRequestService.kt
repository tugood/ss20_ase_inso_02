package com.ase.tugood.requestservice.application

import com.ase.tugood.requestservice.application.data.user.UserDTO
import com.ase.tugood.requestservice.application.data.match.MatchStatus
import com.ase.tugood.requestservice.application.data.request.HelpRequestDTO
import com.ase.tugood.requestservice.application.data.request.RateLimitDTO
import com.ase.tugood.requestservice.domain.request.service.IHelpRequestService
import com.ase.tugood.requestservice.domain.request.service.mapper.HelpRequestMapper
import com.ase.tugood.requestservice.domain.user.service.IUserService
import com.ase.tugood.requestservice.infrastructure.configuration.RateConfiguration
import com.ase.tugood.requestservice.infrastructure.persistence.HelpRequestRepository
import com.ase.tugood.requestservice.infrastructure.persistence.MatchRepository
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import com.ase.tugood.requestservice.infrastructure.web.rest.error.TUGoodException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.lang.Long.max

@Service
class HelpRequestService(@Autowired private val helpRequestRepository: HelpRequestRepository,
                         @Autowired private val matchRepository: MatchRepository,
                         @Autowired private val helpRequestMapper: HelpRequestMapper,
                         @Autowired private val userService: IUserService,
                         @Autowired private val rateConfiguration: RateConfiguration) : IHelpRequestService {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun addNewHelpRequest(helpRequestDTO: HelpRequestDTO, userInfo: UserInfo): Mono<HelpRequestDTO> {
        val user = userService.getUserForId(userId = userInfo.id)
        return getRemainingHelpRequests(userInfo)
            .filter {
                it.remaining == 0L
            }
            .flatMap {
                Mono.error<TUGoodException>(TUGoodException(message = "No Remaining Requests", status = HttpStatus.TOO_MANY_REQUESTS, type = "RateLimit", cause = null))
            }.then(
                helpRequestRepository
                    .save(helpRequestMapper.toEntity(helpRequestDTO, userInfo))
                    .flatMap { req -> helpRequestMapper.toDto(req, user) }
                    .onErrorMap { cause ->
                        logger.error("cannot save the help request {}", cause.message); throw cause;
                    }
                    .log("added new help request: ${helpRequestDTO.id}")
            )
    }

    override fun getRemainingHelpRequests(userInfo: UserInfo): Mono<RateLimitDTO> {
        return getHelpRequestsByRequester(userInfo)
            .count()
            .flatMap { entries ->
                Mono.just(RateLimitDTO().apply {
                    this.max = rateConfiguration.maxCreationLimit.toLong()
                    this.current = entries
                    this.remaining = max(0, (rateConfiguration.maxCreationLimit.toLong() - entries))
                })
            }
            .log("calculated remaining requests")
    }

    override fun updateHelpRequest(id: String, helpRequestDTO: HelpRequestDTO, userInfo: UserInfo): Mono<HelpRequestDTO> {
        val user = userService.getUserForId(userId = userInfo.id)
        return helpRequestRepository
            .findByIdAndRequesterId(id, userInfo.id)
            .flatMap { req -> helpRequestRepository.save(helpRequestMapper.merge(req, helpRequestDTO, userInfo)) }
            .flatMap { req -> helpRequestMapper.toDto(req, user) }
            .onErrorMap { cause ->
                logger.error("cannot update the help request {}", cause.message); throw cause;
            }
            .log("updated help request: ${helpRequestDTO.id}")
    }

    override fun getHelpRequestByIdAndRequester(id: String, userInfo: UserInfo): Mono<HelpRequestDTO> {
        val user = userService.getUserForId(userId = userInfo.id)
        return helpRequestRepository
            .findByIdAndRequesterId(id, userInfo.id)
            .flatMap { req -> helpRequestMapper.toDto(req, user) }
            .onErrorMap { cause ->
                logger.error("cannot get the help request {}", cause.message); throw cause;
            }
            .log("got help request: $id")
    }

    override fun getHelpRequestsByRequester(userInfo: UserInfo): Flux<HelpRequestDTO> {
        val user = userService.getUserForId(userId = userInfo.id)
        return helpRequestRepository
            .findAllByRequesterId(userInfo.id)
            .filterWhen { potentialHelpRequest ->
                matchRepository.findAllByHelpRequestId(potentialHelpRequest.id!!).all { matchOfRequest ->
                    when (matchOfRequest.status) {
                        MatchStatus.CREATED -> true
                        else -> false
                    }
                }
            }
            .flatMap { req -> helpRequestMapper.toDto(req, user) }
            .onErrorMap { cause ->
                logger.error("cannot get help requests {}", cause.message); throw cause;
            }
            .log("got help requests for requester: ${userInfo.id}")
    }

    override fun getHelpRequestByIdAndSetRequester(id: String, requester: Mono<UserDTO>): Mono<HelpRequestDTO> {
        return helpRequestRepository
            .findById(id)
            .flatMap { req ->
                helpRequestMapper.toDto(req, requester)
            }
            .onErrorMap { cause ->
                logger.error("cannot get the help request {}", cause.message); throw cause;
            }
            .log("got help request: $id")
    }

    override fun deleteHelpRequestById(id: String, userInfo: UserInfo): Mono<Void> {
        return helpRequestRepository
            .deleteByIdAndRequesterId(id, userInfo.id)
            .log("deleted help request: $id")
    }
}
