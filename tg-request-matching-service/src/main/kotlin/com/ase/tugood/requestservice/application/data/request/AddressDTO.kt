package com.ase.tugood.requestservice.application.data.request

import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotNull

data class AddressDTO(
    @JsonProperty("streetName")
    var streetName: String? = null,

    @JsonProperty("streetNumber")
    var streetNumber: String? = null,

    @JsonProperty("postalCode")
    var postalCode: String? = null,

    @JsonProperty("city")
    var city: String? = null,

    @JsonProperty("country")
    var country: String? = null,

    @NotNull
    @JsonProperty("location")
    var location: LocationDTO? = null
)
