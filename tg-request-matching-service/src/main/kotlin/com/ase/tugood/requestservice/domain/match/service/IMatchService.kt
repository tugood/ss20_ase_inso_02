package com.ase.tugood.requestservice.domain.match.service

import com.ase.tugood.requestservice.application.data.match.MatchDTO
import com.ase.tugood.requestservice.application.data.match.MatchStatus
import com.ase.tugood.requestservice.application.data.match.RatingDTO
import com.ase.tugood.requestservice.domain.match.model.Match
import reactor.core.publisher.Mono

/**
 * This interface aims in providing an API that performs match operations on
 * data storage triggered by the reactive web controller
 */
interface IMatchService {

    /**
     * get the match with the id
     *
     * @param id the id of the match to be returned
     * @return the new match as reactive type
     */
    fun getMatchById(id: String): Mono<MatchDTO>

    /**
     * Fetches users from the user service and helpRequest from the HelpRequestService
     *
     * @param match match instance that is providing the required user and help request data
     * @return a data stream holding the match DTO with other DTOs set
     */
    fun getUsersAndHelpRequest(match: Match): Mono<MatchDTO>

    /**
     * Calls the KarmaService to change the karma points after a cancel occurred
     *
     * @param userId the id of the user we need to change the karma
     * @param matchDTO the cancelled match DTO
     */
    fun changeKarmaOnCancelMatch(userId: String, matchDTO: MatchDTO)


    /**
     * Calls the KarmaService to change the karma points after a match was finished
     *
     * @param userId the id of the user we need to change the karma
     * @param matchDTO the finished match DTO
     * @param ratingDTO the awarded rating DTO
     * @param helperId the id of the helper that served the match
     */
    fun changeKarmaOnFinishMatch(userId: String, matchDTO: MatchDTO, ratingDTO: RatingDTO?, helperId: String?)

    /**
     * Change match status
     *
     * @param id the id of the match
     * @param match the match we want to change the status of and persist
     * @param status the match status that needs to be assigned to the match
     * @return a data stream representing the match DTO with the updated status
     */
    fun changeMatchStatus(id: String, match: Match, status: MatchStatus): Mono<MatchDTO>

}
