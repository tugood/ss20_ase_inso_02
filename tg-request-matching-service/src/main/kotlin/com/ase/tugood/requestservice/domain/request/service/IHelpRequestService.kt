package com.ase.tugood.requestservice.domain.request.service

import com.ase.tugood.requestservice.application.data.user.UserDTO
import com.ase.tugood.requestservice.application.data.request.HelpRequestDTO
import com.ase.tugood.requestservice.application.data.request.RateLimitDTO
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

/**
 * This interface aims in providing an API that performs help request operations on
 * data storage triggered by the reactive web controller
 */
interface IHelpRequestService {
    /**
     * The aim of this function is to persist a new help request.
     * @param helpRequestDTO the new help request to be added
     * @return the new help request as reactive type
     */
    fun addNewHelpRequest(helpRequestDTO: HelpRequestDTO, userInfo: UserInfo): Mono<HelpRequestDTO>

    /**
     * The aim of this function is to return the number of remaining requests for this specific user.
     * @return the number of requests remaining, current and max
     */
    fun getRemainingHelpRequests(userInfo: UserInfo): Mono<RateLimitDTO>

    /**
     * The aim of this function is to update the data of a given help request in the data storage.
     * @param id the id of the help request to be updated
     * @param helpRequestDTO the new data of the help request
     * @return the new help request as reactive type
     */
    fun updateHelpRequest(id: String, helpRequestDTO: HelpRequestDTO, userInfo: UserInfo): Mono<HelpRequestDTO>

    /**
     * The aim of this function is to return the given help request by id.
     * @param id the id of the help request to be returned
     * @return the new help request as reactive type
     */
    fun getHelpRequestByIdAndRequester(id: String, userInfo: UserInfo): Mono<HelpRequestDTO>

    /**
     * The aim of this function is to return all help requests corresponding to the given user id
     * @param userInfo the UserInfo of the user to get the help requests for
     * @return a reactive list of all requests of the user
     */
    fun getHelpRequestsByRequester(userInfo: UserInfo): Flux<HelpRequestDTO>


    /**
     * Used internally
     * The aim of this function is to return the given help request by id and setting the requester
     * @param id the id of the help request to be returned
     * @param requester the user to be set as requester
     * @return the new help request as reactive type
     */
    fun getHelpRequestByIdAndSetRequester(id: String, requester: Mono<UserDTO>): Mono<HelpRequestDTO>

    /**
     * The aim of this function is to delete a helpRequest by its id.
     * @param id the id of the helpRequest to be deleted
     * @param requester the user which needs to be checked as requester
     */
    fun deleteHelpRequestById(id: String, userInfo: UserInfo): Mono<Void>

}
