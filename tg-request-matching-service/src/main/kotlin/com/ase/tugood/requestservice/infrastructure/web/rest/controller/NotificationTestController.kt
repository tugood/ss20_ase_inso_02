package com.ase.tugood.requestservice.infrastructure.web.rest.controller

import com.ase.tugood.requestservice.application.NotificationService
import com.ase.tugood.requestservice.application.data.match.MatchDTO
import com.ase.tugood.requestservice.application.data.notification.ServiceNotificationDTO
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.swagger.v3.oas.annotations.ExternalDocumentation
import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.info.Contact
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.servers.Server
import io.swagger.v3.oas.annotations.servers.ServerVariable
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import java.util.*


@RestController
@RequestMapping("/api/requests/notifications")
@OpenAPIDefinition(
    info = Info(title = "TUgood Request Matching Service", description = "This is the API for the Request Matching Service", contact = Contact(email = "silvio.vasiljevic@tuwien.ac.at"), version = "1.0.0"),
    externalDocs = ExternalDocumentation(description = "Project Repo", url = "https://reset.inso.tuwien.ac.at/repo/2020ss-ASE-group/ss20_ase_inso_02/-/tree/master/tg-request-matching-service"),
    servers = [
        Server(url = "http://127.0.0.1:{port}", description = "local development setup", variables = [ServerVariable(name = "port", defaultValue = "8084")]),
        Server(url = "http://dev.tugood.team:{port}", description = "digital ocean development setup", variables = [ServerVariable(name = "port", defaultValue = "8000")])
    ]
)
class NotificationTestController(@Autowired private val notificationService: NotificationService) {

    val logger: Logger = LoggerFactory.getLogger(javaClass)

    @Operation(
        description = "Create a notification test object",
        method = "getNotificationTest",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "Matches retrieved", content = [Content(schema = Schema(implementation = MatchDTO::class))])])
    @RequestMapping(
        value = [""],
        produces = [MediaType.APPLICATION_JSON_VALUE],
        method = [RequestMethod.GET])
    fun getNotificationTest(
        @RequestHeader("X-Userinfo") userInfo: String
    ): Mono<ResponseEntity<Map<String, String>>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        logger.info("Create get userDTO for user {}", info.id)

        val notify = notificationService.notify(info.id,
            Pair("notification.match.accepted.title", null),
            Pair("notification.match.accepted.body", arrayOf(info.name)),
            ServiceNotificationDTO.NotificationType.PUSH)

        return notify.ignoreElement().map { ResponseEntity.ok(mapOf("notification" to "maybe sent")) }
    }
}
