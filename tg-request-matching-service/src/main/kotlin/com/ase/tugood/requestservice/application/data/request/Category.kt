package com.ase.tugood.requestservice.application.data.request

import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

enum class Category(val category: String) : Serializable {
    @JsonProperty("GROCERY_SHOPPING")
    GROCERY_SHOPPING("GROCERY_SHOPPING"),

    @JsonProperty("ANIMAL_SITTING")
    ANIMAL_SITTING("ANIMAL_SITTING"),

    @JsonProperty("TRANSPORT")
    TRANSPORT("TRANSPORT"),

    @JsonProperty("CHILD_CARE")
    CHILD_CARE("CHILD_CARE"),

    @JsonProperty("PERSONAL_TRAINING")
    PERSONAL_TRAINING("PERSONAL_TRAINING"),

    @JsonProperty("OTHER")
    OTHER("OTHER");
}
