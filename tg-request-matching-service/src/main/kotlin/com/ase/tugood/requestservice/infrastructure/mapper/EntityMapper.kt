package com.ase.tugood.requestservice.infrastructure.mapper

import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo

/**
 * Contract for a generic dto to entity mapper.
 *
 * @param <D> - DTO type parameter.
 * @param <E> - Entity type parameter.
 */
interface EntityMapper<D, E> {

    /**
     * This function maps a generic DTO to the generic entity.
     *
     * @param dto the DTO to be mapped to an entity
     * @param userInfo the userInfo object that holds user specific information to be mapped
     * @return the mapped entity instance
     */
    fun toEntity(dto: D, userInfo: UserInfo): E

    /**
     * The aim of this function is to set merge fields of the DTO into the given entity.
     *
     * @param entity the entity to merge the fields into
     * @param dto the DTO providing the fields to merge into the entity
     * @return the merged entity
     */
    fun merge(entity: E, dto: D, userInfo: UserInfo): E

    /**
     * The aim of this function is to map a generic entity to a generic DTO.
     *
     * @param entity the entity to be mapped to a DTO
     * @return the mapped DTO
     */
    fun toDto(entity: E): D

    /**
     * The aim of this function is to map a list of generic DTOs to a list of generic entities.
     *
     * @param dtoList a list of generic DTOs to be mapped to a list of entities
     * @param userInfo the userInfo of the user to be mapped
     * @return the mapped list of entities
     */
    fun toEntity(dtoList: List<D>, userInfo: UserInfo): List<E> {
        return dtoList.map { toEntity(it, userInfo) }.toList()
    }

    /**
     * The aim of this function is to map a list of generic entities to a list of generic DTOs.
     *
     * @param entityList a list of generic entities to be mapped to a list of DTOs
     * @return the mapped list of DTOs
     */
    fun toDto(entityList: List<E>): List<D> {
        return entityList.map { toDto(it) }.toList()
    }
}
