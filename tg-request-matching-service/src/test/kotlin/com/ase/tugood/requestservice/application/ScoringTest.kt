package com.ase.tugood.requestservice.application

import com.ase.tugood.requestservice.application.data.user.UserDTO
import com.ase.tugood.requestservice.TestScoreDataBuilder
import com.ase.tugood.requestservice.application.data.match.MatchStatus
import com.ase.tugood.requestservice.application.data.request.AddressDTO
import com.ase.tugood.requestservice.application.data.request.HelpRequestDTO
import com.ase.tugood.requestservice.application.data.request.LocationDTO
import com.ase.tugood.requestservice.application.data.request.MatchRequestDTO
import com.ase.tugood.requestservice.domain.match.model.Match
import com.ase.tugood.requestservice.domain.match.service.IHelperMatchService
import com.ase.tugood.requestservice.domain.match.service.IMatchService
import com.ase.tugood.requestservice.domain.match.service.mapper.MatchMapper
import com.ase.tugood.requestservice.domain.notification.service.INotificationService
import com.ase.tugood.requestservice.domain.request.model.Address
import com.ase.tugood.requestservice.domain.request.model.HelpRequest
import com.ase.tugood.requestservice.domain.request.model.TimeFrame
import com.ase.tugood.requestservice.domain.request.service.IHelpRequestService
import com.ase.tugood.requestservice.domain.user.service.IUserService
import com.ase.tugood.requestservice.infrastructure.configuration.RateConfiguration
import com.ase.tugood.requestservice.infrastructure.configuration.ScoringConfiguration
import com.ase.tugood.requestservice.infrastructure.persistence.HelpRequestRepository
import com.ase.tugood.requestservice.infrastructure.persistence.MatchRepository
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.mongodb.core.geo.GeoJsonPoint
import reactor.core.publisher.Mono
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

@SpringBootTest
@ExtendWith(MockKExtension::class)
internal class ScoringTest {

    @Autowired
    private lateinit var helperMatchService: IHelperMatchService


    @Autowired
    private lateinit var helpRequestRepository: HelpRequestRepository

    @Autowired
    private lateinit var matchRepository: MatchRepository

    @Autowired
    private lateinit var matchMapper: MatchMapper

    @MockK
    private lateinit var mockUserService: IUserService

    @MockK
    private lateinit var mockNotificationService: INotificationService

    @MockkBean
    private lateinit var mockRateConfiguration: RateConfiguration

    @MockkBean
    private lateinit var mockScoreConfiguration: ScoringConfiguration

    @MockK
    private lateinit var mockHelpRequestService: IHelpRequestService

    @MockK
    private lateinit var mockMatchService: IMatchService

    private lateinit var requesterDTO: UserDTO
    private lateinit var requesterInfo: UserInfo
    private lateinit var perfectUserDTO: UserDTO
    private lateinit var perfectUserDTO2: UserDTO
    private lateinit var worstUserDTO: UserDTO
    private lateinit var goodUserDTO: UserDTO
    private lateinit var badUserDTO: UserDTO


    @BeforeEach
    fun setUp() {
        helperMatchService = HelperMatchService(matchRepository,
            helpRequestRepository,
            matchMapper,
            mockHelpRequestService,
            mockUserService,
            mockMatchService,
            mockNotificationService,
            mockRateConfiguration,
            mockScoreConfiguration
        )
        requesterDTO = TestScoreDataBuilder().requesterUserDTO()
        requesterInfo = UserInfo(id = requesterDTO.id)
        perfectUserDTO = TestScoreDataBuilder().perfectUserDTO()
        perfectUserDTO2 = TestScoreDataBuilder().perfectUserDTO().apply { id = "secondPerfect" }
        worstUserDTO = TestScoreDataBuilder().worstUserDTO()
        goodUserDTO = TestScoreDataBuilder().goodUserDTO()
        badUserDTO = TestScoreDataBuilder().badUserDTO()
        every { mockUserService.getUserForId(requesterDTO.id) } answers { Mono.just(requesterDTO) }
        every { mockUserService.getUserForId(perfectUserDTO.id) } answers { Mono.just(perfectUserDTO) }
        every { mockUserService.getUserForId(perfectUserDTO2.id) } answers { Mono.just(perfectUserDTO.apply { perfectUserDTO2 }) }
        every { mockUserService.getUserForId(worstUserDTO.id) } answers { Mono.just(worstUserDTO) }
        every { mockUserService.getUserForId(goodUserDTO.id) } answers { Mono.just(goodUserDTO) }
        every { mockUserService.getUserForId(badUserDTO.id) } answers { Mono.just(badUserDTO) }

        every {
            mockRateConfiguration.maxCreationLimit
        } answers {
            30
        }

        every {
            mockRateConfiguration.maxMatchLimit
        } answers {
            30
        }

        every { mockScoreConfiguration.lowestViableScore } answers { 20 }
        every { mockScoreConfiguration.maxBaseScore } answers { 80 }
        every { mockScoreConfiguration.maxDistanceKm } answers { 30 }
        every { mockScoreConfiguration.maxScore } answers { 100 }
        every { mockScoreConfiguration.minScore } answers { 0 }
    }

    @AfterEach
    fun tearDown() {
        helpRequestRepository.deleteAll().block()
        matchRepository.deleteAll().block()
    }

    @Test
    fun `test nearer requests receive better scores than farther requests`() {
        val nearRequest = HelpRequest(id = "nearest",
            address = Address(location = GeoJsonPoint(1.0, 1.0)),
            timeFrame = TimeFrame(to = Instant.now().plusMillis(1000)),
            requesterId = perfectUserDTO.id
        )
        val nearRequestDTO = HelpRequestDTO(id = "nearest",
            address = AddressDTO(location = LocationDTO(1.0, 1.0)),
            requester = perfectUserDTO)


        val farRequest = HelpRequest(id = "farthest",
            address = Address(location = GeoJsonPoint(1.0, 1.05)),
            timeFrame = TimeFrame(to = Instant.now().plusMillis(1000)),
            requesterId = perfectUserDTO2.id
        )
        val farRequestDTO = HelpRequestDTO(id = "farthest",
            address = AddressDTO(location = LocationDTO(1.0, 1.05)),
            requester = perfectUserDTO2
        )

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(nearRequest.id!!, any())
        } answers {
            Mono.just(nearRequestDTO)
        }
        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(farRequest.id!!, any())
        } answers {
            Mono.just(farRequestDTO)
        }

        helpRequestRepository.save(nearRequest).block()
        helpRequestRepository.save(farRequest).block()


        val matchDTOs = helperMatchService.createMatches(MatchRequestDTO(LocationDTO(1.0, 1.0)), requesterInfo).collectList().block()

        assertNotNull(matchDTOs)
        assertFalse(matchDTOs.isEmpty())
        assertTrue { matchDTOs.size == 2 }
        assertTrue(matchDTOs.stream().anyMatch {
            it.helpRequestDTO!!.id == "nearest"
        })
        assertTrue(matchDTOs.stream().anyMatch {
            it.helpRequestDTO!!.id == "farthest"
        })
        val nearestScore = matchDTOs.first { it.helpRequestDTO!!.id == "nearest" }.matchingScore!!
        val farthestScore = matchDTOs.first { it.helpRequestDTO!!.id == "farthest" }.matchingScore!!
        println(nearestScore)
        println(farthestScore)
        assertTrue(farthestScore < nearestScore)
    }

    @Test
    fun `test requesters who have cancelled in the past receive a worse score`() {
        val perfect = HelpRequest(id = "perfect",
            address = Address(location = GeoJsonPoint(1.0, 1.0)),
            timeFrame = TimeFrame(to = Instant.now().plusMillis(1000)),
            requesterId = this.perfectUserDTO.id
        )
        val perfectDTO = HelpRequestDTO(id = "perfect",
            address = AddressDTO(location = LocationDTO(1.0, 1.0)),
            requester = this.perfectUserDTO)


        val canceller = HelpRequest(id = "canceller",
            address = Address(location = GeoJsonPoint(1.0, 1.0)),
            timeFrame = TimeFrame(to = Instant.now().plusMillis(1000)),
            requesterId = this.perfectUserDTO2.id
        )
        val cancellerDTO = HelpRequestDTO(id = "canceller",
            address = AddressDTO(location = LocationDTO(1.0, 1.0)),
            requester = this.perfectUserDTO2
        )

        val cancelled = HelpRequest(id = "cancelled",
            address = Address(location = GeoJsonPoint(1.0, 1.0)),
            timeFrame = TimeFrame(to = Instant.now().minusMillis(1000)),
            requesterId = this.perfectUserDTO2.id
        )
        val cancelledDTO = HelpRequestDTO(id = "cancelled",
            address = AddressDTO(location = LocationDTO(1.0, 1.0)),
            requester = this.perfectUserDTO2
        )
        val cancelledMatch = Match(id = "cancelledMatch", status = MatchStatus.CANCELLED_BY_REQUESTER,
            helperId = this.perfectUserDTO.id, requesterId = perfectUserDTO2.id, helpRequestId = "cancelled", matchingScore = 0)
        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(perfect.id!!, any())
        } answers {
            Mono.just(perfectDTO)
        }
        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(canceller.id!!, any())
        } answers {
            Mono.just(cancellerDTO)
        }
        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(cancelled.id!!, any())
        } answers {
            Mono.just(cancelledDTO)
        }

        helpRequestRepository.save(perfect).block()
        helpRequestRepository.save(canceller).block()
        helpRequestRepository.save(cancelled).block()
        matchRepository.save(cancelledMatch).block()


        val matchDTOs = helperMatchService.createMatches(MatchRequestDTO(LocationDTO(1.0, 1.0)), requesterInfo).collectList().block()

        assertNotNull(matchDTOs)
        assertFalse(matchDTOs.isEmpty())
        assertTrue { matchDTOs.size == 2 }
        assertTrue(matchDTOs.stream().anyMatch {
            it.helpRequestDTO!!.id == "perfect"
        })
        assertTrue(matchDTOs.stream().anyMatch {
            it.helpRequestDTO!!.id == "canceller"
        })
        val perfectScore = matchDTOs.first { it.helpRequestDTO!!.id == "perfect" }.matchingScore!!
        val cancellerScore = matchDTOs.first { it.helpRequestDTO!!.id == "canceller" }.matchingScore!!
        println(perfectScore)
        println(cancellerScore)
        assertTrue(cancellerScore < perfectScore)
    }

    @Test
    fun `test requesters with better ratings receive better scores`() {
        val perfect = HelpRequest(id = "perfect",
            address = Address(location = GeoJsonPoint(1.0, 1.0)),
            timeFrame = TimeFrame(to = Instant.now().plusMillis(1000)),
            requesterId = this.perfectUserDTO.id
        )
        val perfectDTO = HelpRequestDTO(id = "perfect",
            address = AddressDTO(location = LocationDTO(1.0, 1.0)),
            requester = this.perfectUserDTO)


        val worst = HelpRequest(id = "worst",
            address = Address(location = GeoJsonPoint(1.0, 1.0)),
            timeFrame = TimeFrame(to = Instant.now().plusMillis(1000)),
            requesterId = this.worstUserDTO.id
        )
        val worstDTO = HelpRequestDTO(id = "worst",
            address = AddressDTO(location = LocationDTO(1.0, 1.0)),
            requester = this.worstUserDTO
        )

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(perfect.id!!, any())
        } answers {
            Mono.just(perfectDTO)
        }
        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(worst.id!!, any())
        } answers {
            Mono.just(worstDTO)
        }


        helpRequestRepository.save(perfect).block()
        helpRequestRepository.save(worst).block()

        val matchDTOs = helperMatchService.createMatches(MatchRequestDTO(LocationDTO(1.0, 1.0)), requesterInfo).collectList().block()

        assertNotNull(matchDTOs)
        assertFalse(matchDTOs.isEmpty())
        assertTrue { matchDTOs.size == 2 }
        assertTrue(matchDTOs.stream().anyMatch {
            it.helpRequestDTO!!.id == "perfect"
        })
        assertTrue(matchDTOs.stream().anyMatch {
            it.helpRequestDTO!!.id == "worst"
        })
        val perfectScore = matchDTOs.first { it.helpRequestDTO!!.id == "perfect" }.matchingScore!!
        val worstScore = matchDTOs.first { it.helpRequestDTO!!.id == "worst" }.matchingScore!!
        println(perfectScore)
        println(worstScore)
        assertTrue(worstScore < perfectScore)
    }

    @Test
    fun `test reduce score when users with better rating are already matched`() {
        val control = HelpRequest(id = "control",
            address = Address(location = GeoJsonPoint(1.0, 1.0)),
            timeFrame = TimeFrame(to = Instant.now().plusMillis(1000)),
            requesterId = this.perfectUserDTO.id
        )
        val controlDTO = HelpRequestDTO(id = "control",
            address = AddressDTO(location = LocationDTO(1.0, 1.0)),
            requester = this.perfectUserDTO)


        val matchedWithBetter = HelpRequest(id = "matchedWithBetter",
            address = Address(location = GeoJsonPoint(1.0, 1.0)),
            timeFrame = TimeFrame(to = Instant.now().plusMillis(1000)),
            requesterId = this.perfectUserDTO2.id
        )
        val matchedWithBetterDTO = HelpRequestDTO(id = "matchedWithBetter",
            address = AddressDTO(location = LocationDTO(1.0, 1.0)),
            requester = this.perfectUserDTO2
        )

        val betterMatch = Match(id = "betterMatch", status = MatchStatus.CREATED,
            helperId = this.perfectUserDTO.id, requesterId = perfectUserDTO2.id, helpRequestId = "matchedWithBetter", matchingScore = 100)

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(control.id!!, any())
        } answers {
            Mono.just(controlDTO)
        }
        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(matchedWithBetter.id!!, any())
        } answers {
            Mono.just(matchedWithBetterDTO)
        }

        helpRequestRepository.save(control).block()
        helpRequestRepository.save(matchedWithBetter).block()
        matchRepository.save(betterMatch).block()


        val matchDTOs = helperMatchService.createMatches(MatchRequestDTO(LocationDTO(1.0, 1.0)), requesterInfo).collectList().block()

        assertNotNull(matchDTOs)
        assertFalse(matchDTOs.isEmpty())
        assertTrue { matchDTOs.size == 2 }
        assertTrue(matchDTOs.stream().anyMatch {
            it.helpRequestDTO!!.id == "control"
        })
        assertTrue(matchDTOs.stream().anyMatch {
            it.helpRequestDTO!!.id == "matchedWithBetter"
        })
        val controlScore = matchDTOs.first { it.helpRequestDTO!!.id == "control" }.matchingScore!!
        val matchedWithBetterScore = matchDTOs.first { it.helpRequestDTO!!.id == "matchedWithBetter" }.matchingScore!!
        println(controlScore)
        println(matchedWithBetterScore)
        assertTrue(matchedWithBetterScore < controlScore)
    }

    @Test
    fun `test negative scores cannot occur and are set to score==0 automatically`() {
        every { mockScoreConfiguration.lowestViableScore } answers { 0 }
        // distance should be around 29km which will lead to a score near 0
        // the bad rating should lead to a further reduction below 0. this needs to be capped to 0 again.
        val negativeScorer = HelpRequest(id = "negativeScorer",
            address = Address(location = GeoJsonPoint(1.0, 1.26)),
            timeFrame = TimeFrame(to = Instant.now().plusMillis(1000)),
            requesterId = this.worstUserDTO.id
        )
        val negativeScorerDTO = HelpRequestDTO(id = "negativeScorer",
            address = AddressDTO(location = LocationDTO(1.0, 1.26)),
            requester = this.worstUserDTO)

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(negativeScorer.id!!, any())
        } answers {
            Mono.just(negativeScorerDTO)
        }

        helpRequestRepository.save(negativeScorer).block()

        val matchDTOs = helperMatchService.createMatches(MatchRequestDTO(LocationDTO(1.0, 1.0)), requesterInfo).collectList().block()

        println(matchDTOs)
        assertNotNull(matchDTOs)
        assertFalse(matchDTOs.isEmpty())
        assertTrue { matchDTOs.size == 1 }
        assertTrue(matchDTOs.stream().anyMatch {
            it.helpRequestDTO!!.id == "negativeScorer"
        })

        val score = matchDTOs.first { it.helpRequestDTO!!.id == "negativeScorer" }.matchingScore!!
        assertEquals(0, score)
    }

}
