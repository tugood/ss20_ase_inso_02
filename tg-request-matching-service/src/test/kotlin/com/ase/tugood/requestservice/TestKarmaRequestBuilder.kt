package com.ase.tugood.requestservice

import com.ase.tugood.requestservice.application.data.karma.InternalKarmaChangeType
import com.ase.tugood.requestservice.application.data.karma.KarmaChangeDTO
import com.ase.tugood.requestservice.application.data.karma.KarmaChangeType
import com.ase.tugood.requestservice.application.data.karma.KarmaRequestDTO
import com.ase.tugood.requestservice.application.data.match.RatingDTO
import java.time.Instant
import java.util.*

class TestKarmaRequestBuilder {

    private val userId: String = "userId"
    private val helperId: String = "helperId"
    private val internalKarmaChangeType: InternalKarmaChangeType = InternalKarmaChangeType.FINISH_REQUEST
    private val karmaChangeType: KarmaChangeType = KarmaChangeType.INTERNAL_FINISH_REQUEST
    private val rating = RatingDTO(rating = 5)

    fun getKarmaRequestDTO(): KarmaRequestDTO {
        return KarmaRequestDTO(userId = userId, type = internalKarmaChangeType, helperId = helperId, ratingDTO = rating)
    }

    fun getKarmaChangeDTO(): KarmaChangeDTO {
        return KarmaChangeDTO(id = UUID.randomUUID().toString(), value = 200, timestamp = Instant.now(), type = karmaChangeType)
    }
}
