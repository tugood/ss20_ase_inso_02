package com.ase.tugood.requestservice.application

import com.ase.tugood.requestservice.application.data.user.UserDTO
import com.ase.tugood.requestservice.TestHelpRequestBuilder
import com.ase.tugood.requestservice.TestUserBuilder
import com.ase.tugood.requestservice.TestUserInfoBuilder
import com.ase.tugood.requestservice.application.data.request.HelpRequestDTO
import com.ase.tugood.requestservice.domain.request.model.HelpRequest
import com.ase.tugood.requestservice.domain.request.service.mapper.HelpRequestMapper
import com.ase.tugood.requestservice.infrastructure.configuration.RateConfiguration
import com.ase.tugood.requestservice.infrastructure.configuration.ScoringConfiguration
import com.ase.tugood.requestservice.infrastructure.persistence.HelpRequestRepository
import com.ase.tugood.requestservice.infrastructure.persistence.MatchRepository
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertNotNull

@SpringBootTest
@ExtendWith(MockKExtension::class)
internal class HelpRequestServiceTest {

    private lateinit var helpRequestService: HelpRequestService

    @MockK
    private lateinit var mockHelpRequestRepository: HelpRequestRepository

    @MockK
    private lateinit var mockMatchRepository: MatchRepository

    @MockK
    private lateinit var userService: UserService

    @MockkBean
    private lateinit var mockRateConfiguration: RateConfiguration

    @MockkBean
    private lateinit var mockScoringConfiguration: ScoringConfiguration

    @Autowired
    private lateinit var helpRequestMapper: HelpRequestMapper

    private lateinit var helpRequestDTO: HelpRequestDTO

    private lateinit var helpRequest: HelpRequest

    private lateinit var userInfo: UserInfo
    private lateinit var userDTO: UserDTO

    @BeforeEach
    fun setUp() {
        helpRequestService = HelpRequestService(
            helpRequestRepository = mockHelpRequestRepository,
            matchRepository = mockMatchRepository,
            helpRequestMapper = helpRequestMapper,
            userService = userService,
            rateConfiguration = mockRateConfiguration
        )
        helpRequestDTO = TestHelpRequestBuilder().validDefaultHelpRequestDTO()
        helpRequest = TestHelpRequestBuilder().validUpdatedHelpRequest()
        userInfo = TestUserInfoBuilder().validDefaultUserInfo()
        userDTO = TestUserBuilder().validDefaultUserDTO()

        userDTO.apply {
            this.id = userInfo.id
        }

        every {
            userService.getUserForId(userDTO.id)
        } answers {
            Mono.just(userDTO)
        }
    }

    @Test
    fun `test add new helpRequest`() {
        val slot = slot<HelpRequest>()
        every { mockHelpRequestRepository.save(capture(slot)) } answers { Mono.just(slot.captured) }
        every { mockHelpRequestRepository.findAllByRequesterId(userInfo.id) } returns Flux.just(helpRequest)

        val insertedRequest = helpRequestService.addNewHelpRequest(helpRequestDTO, userInfo)
        assertNotNull(insertedRequest)

        verify(exactly = 2) { userService.getUserForId(userInfo.id) }
    }

    @Test
    fun `test new helpRequest error`() {
        every { mockHelpRequestRepository.save(any<HelpRequest>()) } throws RuntimeException()

        assertThrows<RuntimeException> { helpRequestService.addNewHelpRequest(helpRequestDTO, userInfo) }
        verify(exactly = 2) { userService.getUserForId(userInfo.id) }
    }

    @Test
    fun `test update helpRequest`() {
        val slot = slot<HelpRequest>()
        every { mockHelpRequestRepository.findByIdAndRequesterId(any(), any()) } returns Mono.just(helpRequest)
        every { mockHelpRequestRepository.save(capture(slot)) } answers { Mono.just(slot.captured) }

        val actualRequest = helpRequestService.updateHelpRequest("a", helpRequestDTO, userInfo).block()

        assertNotEquals(helpRequestDTO.lastModifiedDate, actualRequest?.lastModifiedDate)
        verify(exactly = 1) { userService.getUserForId(userInfo.id) }
    }


    @Test
    fun `test get helpRequest by id`() {
        every { mockHelpRequestRepository.findByIdAndRequesterId(any(), any()) } returns Mono.just(helpRequest)

        assertEquals(helpRequestMapper.toDto(helpRequest, Mono.just(userDTO)).block(),
            helpRequestService.getHelpRequestByIdAndRequester("a", userInfo).block())
        verify(exactly = 1) { userService.getUserForId(userInfo.id) }
    }


}
