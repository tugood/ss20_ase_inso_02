package com.ase.tugood.requestservice.application

import com.ase.tugood.requestservice.application.data.user.UserDTO
import com.ase.tugood.requestservice.TestServiceNotificationBuilder
import com.ase.tugood.requestservice.TestUserBuilder
import com.ase.tugood.requestservice.TestUserInfoBuilder
import com.ase.tugood.requestservice.application.data.match.RatingDTO
import com.ase.tugood.requestservice.application.data.notification.ServiceNotificationDTO
import com.ase.tugood.requestservice.configuration.WireMockInitializer
import com.ase.tugood.requestservice.infrastructure.configuration.RateConfiguration
import com.ase.tugood.requestservice.infrastructure.configuration.ScoringConfiguration
import com.ase.tugood.requestservice.infrastructure.web.client.UserServiceClient
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.*
import com.google.gson.Gson
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.slot
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.cloud.client.discovery.DiscoveryClient
import org.springframework.context.ApplicationContext
import org.springframework.core.env.get
import org.springframework.test.context.ContextConfiguration
import reactor.test.StepVerifier
import java.net.URI

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(MockKExtension::class)
@ContextConfiguration(initializers = [WireMockInitializer::class])
internal class UserServiceTest {
    @Autowired
    private lateinit var context: ApplicationContext

    private lateinit var userService: UserService

    @Autowired
    private lateinit var userServiceClient: UserServiceClient

    @MockkBean
    private lateinit var discoveryClient: DiscoveryClient

    @MockkBean
    private lateinit var mockRateConfiguration: RateConfiguration

    @MockkBean
    private lateinit var mockScoringConfiguration: ScoringConfiguration

    private lateinit var serviceNotificationDTO: ServiceNotificationDTO

    private lateinit var userInfo: UserInfo

    private lateinit var userDTO: UserDTO

    /**
     * @see com.ase.tugood.requestservice.configuration.WireMockInitializer#initialize()
     */
    @Autowired
    @Qualifier("wireMockServer")
    private lateinit var wireMockServer: WireMockServer

    @LocalServerPort
    private val port: Int? = null

    @BeforeEach
    fun setUp() {
        userService = UserService(userServiceClient = userServiceClient)
        serviceNotificationDTO = TestServiceNotificationBuilder().getDefaultServiceNotificationDTO()
        userInfo = TestUserInfoBuilder().validDefaultUserInfo()
        userDTO = TestUserBuilder().validDefaultUserDTO()

        val serviceIdSlot = slot<String>()
        every { discoveryClient.getInstances(capture(serviceIdSlot)) } answers {
            val serviceURIString = context.environment[serviceIdSlot.captured]

            listOf(mockk() {
                every { serviceId } returns serviceIdSlot.captured
                every { uri } returns if (serviceURIString != null) URI.create(serviceURIString) else null
            })
        }
    }

    @AfterEach
    fun afterEach() {
        wireMockServer.resetAll()
    }

    @Test
    fun `test get user should return userDTO`() {

        // just return something with ok
        this.wireMockServer.stubFor(get(
            urlPathEqualTo("/internal/users/accounts/${userDTO.id}"))
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBody(Gson().toJson(userDTO))))

        val getUserProvider = userService.getUserForId(userDTO.id)

        StepVerifier
            .create<UserDTO>(getUserProvider)
            .consumeNextWith { dto ->
                assertNotNull(dto)
                assertEquals(userDTO.id, dto.id)
                assertEquals(userDTO.email, dto.email)
                assertEquals(userDTO.langKey, dto.langKey)
            }
            .expectComplete()
            .verify()
    }

    @Test
    fun `test create rating should return userDTO`() {

        val ratingDTO = RatingDTO().apply {
            rating = 3
        }

        // just return something with ok
        this.wireMockServer.stubFor(post(
            urlPathEqualTo("/internal/users/ratings/${userDTO.id}"))
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBody(Gson().toJson(userDTO))))

        val postRatingProvider = userService.createRating(userDTO.id, ratingDTO = ratingDTO)

        StepVerifier
            .create<UserDTO>(postRatingProvider)
            .consumeNextWith { dto ->
                assertNotNull(dto)
                assertEquals(userDTO.id, dto.id)
                assertEquals(userDTO.email, dto.email)
                assertEquals(userDTO.langKey, dto.langKey)
            }
            .expectComplete()
            .verify()
    }
}
