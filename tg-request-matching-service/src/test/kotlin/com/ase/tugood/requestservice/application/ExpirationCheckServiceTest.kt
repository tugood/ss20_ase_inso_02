package com.ase.tugood.requestservice.application

import com.ase.tugood.requestservice.TestHelpRequestBuilder
import com.ase.tugood.requestservice.TestMatchBuilder
import com.ase.tugood.requestservice.application.data.match.MatchStatus
import com.ase.tugood.requestservice.application.data.notification.ServiceNotificationDTO
import com.ase.tugood.requestservice.application.data.user.UserDTO
import com.ase.tugood.requestservice.domain.match.model.Match
import com.ase.tugood.requestservice.domain.request.model.HelpRequest
import com.ase.tugood.requestservice.domain.request.model.TimeFrame
import com.ase.tugood.requestservice.infrastructure.configuration.RateConfiguration
import com.ase.tugood.requestservice.infrastructure.configuration.ScoringConfiguration
import com.ase.tugood.requestservice.infrastructure.persistence.HelpRequestRepository
import com.ase.tugood.requestservice.infrastructure.persistence.MatchRepository
import com.ninjasquad.springmockk.MockkBean
import com.ninjasquad.springmockk.SpykBean
import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import reactor.test.StepVerifier
import reactor.test.publisher.TestPublisher
import java.lang.Thread.sleep
import java.time.Instant
import java.time.temporal.ChronoUnit
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

@SpringBootTest
@ExtendWith(MockKExtension::class)
internal class ExpirationCheckServiceTest {

    /* mocks needed due to the spring application context */
    @MockkBean
    @Suppress("unused")
    private lateinit var mockRateConfiguration: RateConfiguration

    @MockkBean
    @Suppress("unused")
    private lateinit var scoringConfiguration: ScoringConfiguration
    /* -------------------------------------------------- */


    @Autowired
    private lateinit var expirationCheckService: ExpirationCheckService

    @SpykBean
    private lateinit var helpRequestRepository: HelpRequestRepository

    @SpykBean
    private lateinit var matchRepository: MatchRepository

    @MockkBean
    private lateinit var mockNotificationService: NotificationService

    private lateinit var expiredUser: UserDTO

    private lateinit var helpRequest: HelpRequest

    private lateinit var helpRequestExpired: HelpRequest

    @BeforeEach
    fun setUp() {
        helpRequest = TestHelpRequestBuilder().validDefaultHelpRequestEntity()
        val timeFrameFrom = Instant.now().plus(1, ChronoUnit.HOURS)
        val timeFrameTo = Instant.now().plus(2, ChronoUnit.HOURS)
        helpRequest.expirationDate = null
        helpRequest.timeFrame = TimeFrame(
            from = timeFrameFrom,
            to = timeFrameTo
        )

        helpRequestExpired = TestHelpRequestBuilder().validUpdatedHelpRequest()
        val timeFrameExpiredFrom = Instant.now().minus(3, ChronoUnit.HOURS)
        val timeFrameExpiredTo = Instant.now().minus(2, ChronoUnit.HOURS)
        helpRequestExpired.expirationDate = null
        helpRequestExpired.timeFrame = TimeFrame(
            from = timeFrameExpiredFrom,
            to = timeFrameExpiredTo
        )

        StepVerifier
            .create<Void>(helpRequestRepository.deleteAll())
            .expectComplete()
            .verify()

        StepVerifier
            .create<Void>(matchRepository.deleteAll())
            .expectComplete()
            .verify()

        StepVerifier
            .create<HelpRequest>(helpRequestRepository.insert(helpRequest))
            .consumeNextWith { request ->
                assertNotNull(request)
                assertNull(request.expirationDate)
            }
            .expectComplete()
            .verify()

        StepVerifier
            .create<HelpRequest>(helpRequestRepository.insert(helpRequestExpired))
            .consumeNextWith { request ->
                assertNotNull(request)
                assertNull(request.expirationDate)
            }
            .expectComplete()
            .verify()

        expiredUser = UserDTO(id = helpRequestExpired.requesterId!!)

        every {
            mockNotificationService.notify(expiredUser.id, any(), any(), ServiceNotificationDTO.NotificationType.PLAIN_OLD)
        } returns TestPublisher.createCold<Void>().complete().mono()
    }

    @Test
    fun `send notification for expired tasks with no match`() {
        expirationCheckService.helpRequestExpirationCheck()

        sleep(1000)

        StepVerifier
            .create<HelpRequest>(helpRequestRepository.findById(helpRequestExpired.id!!))
            .consumeNextWith { request ->
                assertNotNull(request)
                assertNotNull(request.expirationDate)
            }
            .expectComplete()
            .verify()

        verify(exactly = 1) {
            mockNotificationService.notify(expiredUser.id, any(), any(), ServiceNotificationDTO.NotificationType.PLAIN_OLD)
        }
    }

    @Test
    fun `send no notification for expired tasks with finished match`() {
        val match: Match = TestMatchBuilder().validDefaultMatch()
        match.helpRequestId = helpRequestExpired.id
        match.status = MatchStatus.FINISHED_BY_HELPER

        StepVerifier
            .create<Match>(matchRepository.insert(match))
            .consumeNextWith { m ->
                assertNotNull(m)
                assertNotNull(m.helpRequestId)
                assertEquals(m.helpRequestId, helpRequestExpired.id)
                assertEquals(m.status, MatchStatus.FINISHED_BY_HELPER)
            }
            .expectComplete()
            .verify()

        expirationCheckService.helpRequestExpirationCheck()

        StepVerifier
            .create<HelpRequest>(helpRequestRepository.findById(helpRequestExpired.id!!))
            .consumeNextWith { request ->
                assertNotNull(request)
                assertNull(request.expirationDate)
            }
            .expectComplete()
            .verify()

        verify(exactly = 0) {
            mockNotificationService.notify(expiredUser.id, any(), any(), ServiceNotificationDTO.NotificationType.PLAIN_OLD)
        }
    }
}
