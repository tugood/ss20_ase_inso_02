package com.ase.tugood.requestservice.application

import com.ase.tugood.requestservice.application.data.user.UserDTO
import com.ase.tugood.requestservice.application.data.karma.InternalKarmaChangeType
import com.ase.tugood.requestservice.application.data.karma.KarmaRequestDTO
import com.ase.tugood.requestservice.application.data.match.MatchDTO
import com.ase.tugood.requestservice.application.data.match.MatchStatus
import com.ase.tugood.requestservice.application.data.match.RatingDTO
import com.ase.tugood.requestservice.application.data.request.HelpRequestDTO
import com.ase.tugood.requestservice.domain.match.model.Match
import com.ase.tugood.requestservice.infrastructure.configuration.RateConfiguration
import com.ase.tugood.requestservice.infrastructure.configuration.ScoringConfiguration
import com.ase.tugood.requestservice.infrastructure.persistence.HelpRequestRepository
import com.ase.tugood.requestservice.infrastructure.persistence.MatchRepository
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.verifyAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import reactor.core.publisher.Mono
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@SpringBootTest
@ExtendWith(MockKExtension::class)
internal class MatchServiceTest {

    @Autowired
    private lateinit var matchService: MatchService

    @Autowired
    private lateinit var helpRequestRepository: HelpRequestRepository

    @MockkBean
    private lateinit var mockRateConfiguration: RateConfiguration

    @MockkBean
    private lateinit var mockScoringConfiguration: ScoringConfiguration

    @Autowired
    private lateinit var matchRepository: MatchRepository

    @MockkBean
    private lateinit var mockKarmaService: KarmaService

    @MockkBean
    private lateinit var mockHelpRequestService: HelpRequestService

    @MockkBean
    private lateinit var mockUserService: UserService

    private val requesterDTO = UserDTO(id = "requesterId")
    private val helperDTO = UserDTO(id = "helperId", distance = 20)
    private val helperInfo = UserInfo(id = "helperId", email_verified = true)

    @BeforeEach
    fun setUp() {
        every {
            mockUserService.getUserForId(helperInfo.id)
        } answers {
            Mono.just(helperDTO)
        }

        every {
            mockUserService.getUserForId(requesterDTO.id)
        } answers {
            Mono.just(requesterDTO)
        }


        helpRequestRepository.deleteAll().block()
        matchRepository.deleteAll().block()
    }

    @Test
    fun `getUsersAndHelpRequest should call services`() {

        val helpRequestDTO = HelpRequestDTO(id = "request1", requester = requesterDTO)
        val match = Match(id = "match1", status = MatchStatus.CREATED, helperId = helperDTO.id, requesterId = requesterDTO.id, helpRequestId = "request1")

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequestDTO.id!!, any())
        } answers {
            Mono.just(helpRequestDTO)
        }

        matchService.getUsersAndHelpRequest(match).block()

        verifyAll {
            mockUserService.getUserForId(requesterDTO.id)
            mockUserService.getUserForId(helperDTO.id)
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequestDTO.id!!, any())
        }
    }

    @Test
    fun `changeKarmaOnCancelMatch should call karma service`() {

        val helpRequestDTO = HelpRequestDTO(id = "request1", requester = requesterDTO)
        val matchDTO = MatchDTO(id = "match1", status = MatchStatus.CANCELLED_BY_REQUESTER, helper = helperDTO, helpRequestDTO = helpRequestDTO)

        every {
            mockKarmaService.createKarma(KarmaRequestDTO(userId = requesterDTO.id, type = InternalKarmaChangeType.CANCEL_REQUEST))
        } answers {
            Mono.just(emptyList())
        }

        matchService.changeKarmaOnCancelMatch(requesterDTO.id, matchDTO = matchDTO)

        verifyAll {
            mockKarmaService.createKarma(KarmaRequestDTO(userId = requesterDTO.id, type = InternalKarmaChangeType.CANCEL_REQUEST))
        }
    }

    @Test
    fun `changeKarmaOnFinishMatch should call karma service`() {

        val helpRequestDTO = HelpRequestDTO(id = "request1", requester = requesterDTO)
        val matchDTO = MatchDTO(id = "match1", status = MatchStatus.FINISHED_BY_REQUESTER, helper = helperDTO, helpRequestDTO = helpRequestDTO)
        val ratingDTO = RatingDTO(3, "lel")

        every {
            mockKarmaService.createKarma(KarmaRequestDTO(userId = requesterDTO.id, type = InternalKarmaChangeType.FINISH_REQUEST, ratingDTO = ratingDTO, helperId = helperDTO.id))
        } answers {
            Mono.just(emptyList())
        }

        matchService.changeKarmaOnFinishMatch(requesterDTO.id, matchDTO = matchDTO, ratingDTO = ratingDTO, helperId = helperDTO.id)

        verifyAll {
            mockKarmaService.createKarma(KarmaRequestDTO(userId = requesterDTO.id, type = InternalKarmaChangeType.FINISH_REQUEST, ratingDTO = ratingDTO, helperId = helperDTO.id))
        }
    }

    @Test
    fun `changeMatchStatus`() {

        val helpRequestDTO = HelpRequestDTO(id = "request1", requester = requesterDTO)
        val match = Match(id = "match1", status = MatchStatus.CREATED, helperId = helperDTO.id, requesterId = requesterDTO.id, helpRequestId = "request1")

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequestDTO.id!!, any())
        } answers {
            Mono.just(helpRequestDTO)
        }

        matchService.changeMatchStatus(match.id!!, match, status = MatchStatus.ACCEPTED).block()

        val result = matchRepository.findById(match.id!!).block()

        assertNotNull(result)
        assertEquals(MatchStatus.ACCEPTED, result.status)
    }

    @Test
    fun getMatchById() {

        val helpRequestDTO = HelpRequestDTO(id = "request1", requester = requesterDTO)
        val match = Match(id = "match1", status = MatchStatus.CREATED, helperId = helperDTO.id, requesterId = requesterDTO.id, helpRequestId = "request1")

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequestDTO.id!!, any())
        } answers {
            Mono.just(helpRequestDTO)
        }

        matchRepository.save(match).block()

        val result = matchService.getMatchById(match.id!!).block()

        assertNotNull(result)
        assertEquals(match.helpRequestId, result.helpRequestDTO?.id)
    }
}

