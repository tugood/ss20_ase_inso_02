package com.ase.tugood.requestservice.domain.match.service.mapper

import com.ase.tugood.requestservice.TestMatchBuilder
import com.ase.tugood.requestservice.TestUserInfoBuilder
import com.ase.tugood.requestservice.application.data.match.MatchDTO
import com.ase.tugood.requestservice.domain.match.model.Match
import com.ase.tugood.requestservice.infrastructure.configuration.DateTimeProvider
import com.ase.tugood.requestservice.infrastructure.configuration.RateConfiguration
import com.ase.tugood.requestservice.infrastructure.configuration.ScoringConfiguration
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import reactor.core.publisher.Mono
import java.time.Instant

@SpringBootTest
internal class MatchMapperTest {

    private lateinit var matchMapper: MatchMapper

    @Autowired
    private lateinit var completionMapper: CompletionMapper

    private lateinit var dateTimeProvider: DateTimeProvider

    private lateinit var match: Match

    private lateinit var matchDTO: MatchDTO

    private lateinit var userInfo: UserInfo

    @MockkBean
    private lateinit var mockRateConfiguration: RateConfiguration

    @MockkBean
    private lateinit var mockScoreConfiguration: ScoringConfiguration


    @BeforeEach
    fun setUp() {
        dateTimeProvider = mockk()
        every { dateTimeProvider.now() } returns Instant.ofEpochSecond(1587910583) // Sun, 26 Apr 2020 14:16:23 GMT

        match = TestMatchBuilder(dateTimeProvider = dateTimeProvider).validDefaultMatch()
        matchDTO = TestMatchBuilder(dateTimeProvider = dateTimeProvider).validDefaultMatchDTO()
        matchMapper = MatchMapper(dateTimeProvider = dateTimeProvider, completionMapper = completionMapper)
        userInfo = TestUserInfoBuilder().validDefaultUserInfo()
    }

    @Test
    fun `test dto to entity`() {
        val matchMapped = matchMapper.toEntity(matchDTO, userInfo)

        assertNotNull(match)
        assertEquals(match.id, matchMapped.id)
        assertEquals(match.status, matchMapped.status)
        assertEquals(match.matchDate, matchMapped.matchDate)
        assertEquals(match.matchingScore, matchMapped.matchingScore)
        assertEquals(match.completionInfo, matchMapped.completionInfo)
        assertEquals(match.helpRequestId, matchMapped.helpRequestId)
        assertEquals(match.helperId, matchMapped.helperId)
        assertEquals(match.createdDate, dateTimeProvider.now())
        assertEquals(match.lastModifiedDate, dateTimeProvider.now())
    }

    @Test
    fun `test entity to dto`() {
        val matchDTOMapped = matchMapper.toDto(match, Mono.empty(), Mono.empty()).block()

        assertNotNull(matchDTO)
        assertEquals(match.id, matchDTOMapped?.id)
        assertEquals(match.status, matchDTOMapped?.status)
        assertEquals(match.matchDate, matchDTOMapped?.matchDate)
        assertEquals(match.matchingScore, matchDTOMapped?.matchingScore)
        assertEquals(match.completionInfo?.requesterRating?.rating, matchDTOMapped?.completionInfoDTO?.requesterRating?.rating)
        assertEquals(match.helpRequestId, matchDTOMapped?.helpRequestDTO?.id)
        assertEquals(match.helperId, matchDTOMapped?.helper?.id)
        assertEquals(match.createdDate, matchDTOMapped?.createdDate)
        assertEquals(match.lastModifiedDate, matchDTOMapped?.lastModifiedDate)
    }

    @Test
    fun `test merge with empty`() {
        val newMatch = matchMapper.merge(match.copy(), MatchDTO(), userInfo)

        assertEquals(newMatch.matchDate, match.matchDate)
    }
}
