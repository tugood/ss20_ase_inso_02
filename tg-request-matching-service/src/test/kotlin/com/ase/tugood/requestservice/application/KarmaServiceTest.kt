package com.ase.tugood.requestservice.application

import com.ase.tugood.requestservice.TestKarmaRequestBuilder
import com.ase.tugood.requestservice.TestUserInfoBuilder
import com.ase.tugood.requestservice.application.data.karma.KarmaChangeDTO
import com.ase.tugood.requestservice.application.data.karma.KarmaRequestDTO
import com.ase.tugood.requestservice.configuration.WireMockInitializer
import com.ase.tugood.requestservice.infrastructure.configuration.RateConfiguration
import com.ase.tugood.requestservice.infrastructure.configuration.ScoringConfiguration
import com.ase.tugood.requestservice.infrastructure.web.client.KarmaServiceClient
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import com.fasterxml.jackson.databind.ObjectMapper
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.slot
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.cloud.client.discovery.DiscoveryClient
import org.springframework.context.ApplicationContext
import org.springframework.core.env.get
import org.springframework.test.context.ContextConfiguration
import reactor.test.StepVerifier
import java.net.URI

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(MockKExtension::class)
@ContextConfiguration(initializers = [WireMockInitializer::class])
internal class KarmaServiceTest {
    @Autowired
    private lateinit var context: ApplicationContext

    private lateinit var karmaService: KarmaService

    @MockkBean
    private lateinit var mockRateConfiguration: RateConfiguration

    @MockkBean
    private lateinit var mockScoringConfiguration: ScoringConfiguration

    @Autowired
    private lateinit var karmaServiceClient: KarmaServiceClient

    @MockkBean
    private lateinit var discoveryClient: DiscoveryClient

    private lateinit var karmaRequestDTO: KarmaRequestDTO

    private lateinit var karmaChangeDTO: KarmaChangeDTO

    private lateinit var userInfo: UserInfo

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    /**
     * @see com.ase.tugood.requestservice.configuration.WireMockInitializer#initialize()
     */
    @Autowired
    @Qualifier("wireMockServer")
    private lateinit var wireMockServer: WireMockServer

    @LocalServerPort
    private val port: Int? = null

    @BeforeEach
    fun setUp() {
        karmaService = KarmaService(karmaServiceClient = karmaServiceClient)
        karmaRequestDTO = TestKarmaRequestBuilder().getKarmaRequestDTO()
        karmaChangeDTO = TestKarmaRequestBuilder().getKarmaChangeDTO()

        userInfo = TestUserInfoBuilder().validDefaultUserInfo()

        val serviceIdSlot = slot<String>()
        every { discoveryClient.getInstances(capture(serviceIdSlot)) } answers {
            val serviceURIString = context.environment[serviceIdSlot.captured]

            listOf(mockk() {
                every { serviceId } returns serviceIdSlot.captured
                every { uri } returns if (serviceURIString != null) URI.create(serviceURIString) else null
            })
        }
    }

    @AfterEach
    fun afterEach() {
        wireMockServer.resetAll()
    }

    @Test
    fun `test create karma should return karma dto`() {

        // just return something with ok
        this.wireMockServer.stubFor(WireMock.post(
            WireMock.urlPathEqualTo("/internal/karma"))
            .willReturn(WireMock.aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBody(objectMapper.writeValueAsString(listOf(karmaChangeDTO)))))

        val postKarmaProvider = karmaService.createKarma(karmaRequestDTO)

        StepVerifier
            .create<List<KarmaChangeDTO>>(postKarmaProvider)
            .consumeNextWith { dto ->
                Assertions.assertNotNull(dto)
                Assertions.assertEquals(karmaChangeDTO.id, dto[0].id)
                Assertions.assertEquals(karmaChangeDTO.timestamp, dto[0].timestamp)
                Assertions.assertEquals(karmaChangeDTO.type, dto[0].type)
                Assertions.assertEquals(karmaChangeDTO.value, dto[0].value)
            }
            .expectComplete()
            .verify()
    }
}

