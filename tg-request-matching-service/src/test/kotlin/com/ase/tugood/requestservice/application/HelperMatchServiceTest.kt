package com.ase.tugood.requestservice.application

import com.ase.tugood.requestservice.application.data.user.UserDTO
import com.ase.tugood.requestservice.application.data.karma.InternalKarmaChangeType
import com.ase.tugood.requestservice.application.data.karma.KarmaRequestDTO
import com.ase.tugood.requestservice.application.data.match.MatchStatus
import com.ase.tugood.requestservice.application.data.match.RatingDTO
import com.ase.tugood.requestservice.application.data.notification.ServiceNotificationDTO
import com.ase.tugood.requestservice.application.data.request.*
import com.ase.tugood.requestservice.domain.match.model.Match
import com.ase.tugood.requestservice.domain.request.model.Address
import com.ase.tugood.requestservice.domain.request.model.HelpRequest
import com.ase.tugood.requestservice.domain.request.model.TimeFrame
import com.ase.tugood.requestservice.infrastructure.configuration.RateConfiguration
import com.ase.tugood.requestservice.infrastructure.configuration.ScoringConfiguration
import com.ase.tugood.requestservice.infrastructure.persistence.HelpRequestRepository
import com.ase.tugood.requestservice.infrastructure.persistence.MatchRepository
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.verifyAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.mongodb.core.geo.GeoJsonPoint
import reactor.core.publisher.Mono
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

@SpringBootTest
@ExtendWith(MockKExtension::class)
internal class HelperMatchServiceTest {

    @Autowired
    private lateinit var helperMatchService: HelperMatchService

    @MockkBean
    private lateinit var mockUserService: UserService

    @MockkBean
    private lateinit var mockNotificationService: NotificationService

    @MockkBean
    private lateinit var mockHelpRequestService: HelpRequestService

    @Autowired
    private lateinit var helpRequestRepository: HelpRequestRepository

    @MockkBean
    private lateinit var mockRateConfiguration: RateConfiguration

    @MockkBean
    private lateinit var mockScoreConfiguration: ScoringConfiguration

    @Autowired
    private lateinit var matchRepository: MatchRepository

    @MockkBean
    private lateinit var mockKarmaService: KarmaService

    private val requesterDTO = UserDTO(id = "requesterId")
    private val helperDTO = UserDTO(id = "helperId", distance = 20)
    private val helperInfo = UserInfo(id = "helperId", email_verified = true)

    @BeforeEach
    internal fun setUp() {
        every {
            mockUserService.getUserForId(helperInfo.id)
        } answers {
            Mono.just(helperDTO)
        }

        every {
            mockUserService.getUserForId(requesterDTO.id)
        } answers {
            Mono.just(requesterDTO)
        }

        every {
            mockUserService.createRating(requesterDTO.id, any())
        } answers {
            Mono.just(requesterDTO)
        }

        helpRequestRepository.deleteAll().block()
        matchRepository.deleteAll().block()

        every {
            mockRateConfiguration.maxCreationLimit
        } answers {
            30
        }

        every {
            mockRateConfiguration.maxMatchLimit
        } answers {
            30
        }

        every { mockScoreConfiguration.lowestViableScore } answers { 20 }
        every { mockScoreConfiguration.maxBaseScore } answers { 80 }
        every { mockScoreConfiguration.maxDistanceKm } answers { 30 }
        every { mockScoreConfiguration.maxScore } answers { 100 }
        every { mockScoreConfiguration.minScore } answers { 0 }
    }

    @Test
    fun `createMatches should filter help requests that are too far`() {

        val helpRequest1 = HelpRequest(id = "request1",
            address = Address(location = GeoJsonPoint(1.0, 1.0)),
            timeFrame = TimeFrame(to = Instant.now().plusMillis(1000)),
            requesterId = requesterDTO.id
        )
        val helpRequestDTO1 = HelpRequestDTO(id = "request1",
            address = AddressDTO(location = LocationDTO(1.0, 1.0)),
            requester = requesterDTO)

        val helpRequest2 = HelpRequest(id = "request2",
            address = Address(location = GeoJsonPoint(1.0, 2.00)),
            timeFrame = TimeFrame(to = Instant.now().plusMillis(1000)),
            requesterId = requesterDTO.id
        )

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequestDTO1.id!!, any())
        } answers {
            Mono.just(helpRequestDTO1)
        }

        helpRequestRepository.save(helpRequest1).block()
        helpRequestRepository.save(helpRequest2).block()

        val matchDTOs = helperMatchService.createMatches(MatchRequestDTO(LocationDTO(1.0, 1.0)), helperInfo).collectList().block()

        assertNotNull(matchDTOs)
        kotlin.test.assertFalse(matchDTOs.isEmpty())
        kotlin.test.assertTrue { matchDTOs.size == 1 }
        kotlin.test.assertTrue(matchDTOs.stream().anyMatch {
            it.helpRequestDTO!!.id == "request1"
        })
    }

    @Test
    fun `createMatches should filter own help requests`() {

        val helpRequest1 = HelpRequest(id = "request1",
            address = Address(location = GeoJsonPoint(1.0, 1.0)),
            timeFrame = TimeFrame(to = Instant.now().plusMillis(1000)),
            requesterId = helperDTO.id
        )

        val helpRequest2 = HelpRequest(id = "request2",
            address = Address(location = GeoJsonPoint(1.0, 1.0)),
            timeFrame = TimeFrame(to = Instant.now().plusMillis(1000)),
            requesterId = requesterDTO.id
        )

        val helpRequestDTO2 = HelpRequestDTO(id = "request2",
            address = AddressDTO(location = LocationDTO(1.0, 1.0)),
            requester = helperDTO)


        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequestDTO2.id!!, any())
        } answers {
            Mono.just(helpRequestDTO2)
        }

        helpRequestRepository.save(helpRequest1).block()
        helpRequestRepository.save(helpRequest2).block()

        val matchDTOs = helperMatchService.createMatches(MatchRequestDTO(LocationDTO(1.0, 1.0)), helperInfo).collectList().block()

        assertNotNull(matchDTOs)
        kotlin.test.assertFalse(matchDTOs.isEmpty())
        kotlin.test.assertTrue { matchDTOs.size == 1 }
        kotlin.test.assertTrue(matchDTOs.stream().anyMatch {
            it.helpRequestDTO!!.id == "request2"
        })
    }

    @Test
    fun `getAcceptableMatches should filter timeout matches`() {

        val helpRequest1 = HelpRequest(id = "request1", timeFrame = TimeFrame(to = Instant.now().minusMillis(1000)))
        val helpRequest2 = HelpRequest(id = "request2", timeFrame = TimeFrame(to = Instant.now().plusMillis(1000)))
        val helpRequestDTO2 = HelpRequestDTO(id = "request2")
        val match1 = Match(id = "match1", status = MatchStatus.CREATED, helperId = helperInfo.id, requesterId = requesterDTO.id, helpRequestId = "request1")
        val match2 = Match(id = "match2", status = MatchStatus.CREATED, helperId = helperInfo.id, requesterId = requesterDTO.id, helpRequestId = "request2")

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequestDTO2.id!!, any())
        } answers {
            Mono.just(helpRequestDTO2)
        }

        helpRequestRepository.save(helpRequest1).block()
        helpRequestRepository.save(helpRequest2).block()

        matchRepository.save(match1).block()
        matchRepository.save(match2).block()

        val matches = helperMatchService.getAcceptableMatches(helperInfo).collectList().block()

        assertNotNull(matches)
        matches!!.stream().forEach { print(it) }
        kotlin.test.assertTrue { matches.size == 1 }
        kotlin.test.assertTrue(matches.stream().anyMatch {
            it.id == "match2"
        }
        )
        kotlin.test.assertTrue(matches.stream().allMatch() {
            it.status == MatchStatus.CREATED
        }
        )
    }

    @Test
    fun `getAcceptableMatches should filter matches which were accepted by other`() {

        val helpRequest1 = HelpRequest(id = "request1", timeFrame = TimeFrame(to = Instant.now().plusMillis(1000)))
        val helpRequestDTO1 = HelpRequestDTO(id = "request1", timeFrame = TimeFrameDTO(to = Instant.now().plusMillis(1000)))
        val helpRequest2 = HelpRequest(id = "request2", timeFrame = TimeFrame(to = Instant.now().plusMillis(2000)))
        val match1 = Match(id = "match1", status = MatchStatus.CREATED, helperId = helperInfo.id, requesterId = requesterDTO.id, helpRequestId = "request1")
        val match2 = Match(id = "match2", status = MatchStatus.ACCEPTED, helperId = requesterDTO.id, requesterId = requesterDTO.id, helpRequestId = "request2")
        val match3 = Match(id = "match3", status = MatchStatus.CREATED, helperId = helperDTO.id, requesterId = requesterDTO.id, helpRequestId = "request2")

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequestDTO1.id!!, any())
        } answers {
            Mono.just(helpRequestDTO1)
        }

        helpRequestRepository.save(helpRequest1).block()
        helpRequestRepository.save(helpRequest2).block()

        matchRepository.save(match1).block()
        matchRepository.save(match2).block()
        matchRepository.save(match3).block()

        val matches = helperMatchService.getAcceptableMatches(helperInfo).collectList().block()

        assertNotNull(matches)
        matches!!.stream().forEach { print(it) }
        assertEquals(1, matches.size)
        assertTrue(matches.stream().allMatch {
            it.id == "match1"
        }
        )
        assertTrue(matches.stream().allMatch() {
            it.status == MatchStatus.CREATED
        }
        )
    }

    @Test
    fun `getAcceptableMatches should return only created`() {

        val helpRequest1 = HelpRequest(id = "request1", timeFrame = TimeFrame(to = Instant.now().plusMillis(1000)))
        val helpRequest2 = HelpRequest(id = "request2", timeFrame = TimeFrame(to = Instant.now().plusMillis(1000)))
        val helpRequestDTO2 = HelpRequestDTO(id = "request2")
        val match1 = Match(id = "match1", status = MatchStatus.ACCEPTED, helperId = helperInfo.id, requesterId = requesterDTO.id, helpRequestId = "request1")
        val match2 = Match(id = "match2", status = MatchStatus.CREATED, helperId = helperInfo.id, requesterId = requesterDTO.id, helpRequestId = "request2")

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequestDTO2.id!!, any())
        } answers {
            Mono.just(helpRequestDTO2)
        }

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequest1.id!!, any())
        } answers {
            Mono.empty()
        }

        helpRequestRepository.save(helpRequest1).block()
        helpRequestRepository.save(helpRequest2).block()

        matchRepository.save(match1).block()
        matchRepository.save(match2).block()

        val matches = helperMatchService.getAcceptableMatches(helperInfo).collectList().block()

        assertNotNull(matches)
        matches!!.stream().forEach { print(it) }
        kotlin.test.assertTrue { matches.size == 1 }
        kotlin.test.assertTrue(matches.stream().anyMatch {
            it.id == "match2"
        }
        )
        kotlin.test.assertTrue(matches.stream().allMatch() {
            it.status == MatchStatus.CREATED
        }
        )
    }

    @Test
    fun `finishMatch should be finished and rating set`() {

        val helpRequest1 = HelpRequest(id = "request1")
        val helpRequestDTO1 = HelpRequestDTO(id = "request1")
        val match1 = Match(id = "match1", status = MatchStatus.ACCEPTED, helperId = helperInfo.id, requesterId = requesterDTO.id, helpRequestId = "request1")
        val ratingDTO = RatingDTO(3, "comment")

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequestDTO1.id!!, any())
        } answers {
            Mono.just(helpRequestDTO1)
        }

        every {
            mockKarmaService.createKarma(KarmaRequestDTO(userId = helperDTO.id, type = InternalKarmaChangeType.FINISH_REQUEST, ratingDTO = ratingDTO))
        } answers {
            Mono.just(emptyList())
        }

        helpRequestRepository.save(helpRequest1).block()
        matchRepository.save(match1).block()

        val resultMatch = helperMatchService.finishMatch(match1.id!!, helperInfo, ratingDTO = ratingDTO).block()

        assertNotNull(resultMatch)
        assertEquals("match1", resultMatch.id)
        assertEquals(MatchStatus.FINISHED_BY_HELPER, resultMatch.status)
        assertEquals(ratingDTO.rating, resultMatch.completionInfoDTO?.helperRating?.rating)
        assertEquals(ratingDTO.comment, resultMatch.completionInfoDTO?.helperRating?.comment)

        val matchInDatabase = matchRepository.findById(match1.id!!).block()

        assertEquals(MatchStatus.FINISHED_BY_HELPER, matchInDatabase?.status)
        assertEquals(ratingDTO.rating, matchInDatabase?.completionInfo?.helperRating?.rating)
        assertEquals(ratingDTO.comment, matchInDatabase?.completionInfo?.helperRating?.comment)
    }

    @Test
    fun `finishMatch should call user service and karma service`() {

        val helpRequest1 = HelpRequest(id = "request1")
        val helpRequestDTO1 = HelpRequestDTO(id = "request1")
        val match1 = Match(id = "match1", status = MatchStatus.ACCEPTED, helperId = helperInfo.id, requesterId = requesterDTO.id, helpRequestId = "request1")
        val ratingDTO = RatingDTO(3, "comment")

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequestDTO1.id!!, any())
        } answers {
            Mono.just(helpRequestDTO1)
        }

        every {
            mockKarmaService.createKarma(KarmaRequestDTO(userId = helperDTO.id, type = InternalKarmaChangeType.FINISH_REQUEST, ratingDTO = ratingDTO))
        } answers {
            Mono.just(emptyList())
        }

        helpRequestRepository.save(helpRequest1).block()
        matchRepository.save(match1).block()

        helperMatchService.finishMatch(match1.id!!, helperInfo, ratingDTO = ratingDTO).block()

        verifyAll {
            mockUserService.getUserForId(helperDTO.id)
            mockUserService.createRating(requesterDTO.id, ratingDTO)
            mockKarmaService.createKarma(KarmaRequestDTO(userId = helperDTO.id, type = InternalKarmaChangeType.FINISH_REQUEST, ratingDTO = ratingDTO))
        }
    }

    @Test
    fun `cancelMatch should be cancelled`() {

        val helpRequest1 = HelpRequest(id = "request1")
        val helpRequestDTO1 = HelpRequestDTO(id = "request1")
        val match1 = Match(id = "match1", status = MatchStatus.ACCEPTED, helperId = helperInfo.id, requesterId = requesterDTO.id, helpRequestId = "request1")
        val ratingDTO = RatingDTO(3, "comment")
        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequestDTO1.id!!, any())
        } answers {
            Mono.just(helpRequestDTO1)
        }

        every {
            mockKarmaService.createKarma(KarmaRequestDTO(helperDTO.id, type = InternalKarmaChangeType.CANCEL_REQUEST))
        } answers {
            Mono.just(emptyList())
        }

        helpRequestRepository.save(helpRequest1).block()
        matchRepository.save(match1).block()

        val resultMatch = helperMatchService.cancelMatch(match1.id!!, helperInfo).block()

        assertNotNull(resultMatch)
        assertEquals("match1", resultMatch.id)
        assertEquals(MatchStatus.CANCELLED_BY_HELPER, resultMatch.status)

        val matchInDatabase = matchRepository.findById(match1.id!!).block()

        assertEquals(MatchStatus.CANCELLED_BY_HELPER, matchInDatabase?.status)
    }

    @Test
    fun `cancelMatch should call karma service`() {
        val helpRequest1 = HelpRequest(id = "request1")
        val helpRequestDTO1 = HelpRequestDTO(id = "request1")
        val match1 = Match(id = "match1", status = MatchStatus.ACCEPTED, helperId = helperInfo.id, requesterId = requesterDTO.id, helpRequestId = "request1")

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequest1.id!!, any())
        } answers {
            Mono.just(helpRequestDTO1)
        }

        every {
            mockKarmaService.createKarma(KarmaRequestDTO(helperDTO.id, type = InternalKarmaChangeType.CANCEL_REQUEST))
        } answers {
            Mono.just(emptyList())
        }

        helpRequestRepository.save(helpRequest1).block()
        matchRepository.save(match1).block()

        helperMatchService.cancelMatch(match1.id!!, helperInfo).block()

        verifyAll {
            mockKarmaService.createKarma(KarmaRequestDTO(userId = helperDTO.id, type = InternalKarmaChangeType.CANCEL_REQUEST))
        }
    }

    @Test
    fun `acceptMatch should be accepted`() {

        val helpRequest1 = HelpRequest(id = "request1")
        val helpRequestDTO1 = HelpRequestDTO(id = "request1", requester = requesterDTO)
        val match1 = Match(id = "match1", status = MatchStatus.CREATED, helperId = helperInfo.id, requesterId = requesterDTO.id, helpRequestId = "request1")

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequest1.id!!, any())
        } answers {
            Mono.just(helpRequestDTO1)
        }

        every {
            mockNotificationService.notify(requesterDTO.id, any(), any(), any())
        } answers {
            Mono.empty()
        }

        helpRequestRepository.save(helpRequest1).block()
        matchRepository.save(match1).block()

        val resultMatch = helperMatchService.acceptMatch(match1.id!!, helperInfo).block()

        assertNotNull(resultMatch)
        assertEquals("match1", resultMatch.id)
        assertEquals(MatchStatus.ACCEPTED, resultMatch.status)

        val matchInDatabase = matchRepository.findById(match1.id!!).block()

        assertEquals(MatchStatus.ACCEPTED, matchInDatabase?.status)
    }

    @Test
    fun `acceptMatch should call notificationService`() {
        val helpRequest1 = HelpRequest(id = "request1")
        val helpRequestDTO1 = HelpRequestDTO(id = "request1", requester = requesterDTO)
        val match1 = Match(id = "match1", status = MatchStatus.CREATED, helperId = helperInfo.id, requesterId = requesterDTO.id, helpRequestId = "request1")

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequest1.id!!, any())
        } answers {
            Mono.just(helpRequestDTO1)
        }

        every {
            mockNotificationService.notify(requesterDTO.id, any(), any(), any())
        } answers {
            Mono.empty()
        }

        helpRequestRepository.save(helpRequest1).block()
        matchRepository.save(match1).block()

        helperMatchService.acceptMatch(match1.id!!, helperInfo).block()

        verifyAll {
            mockNotificationService.notify(requesterDTO.id, any(), any(), ServiceNotificationDTO.NotificationType.PUSH)
        }

    }

    @Test
    fun `rejectMatch should be rejected`() {

        val helpRequest1 = HelpRequest(id = "request1")
        val helpRequestDTO1 = HelpRequestDTO(id = "request1")
        val match1 = Match(id = "match1", status = MatchStatus.CREATED, helperId = helperInfo.id, requesterId = requesterDTO.id, helpRequestId = "request1")

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequest1.id!!, any())
        } answers {
            Mono.just(helpRequestDTO1)
        }

        helpRequestRepository.save(helpRequest1).block()
        matchRepository.save(match1).block()

        val resultMatch = helperMatchService.rejectMatch(match1.id!!, helperInfo).block()

        assertNotNull(resultMatch)
        assertEquals("match1", resultMatch.id)
        assertEquals(MatchStatus.REJECTED, resultMatch.status)

        val matchInDatabase = matchRepository.findById(match1.id!!).block()

        assertEquals(MatchStatus.REJECTED, matchInDatabase?.status)
    }

    @Test
    fun getHistoricMatches() {

        val helpRequest1 = HelpRequest(id = "request1")
        val helpRequest2 = HelpRequest(id = "request2")
        val helpRequest3 = HelpRequest(id = "request3")
        val helpRequestDTO1 = HelpRequestDTO(id = "request1")
        val helpRequestDTO2 = HelpRequestDTO(id = "request2")
        val match1 = Match(id = "match1", status = MatchStatus.CANCELLED_BY_HELPER, helperId = helperInfo.id, requesterId = requesterDTO.id, helpRequestId = "request1")
        val match2 = Match(id = "match2", status = MatchStatus.FINISHED_BY_REQUESTER, helperId = helperInfo.id, requesterId = requesterDTO.id, helpRequestId = "request2")
        val match3 = Match(id = "match3", status = MatchStatus.FINISHED_BY_HELPER, helperId = helperInfo.id, requesterId = requesterDTO.id, helpRequestId = "request3")

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequest1.id!!, any())
        } answers {
            Mono.just(helpRequestDTO1)
        }

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequest2.id!!, any())
        } answers {
            Mono.just(helpRequestDTO2)
        }

        helpRequestRepository.save(helpRequest1).block()
        helpRequestRepository.save(helpRequest2).block()
        helpRequestRepository.save(helpRequest3).block()
        matchRepository.save(match1).block()
        matchRepository.save(match2).block()
        matchRepository.save(match3).block()

        val result = helperMatchService.getHistoricMatches(helperInfo).collectList().block()

        assertNotNull(result)
        assertEquals(2, result.size)
        assertEquals(match1.id, result[0]?.id)
        assertEquals(match2.id, result[1]?.id)
    }

    @Test
    fun getActiveMatches() {

        val helpRequest1 = HelpRequest(id = "request1")
        val helpRequest2 = HelpRequest(id = "request2")
        val helpRequest3 = HelpRequest(id = "request3")
        val helpRequestDTO3 = HelpRequestDTO(id = "request3")
        val match1 = Match(id = "match1", status = MatchStatus.CANCELLED_BY_HELPER, helperId = helperInfo.id, requesterId = requesterDTO.id, helpRequestId = "request1")
        val match2 = Match(id = "match2", status = MatchStatus.FINISHED_BY_REQUESTER, helperId = helperInfo.id, requesterId = requesterDTO.id, helpRequestId = "request2")
        val match3 = Match(id = "match3", status = MatchStatus.FINISHED_BY_HELPER, helperId = helperInfo.id, requesterId = requesterDTO.id, helpRequestId = "request3")


        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequest3.id!!, any())
        } answers {
            Mono.just(helpRequestDTO3)
        }

        helpRequestRepository.save(helpRequest1).block()
        helpRequestRepository.save(helpRequest2).block()
        helpRequestRepository.save(helpRequest3).block()
        matchRepository.save(match1).block()
        matchRepository.save(match2).block()
        matchRepository.save(match3).block()

        val result = helperMatchService.getActiveMatches(helperInfo).collectList().block()

        assertNotNull(result)
        assertEquals(1, result.size)
        assertEquals(match3.id, result[0]?.id)
    }

}
