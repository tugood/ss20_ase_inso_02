package com.ase.tugood.requestservice

import com.ase.tugood.requestservice.application.data.user.UserDTO
import com.ase.tugood.requestservice.application.data.request.*
import com.ase.tugood.requestservice.domain.request.model.Address
import com.ase.tugood.requestservice.domain.request.model.HelpRequest
import com.ase.tugood.requestservice.domain.request.model.TimeFrame
import com.ase.tugood.requestservice.infrastructure.configuration.DateTimeProvider
import org.springframework.data.mongodb.core.geo.GeoJsonPoint
import java.time.Instant
import java.time.temporal.ChronoUnit

data class TestHelpRequestBuilder(
    var id: String? = null,
    var category: Category? = null,
    var addressDTO: AddressDTO? = null,
    var isAsap: Boolean? = null,
    var timeFrameDTO: TimeFrameDTO? = null,
    var requester: UserDTO? = null,
    var description: String? = null,
    var expirationDate: Instant? = null,
    var createdDate: Instant? = Instant.now(),
    var lastModifiedDate: Instant? = Instant.now(),
    var dateTimeProvider: DateTimeProvider = DateTimeProvider()
) {
    val defaultId: String = "AAAAAAAAAAAAAAAAAAAA"
    val defaultTime: Instant = dateTimeProvider.now()
    val defaultCategory = Category.GROCERY_SHOPPING
    val defaultLocationLongitude = 14.5
    val defaultLocationLatitude = 14.5
    val defaultLocationDTO = LocationDTO().apply {
        longitude = defaultLocationLongitude
        latitude = defaultLocationLatitude
    }
    val defaultAddressDTO = AddressDTO().apply {
        this.streetName = "Karlsplatz"
        this.streetNumber = "5-7"
        this.postalCode = "1040"
        this.city = "Vienna"
        this.country = "Austria"
        this.location = defaultLocationDTO
    }
    val defaultLocation = GeoJsonPoint(defaultLocationLongitude, defaultLocationLatitude)
    val defaultAddress = Address().apply {
        this.streetName = "Karlsplatz"
        this.streetNumber = "5-7"
        this.postalCode = "1040"
        this.city = "Vienna"
        this.country = "Austria"
        this.location = defaultLocation
    }
    val defaultIsAsap = false
    val defaultTimeFrameFrom = dateTimeProvider.now().plus(1, ChronoUnit.HOURS)
    val defaultTimeFrameTo = dateTimeProvider.now().plus(2, ChronoUnit.HOURS)
    val defaultTimeFrameDTO = TimeFrameDTO().apply {
        from = defaultTimeFrameFrom
        to = defaultTimeFrameTo
    }
    val defaultTimeFrame = TimeFrame().apply {
        from = defaultTimeFrameFrom
        to = defaultTimeFrameTo
    }
    val defaultDescription = "I need some help"
    val defaultExpirationDate = defaultTimeFrameDTO.from?.minus(30, ChronoUnit.MINUTES)
    val defaultRequester = UserDTO().apply {
        this.id = "aaaa"
        this.firstName = "Max"
        this.lastName = "Mustermann"
    }

    val updatedId: String = "BBBBBBBBBBBBBBBBBBBB"
    val updatedTime: Instant = dateTimeProvider.now().minus(1, ChronoUnit.DAYS)
    val updatedCategory = Category.ANIMAL_SITTING
    val updatedLocationLongitude = 25.5
    val updatedLocationLatitude = 25.5
    val updatedLocationDTO = LocationDTO().apply {
        longitude = updatedLocationLongitude
        latitude = updatedLocationLatitude
    }
    val updatedAddressDTO = AddressDTO().apply {
        this.streetName = "Stephansplatz"
        this.streetNumber = "10"
        this.postalCode = "1010"
        this.city = "Vienna"
        this.country = "Austria"
        this.location = updatedLocationDTO
    }
    val updatedLocation = GeoJsonPoint(updatedLocationLongitude, updatedLocationLatitude)
    val updatedAddress = Address().apply {
        this.streetName = "Stephansplatz"
        this.streetName = "10"
        this.streetName = "1010"
        this.city = "Vienna"
        this.city = "Austria"
        this.location = updatedLocation
    }

    val updatedIsAsap = true
    val updatedTimeFrameFrom = dateTimeProvider.now()
    val updatedTimeFrameTo = dateTimeProvider.now().plus(5, ChronoUnit.HOURS)
    val updatedTimeFrameDTO = TimeFrameDTO().apply {
        from = updatedTimeFrameFrom
        to = updatedTimeFrameTo
    }
    val updatedTimeFrame = TimeFrame().apply {
        from = updatedTimeFrameFrom
        to = updatedTimeFrameTo
    }
    val updatedDescription = "I need advanced help"
    val updatedExpirationDate = dateTimeProvider.now().minus(3, ChronoUnit.MINUTES)
    val updatedRequester = UserDTO().apply {
        this.id = "bbbbb"
        this.firstName = "Fredl"
        this.lastName = "Firlefanz"
    }


    fun build() = HelpRequestDTO(id, category, addressDTO, isAsap, timeFrameDTO, description, requester, expirationDate, createdDate, lastModifiedDate)
    fun validDefaultHelpRequestDTO() = HelpRequestDTO(defaultId, defaultCategory, defaultAddressDTO, defaultIsAsap, defaultTimeFrameDTO, defaultDescription, defaultRequester, defaultExpirationDate, defaultTime, defaultTime)
    fun validDefaultHelpRequestEntity() = HelpRequest(defaultId, defaultCategory, defaultAddress, defaultIsAsap, defaultTimeFrame, defaultDescription, defaultRequester.id, defaultExpirationDate, defaultTime, defaultTime)
    fun validUpdatedHelpRequestDTO() = HelpRequestDTO(updatedId, updatedCategory, updatedAddressDTO, updatedIsAsap, updatedTimeFrameDTO, updatedDescription, updatedRequester, updatedExpirationDate, updatedTime, updatedTime)
    fun validUpdatedHelpRequest() = HelpRequest(updatedId, updatedCategory, updatedAddress, updatedIsAsap, updatedTimeFrame, updatedDescription, updatedRequester.id, updatedExpirationDate, updatedTime, updatedTime)
}
