package com.ase.tugood.requestservice.domain.request.service.mapper

import com.ase.tugood.requestservice.TestHelpRequestBuilder
import com.ase.tugood.requestservice.TestUserInfoBuilder
import com.ase.tugood.requestservice.application.data.request.HelpRequestDTO
import com.ase.tugood.requestservice.domain.request.model.HelpRequest
import com.ase.tugood.requestservice.infrastructure.configuration.DateTimeProvider
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import reactor.core.publisher.Mono
import java.time.Instant

internal class HelpRequestMapperTest {

    private lateinit var helpRequestMapper: HelpRequestMapper

    private lateinit var dateTimeProvider: DateTimeProvider

    private lateinit var helpRequest: HelpRequest

    private lateinit var helpRequestDTO: HelpRequestDTO

    private lateinit var userInfo: UserInfo


    @BeforeEach
    fun setUp() {
        dateTimeProvider = mockk()
        every { dateTimeProvider.now() } returns Instant.ofEpochSecond(1587910583) // Sun, 26 Apr 2020 14:16:23 GMT

        helpRequest = TestHelpRequestBuilder(dateTimeProvider = dateTimeProvider).validDefaultHelpRequestEntity()
        helpRequestDTO = TestHelpRequestBuilder(dateTimeProvider = dateTimeProvider).validDefaultHelpRequestDTO()
        helpRequestMapper = HelpRequestMapper(dateTimeProvider = dateTimeProvider)
        userInfo = TestUserInfoBuilder().validDefaultUserInfo()
    }

    @Test
    fun `test dto to entity`() {
        val helpRequestMapped = helpRequestMapper.toEntity(helpRequestDTO, userInfo)

        assertNotNull(helpRequestMapped)
        assertEquals(helpRequest.id, helpRequestMapped.id)
        assertEquals(helpRequest.category, helpRequestMapped.category)
        assertEquals(helpRequest.address, helpRequestMapped.address)
        assertEquals(helpRequest.isAsap, helpRequestMapped.isAsap)
        assertEquals(helpRequest.timeFrame, helpRequestMapped.timeFrame)
        assertEquals(helpRequest.description, helpRequestMapped.description)
        assertEquals(helpRequest.expirationDate, helpRequestMapped.expirationDate)
        assertEquals(helpRequest.createdDate, dateTimeProvider.now())
        assertEquals(helpRequest.lastModifiedDate, dateTimeProvider.now())
    }

    @Test
    fun `test entity to dto`() {
        val helpRequestDTOMapped = helpRequestMapper.toDto(helpRequest, Mono.just(helpRequestDTO.requester!!)).block()

        assertNotNull(helpRequestDTOMapped)
        assertEquals(helpRequestDTO.id, helpRequestDTOMapped?.id)
        assertEquals(helpRequestDTO.category, helpRequestDTOMapped?.category)
        assertEquals(helpRequestDTO.address, helpRequestDTOMapped?.address)
        assertEquals(helpRequestDTO.isAsap, helpRequestDTOMapped?.isAsap)
        assertEquals(helpRequestDTO.timeFrame, helpRequestDTOMapped?.timeFrame)
        assertEquals(helpRequestDTO.description, helpRequestDTOMapped?.description)
        assertEquals(helpRequestDTO.expirationDate, helpRequestDTOMapped?.expirationDate)
        assertEquals(helpRequestDTO.createdDate, helpRequestDTOMapped?.createdDate)
        assertEquals(helpRequestDTO.lastModifiedDate, helpRequestDTOMapped?.lastModifiedDate)
        assertEquals(helpRequestDTO.requester, helpRequestDTOMapped?.requester)
    }

}
