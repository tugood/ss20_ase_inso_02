package com.ase.tugood.requestservice.infrastructure.web.rest.controller

import com.ase.tugood.requestservice.application.data.user.UserDTO
import com.ase.tugood.requestservice.TestHelpRequestBuilder
import com.ase.tugood.requestservice.TestUserBuilder
import com.ase.tugood.requestservice.TestUserInfoBuilder
import com.ase.tugood.requestservice.application.UserService
import com.ase.tugood.requestservice.application.data.request.HelpRequestDTO
import com.ase.tugood.requestservice.domain.request.model.HelpRequest
import com.ase.tugood.requestservice.domain.request.service.mapper.HelpRequestMapper
import com.ase.tugood.requestservice.infrastructure.configuration.RateConfiguration
import com.ase.tugood.requestservice.infrastructure.configuration.ScoringConfiguration
import com.ase.tugood.requestservice.infrastructure.persistence.HelpRequestRepository
import com.ase.tugood.requestservice.infrastructure.web.client.NotificationServiceClient
import com.ase.tugood.requestservice.infrastructure.web.client.UserServiceClient
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Mono
import java.time.Instant
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertNull


@AutoConfigureWebTestClient
@SpringBootTest
internal class HelpRequestControllerTest {

    @MockkBean
    private lateinit var userService: UserService

    @MockkBean
    private lateinit var userServiceClient: UserServiceClient

    @MockkBean
    private lateinit var notificationServiceClient: NotificationServiceClient

    @MockkBean
    private lateinit var mockRateConfiguration: RateConfiguration

    @MockkBean
    private lateinit var mockScoreConfiguration: ScoringConfiguration

    @Autowired
    private lateinit var helpRequestRepository: HelpRequestRepository

    @Autowired
    private lateinit var webTestClient: WebTestClient

    private lateinit var helpRequestDTO: HelpRequestDTO

    private lateinit var helpRequest: HelpRequest

    private lateinit var userInfo: UserInfo

    private lateinit var userInfoB64: String

    @Autowired
    private lateinit var helpRequestMapper: HelpRequestMapper

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    private lateinit var userDTO: UserDTO
    private lateinit var instant: Instant

    @BeforeEach
    fun initTest() {
        helpRequestRepository.deleteAll().block()
        helpRequestDTO = TestHelpRequestBuilder().validDefaultHelpRequestDTO()
        helpRequest = TestHelpRequestBuilder().validUpdatedHelpRequest()
        userInfo = TestUserInfoBuilder().validDefaultUserInfo()
        userInfoB64 = Base64.getEncoder().encodeToString(jacksonObjectMapper().writeValueAsBytes(userInfo))
        userDTO = TestUserBuilder().validDefaultUserDTO().apply {
            this.id = userInfo.id
        }
        instant = Instant.parse("2020-05-23T19:56:00.318Z")

        helpRequestDTO.apply {
            this.timeFrame?.from = instant
        }

        every {
            userService.getUserForId(userDTO.id)
        } answers {
            Mono.just(userDTO)
        }


        every {
            mockRateConfiguration.maxCreationLimit
        } answers {
            30
        }

        every {
            mockRateConfiguration.maxMatchLimit
        } answers {
            30
        }

    }

    @Test
    fun `POST a new help request`() {
        webTestClient.post().uri("/api/requests")
            .contentType(MediaType.APPLICATION_JSON)
            .header("X-Userinfo", userInfoB64)
            .body(Mono.just(helpRequestDTO), HelpRequestDTO::class.java)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .consumeWith {
                val helpRequestDTO = objectMapper.readValue(it.responseBody, HelpRequestDTO::class.java)
                assertEquals(instant, helpRequestDTO.timeFrame?.from)
                assertEquals(userDTO.id, helpRequestDTO.requester?.id)
                assertEquals(userDTO.firstName, helpRequestDTO.requester?.firstName!!)
            }
            .jsonPath("id").isEqualTo(helpRequestDTO.id!!)
            .jsonPath("category").isEqualTo(helpRequestDTO.category!!.toString())
            .jsonPath("address").isEqualTo(helpRequestDTO.address!!)
            .jsonPath("isAsap").isEqualTo(helpRequestDTO.isAsap!!)
            .jsonPath("description").isEqualTo(helpRequestDTO.description!!)
    }

    @Test
    fun `PUT update a help request`() {
        val toUpdate = helpRequestMapper.toEntity(helpRequestDTO, userInfo)
        helpRequestRepository.save(toUpdate).block()
        helpRequestDTO.description = "meme"
        webTestClient.put().uri("/api/requests/" + toUpdate.id)
            .contentType(MediaType.APPLICATION_JSON)
            .header("X-Userinfo", userInfoB64)
            .body(Mono.just(helpRequestDTO), HelpRequestDTO::class.java)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .consumeWith {
                val helpRequestDTO = objectMapper.readValue(it.responseBody, HelpRequestDTO::class.java)
                assertEquals(instant, helpRequestDTO.timeFrame?.from)
                assertEquals(userDTO.id, helpRequestDTO.requester?.id)
                assertEquals(userDTO.firstName, helpRequestDTO.requester?.firstName!!)
            }
            .jsonPath("id").isEqualTo(helpRequestDTO.id!!)
            .jsonPath("category").isEqualTo(helpRequestDTO.category!!.toString())
            .jsonPath("address").isEqualTo(helpRequestDTO.address!!)
            .jsonPath("isAsap").isEqualTo(helpRequestDTO.isAsap!!)
            .jsonPath("description").isEqualTo("meme")
    }

    @Test
    fun `GET help request by id`() {

        val toGet = helpRequestMapper.toEntity(helpRequestDTO, userInfo)
        helpRequestRepository.save(toGet).block()
        webTestClient.get().uri("/api/requests/" + helpRequestDTO.id)
            .header("X-Userinfo", userInfoB64)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .consumeWith {
                val helpRequestDTO = objectMapper.readValue(it.responseBody, HelpRequestDTO::class.java)
                assertEquals(instant, helpRequestDTO.timeFrame?.from)
                assertEquals(userDTO.id, helpRequestDTO.requester?.id)
                assertEquals(userDTO.firstName, helpRequestDTO.requester?.firstName!!)
            }

            .jsonPath("id").isEqualTo(helpRequestDTO.id!!)
            .jsonPath("category").isEqualTo(helpRequestDTO.category!!.toString())
            .jsonPath("address").isEqualTo(helpRequestDTO.address!!)
            .jsonPath("isAsap").isEqualTo(helpRequestDTO.isAsap!!)
            .jsonPath("description").isEqualTo(helpRequestDTO.description!!)
    }

    @Test
    fun `GET all help requests`() {
        val toGet = helpRequestMapper.toEntity(helpRequestDTO, userInfo)
        helpRequestRepository.save(toGet).block()
        webTestClient.get().uri("/api/requests")
            .header("X-Userinfo", userInfoB64)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBodyList(HelpRequestDTO::class.java)
    }

    @Test
    fun `DELETE help request by id and requester id`() {
        val toDelete = helpRequestMapper.toEntity(helpRequestDTO, userInfo)
        helpRequestRepository.save(toDelete).block()
        webTestClient.delete().uri("/api/requests/${toDelete.id}/delete")
            .header("X-Userinfo", userInfoB64)
            .exchange()
            .expectStatus().isOk

        val result = webTestClient.get().uri("/api/requests/" + helpRequestDTO.id)
            .header("X-Userinfo", userInfoB64)
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .returnResult()
            .responseBody

        assertNull(result)
    }
}
