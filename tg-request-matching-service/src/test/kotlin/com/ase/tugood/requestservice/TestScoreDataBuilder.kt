package com.ase.tugood.requestservice

import com.ase.tugood.requestservice.application.data.user.UserDTO
import com.ase.tugood.notificationservice.application.data.user.UserState
import java.time.Instant

class TestScoreDataBuilder {
    companion object {
        //common user data which is not relevant for scoring and set the same for all
        const val commonLangKey = "en"
        const val commonPhoneNr = "06504206969"
        const val commonDescription = "a testing user"
        const val commonHasImage = false
        val commonState = UserState.ACTIVE
        val commonCreationDate = Instant.MIN

        //Requester User
        const val requesterId = "b1dd517a-e511-40f8-8c18-dc22b7badffc"
        const val requesterFirstName = "Requester"
        const val requesterLastName = "Boi"
        const val requesterEmail = "requester@boi.com"
        const val requesterDistance = 30L
        const val requesterRating = 4.5

        //Perfect matched User
        const val perfectId = "491fbc99-d013-4fc5-83c2-1c8d444010f1"
        const val perfectFirstName = "Perfect"
        const val perfectLastName = "Match"
        const val perfectEmail = "perfect@match.com"
        const val perfectDistance = 30L
        const val perfectRating = 5.0

        //worst matched User
        const val worstId = "a2e9275e-fa58-4d99-b3c0-67d53c96a21d"
        const val worstFirstName = "Worst"
        const val worstLastName = "Match"
        const val worstEmail = "worst@match.com"
        const val worstDistance = 1L
        const val worstRating = 0.0

        //good matched User
        const val goodId = "8e5fdf98-5cac-41c3-b8b9-f8d3e0c79f84"
        const val goodFirstName = "Good"
        const val goodLastName = "Match"
        const val goodEmail = "good@match.com"
        const val goodDistance = 25L
        const val goodRating = 4.0

        //bad matched User
        const val badId = "8e5fdf98-5cac-41c3-b8b9-f8d3e0c79f84"
        const val badFirstName = "Bad"
        const val badLastName = "Match"
        const val badEmail = "bad@match.com"
        const val badDistance = 10L
        const val badRating = 1.5
    }

    fun requesterUserDTO() =
        UserDTO(
            id = requesterId,
            firstName = requesterFirstName,
            lastName = requesterLastName,
            email = requesterEmail,
            phoneNr = commonPhoneNr,
            langKey = commonLangKey,
            description = commonDescription,
            hasImage = commonHasImage,
            userState = commonState,
            distance = requesterDistance,
            avgRating = requesterRating,
            createdDate = commonCreationDate,
            lastModifiedDate = commonCreationDate
        )

    fun perfectUserDTO() =
        UserDTO(
            id = perfectId,
            firstName = perfectFirstName,
            lastName = perfectLastName,
            email = perfectEmail,
            phoneNr = commonPhoneNr,
            langKey = commonLangKey,
            description = commonDescription,
            hasImage = commonHasImage,
            userState = commonState,
            distance = perfectDistance,
            avgRating = perfectRating,
            createdDate = commonCreationDate,
            lastModifiedDate = commonCreationDate
        )

    fun worstUserDTO() =
        UserDTO(
            id = worstId,
            firstName = worstFirstName,
            lastName = worstLastName,
            email = worstEmail,
            phoneNr = commonPhoneNr,
            langKey = commonLangKey,
            description = commonDescription,
            hasImage = commonHasImage,
            userState = commonState,
            distance = worstDistance,
            avgRating = worstRating,
            createdDate = commonCreationDate,
            lastModifiedDate = commonCreationDate
        )

    fun goodUserDTO() =
        UserDTO(
            id = goodId,
            firstName = goodFirstName,
            lastName = goodLastName,
            email = goodEmail,
            phoneNr = commonPhoneNr,
            langKey = commonLangKey,
            description = commonDescription,
            hasImage = commonHasImage,
            userState = commonState,
            distance = goodDistance,
            avgRating = goodRating,
            createdDate = commonCreationDate,
            lastModifiedDate = commonCreationDate
        )

    fun badUserDTO() =
        UserDTO(
            id = badId,
            firstName = badFirstName,
            lastName = badLastName,
            email = badEmail,
            phoneNr = commonPhoneNr,
            langKey = commonLangKey,
            description = commonDescription,
            hasImage = commonHasImage,
            userState = commonState,
            distance = badDistance,
            avgRating = badRating,
            createdDate = commonCreationDate,
            lastModifiedDate = commonCreationDate
        )
}
