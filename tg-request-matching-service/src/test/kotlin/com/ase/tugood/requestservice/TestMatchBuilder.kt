package com.ase.tugood.requestservice

import com.ase.tugood.requestservice.application.data.user.UserDTO
import com.ase.tugood.requestservice.application.data.match.CompletionInfoDTO
import com.ase.tugood.requestservice.application.data.match.MatchDTO
import com.ase.tugood.requestservice.application.data.match.MatchStatus
import com.ase.tugood.requestservice.application.data.match.RatingDTO
import com.ase.tugood.requestservice.domain.match.model.CompletionInfo
import com.ase.tugood.requestservice.domain.match.model.Match
import com.ase.tugood.requestservice.domain.match.model.Rating
import com.ase.tugood.requestservice.infrastructure.configuration.DateTimeProvider
import java.time.Instant
import java.time.temporal.ChronoUnit

class TestMatchBuilder(dateTimeProvider: DateTimeProvider = DateTimeProvider()) {
    private val defaultId: String = "AAAAAAAAAAAAAAAAAAAA"
    private val defaultTime: Instant = dateTimeProvider.now()
    private val defaultStatus: MatchStatus = MatchStatus.CREATED
    private val defaultMatchDate: Instant = defaultTime
    private val defaultMatchingScore: Long = 1L

    private val defaultRequesterFinishedOn: Instant = defaultTime.plus(45, ChronoUnit.MINUTES)
    private val defaultHelperFinishedOn: Instant = defaultTime.plus(30, ChronoUnit.MINUTES)
    private val defaultHelperRatingValue: Int = 3
    private val defaultHelperRatingComment: String = "Pass on what you have learned."
    private val defaultHelperRatingDTO: RatingDTO = RatingDTO().apply {
        rating = defaultHelperRatingValue
        comment = defaultHelperRatingComment
    }
    private val defaultRequesterRatingValue: Int = 4
    private val defaultRequesterRatingComment: String = "The greatest teacher, failure is."
    private val defaultRequesterRatingDTO: RatingDTO = RatingDTO().apply {
        rating = defaultRequesterRatingValue
        comment = defaultRequesterRatingComment
    }
    private val defaultCompletionInfoDTO: CompletionInfoDTO = CompletionInfoDTO().apply {
        requesterFinishedOn = defaultRequesterFinishedOn
        helperRating = defaultHelperRatingDTO
        helperFinishedOn = defaultHelperFinishedOn
        requesterRating = defaultRequesterRatingDTO
    }

    private val defaultHelperRating: Rating = Rating().apply {
        rating = defaultHelperRatingValue
        comment = defaultHelperRatingComment
    }
    private val defaultRequesterRating: Rating = Rating().apply {
        rating = defaultRequesterRatingValue
        comment = defaultRequesterRatingComment
    }
    private val defaultCompletionInfo: CompletionInfo = CompletionInfo().apply {
        requesterFinishedOn = defaultRequesterFinishedOn
        helperRating = defaultHelperRating
        helperFinishedOn = defaultHelperFinishedOn
        requesterRating = defaultRequesterRating
    }
    private val defaultHelpRequestDTO = TestHelpRequestBuilder(dateTimeProvider = dateTimeProvider).validDefaultHelpRequestDTO()
    private val defaultHelpRequestId = defaultHelpRequestDTO.id
    private val defaultHelper = UserDTO().apply {
        this.id = "aaa"
        this.firstName = "Peter"
        this.lastName = "Pan"
    }

    private val updatedId: String = "BBBBBBBBBBBBBBBBBBBB"
    private val updatedTime: Instant = dateTimeProvider.now().minus(1, ChronoUnit.DAYS)
    private val updatedStatus: MatchStatus = MatchStatus.ACCEPTED
    private val updatedMatchDate: Instant = updatedTime
    private val updatedMatchingScore: Long = 2L

    private val updatedRequesterFinishedOn: Instant = updatedTime.plus(45, ChronoUnit.MINUTES)
    private val updatedHelperFinishedOn: Instant = updatedTime.plus(30, ChronoUnit.MINUTES)
    private val updatedHelperRatingValue: Int = 5
    private val updatedHelperRatingComment: String = "You don't know the power of the dark side!"
    private val updatedHelperRatingDTO: RatingDTO = RatingDTO().apply {
        rating = updatedHelperRatingValue
        comment = updatedHelperRatingComment
    }
    private val updatedRequesterRatingValue: Int = 1
    private val updatedRequesterRatingComment: String = "There is a great disturbance in the Force."
    private val updatedRequesterRatingDTO: RatingDTO = RatingDTO().apply {
        rating = updatedRequesterRatingValue
        comment = updatedRequesterRatingComment
    }
    private val updatedCompletionInfoDTO: CompletionInfoDTO = CompletionInfoDTO().apply {
        requesterFinishedOn = updatedRequesterFinishedOn
        helperRating = updatedHelperRatingDTO
        helperFinishedOn = updatedHelperFinishedOn
        requesterRating = updatedRequesterRatingDTO
    }

    private val updatedHelperRating: Rating = Rating().apply {
        rating = updatedHelperRatingValue
        comment = updatedHelperRatingComment
    }
    private val updatedRequesterRating: Rating = Rating().apply {
        rating = updatedRequesterRatingValue
        comment = updatedRequesterRatingComment
    }
    private val updatedCompletionInfo: CompletionInfo = CompletionInfo().apply {
        requesterFinishedOn = updatedRequesterFinishedOn
        helperRating = updatedHelperRating
        helperFinishedOn = updatedHelperFinishedOn
        requesterRating = updatedRequesterRating
    }
    private val updatedHelpRequestDTO = TestHelpRequestBuilder(dateTimeProvider = dateTimeProvider).validUpdatedHelpRequestDTO()
    private val updatedHelpRequestId = updatedHelpRequestDTO.id
    private val updatedHelper = UserDTO().apply {
        this.id = "bbb"
        this.firstName = "Lisa"
        this.lastName = "Müller"
    }

    fun validDefaultMatchDTO() = MatchDTO(defaultId, defaultStatus, defaultMatchDate, defaultMatchingScore, defaultCompletionInfoDTO, defaultHelpRequestDTO, defaultHelper, defaultTime, defaultTime)
    fun validDefaultMatch() = Match(defaultId, defaultStatus, defaultMatchDate, defaultMatchingScore, defaultCompletionInfo, defaultHelpRequestId, defaultHelper.id, updatedHelper.id, defaultTime, defaultTime)
    fun validUpdatedMatchDTO() = MatchDTO(updatedId, updatedStatus, updatedMatchDate, updatedMatchingScore, updatedCompletionInfoDTO, updatedHelpRequestDTO, updatedHelper, updatedTime, updatedTime)
    fun validUpdatedMatch() = Match(updatedId, updatedStatus, updatedMatchDate, updatedMatchingScore, updatedCompletionInfo, updatedHelpRequestId, updatedHelper.id, defaultHelper.id, updatedTime, updatedTime)
}
