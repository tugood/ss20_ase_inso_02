package com.ase.tugood.requestservice.infrastructure.web.rest.controller

import com.ase.tugood.requestservice.application.data.user.UserDTO
import com.ase.tugood.requestservice.TestHelpRequestBuilder
import com.ase.tugood.requestservice.TestMatchBuilder
import com.ase.tugood.requestservice.TestUserBuilder
import com.ase.tugood.requestservice.TestUserInfoBuilder
import com.ase.tugood.requestservice.application.UserService
import com.ase.tugood.requestservice.application.data.match.MatchDTO
import com.ase.tugood.requestservice.application.data.request.HelpRequestDTO
import com.ase.tugood.requestservice.application.data.request.MatchRequestDTO
import com.ase.tugood.requestservice.domain.match.model.Match
import com.ase.tugood.requestservice.domain.match.service.mapper.MatchMapper
import com.ase.tugood.requestservice.domain.request.service.mapper.HelpRequestMapper
import com.ase.tugood.requestservice.infrastructure.configuration.RateConfiguration
import com.ase.tugood.requestservice.infrastructure.configuration.ScoringConfiguration
import com.ase.tugood.requestservice.infrastructure.persistence.HelpRequestRepository
import com.ase.tugood.requestservice.infrastructure.persistence.MatchRepository
import com.ase.tugood.requestservice.infrastructure.web.client.NotificationServiceClient
import com.ase.tugood.requestservice.infrastructure.web.client.UserServiceClient
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Mono
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@AutoConfigureWebTestClient
@SpringBootTest
internal class MatchControllerTest {

    @MockkBean
    private lateinit var userService: UserService

    @MockkBean
    private lateinit var userServiceClient: UserServiceClient

    @MockkBean
    private lateinit var notificationServiceClient: NotificationServiceClient

    @MockkBean
    private lateinit var mockRateConfiguration: RateConfiguration

    @MockkBean
    private lateinit var mockScoreConfiguration: ScoringConfiguration

    @Autowired
    private lateinit var helpRequestMapper: HelpRequestMapper

    @Autowired
    private lateinit var matchRepository: MatchRepository

    @Autowired
    private lateinit var helpRequestRepository: HelpRequestRepository

    @Autowired
    private lateinit var webTestClient: WebTestClient

    private lateinit var helpRequestDTO: HelpRequestDTO

    private lateinit var match: Match
    private lateinit var matchDTO: MatchDTO

    private lateinit var userInfo1: UserInfo
    private lateinit var userInfo2: UserInfo

    private lateinit var userInfo1B64: String
    private lateinit var userInfo2B64: String

    @Autowired
    private lateinit var matchMapper: MatchMapper

    private lateinit var userDTO1: UserDTO
    private lateinit var userDTO2: UserDTO

    @BeforeEach
    fun initTest() {
        matchRepository.deleteAll().block()
        helpRequestRepository.deleteAll().block()
        helpRequestDTO = TestHelpRequestBuilder().validDefaultHelpRequestDTO()
        userInfo1 = TestUserInfoBuilder().validDefaultUserInfo()
        userInfo2 = TestUserInfoBuilder().validUpdatedUserInfo()
        match = TestMatchBuilder().validDefaultMatch()
        matchDTO = TestMatchBuilder().validDefaultMatchDTO()
        userInfo1B64 = Base64.getEncoder().encodeToString(jacksonObjectMapper().writeValueAsBytes(userInfo1))
        userInfo2B64 = Base64.getEncoder().encodeToString(jacksonObjectMapper().writeValueAsBytes(userInfo2))
        userDTO1 = TestUserBuilder().validDefaultUserDTO()
        userDTO2 = TestUserBuilder().validAdminUserDTO()

        userDTO1.apply {
            id = userInfo1.id
        }


        userDTO2.apply {
            id = userInfo2.id
        }

        every {
            userService.getUserForId(userInfo1.id)
        } answers {
            Mono.just(userDTO1)
        }

        every {
            userService.getUserForId(userInfo2.id)
        } answers {
            Mono.just(userDTO2)
        }

        every {
            mockRateConfiguration.maxCreationLimit
        } answers {
            3
        }

        every {
            mockRateConfiguration.maxMatchLimit
        } answers {
            3
        }

        every { mockScoreConfiguration.lowestViableScore } answers { 20 }
        every { mockScoreConfiguration.maxBaseScore } answers { 80 }
        every { mockScoreConfiguration.maxDistanceKm } answers { 30 }
        every { mockScoreConfiguration.maxScore } answers { 100 }
        every { mockScoreConfiguration.minScore } answers { 0 }
    }

    @Test
    fun `GET all matches`() {
        val matchInDb = matchMapper.toEntity(matchDTO, userInfo1)
        matchRepository.save(matchInDb).block()
        webTestClient.get().uri("/api/requests/matches/acceptable")
            .header("X-Userinfo", userInfo2B64)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBodyList(MatchDTO::class.java)
    }

    @Test
    fun `Create matches`() {

        helpRequestDTO.timeFrame?.from = Instant.now().plus(1, ChronoUnit.DAYS)
        val helpRequestToSave = helpRequestMapper.toEntity(helpRequestDTO, userInfo1)

        assertEquals(userInfo1.id, helpRequestToSave.requesterId)

        helpRequestRepository.save(helpRequestToSave).block()

        val matchRequestDTO = MatchRequestDTO(helpRequestDTO.address?.location)

        val matchList = webTestClient.put().uri("/api/requests/matches")
            .header("X-Userinfo", userInfo2B64)
            .body(Mono.just(matchRequestDTO), MatchRequestDTO::class.java)
            .exchange()
            .expectStatus().isCreated
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBodyList(MatchDTO::class.java)
            .returnResult()
            .responseBody

        assertEquals(1, matchList?.size)

        val match = matchList?.get(0)

        assertNotNull(match?.helpRequestDTO)
        assertNotNull(match?.helpRequestDTO?.requester)
        assertEquals(userInfo2.id, match?.helper?.id)
        assertEquals(userInfo1.id, match?.helpRequestDTO?.requester?.id)
    }

}
