package com.ase.tugood.requestservice.application

import com.ase.tugood.requestservice.application.data.user.UserDTO
import com.ase.tugood.requestservice.application.data.karma.InternalKarmaChangeType
import com.ase.tugood.requestservice.application.data.karma.KarmaRequestDTO
import com.ase.tugood.requestservice.application.data.match.MatchStatus
import com.ase.tugood.requestservice.application.data.match.RatingDTO
import com.ase.tugood.requestservice.application.data.request.HelpRequestDTO
import com.ase.tugood.requestservice.domain.match.model.Match
import com.ase.tugood.requestservice.domain.request.model.HelpRequest
import com.ase.tugood.requestservice.infrastructure.configuration.RateConfiguration
import com.ase.tugood.requestservice.infrastructure.configuration.ScoringConfiguration
import com.ase.tugood.requestservice.infrastructure.persistence.HelpRequestRepository
import com.ase.tugood.requestservice.infrastructure.persistence.MatchRepository
import com.ase.tugood.requestservice.infrastructure.web.model.request.UserInfo
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.verifyAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import reactor.core.publisher.Mono
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@SpringBootTest
@ExtendWith(MockKExtension::class)
internal class RequesterMatchServiceTest {

    @Autowired
    private lateinit var requesterMatchService: RequesterMatchService

    @MockkBean
    private lateinit var mockUserService: UserService

    @MockkBean
    private lateinit var mockNotificationService: NotificationService

    @MockkBean
    private lateinit var mockRateConfiguration: RateConfiguration

    @MockkBean
    private lateinit var mockScoringConfiguration: ScoringConfiguration

    @MockkBean
    private lateinit var mockHelpRequestService: HelpRequestService

    @Autowired
    private lateinit var helpRequestRepository: HelpRequestRepository

    @Autowired
    private lateinit var matchRepository: MatchRepository

    @MockkBean
    private lateinit var mockKarmaService: KarmaService

    private val requesterDTO = UserDTO(id = "requesterId")
    private val helperDTO = UserDTO(id = "helperId", distance = 20)
    private val requesterInfo = UserInfo(id = "requesterId", email_verified = true)

    @BeforeEach
    internal fun setUp() {

        every {
            mockUserService.getUserForId(helperDTO.id)
        } answers {
            Mono.just(helperDTO)
        }

        every {
            mockUserService.getUserForId(requesterInfo.id)
        } answers {
            Mono.just(requesterDTO)
        }

        every {
            mockUserService.createRating(helperDTO.id, any())
        } answers {
            Mono.just(helperDTO)
        }

        helpRequestRepository.deleteAll().block()
        matchRepository.deleteAll().block()
    }

    @Test
    fun `finishMatch should be finished and rating stored`() {

        val helpRequest1 = HelpRequest(id = "request1")
        val helpRequestDTO1 = HelpRequestDTO(id = "request1")
        val match1 = Match(id = "match1", status = MatchStatus.FINISHED_BY_HELPER, helperId = helperDTO.id, requesterId = requesterDTO.id, helpRequestId = "request1")
        val ratingDTO = RatingDTO(3, "comment")

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequestDTO1.id!!, any())
        } answers {
            Mono.just(helpRequestDTO1)
        }

        every {
            mockKarmaService.createKarma(KarmaRequestDTO(requesterDTO.id, type = InternalKarmaChangeType.FINISH_REQUEST, ratingDTO = ratingDTO, helperId = helperDTO.id))
        } answers {
            Mono.just(emptyList())
        }

        helpRequestRepository.save(helpRequest1).block()
        matchRepository.save(match1).block()

        val resultMatch = requesterMatchService.finishMatch(match1.id!!, requesterInfo, ratingDTO = ratingDTO).block()

        kotlin.test.assertNotNull(resultMatch)
        kotlin.test.assertEquals("match1", resultMatch.id)
        kotlin.test.assertEquals(MatchStatus.FINISHED_BY_REQUESTER, resultMatch.status)
        kotlin.test.assertEquals(ratingDTO.rating, resultMatch.completionInfoDTO?.requesterRating?.rating)
        kotlin.test.assertEquals(ratingDTO.comment, resultMatch.completionInfoDTO?.requesterRating?.comment)

        val matchInDatabase = matchRepository.findById(match1.id!!).block()

        kotlin.test.assertEquals(MatchStatus.FINISHED_BY_REQUESTER, matchInDatabase?.status)
        kotlin.test.assertEquals(ratingDTO.rating, matchInDatabase?.completionInfo?.requesterRating?.rating)
        kotlin.test.assertEquals(ratingDTO.comment, matchInDatabase?.completionInfo?.requesterRating?.comment)
    }


    @Test
    fun `finishMatch should call karma and userService`() {

        val helpRequest1 = HelpRequest(id = "request1")
        val helpRequestDTO1 = HelpRequestDTO(id = "request1")
        val match1 = Match(id = "match1", status = MatchStatus.FINISHED_BY_HELPER, helperId = helperDTO.id, requesterId = requesterDTO.id, helpRequestId = "request1")
        val ratingDTO = RatingDTO(3, "comment")

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequestDTO1.id!!, any())
        } answers {
            Mono.just(helpRequestDTO1)
        }

        every {
            mockKarmaService.createKarma(KarmaRequestDTO(requesterDTO.id, type = InternalKarmaChangeType.FINISH_REQUEST, ratingDTO = ratingDTO, helperId = helperDTO.id))
        } answers {
            Mono.just(emptyList())
        }

        helpRequestRepository.save(helpRequest1).block()
        matchRepository.save(match1).block()

        requesterMatchService.finishMatch(match1.id!!, requesterInfo, ratingDTO = ratingDTO).block()

        verifyAll {
            mockUserService.getUserForId(requesterDTO.id)
            mockKarmaService.createKarma(KarmaRequestDTO(userId = requesterDTO.id, type = InternalKarmaChangeType.FINISH_REQUEST, ratingDTO = ratingDTO, helperId = helperDTO.id))
            mockUserService.createRating(userId = helperDTO.id, ratingDTO = ratingDTO)
        }

    }

    @Test
    fun `cancelMatch should be cancelled`() {

        val helpRequest1 = HelpRequest(id = "request1")
        val helpRequestDTO1 = HelpRequestDTO(id = "request1")
        val match1 = Match(id = "match1", status = MatchStatus.ACCEPTED, helperId = helperDTO.id, requesterId = requesterDTO.id, helpRequestId = "request1")
        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequestDTO1.id!!, any())
        } answers {
            Mono.just(helpRequestDTO1)
        }

        every {
            mockKarmaService.createKarma(KarmaRequestDTO(requesterDTO.id, type = InternalKarmaChangeType.CANCEL_REQUEST))
        } answers {
            Mono.just(emptyList())
        }

        helpRequestRepository.save(helpRequest1).block()
        matchRepository.save(match1).block()

        val resultMatch = requesterMatchService.cancelMatch(match1.id!!, requesterInfo).block()

        assertNotNull(resultMatch)
        assertEquals("match1", resultMatch.id)
        assertEquals(MatchStatus.CANCELLED_BY_REQUESTER, resultMatch.status)

        val matchInDatabase = matchRepository.findById(match1.id!!).block()

        assertEquals(MatchStatus.CANCELLED_BY_REQUESTER, matchInDatabase?.status)
    }

    @Test
    fun `cancelMatch should call karma service`() {

        val helpRequest1 = HelpRequest(id = "request1")
        val helpRequestDTO1 = HelpRequestDTO(id = "request1")
        val match1 = Match(id = "match1", status = MatchStatus.ACCEPTED, helperId = helperDTO.id, requesterId = requesterDTO.id, helpRequestId = "request1")

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequestDTO1.id!!, any())
        } answers {
            Mono.just(helpRequestDTO1)
        }

        every {
            mockKarmaService.createKarma(KarmaRequestDTO(requesterDTO.id, type = InternalKarmaChangeType.CANCEL_REQUEST))
        } answers {
            Mono.just(emptyList())
        }

        helpRequestRepository.save(helpRequest1).block()
        matchRepository.save(match1).block()

        requesterMatchService.cancelMatch(match1.id!!, requesterInfo).block()

        verifyAll {
            mockKarmaService.createKarma(KarmaRequestDTO(userId = requesterInfo.id, type = InternalKarmaChangeType.CANCEL_REQUEST))
        }
    }

    @Test
    fun `getFinishableMatches should return right values`() {

        val helpRequest1 = HelpRequest(id = "request1")
        val helpRequest2 = HelpRequest(id = "request2")
        val helpRequest3 = HelpRequest(id = "request3")
        val helpRequestDTO3 = HelpRequestDTO(id = "request3")
        val match1 = Match(id = "match1", status = MatchStatus.CANCELLED_BY_REQUESTER, helperId = helperDTO.id, requesterId = requesterDTO.id, helpRequestId = "request1")
        val match2 = Match(id = "match2", status = MatchStatus.FINISHED_BY_REQUESTER, helperId = helperDTO.id, requesterId = requesterDTO.id, helpRequestId = "request2")
        val match3 = Match(id = "match3", status = MatchStatus.FINISHED_BY_HELPER, helperId = helperDTO.id, requesterId = requesterDTO.id, helpRequestId = "request3")

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequest3.id!!, any())
        } answers {
            Mono.just(helpRequestDTO3)
        }

        helpRequestRepository.save(helpRequest1).block()
        helpRequestRepository.save(helpRequest2).block()
        helpRequestRepository.save(helpRequest3).block()
        matchRepository.save(match1).block()
        matchRepository.save(match2).block()
        matchRepository.save(match3).block()

        val result = requesterMatchService.getFinishableMatches(requesterInfo).collectList().block()

        assertNotNull(result)
        assertEquals(1, result.size)
        assertEquals(match3.id, result[0]?.id)
    }

    @Test
    fun `getHistoricMatches`() {
        val helpRequest1 = HelpRequest(id = "request1")
        val helpRequest2 = HelpRequest(id = "request2")
        val helpRequest3 = HelpRequest(id = "request3")
        val helpRequestDTO1 = HelpRequestDTO(id = "request1")
        val helpRequestDTO2 = HelpRequestDTO(id = "request2")
        val match1 = Match(id = "match1", status = MatchStatus.CANCELLED_BY_HELPER, helperId = helperDTO.id, requesterId = requesterDTO.id, helpRequestId = "request1")
        val match2 = Match(id = "match2", status = MatchStatus.FINISHED_BY_REQUESTER, helperId = helperDTO.id, requesterId = requesterDTO.id, helpRequestId = "request2")
        val match3 = Match(id = "match3", status = MatchStatus.FINISHED_BY_HELPER, helperId = helperDTO.id, requesterId = requesterDTO.id, helpRequestId = "request3")

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequest1.id!!, any())
        } answers {
            Mono.just(helpRequestDTO1)
        }

        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequest2.id!!, any())
        } answers {
            Mono.just(helpRequestDTO2)
        }

        helpRequestRepository.save(helpRequest1).block()
        helpRequestRepository.save(helpRequest2).block()
        helpRequestRepository.save(helpRequest3).block()
        matchRepository.save(match1).block()
        matchRepository.save(match2).block()
        matchRepository.save(match3).block()

        val result = requesterMatchService.getHistoricMatches(requesterInfo).collectList().block()

        assertNotNull(result)
        assertEquals(2, result.size)
        assertEquals(match1.id, result[0]?.id)
        assertEquals(match2.id, result[1]?.id)
    }

    @Test
    fun getActiveMatches() {

        val helpRequest1 = HelpRequest(id = "request1")
        val helpRequest2 = HelpRequest(id = "request2")
        val helpRequest3 = HelpRequest(id = "request3")
        val helpRequestDTO3 = HelpRequestDTO(id = "request3")
        val match1 = Match(id = "match1", status = MatchStatus.CANCELLED_BY_HELPER, helperId = requesterInfo.id, requesterId = requesterDTO.id, helpRequestId = "request1")
        val match2 = Match(id = "match2", status = MatchStatus.FINISHED_BY_REQUESTER, helperId = requesterInfo.id, requesterId = requesterDTO.id, helpRequestId = "request2")
        val match3 = Match(id = "match3", status = MatchStatus.FINISHED_BY_HELPER, helperId = requesterInfo.id, requesterId = requesterDTO.id, helpRequestId = "request3")


        every {
            mockHelpRequestService.getHelpRequestByIdAndSetRequester(helpRequest3.id!!, any())
        } answers {
            Mono.just(helpRequestDTO3)
        }

        helpRequestRepository.save(helpRequest1).block()
        helpRequestRepository.save(helpRequest2).block()
        helpRequestRepository.save(helpRequest3).block()
        matchRepository.save(match1).block()
        matchRepository.save(match2).block()
        matchRepository.save(match3).block()

        val result = requesterMatchService.getActiveMatches(requesterInfo).collectList().block()

        assertNotNull(result)
        assertEquals(1, result.size)
        assertEquals(match3.id, result[0]?.id)
    }

}
