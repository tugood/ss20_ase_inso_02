package com.ase.tugood.requestservice

import com.ase.tugood.requestservice.application.data.user.UserDTO
import com.ase.tugood.notificationservice.application.data.user.UserState
import java.time.Instant
import java.util.*

data class TestUserBuilder(
    var id: String? = null,
    var firstName: String? = null,
    var lastName: String? = null,
    var email: String? = null,
    var phoneNr: String? = null,
    var langKey: String? = null,
    var description: String? = null,
    var hasImage: Boolean = false,
    var userState: UserState? = UserState.ACTIVE,
    var avgRating: Double? = null,
    var createdDate: Instant? = Instant.now(),
    var lastModifiedDate: Instant? = Instant.now()
) {
    companion object {
        val defaultId: String = UUID.randomUUID().toString()
        val defaultTime: Instant = Instant.now()
        const val defaultFirstName = "Karl"
        const val defaultLastName = "Ludwig"
        const val defaultEmail = "karl@ludwig.at"
        const val defaultPhoneNr = "+4366543345"
        const val defaultLangKey = "en"
        const val defaultDescription = "I am a rich helper"
        const val defaultHasImage = false
        val defaultUserState: UserState = UserState.ACTIVE
        const val defaultRating = 4.1
        const val defaultDistance = 10000L
        val adminId: String = UUID.randomUUID().toString()
        const val adminFirstName = "Andi"
        const val adminLastName = "Admin"
        const val adminEmail = "admin@admin.at"
        const val adminPhoneNr = "+4366412312334"
        const val adminLangKey = "de"
        const val adminDescription = "I am the boss"
        const val adminHasImage = true
        val adminUserState: UserState = UserState.ACTIVE
        const val adminRating = 4.7
        const val adminDistance = 5000L
    }

    fun validDefaultUserDTO() = UserDTO(defaultId,
        defaultFirstName,
        defaultLastName,
        defaultEmail,
        defaultPhoneNr,
        defaultLangKey,
        defaultDescription,
        defaultHasImage,
        defaultUserState,
        defaultDistance,
        defaultRating,
        defaultTime)

    fun validAdminUserDTO() = UserDTO(adminId,
        adminFirstName,
        adminLastName,
        adminEmail,
        adminPhoneNr,
        adminLangKey,
        adminDescription,
        adminHasImage,
        adminUserState,
        adminDistance,
        adminRating,
        defaultTime,
        defaultTime)
}
