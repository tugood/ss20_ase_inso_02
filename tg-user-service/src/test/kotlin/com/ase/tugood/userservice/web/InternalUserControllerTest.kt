package com.ase.tugood.userservice.web

import com.ase.tugood.userservice.TestUserBuilder
import com.ase.tugood.userservice.application.data.RatingDTO
import com.ase.tugood.userservice.application.data.UserDTO
import com.ase.tugood.userservice.application.data.UserState
import com.ase.tugood.userservice.domain.user.model.User
import com.ase.tugood.userservice.domain.user.service.mapper.UserMapper
import com.ase.tugood.userservice.infrastructure.configuration.MailConfiguration
import com.ase.tugood.userservice.infrastructure.persistence.UserRepository
import com.ase.tugood.userservice.infrastructure.web.model.request.UserInfo
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.keycloak.admin.client.Keycloak
import org.keycloak.admin.client.resource.RealmResource
import org.keycloak.admin.client.resource.UserResource
import org.keycloak.admin.client.resource.UsersResource
import org.keycloak.representations.idm.UserRepresentation
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Mono
import java.util.*
import kotlin.test.assertNotNull

@AutoConfigureWebTestClient
@SpringBootTest
internal class InternalUserControllerTest {

    @Autowired
    private lateinit var userRepository: UserRepository

    @Autowired
    private lateinit var webTestClient: WebTestClient

    @Autowired
    private lateinit var userMapper: UserMapper

    @MockBean
    private lateinit var keycloak: Keycloak;

    @MockBean
    private lateinit var realmResource: RealmResource

    @MockBean
    private lateinit var usersResource: UsersResource

    @MockBean
    private lateinit var userResource: UserResource

    private lateinit var userRepresentation: UserRepresentation

    @MockBean
    private lateinit var mailConfiguration: MailConfiguration

    private lateinit var userDTO: UserDTO
    private lateinit var user: User
    private lateinit var userInfo: UserInfo
    private lateinit var ratingDTO: RatingDTO

    @BeforeEach
    fun initTest() {
        userRepository.deleteAll().block()
        userInfo = TestUserBuilder().validUserInfo()
        userDTO = TestUserBuilder().validDefaultUserDTO()
        userDTO.id = userInfo.id
        user = TestUserBuilder().validDefaultUserEntity()
        user.id = userInfo.id
        ratingDTO = RatingDTO(3)

        userRepresentation = UserRepresentation()
        userRepresentation.firstName = userDTO.firstName
        userRepresentation.lastName = userDTO.lastName
        userRepresentation.email = userDTO.email
        userRepresentation.username = userDTO.email
        userRepresentation.singleAttribute("locale", userDTO.langKey)

        Mockito.`when`(keycloak.realm("tugood")).thenReturn(realmResource)
        Mockito.`when`(realmResource.users()).thenReturn(usersResource)
        Mockito.`when`(usersResource.get(userDTO.id)).thenReturn(userResource)
        Mockito.`when`(userResource.toRepresentation()).thenReturn(userRepresentation)
    }


    @Test
    fun getUserById() {
        user.ratingCount = 0
        user.ratingSum = 0
        userRepository.save(user).block()
        webTestClient.get().uri("/internal/users/accounts/${user.id}")
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("firstName").isEqualTo(user.firstName!!)
            .jsonPath("lastName").isEqualTo(user.lastName!!)
            .jsonPath("email").isEqualTo(user.email!!)
            .jsonPath("phoneNr").isEqualTo(user.phoneNr!!)
            .jsonPath("langKey").isEqualTo(user.langKey!!)
            .jsonPath("description").isEqualTo(user.description!!)
            .jsonPath("distance").isEqualTo(user.distance!!)
            .jsonPath("hasImage").isEqualTo(user.hasImage)
            .jsonPath("avgRating").doesNotExist()
            .jsonPath("userState").isEqualTo(user.userState.toString())
    }

    @Test
    fun createRating() {
        userRepository.save(user).block()

        assertEquals(4.1, userMapper.toDto(user).avgRating)
        webTestClient.post().uri("/internal/users/ratings/${user.id}")
            .body(Mono.just(ratingDTO), RatingDTO::class.java)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("firstName").isEqualTo(user.firstName!!)
            .jsonPath("lastName").isEqualTo(user.lastName!!)
            .jsonPath("email").isEqualTo(user.email!!)
            .jsonPath("phoneNr").isEqualTo(user.phoneNr!!)
            .jsonPath("langKey").isEqualTo(user.langKey!!)
            .jsonPath("description").isEqualTo(user.description!!)
            .jsonPath("distance").isEqualTo(user.distance!!)
            .jsonPath("hasImage").isEqualTo(user.hasImage)
            .jsonPath("avgRating").isEqualTo(4)
            .jsonPath("userState").isEqualTo(user.userState.toString())

        val storedUser = userRepository.findById(user.id!!).block()

        assertEquals(11, storedUser?.ratingCount)
        assertEquals(44, storedUser?.ratingSum)

    }

    @Test
    fun `GET get user by id without user in database`() {
        webTestClient.get().uri("/internal/users/accounts/${userInfo.id}")
            .header("X-Userinfo", Base64.getEncoder().encodeToString(jacksonObjectMapper().writeValueAsBytes(userInfo)))
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("firstName").isEqualTo(userInfo.given_name!!)
            .jsonPath("lastName").isEqualTo(userInfo.family_name!!)
            .jsonPath("email").isEqualTo(userInfo.email!!)
            .jsonPath("id").isEqualTo(userInfo.id)
            .jsonPath("userState").isEqualTo(UserState.ACTIVE.toString())
            .jsonPath("langKey").isEqualTo(userInfo.locale!!)

        val user = userRepository.findById(userInfo.id).block()
        assertNotNull(user)
    }

}
