package com.ase.tugood.userservice.web

import com.ase.tugood.userservice.TestUserReportBuilder
import com.ase.tugood.userservice.application.NotificationService
import com.ase.tugood.userservice.application.UserService
import com.ase.tugood.userservice.application.data.UserDTO
import com.ase.tugood.userservice.application.data.UserReportDTO
import com.ase.tugood.userservice.application.data.notification.ServiceNotificationDTO
import com.ase.tugood.userservice.domain.userreport.model.UserReport
import com.ase.tugood.userservice.infrastructure.configuration.MailConfiguration
import com.ase.tugood.userservice.infrastructure.persistence.UserReportRepository
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Mono
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@AutoConfigureWebTestClient
@SpringBootTest
class UserReportResourceTest {
    @Autowired
    private lateinit var userReportRepository: UserReportRepository

    @MockkBean
    private lateinit var userService: UserService

    @MockkBean
    private lateinit var notificationService: NotificationService

    @Autowired
    private lateinit var webTestClient: WebTestClient

    @MockkBean
    private lateinit var mailConfiguration: MailConfiguration

    private lateinit var userReportDTO: UserReportDTO
    private lateinit var userReportEntity: UserReport
    private lateinit var userReportEntity2: UserReport
    private lateinit var userReportUserDTO: UserDTO

    @BeforeEach
    fun initTest() {
        userReportRepository.deleteAll().block()
        userReportDTO = TestUserReportBuilder().validDefaultUserReportDTO()
        userReportEntity = TestUserReportBuilder().validDefaultUserReport()
        userReportEntity2 = TestUserReportBuilder().validDefaultUserReport2()
        userReportUserDTO = TestUserReportBuilder().validDefaultUserReportUser()

        every {
            userService.getUserById(userReportDTO.userId)
        } answers {
            Mono.just(userReportUserDTO)
        }

        every {
            notificationService.notify(userReportDTO.userId, "notification.report.user.title",
                "notification.report.user.body", ServiceNotificationDTO.NotificationType.BOTH)
        } answers {
            Mono.empty()
        }
    }

    @Test
    fun `POST successfully create user report`() {
        val result = webTestClient.post().uri("/api/users/report")
            .contentType(MediaType.APPLICATION_JSON)
            .body(Mono.just(userReportDTO), UserReportDTO::class.java)
            .exchange()
            .expectStatus().isCreated
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody(UserReportDTO::class.java)
            .returnResult()
            .responseBody

        assertNotNull(result)
        assertEquals(userReportDTO.userId, result.userId)
    }

    @Test
    fun `GET successfully get all user reports by user id`() {
        userReportRepository.save(userReportEntity).block()
        userReportRepository.save(userReportEntity2).block()

        val results = webTestClient.get().uri("/api/users/report/" + userReportEntity.userId)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBodyList(UserReportDTO::class.java)
            .returnResult()
            .responseBody

        assertNotNull(results)
        assertEquals(2, results.size)
    }
}
