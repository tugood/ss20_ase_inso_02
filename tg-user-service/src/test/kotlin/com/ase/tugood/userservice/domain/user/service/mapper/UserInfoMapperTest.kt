package com.ase.tugood.userservice.domain.user.service.mapper

import com.ase.tugood.userservice.TestUserBuilder
import com.ase.tugood.userservice.application.data.UserState
import com.ase.tugood.userservice.infrastructure.web.model.request.UserInfo
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class UserInfoMapperTest {

    private var userInfoMapper = UserInfoMapper()
    private lateinit var userInfo: UserInfo

    @BeforeEach
    fun initTest() {
        userInfo = TestUserBuilder().validUserInfo()
    }

    @Test
    fun `test userInfo to dto`() {
        val userDto = userInfoMapper.toDto(userInfo)

        assertNotNull(userDto)
        assertEquals(userInfo.email, userDto.email)
        assertEquals(userInfo.family_name, userDto.lastName)
        assertEquals(userInfo.given_name, userDto.firstName)
        assertEquals(userInfo.id, userDto.id)
        assertEquals(userInfo.locale, userDto.langKey)
        assertEquals(UserState.ACTIVE, userDto.userState)
    }
}
