package com.ase.tugood.userservice.web

import com.ase.tugood.userservice.TestUserBuilder
import com.ase.tugood.userservice.application.UserService
import com.ase.tugood.userservice.infrastructure.configuration.MailConfiguration
import com.ase.tugood.userservice.infrastructure.web.model.request.TestUserDTO
import com.ase.tugood.userservice.infrastructure.web.model.request.UserInfo
import com.ase.tugood.userservice.infrastructure.web.rest.error.UserNotFoundException
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.unmockkAll
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Mono
import kotlin.test.assertEquals
import kotlin.test.assertTrue

@AutoConfigureWebTestClient
@SpringBootTest
@ActiveProfiles("generatedata")
@ExtendWith(MockKExtension::class)
internal class TestDataResourceTest {


    @Autowired
    private lateinit var webTestClient: WebTestClient

    @MockkBean
    private lateinit var userService: UserService

    @MockkBean
    private lateinit var mailConfiguration: MailConfiguration

    private lateinit var testUser: TestUserDTO

    @BeforeEach
    fun setUp() {
        val userBuilder = TestUserBuilder()
        val user = userBuilder.validDefaultUserDTO()
        val userInfo = userBuilder.validUserInfo()

        testUser = TestUserDTO(user, userInfo)
    }

    @AfterEach
    fun tearDown() {
        unmockkAll()
    }

    @Test
    fun `POST user account returns empty for existing user`() {
        every { userService.getUserById(any()) } answers { Mono.just(testUser.userDTO) }

        val result = this.webTestClient.post().uri("/generate/users/accounts")
            .body(Mono.just(listOf(testUser)), TestUserDTO::class.java)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody(String::class.java)
            .returnResult()
            .responseBody

        assertEquals("[]", result)
    }

    @Test
    fun `POST user account returns created id for new user`() {
        every { userService.getUserById(any()) } answers { Mono.error { UserNotFoundException("fake-id") } }
        every { userService.updateUser(any<UserInfo>(), any()) } answers { Mono.just(testUser.userDTO) }

        val result = this.webTestClient.post().uri("/generate/users/accounts")
            .body(Mono.just(listOf(testUser)), TestUserDTO::class.java)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody(String::class.java)
            .returnResult()
            .responseBody

        assertTrue(result?.contains(testUser.userDTO.id!!) ?: false)
    }

    @Test
    fun `POST user account returns undefined for existing user without id`() {
        every { userService.getUserById(any()) } answers { Mono.error { UserNotFoundException("fake-id") } }
        every { userService.updateUser(any<UserInfo>(), any()) } answers {
            Mono.just(testUser.userDTO.apply { id = null })
        }

        val result = this.webTestClient.post().uri("/generate/users/accounts")
            .body(Mono.just(listOf(testUser)), TestUserDTO::class.java)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody(String::class.java)
            .returnResult()
            .responseBody

        assertTrue(result?.contains("undefined") ?: false)
    }

}
