package com.ase.tugood.userservice.web

import com.ase.tugood.userservice.TestUserBuilder
import com.ase.tugood.userservice.TestUserReportBuilder
import com.ase.tugood.userservice.application.MailService
import com.ase.tugood.userservice.application.NotificationService
import com.ase.tugood.userservice.application.UserService
import com.ase.tugood.userservice.application.data.UserDTO
import com.ase.tugood.userservice.application.data.notification.ServiceNotificationDTO
import com.ase.tugood.userservice.domain.userreport.model.UserReport
import com.ase.tugood.userservice.infrastructure.configuration.MailConfiguration
import com.ase.tugood.userservice.infrastructure.persistence.UserReportRepository
import com.ase.tugood.userservice.infrastructure.web.model.request.UserInfo
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Mono
import java.util.*

@AutoConfigureWebTestClient
@SpringBootTest
class AbuseManagementResourceTest {
    @Autowired
    private lateinit var userReportRepository: UserReportRepository

    @Autowired
    private lateinit var webTestClient: WebTestClient

    @MockkBean
    private lateinit var mailConfiguration: MailConfiguration

    @MockkBean
    private lateinit var notificationService: NotificationService

    @MockkBean
    private lateinit var mailService: MailService

    @MockkBean
    private lateinit var userService: UserService

    private lateinit var adminUserInfo: UserInfo
    private lateinit var adminUserInfoB64: String
    private lateinit var userInfo: UserInfo
    private lateinit var userInfoB64: String
    private lateinit var userReport: UserReport
    private lateinit var userReport2: UserReport
    private lateinit var user: UserDTO

    @BeforeEach
    fun initTest() {
        userReportRepository.deleteAll().block()
        adminUserInfo = TestUserBuilder().validAdminUserInfo()
        adminUserInfoB64 = Base64.getEncoder().encodeToString(jacksonObjectMapper().writeValueAsBytes(adminUserInfo))
        userInfo = TestUserBuilder().validUserInfo()
        userInfoB64 = Base64.getEncoder().encodeToString(jacksonObjectMapper().writeValueAsBytes(userInfo))
        userReport = TestUserReportBuilder().validDefaultUserReport()
        userReport2 = TestUserReportBuilder().validDefaultUserReport2()
        user = TestUserBuilder().validDefaultUserDTO()
    }

    @Test
    fun `GET user reports as admin`() {
        userReportRepository.save(userReport).block()

        every {
            userService.getUserById(userReport.userId)
        } answers {
            Mono.just(user)
        }

        this.webTestClient.get().uri("/api/users/abuse/reports?page=0&size=4")
            .header("X-Userinfo", adminUserInfoB64)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$[0].id").isEqualTo(userReport.id!!)
            .jsonPath("$[0].userId").isEqualTo(userReport.userId)
            .jsonPath("$[0].cause").isEqualTo(userReport.cause.toString())
            .jsonPath("$[0].description").isEqualTo(userReport.description)

    }

    @Test
    fun `PUT block user successfully`() {
        userReport.userId = user.id!!
        userReportRepository.save(userReport).block()

        every {
            userService.blockUserById(user.id!!, true)
        } answers {
            Mono.just(user)
        }

        every {
            mailService.sendMail(user, any())
        } answers {
            Unit
        }

        this.webTestClient.put().uri("/api/users/abuse/block/" + userReport.id)
            .header("X-Userinfo", adminUserInfoB64)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("id").isEqualTo(user.id!!)
    }

    @Test
    fun `PUT warn user successfully`() {
        userReport.userId = user.id!!
        userReportRepository.save(userReport).block()

        every {
            userService.warnUserById(user.id!!, true)
        } answers {
            Mono.just(user)
        }

        every {
            notificationService.notify(user.id!!,
                Pair("notification.warning.title", null),
                any(),
                ServiceNotificationDTO.NotificationType.PUSH)
        } answers {
            Mono.empty()
        }

        this.webTestClient.put().uri("/api/users/abuse/warn/" + userReport.id)
            .header("X-Userinfo", adminUserInfoB64)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("id").isEqualTo(user.id!!)
    }

    @Test
    fun `DELETE user report`() {
        userReportRepository.save(userReport).block()

        this.webTestClient.delete().uri("/api/users/abuse/reports/" + userReport.id)
            .header("X-Userinfo", adminUserInfoB64)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("id").isEqualTo(userReport.id!!)
    }
}
