package com.ase.tugood.userservice

import com.ase.tugood.userservice.application.data.notification.ServiceNotificationDTO

class TestServiceNotificationBuilder() {

    private val defaultUserDTO = TestUserBuilder().validDefaultUserDTO()

    private val defaultTitle: String = "He's not the Messiah - he's a very naughty boy."
    private val defaultBadge: String = "My special badge"
    private val defaultBody: String = "Deeds will not be less valiant because they are unpraised."
    private val defaultIcon: String = "/path/to/icon"
    private val defaultUserId: String = defaultUserDTO.id!!
    private val defaultUserEmail: String? = defaultUserDTO.email
    private val defaultTag: String = "some lovely tag"
    private val defaultNotificationType: ServiceNotificationDTO.NotificationType = ServiceNotificationDTO.NotificationType.PLAIN_OLD


    fun getDefaultServiceNotificationDTO() = ServiceNotificationDTO().apply {
        title = defaultTitle
        badge = defaultBadge
        body = defaultBody
        icon = defaultIcon
        userId = defaultUserId
        userEmail = defaultUserEmail
        tag = defaultTag
        notificationType = defaultNotificationType
    }

}
