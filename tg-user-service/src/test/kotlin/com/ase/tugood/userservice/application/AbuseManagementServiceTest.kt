package com.ase.tugood.userservice.application

import com.ase.tugood.userservice.TestUserBuilder
import com.ase.tugood.userservice.application.data.UserReportCause
import com.ase.tugood.userservice.domain.userreport.model.UserReport
import com.ase.tugood.userservice.infrastructure.configuration.MailConfiguration
import com.ase.tugood.userservice.infrastructure.persistence.UserReportRepository
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.PageRequest
import reactor.core.publisher.Mono
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

@SpringBootTest
@ExtendWith(MockKExtension::class)
internal class AbuseManagementServiceTest {

    @Autowired
    private lateinit var abuseManagementService: AbuseManagementService

    @MockkBean
    private lateinit var mockUserService: UserService

    @MockBean
    private lateinit var mockMailService: MailService

    @MockBean
    private lateinit var mailConfiguration: MailConfiguration

    @MockkBean
    private lateinit var mockNotificationService: NotificationService

    @Autowired
    private lateinit var userReportRepository: UserReportRepository

    @BeforeEach
    fun setUp() {
        every {
            mockUserService.getUserById(any())
        } answers {
            Mono.just(TestUserBuilder().validDefaultUserDTO())
        }

        val report1 = UserReport(id = "report1",
            userId = "user1",
            description = "sample",
            createdDate = Instant.EPOCH,
            cause = UserReportCause.ABUSIVE_BEHAVIOUR)

        val report2 = UserReport(id = "report2",
            userId = "user1",
            description = "sample",
            createdDate = Instant.EPOCH.plusMillis(1000),
            cause = UserReportCause.ABUSIVE_BEHAVIOUR)

        val report3 = UserReport(id = "report3",
            userId = "user2",
            description = "sample",
            createdDate = Instant.EPOCH.plusMillis(1000),
            cause = UserReportCause.ABUSIVE_BEHAVIOUR)

        userReportRepository.save(report1).block()
        userReportRepository.save(report2).block()
        userReportRepository.save(report3).block()
    }

    @AfterEach
    fun tearDown() {
        userReportRepository.deleteAll()
    }

    @Test
    fun `test should only return first page of user reports`() {
        val firstPage = PageRequest.of(0, 1)
        val reports = abuseManagementService.getOrderedUserReportPage(firstPage).collectList().block()

        assertNotNull(reports)
        assertFalse(reports.isEmpty())
        assertTrue(reports.size == 1)
        assertTrue(reports.stream().anyMatch {
            it.id == "report1"
        })
    }

    @Test
    fun `test should count reports`() {
        val count = abuseManagementService.countReports().block()
        assertEquals(3, count)
    }

    @Test
    fun `test should count reports per user`() {
        val count = abuseManagementService.countReportsByUser("user1").block()
        assertEquals(2, count)
    }

    @Test
    fun `test should warn user`() {
        var wasWarned = false
        var wasNotified = false

        every {
            mockUserService.warnUserById("user1", true)
        } answers {
            wasWarned = true
            Mono.just(TestUserBuilder().validDefaultUserDTO().apply { id = "user1" })
        }

        every {
            mockNotificationService.notify(any(), Pair("notification.warning.title", null), any(), any())
        } answers {
            wasNotified = true
            Mono.empty()
        }

        val user = abuseManagementService.warnReportedUser("report1").block()

        assertEquals("user1", user?.id)
        assertTrue(wasNotified)
        assertTrue(wasWarned)
    }

    @Test
    fun `test should ban user`() {
        var wasBanned = false

        every {
            mockUserService.blockUserById("user1", true)
        } answers {
            wasBanned = true
            Mono.just(TestUserBuilder().validDefaultUserDTO().apply { id = "user1" })
        }

        val user = abuseManagementService.blockReportedUser("report1").block()

        assertEquals("user1", user?.id)
        assertTrue(wasBanned)
    }

    @Test
    fun `test should delete reports`() {
        val firstPage = PageRequest.of(0, 1)
        val report = abuseManagementService.deleteReport("report1").block()
        val reports = abuseManagementService.getOrderedUserReportPage(firstPage).collectList().block()

        assertNotNull(reports)
        assertFalse(reports.isEmpty())
        assertTrue(reports.size == 1)
        assertTrue(reports.stream().noneMatch {
            it.id == "report1"
        })
        assertEquals("report1", report?.id)
    }
}
