package com.ase.tugood.userservice.configuration

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import org.springframework.boot.test.util.TestPropertyValues
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ApplicationEvent
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.event.ContextClosedEvent

class WireMockInitializer : ApplicationContextInitializer<ConfigurableApplicationContext> {

    override fun initialize(configurableApplicationContext: ConfigurableApplicationContext) {
        val wireMockServer = WireMockServer(WireMockConfiguration().dynamicPort())
        wireMockServer.start()
        configurableApplicationContext.beanFactory.registerSingleton("wireMockServer", wireMockServer)
        configurableApplicationContext.addApplicationListener { applicationEvent: ApplicationEvent? ->
            if (applicationEvent is ContextClosedEvent) {
                wireMockServer.stop()
            }
        }
        TestPropertyValues
            .of("tg-notification-service=http://localhost:" + wireMockServer.port(),
                "tg-user-service=http://localhost:" + wireMockServer.port())
            .applyTo(configurableApplicationContext)
    }
}
