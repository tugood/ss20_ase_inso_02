package com.ase.tugood.userservice.web

import com.ase.tugood.userservice.TestUserBuilder
import com.ase.tugood.userservice.application.ImageUploadService
import com.ase.tugood.userservice.application.data.UserDTO
import com.ase.tugood.userservice.infrastructure.configuration.MailConfiguration
import com.ase.tugood.userservice.infrastructure.web.model.request.UserInfo
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.keycloak.admin.client.Keycloak
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.core.io.ClassPathResource
import org.springframework.http.MediaType
import org.springframework.http.client.MultipartBodyBuilder
import org.springframework.test.web.reactive.server.WebTestClient
import java.util.*
import kotlin.test.assertNotNull

@AutoConfigureWebTestClient
@SpringBootTest
class ImageUploadResourceTest {
    private val LOG: Logger = LoggerFactory.getLogger(javaClass)

    @Autowired
    private lateinit var imageUploadService: ImageUploadService

    @MockBean
    private lateinit var keycloak: Keycloak

    @MockBean
    private lateinit var mailConfiguration: MailConfiguration

    @Autowired
    private lateinit var webclient: WebTestClient

    private lateinit var userDTO: UserDTO
    private lateinit var userInfo: UserInfo

    @BeforeEach
    fun initTest() {
        userDTO = TestUserBuilder().validDefaultUserDTO()
        userInfo = TestUserBuilder().validUserInfo()
    }

    @Test
    fun `upload and get image test`() {
        val bodyBuilder = MultipartBodyBuilder()

        LOG.info("X-Userinf: " + Base64.getEncoder().encodeToString(jacksonObjectMapper().writeValueAsBytes(userInfo)))

        bodyBuilder.part("fileParts", ClassPathResource("/test_image.jpg", ImageUploadResourceTest::class.java))

        val result = webclient.put().uri("/api/users/accounts/self/image")
            .contentType(MediaType.MULTIPART_FORM_DATA)
            .header("X-Userinfo", Base64.getEncoder().encodeToString(jacksonObjectMapper().writeValueAsBytes(userInfo)))
            .bodyValue(bodyBuilder.build())
            .exchange()
            .expectStatus().isOk
            .expectBody().returnResult().responseBody

        assertNotNull(result)
        LOG.info("ObjectId: " + String(result))

        webclient.get().uri("/api/users/image/" + userInfo.id)
            .exchange()
            .expectStatus().isOk
    }

}
