package com.ase.tugood.userservice.service

import com.ase.tugood.userservice.TestUserBuilder
import com.ase.tugood.userservice.application.data.UserDTO
import com.ase.tugood.userservice.application.data.UserState
import com.ase.tugood.userservice.domain.user.model.User
import com.ase.tugood.userservice.domain.user.service.mapper.UserMapper
import com.ase.tugood.userservice.infrastructure.configuration.MailConfiguration
import com.ase.tugood.userservice.infrastructure.web.model.request.UserInfo
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.keycloak.admin.client.Keycloak
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@SpringBootTest
class UserMapperTest {

    @Autowired
    private lateinit var userMapper: UserMapper

    @MockBean
    private lateinit var keycloak: Keycloak;

    private lateinit var user: User

    private lateinit var userDTO: UserDTO

    private lateinit var userInfo: UserInfo

    @MockBean
    private lateinit var mailConfiguration: MailConfiguration

    @BeforeEach
    fun initTest() {
        user = TestUserBuilder().validAdminUserEntity()
        userDTO = TestUserBuilder().validDefaultUserDTO()
        userInfo = TestUserBuilder().validUserInfo()
    }

    @Test
    fun `test dto to entity`() {
        val userEntityMapped = userMapper.toEntity(userDTO, userInfo)

        assertNotNull(userEntityMapped)
        assertEquals(userInfo.id, userEntityMapped.id)
        assertEquals(userInfo.email, userEntityMapped.email)
        assertEquals(userDTO.firstName, userEntityMapped.firstName)
        assertEquals(userDTO.lastName, userEntityMapped.lastName)
        assertEquals(userDTO.phoneNr, userEntityMapped.phoneNr)
        assertEquals(userDTO.description, userEntityMapped.description)
        assertEquals(userDTO.distance, userEntityMapped.distance)
        assertEquals(userDTO.hasImage, userEntityMapped.hasImage)
        assertEquals(userDTO.langKey, userEntityMapped.langKey)
        assertEquals(UserState.ACTIVE, userEntityMapped.userState)
    }

    @Test
    fun `test merge dto in entity`() {
        val userEntityMapped = userMapper.merge(user, userDTO)

        assertNotNull(userEntityMapped)
        assertEquals(user.id, userEntityMapped.id)
        assertEquals(user.email, userEntityMapped.email)
        assertEquals(userDTO.firstName, userEntityMapped.firstName)
        assertEquals(userDTO.lastName, userEntityMapped.lastName)
        assertEquals(userDTO.phoneNr, userEntityMapped.phoneNr)
        assertEquals(userDTO.description, userEntityMapped.description)
        assertEquals(userDTO.distance, userEntityMapped.distance)
        assertEquals(userDTO.hasImage, userEntityMapped.hasImage)
        assertEquals(userDTO.langKey, userEntityMapped.langKey)
        assertEquals(UserState.ACTIVE, userEntityMapped.userState)
        assertEquals(user.createdDate, userEntityMapped.createdDate)
    }

    @Test
    fun `test entity to dto`() {
        val userDTOMapped = userMapper.toDto(user)

        assertNotNull(userDTOMapped)
        assertEquals(user.id, userDTOMapped.id)
        assertEquals(user.firstName, userDTOMapped.firstName)
        assertEquals(user.lastName, userDTOMapped.lastName)
        assertEquals(user.email, userDTOMapped.email)
        assertEquals(user.phoneNr, userDTOMapped.phoneNr)
        assertEquals(user.description, userDTOMapped.description)
        assertEquals(user.distance, userDTOMapped.distance)
        assertEquals(user.hasImage, userDTOMapped.hasImage)
        assertEquals(user.langKey, userDTOMapped.langKey)
        assertEquals(user.userState, userDTOMapped.userState)
        assertEquals(user.createdDate, userDTOMapped.createdDate)
        assertEquals(user.lastModifiedDate, userDTOMapped.lastModifiedDate)
        assertEquals(4.5, userDTOMapped.avgRating)
    }

    @Test
    fun `test list of entities to list of dtos`() {
        val userEntityList = listOf(user)
        val userDTOListMapped = userMapper.toDto(userEntityList)

        assertThat(userDTOListMapped).isNotEmpty
        assertThat(userDTOListMapped).size().isEqualTo(1)
    }

}
