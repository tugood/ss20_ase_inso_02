package com.ase.tugood.userservice.application

import com.ase.tugood.userservice.TestUserBuilder
import com.ase.tugood.userservice.application.data.UserState
import com.ase.tugood.userservice.infrastructure.configuration.MailConfiguration
import com.ase.tugood.userservice.infrastructure.persistence.UserRepository
import com.ase.tugood.userservice.infrastructure.web.rest.error.UserNotFoundException
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.spyk
import io.mockk.unmockkAll
import io.mockk.verify
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.extension.ExtendWith
import org.keycloak.admin.client.Keycloak
import org.keycloak.admin.client.resource.RealmResource
import org.keycloak.admin.client.resource.UserResource
import org.keycloak.admin.client.resource.UsersResource
import org.keycloak.representations.idm.UserRepresentation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import kotlin.test.*

@SpringBootTest
@ExtendWith(MockKExtension::class)
internal class UserServiceTest {

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var userRepository: UserRepository

    @MockkBean
    private lateinit var mockKeycloak: Keycloak

    @MockkBean
    private lateinit var mailConfiguration: MailConfiguration

    @MockK
    private lateinit var mockRealmResource: RealmResource

    @MockK
    private lateinit var mockUsersResource: UsersResource

    @MockK(relaxUnitFun = true)
    private lateinit var mockUserResource: UserResource

    private lateinit var mockUserRepresentation: UserRepresentation

    companion object {
        private const val NON_EXISTENT = "non-existent"
    }

    @BeforeEach
    fun setUp() {
        mockUserRepresentation = spyk(UserRepresentation())
        every { mockKeycloak.realm(any()) } answers { mockRealmResource }
        every { mockRealmResource.users() } answers { mockUsersResource }
        every { mockUsersResource.get(any()) } answers { mockUserResource }
        every { mockUserResource.toRepresentation() } answers { mockUserRepresentation }
    }

    @AfterEach
    fun tearDown() {
        userRepository.deleteAll()
        unmockkAll()
    }

    @Test
    fun `test updating user sets fields in keycloak`() {
        val testUser = TestUserBuilder().validDefaultUserEntity()
        userRepository.save(testUser).block()

        val testDto = TestUserBuilder().validDefaultUserDTO()

        val updated = userService.updateUser(testUser.id!!, testDto).block()

        verify(exactly = 1) { mockUserResource.update(any()) }
        assertEquals(testUser.firstName, mockUserRepresentation.firstName)
        assertEquals(testUser.lastName, mockUserRepresentation.lastName)
        assertEquals(testUser.email, mockUserRepresentation.username)
        assertEquals(testUser.email, mockUserRepresentation.email)
        assertTrue(testUser.langKey!! in mockUserRepresentation.attributes["locale"]!!)
        assertNotEquals(updated?.createdDate, updated?.lastModifiedDate)
    }

    @Test
    fun `negative test update non existing user`() {
        assertThrows<UserNotFoundException> {
            userService.updateUser(NON_EXISTENT, TestUserBuilder().validDefaultUserDTO()).block()
        }
    }

    @Test
    fun `test deletion of user`() {
        val testUser = TestUserBuilder().validDefaultUserEntity()
        userRepository.save(testUser).block()

        userService.deleteUserById(testUser.id!!).block()

        val foundUser = userRepository.findById(testUser.id!!).block()
        assertNull(foundUser)
    }

    @Test
    fun `negative test deletion of user`() {
        assertThrows<UserNotFoundException> {
            userService.deleteUserById(NON_EXISTENT).block()
        }
    }

    @Test
    fun `test user is blocked in keycloak and db`() {
        val testUser = TestUserBuilder().validDefaultUserEntity()
        userRepository.save(testUser).block()

        val blocked = userService.blockUserById(testUser.id!!, true).block()

        verify(exactly = 1) { mockUserResource.update(any()) }
        assertFalse(mockUserRepresentation.isEnabled)
        assertEquals(UserState.BLOCKED, blocked?.userState)
        assertNotEquals(blocked?.createdDate, blocked?.lastModifiedDate)
    }

    @Test
    fun `negative test blocking of user`() {
        assertThrows<UserNotFoundException> {
            userService.blockUserById(NON_EXISTENT, true).block()
        }
    }

    @Test
    fun `test user warning`() {
        val testUser = TestUserBuilder().validDefaultUserEntity()
        userRepository.save(testUser).block()

        val warned = userService.warnUserById(testUser.id!!, true).block()

        assertTrue(warned?.isWarned ?: false)
        assertNotEquals(warned?.createdDate, warned?.lastModifiedDate)
    }

    @Test
    fun `negative test warning of user`() {
        assertThrows<UserNotFoundException> {
            userService.warnUserById(NON_EXISTENT, true).block()
        }
    }

}
