package com.ase.tugood.userservice

import com.ase.tugood.userservice.application.data.UserDTO
import com.ase.tugood.userservice.application.data.UserReportCause
import com.ase.tugood.userservice.application.data.UserReportDTO
import com.ase.tugood.userservice.domain.userreport.model.UserReport

data class TestUserReportBuilder(
    var id: String? = null,
    var userId: String? = null,
    var cause: UserReportCause? = null,
    var description: String? = null
) {
    companion object {
        val defaultId = "random_user_report_id"
        val defaultUserId = "random_user_id"
        val defaultCause = UserReportCause.DISCRIMINATION
        val defaultDescription = "Don't be so rude to me!"
        val defaultUserId2 = "random_user_id2"
        val defaultCause2 = UserReportCause.ILLEGAL_ACTIVITY
        val defaultDescription2 = "He tries to sell pot!"
    }

    fun validDefaultUserReportDTO() = UserReportDTO(userId = defaultUserId, cause = defaultCause, description = defaultDescription)
    fun validDefaultUserReport() = UserReport(userId = defaultUserId, cause = defaultCause, description = defaultDescription)
    fun validDefaultUserReport2() = UserReport(userId = defaultUserId, cause = defaultCause2, description = defaultDescription2)
    fun validDefaultUserReportUser() = UserDTO(defaultUserId, "Max", "Mustermann")

}
