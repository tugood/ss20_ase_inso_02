package com.ase.tugood.userservice

import com.ase.tugood.userservice.infrastructure.configuration.MailConfiguration
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean

@SpringBootTest
class MainApplicationTests {
    @MockBean
    private lateinit var mailConfiguration: MailConfiguration

    @Test
    fun contextLoads() {
    }

}
