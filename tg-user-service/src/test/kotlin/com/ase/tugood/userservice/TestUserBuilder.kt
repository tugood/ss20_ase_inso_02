package com.ase.tugood.userservice

import com.ase.tugood.userservice.application.data.UserDTO
import com.ase.tugood.userservice.application.data.UserState
import com.ase.tugood.userservice.domain.user.model.User
import com.ase.tugood.userservice.infrastructure.web.model.request.UserInfo
import java.time.Instant
import java.util.*

data class TestUserBuilder(
    var id: String? = null,
    var firstName: String? = null,
    var lastName: String? = null,
    var email: String? = null,
    var phoneNr: String? = null,
    var langKey: String? = null,
    var description: String? = null,
    var distance: Long? = 10L,
    var hasImage: Boolean = false,
    var userState: UserState? = UserState.ACTIVE,
    var avgRating: Double? = null,
    var createdDate: Instant? = Instant.now(),
    var lastModifiedDate: Instant? = Instant.now()
) {
    companion object {
        val defaultId: String = UUID.randomUUID().toString()
        val defaultTime: Instant = Instant.now()
        const val defaultFirstName = "Karl"
        const val defaultLastName = "Ludwig"
        const val defaultEmail = "karl@ludwig.at"
        const val defaultPhoneNr = "+4366543345"
        const val defaultLangKey = "en"
        const val defaultDescription = "I am a rich helper"
        const val defaultDistance = 10L
        const val defaultHasImage = false
        val defaultUserState: UserState = UserState.ACTIVE
        const val defaultRating = 4.1
        const val defaultRatingSum = 41L
        const val defaultRatingCount = 10L
        val adminId: String = UUID.randomUUID().toString()
        const val adminFirstName = "Andi"
        const val adminLastName = "Admin"
        const val adminEmail = "admin@admin.at"
        const val adminPhoneNr = "+4366412312334"
        const val adminLangKey = "en"
        const val adminDescription = "I am the boss"
        const val adminHasImage = true
        val adminUserState: UserState = UserState.ACTIVE
        const val adminRating = 4.5
        const val adminRatingSum = 45L
        const val adminRatingCount = 10L
    }

    fun build() = UserDTO(id, firstName, lastName, email, phoneNr, langKey, description, distance, hasImage, userState, avgRating, false, 0, createdDate, lastModifiedDate)
    fun validDefaultUserDTO() = UserDTO(defaultId, defaultFirstName, defaultLastName, defaultEmail, defaultPhoneNr, defaultLangKey, defaultDescription, defaultDistance, defaultHasImage, defaultUserState, defaultRating, false, 0, defaultTime, defaultTime)
    fun validDefaultUserEntity() = User(defaultId, defaultFirstName, defaultLastName, defaultEmail, defaultPhoneNr, defaultLangKey, defaultDescription, defaultDistance, defaultHasImage, defaultUserState, defaultRatingSum, false, defaultRatingCount, defaultTime, defaultTime)
    fun validAdminUserDTO() = UserDTO(adminId, adminFirstName, adminLastName, adminEmail, adminPhoneNr, adminLangKey, adminDescription, defaultDistance, adminHasImage, adminUserState, adminRating, false, 0, defaultTime, defaultTime)
    fun validAdminUserEntity() = User(adminId, adminFirstName, adminLastName, adminEmail, adminPhoneNr, adminLangKey, adminDescription, defaultDistance, adminHasImage, adminUserState, adminRatingSum, false, adminRatingCount, defaultTime, defaultTime)
    fun validUserInfo() = UserInfo(defaultId, defaultLangKey, defaultFirstName + " " + defaultLastName, defaultEmail, defaultEmail, defaultFirstName, defaultId, defaultEmail, true, defaultLastName, mapOf())
    fun validAdminUserInfo() = UserInfo(adminId, defaultLangKey, adminFirstName + " " + adminLastName, adminEmail, adminEmail, adminFirstName, adminId, adminEmail, true, adminLastName, mapOf(Pair("roles", listOf("tg-admin"))))
}
