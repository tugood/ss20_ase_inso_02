package com.ase.tugood.userservice.application

import com.ase.tugood.userservice.application.data.UserDTO
import com.ase.tugood.userservice.application.data.notification.ServiceNotificationDTO
import com.ase.tugood.userservice.domain.notification.service.INotificationService
import com.ase.tugood.userservice.infrastructure.web.client.NotificationServiceClient
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import java.util.*

@Service
class NotificationService(@Autowired private val userService: UserService,
                          @Autowired private val notificationServiceClient: NotificationServiceClient,
                          @Autowired private val messageSource: MessageSource) : INotificationService {
    val logger: Logger = LoggerFactory.getLogger(javaClass)

    override fun notify(userId: String, titleMessageSource: String, bodyMessageSource: String, notificationType: ServiceNotificationDTO.NotificationType): Mono<Void> {
        return notify(userId, Pair(titleMessageSource, null), Pair(bodyMessageSource, null), notificationType)
    }

    override fun notify(userId: String, titleMessageSourceAndArguments: Pair<String, Array<*>?>, bodyMessageSourceAndArguments: Pair<String, Array<*>?>, notificationType: ServiceNotificationDTO.NotificationType): Mono<Void> {
        return userService
            .getUserById(userId)
            .flatMap { userDTO ->
                notificationServiceClient
                    .notify(ServiceNotificationDTO().apply {
                        this.title = populateMessageWithArguments(
                            messageSource.getMessage(titleMessageSourceAndArguments.first, null, getLang(userDTO)),
                            titleMessageSourceAndArguments.second)
                        this.body = populateMessageWithArguments(
                            messageSource.getMessage(bodyMessageSourceAndArguments.first, null, getLang(userDTO)),
                            bodyMessageSourceAndArguments.second)
                        this.userId = userDTO.id!!
                        this.userEmail = userDTO.email
                        this.notificationType = notificationType
                    })
            }
            .doOnError { cause -> logger.error("Sending the notification for the user $userId failed! Cause: ${cause.message}") }
            .map { logger.debug("Sent the notification for user $userId successfully") }
            .then()
    }

    /**
     * The aim of this function is to populate the specified message with the given arguments.
     *
     * @param message the message to be populated
     * @param arguments the arguments to be inserted into the message
     * @return the resulting string
     */
    fun populateMessageWithArguments(message: String, arguments: Array<*>?): String {
        return if (arguments == null) {
            message
        } else {
            String.format(message, *arguments)
        }
    }

    /**
     * The aim of this function is to get the language key of the given user.
     *
     * @param userDTO the user DTO of the user we want to retrive the locale from
     * @return the local of the user
     */
    fun getLang(userDTO: UserDTO): Locale {
        if (userDTO.langKey == null || userDTO.langKey!!.isEmpty()) {
            logger.error("No language defined for user ${userDTO.id}")
            return Locale.GERMAN
        }

        return try {
            Locale(userDTO.langKey)
        } catch (e: Exception) {
            logger.error("Cannot convert language key ${userDTO.langKey} for user ${userDTO.id}")
            Locale.GERMAN
        }
    }
}
