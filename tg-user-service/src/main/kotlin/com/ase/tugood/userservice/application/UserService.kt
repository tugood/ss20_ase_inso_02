package com.ase.tugood.userservice.application

import com.ase.tugood.userservice.application.data.RatingDTO
import com.ase.tugood.userservice.application.data.UserDTO
import com.ase.tugood.userservice.application.data.UserState
import com.ase.tugood.userservice.domain.user.model.User
import com.ase.tugood.userservice.domain.user.service.IUserService
import com.ase.tugood.userservice.domain.user.service.mapper.UserInfoMapper
import com.ase.tugood.userservice.domain.user.service.mapper.UserMapper
import com.ase.tugood.userservice.infrastructure.configuration.KeycloakConfiguration
import com.ase.tugood.userservice.infrastructure.persistence.UserRepository
import com.ase.tugood.userservice.infrastructure.web.model.request.UserInfo
import com.ase.tugood.userservice.infrastructure.web.rest.error.UserNotFoundException
import org.keycloak.admin.client.Keycloak
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.switchIfEmpty
import java.time.Instant
import java.util.logging.Level

@Service
class UserService(@Autowired private val repository: UserRepository,
                  @Autowired private val userMapper: UserMapper,
                  @Autowired private val userInfoMapper: UserInfoMapper,
                  @Autowired private val keycloak: Keycloak,
                  @Autowired private val keycloakConfiguration: KeycloakConfiguration) : IUserService {

    // used by admin
    override fun updateUser(id: String, userDTO: UserDTO): Mono<UserDTO> {
        // check if user already exists
        return repository.findById(id).flatMap { user ->
            val updateUser = user.apply {
                firstName = userDTO.firstName
                lastName = userDTO.lastName
                email = userDTO.email
                phoneNr = userDTO.phoneNr
                description = userDTO.description
                distance = userDTO.distance
                hasImage = userDTO.hasImage
                langKey = userDTO.langKey
                userState = userDTO.userState
            }

            updateKeycloakUser(userDTO)

            updateUser.lastModifiedDate = Instant.now()
            repository.save(updateUser).map { u -> userMapper.toDto(u) }

        }.switchIfEmpty(Mono.error(UserNotFoundException(id = id))).log("updated user with id=$id")
    }

    // used by user self
    override fun updateUser(userInfo: UserInfo, userDTO: UserDTO): Mono<UserDTO> {
        // check if user already exists
        return repository.findById(userInfo.id).flatMap { user ->
            val updateUser = userMapper.merge(user, userDTO)
            updateKeycloakUser(userDTO)
            repository.save(updateUser).map { u -> userMapper.toDto(u) }
        }.switchIfEmpty(Mono.just(userMapper.toEntity(userDTO, userInfo)).flatMap { user ->
            repository.save(user).map { u -> userMapper.toDto(u) }
        })

    }

    override fun createRating(id: String, ratingDTO: RatingDTO): Mono<UserDTO> {

        return repository.findById(id).flatMap { user ->
            val updatedUser = user.apply {
                this.ratingCount = if (ratingCount == null) 1 else this.ratingCount!!.inc()
                this.ratingSum = if (ratingSum == null) ratingDTO.rating.toLong() else this.ratingSum!!.plus(ratingDTO.rating)
            }

            repository.save(updatedUser).map { u -> userMapper.toDto(u) }
        }.switchIfEmpty(Mono.error(UserNotFoundException(id = id)))
            .log("create rating for user with id=$id " +
                "and rating $ratingDTO")

    }

    /*
     * if user is not in database the getUser function still works because it returns a userInfo
     * however this function fails, to repair save settings for user
     */
    override fun getUserById(id: String): Mono<UserDTO> {
        return repository.findById(id)
            .flatMap { user -> Mono.just(userMapper.toDto(user)) }
            .switchIfEmpty {
                val kcUser = getKeycloakUser(id) ?: return@switchIfEmpty Mono.error(UserNotFoundException(id = id))

                repository.save(kcUser).map {
                    userMapper.toDto(it)
                }

            }
            .log("retrieved user with id=$id")
    }

    override fun getUser(userInfo: UserInfo): Mono<UserDTO> {

        return repository.findById(userInfo.id)
            .flatMap { user -> Mono.just(userMapper.toDto(user)) }
            .switchIfEmpty(
                repository.save(userMapper.toEntity(UserDTO(userState = UserState.ACTIVE), userInfo))
                    .map { userMapper.toDto(it) }
            )
            .log("retrieved user with id=${userInfo.id}")
    }

    override fun deleteUserById(id: String): Mono<Void> {
        return repository.findById(id)
            .switchIfEmpty(Mono.error(UserNotFoundException(id = id)))
            .flatMap { user -> repository.delete(user) }
            .log("deleted user with id=$id", Level.INFO)
    }

    override fun blockUserById(id: String, block: Boolean): Mono<UserDTO> {
        return repository.findById(id)
            .flatMap { user ->
                if (block)
                    user.userState = UserState.BLOCKED
                else
                    user.userState = UserState.ACTIVE

                user.lastModifiedDate = Instant.now()
                repository.save(user).map { u -> userMapper.toDto(u) }.doOnNext {
                    disableKeycloakUser(it)
                }
            }
            .switchIfEmpty(Mono.error(UserNotFoundException(id = id)))
            .log("blocked user with id=$id")
    }

    override fun warnUserById(id: String, warn: Boolean): Mono<UserDTO> {
        return repository.findById(id)
            .flatMap { user ->
                user.isWarned = warn
                user.lastModifiedDate = Instant.now()
                repository.save(user).map { u -> userMapper.toDto(u) }
            }
            .switchIfEmpty(Mono.error(UserNotFoundException(id = id)))
            .log("warned user with id=$id")
    }

    fun updateKeycloakUser(userDTO: UserDTO) {
        val keycloakUserResource = keycloak.realm(keycloakConfiguration.userRealm).users().get(userDTO.id)
        val keycloakUserRepresentation = keycloakUserResource.toRepresentation()

        keycloakUserRepresentation.firstName = userDTO.firstName
        keycloakUserRepresentation.lastName = userDTO.lastName
        keycloakUserRepresentation.email = userDTO.email
        keycloakUserRepresentation.username = userDTO.email
        keycloakUserRepresentation.singleAttribute("locale", userDTO.langKey)

        keycloakUserResource.update(keycloakUserRepresentation)
    }

    fun disableKeycloakUser(userDTO: UserDTO) {
        val keycloakUserResource = keycloak.realm(keycloakConfiguration.userRealm).users().get(userDTO.id)
        val keycloakUserRepresentation = keycloakUserResource.toRepresentation()

        keycloakUserRepresentation.isEnabled = false
        keycloakUserResource.update(keycloakUserRepresentation)
    }

    fun getKeycloakUser(id: String): User? {
        val keycloakUserResource = keycloak.realm(keycloakConfiguration.userRealm).users().get(id) ?: return null
        val keycloakUserRepresentation = keycloakUserResource.toRepresentation()

        return User(
            id = id,
            firstName = keycloakUserRepresentation.firstName,
            lastName = keycloakUserRepresentation.lastName,
            email = keycloakUserRepresentation.email,
            langKey = if (keycloakUserRepresentation.attributes != null) {keycloakUserRepresentation.attributes["locale"]?.get(0) ?: "de" } else { "de" },
            userState = UserState.ACTIVE
        )
    }
}
