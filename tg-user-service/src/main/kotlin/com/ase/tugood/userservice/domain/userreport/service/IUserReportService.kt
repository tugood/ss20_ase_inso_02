package com.ase.tugood.userservice.domain.userreport.service

import com.ase.tugood.userservice.application.data.UserReportDTO
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

/**
 * This interface aims in providing an API that performs operations for user reports on
 * data storage triggered by the reactive web controller.
 */
interface IUserReportService {
    /**
     * The aim of this function is to persist a new user report to the data storage.
     * @param userReportDTO the user report instance to be persisted
     * @return return the persisted user report as reactive stream
     */
    fun createUserReport(userReportDTO: UserReportDTO): Mono<UserReportDTO>

    /**
     * The aim of this function is to retrieve a list of a user reports that have been persisted for a given user.
     * @param userId the id of user whos reports need to be returned
     * @return a list of user reports for the given user
     */
    fun getUserReportsByUser(userId: String): Flux<UserReportDTO>
}
