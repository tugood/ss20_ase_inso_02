package com.ase.tugood.userservice.infrastructure.web.rest.error

import org.springframework.http.HttpStatus

open class ImageNotFoundException(
    id: String,
    cause: Throwable? = null) :
    TUGoodException(
        "Image with $id was not found",
        HttpStatus.NOT_FOUND,
        "ImageNotFound",
        cause)
