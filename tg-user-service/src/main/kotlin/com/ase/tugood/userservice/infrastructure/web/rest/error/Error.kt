package com.ase.tugood.userservice.infrastructure.web.rest.error

import com.fasterxml.jackson.annotation.JsonProperty

/**
 *
 * @param message
 * @param type
 * @param code
 * @param errors
 */
data class Error(

    @JsonProperty("message") var message: String,

    @JsonProperty("type") var type: String,

    @Deprecated(message = "")
    @JsonProperty("code") var code: Int? = null,

    @JsonProperty("cause") var cause: MutableList<Error?> = mutableListOf()
)

