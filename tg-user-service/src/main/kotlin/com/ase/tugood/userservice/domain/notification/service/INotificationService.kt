package com.ase.tugood.userservice.domain.notification.service

import com.ase.tugood.userservice.application.data.notification.ServiceNotificationDTO
import reactor.core.publisher.Mono

interface INotificationService {

    /**
     * The aim of this function is to generate a notification for the given user and trigger the
     * notification service to actually perform the notification handling within the system.
     *
     * @param userId the id of the user to be notified
     * @param titleMessageSource the notification's title
     * @param bodyMessageSource the notifications's body
     * @param notificationType the notification's type [BOTH, PLAIN_OLD, PUSH]
     */
    fun notify(userId: String, titleMessageSource: String,
               bodyMessageSource: String,
               notificationType: ServiceNotificationDTO.NotificationType): Mono<Void>

    /**
     * The aim of this function is to generate a notification for the given user and trigger the
     * notification service to actually perform the notification handling within the system.
     *
     * @param userId the id of the user to be notified
     * @param titleMessageSourceAndArguments the notification's title as pair of string with custom variables holding text
     * @param bodyMessageSourceAndArguments the notifications's body as pair of string with custom variables holding text
     * @param notificationType the notification's type [BOTH, PLAIN_OLD, PUSH]
     */
    fun notify(userId: String, titleMessageSourceAndArguments: Pair<String, Array<*>?>,
               bodyMessageSourceAndArguments: Pair<String, Array<*>?>,
               notificationType: ServiceNotificationDTO.NotificationType): Mono<Void>
}
