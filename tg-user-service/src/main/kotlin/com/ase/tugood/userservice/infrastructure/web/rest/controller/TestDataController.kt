package com.ase.tugood.userservice.infrastructure.web.rest.controller

import com.ase.tugood.userservice.application.ImageUploadService
import com.ase.tugood.userservice.domain.user.service.IUserService
import com.ase.tugood.userservice.infrastructure.web.model.request.TestUserDTO
import com.ase.tugood.userservice.infrastructure.web.model.request.TestUserPictureDTO
import com.ase.tugood.userservice.infrastructure.web.rest.error.UserNotFoundException
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.bson.types.ObjectId
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.core.io.buffer.DataBufferFactory
import org.springframework.core.io.buffer.DataBufferWrapper
import org.springframework.core.io.buffer.DefaultDataBufferFactory
import org.springframework.http.codec.multipart.FilePart
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.awt.image.BufferedImage
import java.io.ByteArrayInputStream
import java.io.File
import java.util.*
import javax.imageio.ImageIO
import javax.ws.rs.PathParam


@RestController
@Profile("generatedata")
@RequestMapping("/generate/users")
class TestDataController(@Autowired private val userService: IUserService,
                         @Autowired private val imageUploadService: ImageUploadService) {


    val logger: Logger = LoggerFactory.getLogger(javaClass)


    @RequestMapping(
        value = ["/accounts"],
        produces = ["application/json"],
        method = [RequestMethod.POST])
    fun testUserCreate(
        @RequestBody users: List<TestUserDTO>
    ): Mono<List<String>> {

        logger.info(users.toString())

        return Flux.fromIterable(users)
                .flatMap { user ->
                    userService.getUserById(user.userDTO.id!!)
                            .map { "exists" }
                            .onErrorResume {cause ->
                                if (cause is UserNotFoundException) {
                                    userService.updateUser(user.userInfo, user.userDTO)
                                            .map { if (it.id == null) "undefined" else it.id }
                                } else {
                                    Mono.just("error")
                                }
                            }
                }
                .filter { it != "exists" }
                .map { it!! }
                .collectList()
    }

    @RequestMapping(
            value = ["/accounts/pictures/{id}"],
            produces = ["application/json"],
            method = [RequestMethod.PUT])
    fun testUserPictureCreate(
            @PathVariable("id") id: String,
            @RequestBody userPicture: TestUserPictureDTO
    ): Mono<ObjectId> {

        // tokenize the data
        val parts = userPicture.picture.split(",")
        val imageString: String = parts[1]

        // create a buffered image
        val imageByte: ByteArray = Base64.getDecoder().decode(imageString)
        val dataBuffer: Flux<DataBuffer> = Flux.just(DefaultDataBufferFactory().wrap(imageByte))

        return imageUploadService.updateImage(id, dataBuffer)
    }
}
