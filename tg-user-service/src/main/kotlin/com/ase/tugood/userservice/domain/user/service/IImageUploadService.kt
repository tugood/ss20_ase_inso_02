package com.ase.tugood.userservice.domain.user.service

import org.bson.types.ObjectId
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface IImageUploadService {

    /**
     * The aim of this function is to update the data of a given user in the data storage.
     *
     * @param id the id of the user to be updated
     * @param userDTO the new data of the user
     * @return the new user as reactive type
     */
    fun updateImage(userId: String, fileParts: Flux<DataBuffer>): Mono<ObjectId>

    /**
     * The aim of this function is to retrieve a stored image from mongoDB by userId.
     *
     * @param userId the id of the user we want to get the image of
     * @param exchange the server web exchange that is used to respond the retrieved image
     * @return a void reactive stream as the data is returned via server web exchange,
     * because webflux does not support the filepart actions.
     */
    fun getImage(userId: String, exchange: ServerWebExchange): Flux<Void>
}
