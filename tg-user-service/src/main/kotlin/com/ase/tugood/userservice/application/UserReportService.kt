package com.ase.tugood.userservice.application

import com.ase.tugood.userservice.application.data.UserReportDTO
import com.ase.tugood.userservice.domain.userreport.service.IUserReportService
import com.ase.tugood.userservice.domain.userreport.service.mapper.UserReportMapper
import com.ase.tugood.userservice.infrastructure.persistence.UserReportRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class UserReportService(@Autowired private val userReportRepository: UserReportRepository,
                        @Autowired private val userReportMapper: UserReportMapper) : IUserReportService {

    private val logger = LoggerFactory.getLogger(javaClass)

    override fun createUserReport(userReportDTO: UserReportDTO): Mono<UserReportDTO> {
        return userReportRepository
            .save(userReportMapper.toEntity(userReportDTO))
            .map { report -> userReportMapper.toDto(report) }
            .onErrorMap { cause ->
                logger.error("cannot save the user report {}", cause.message);
                throw cause
            }
            .log("added new user report: ${userReportDTO.id}")
    }

    override fun getUserReportsByUser(userId: String): Flux<UserReportDTO> {
        return userReportRepository
            .findAllByUserId(userId)
            .map { userReport -> userReportMapper.toDto(userReport) }
            .onErrorMap { cause ->
                logger.error("cannot retrieve user reports of user {}: {}", userId, cause.message);
                throw cause
            }
            .log("retrieved user reports of user: $userId")
    }
}
