package com.ase.tugood.userservice.domain.userreport.service.mapper

import com.ase.tugood.userservice.application.data.UserReportDTO
import com.ase.tugood.userservice.domain.userreport.model.UserReport
import org.springframework.stereotype.Component

@Component
class UserReportMapper {
    fun toEntity(dto: UserReportDTO): UserReport {
        return UserReport(id = dto.id,
            userId = dto.userId,
            cause = dto.cause,
            createdDate = dto.createdDate,
            description = dto.description)
    }

    fun toDto(entity: UserReport): UserReportDTO {
        return UserReportDTO(id = entity.id,
            userId = entity.userId,
            cause = entity.cause,
            createdDate = entity.createdDate,
            description = entity.description)
    }
}
