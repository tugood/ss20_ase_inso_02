package com.ase.tugood.userservice.infrastructure.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.ClassPathResource
import org.springframework.data.repository.init.AbstractRepositoryPopulatorFactoryBean
import org.springframework.data.repository.init.Jackson2RepositoryPopulatorFactoryBean

@Configuration
class RepositoryPopulatorConfig {
    @Bean
    fun repositoryPopulator(): AbstractRepositoryPopulatorFactoryBean? {
        val factoryBean = Jackson2RepositoryPopulatorFactoryBean()
        factoryBean.setResources(arrayOf(ClassPathResource("templates/test_user.json")))
        return factoryBean
    }
}
