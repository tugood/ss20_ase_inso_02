package com.ase.tugood.userservice.infrastructure.configuration

import org.keycloak.OAuth2Constants
import org.keycloak.admin.client.Keycloak
import org.keycloak.admin.client.KeycloakBuilder
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource

@Configuration
@PropertySource("classpath:application.yml")
class MailConfiguration {

    @Value("\${mail.username}")
    lateinit var username: String

    @Value("\${mail.password}")
    lateinit var password: String
}
