package com.ase.tugood.userservice.infrastructure.web.rest.error

import org.springframework.http.HttpStatus

open class UserNotFoundException(
    id: String,
    cause: Throwable? = null) :
    TUGoodException(
        "User with $id was not found",
        HttpStatus.NOT_FOUND,
        "UserNotFound",
        cause)
