package com.ase.tugood.userservice.infrastructure.web.rest.controller

import com.ase.tugood.userservice.application.data.UserDTO
import com.ase.tugood.userservice.domain.user.service.IImageUploadService
import com.ase.tugood.userservice.infrastructure.web.model.request.UserInfo
import com.ase.tugood.userservice.infrastructure.web.rest.error.Error
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.swagger.v3.oas.annotations.ExternalDocumentation
import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.info.Contact
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.servers.Server
import io.swagger.v3.oas.annotations.servers.ServerVariable
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.codec.multipart.FilePart
import org.springframework.web.bind.annotation.*
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.*

@RestController
@RequestMapping("/api/users")
@OpenAPIDefinition(
    info = Info(title = "TUgood Image Upload Service",
        description = "This is the API for the User Service",
        contact = Contact(email = "e1525631@student.tuwien.ac.at"),
        version = "1.0.0"),
    externalDocs = ExternalDocumentation(description = "Project Repo",
        url = "https://reset.inso.tuwien.ac.at/repo/2020ss-ASE-group/ss20_ase_inso_02/-/tree/master/tg-user-service"),
    servers = [
        Server(url = "http://127.0.0.1:{port}", variables = [ServerVariable(name = "port", defaultValue = "8083")])
    ]
)
class ImageUploadController(@Autowired private val imageService: IImageUploadService) {
    @Operation(
        description = "Upload an new image as a user picture.",
        method = "uploadImage",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "Id of uploaded image returned")])
    @RequestMapping(
        value = ["/accounts/self/image"],
        consumes = [MediaType.MULTIPART_FORM_DATA_VALUE],
        produces = [MediaType.APPLICATION_STREAM_JSON_VALUE],
        method = [RequestMethod.PUT]
    )
    fun uploadImage(
        @RequestHeader("X-Userinfo") userInfo: String,
        @RequestPart fileParts: Mono<FilePart>
    ): Mono<ObjectId> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        return fileParts
                .flatMap { imageService.updateImage(info.id, it.content()) }
    }

    @Operation(
        description = "Fetch user image",
        method = "getUserImage",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "Image object returned")])
    @RequestMapping(
        value = ["/image/{userId}"],
        produces = [MediaType.APPLICATION_OCTET_STREAM_VALUE],
        method = [RequestMethod.GET]
    )
    fun getUserImage(
        @PathVariable userId: String,
        exchange: ServerWebExchange
    ): Flux<Void> {
        return imageService.getImage(userId, exchange)
    }

}
