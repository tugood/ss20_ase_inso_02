package com.ase.tugood.userservice.infrastructure.web.model.request

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotNull

@JsonIgnoreProperties(ignoreUnknown = true)
data class TestUserPictureDTO(

    @NotNull
    @JsonProperty("picture")
    val picture: String
)