package com.ase.tugood.userservice.infrastructure.web.rest.controller


import com.ase.tugood.userservice.application.data.UserDTO
import com.ase.tugood.userservice.domain.user.service.IUserService
import com.ase.tugood.userservice.infrastructure.web.model.request.UserInfo
import com.ase.tugood.userservice.infrastructure.web.rest.error.Error
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.swagger.v3.oas.annotations.ExternalDocumentation
import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.info.Contact
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.servers.Server
import io.swagger.v3.oas.annotations.servers.ServerVariable
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import java.util.*

@RestController
@RequestMapping("/api/users")
@OpenAPIDefinition(
    info = Info(title = "TUgood User Service", description = "This is the API for the User Service", contact = Contact(email = "e1525631@student.tuwien.ac.at"), version = "1.0.0"),
    externalDocs = ExternalDocumentation(description = "Project Repo", url = "https://reset.inso.tuwien.ac.at/repo/2020ss-ASE-group/ss20_ase_inso_02/-/tree/master/tg-user-service"),
    servers = [
        Server(url = "http://127.0.0.1:{port}", variables = [ServerVariable(name = "port", defaultValue = "8083")])
    ]
)
@Tag(name = "Login", description = "CRUD and attribute definitions for users")
class UserController(@Autowired private val userService: IUserService) {

    @Operation(
        description = "Update a user",
        method = "updateUser",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "User updated", content = [Content(schema = Schema(implementation = UserDTO::class))])])
    @RequestMapping(
        value = ["/accounts/self"],
        produces = ["application/json"],
        method = [RequestMethod.PUT])
    fun updateUser(
        @RequestHeader("X-Userinfo") userInfo: String,
        @RequestBody user: UserDTO
    ): Mono<UserDTO> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        user.id=info.id
        return userService.updateUser(info, user)
    }

    @Operation(
        description = "Retrieve current active user",
        method = "getUser",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "User found", content = [Content(schema = Schema(implementation = UserDTO::class))])])
    @RequestMapping(
        value = ["/accounts/self"],
        produces = ["application/json"],
        method = [RequestMethod.GET])
    fun getUser(
        @RequestHeader("X-Userinfo") userInfo: String
    ): Mono<UserDTO> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        return userService.getUser(info)
    }

    @Operation(
        description = "Retrieve user info",
        method = "getUserInfo",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "User info returned")])
    @RequestMapping(
        value = ["/userinfo"],
        produces = ["application/json"],
        method = [RequestMethod.GET]
    )
    fun getUserInfo(@RequestHeader("X-Userinfo") userInfo: String): Mono<ResponseEntity<String>> {
        return Mono.just(userInfo).map { encoded -> ResponseEntity.ok().body(String(Base64.getDecoder().decode(encoded))) }
    }

    @Operation(
        description = "Fetch an access token",
        method = "getAccessToken",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "Access token returned")])
    @RequestMapping(
        value = ["/accessToken"],
        produces = ["application/json"],
        method = [RequestMethod.GET]
    )
    fun getAccessToken(@RequestHeader("X-Access-Token") accessToken: String): Mono<ResponseEntity<String>> {
        return Mono.just(accessToken).map { encoded -> ResponseEntity.ok().body(String(Base64.getDecoder().decode(encoded))) }
    }

    @Operation(
        description = "Fetch the corresponding id token.",
        method = "getIdToken",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "ID token returned")])
    @RequestMapping(
        value = ["/idToken"],
        produces = ["application/json"],
        method = [RequestMethod.GET]
    )
    fun getIdToken(@RequestHeader("X-Id-Token") idToken: String): Mono<ResponseEntity<String>> {
        return Mono.just(idToken).map { encoded -> ResponseEntity.ok().body(String(Base64.getDecoder().decode(encoded))) }
    }

    @Operation(
        description = "Fetch the corresponding id tokens.",
        method = "getIdToken",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "ID tokens returned")])
    @RequestMapping(
        value = ["/printHeader"],
        produces = ["application/json"],
        method = [RequestMethod.GET]
    )
    fun getIdToken(@RequestHeader headers: Map<String, String>): Mono<ResponseEntity<Map<String, String>>> {
        return Mono.just(ResponseEntity.ok().body(headers))
    }
}
