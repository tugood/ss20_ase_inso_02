package com.ase.tugood.userservice.infrastructure.web.rest.controller

import com.ase.tugood.userservice.application.data.UserReportDTO
import com.ase.tugood.userservice.application.data.notification.ServiceNotificationDTO
import com.ase.tugood.userservice.domain.notification.service.INotificationService
import com.ase.tugood.userservice.domain.userreport.service.IUserReportService
import io.swagger.v3.oas.annotations.ExternalDocumentation
import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.info.Contact
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.servers.Server
import io.swagger.v3.oas.annotations.servers.ServerVariable
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/api/users")
@OpenAPIDefinition(
    info = Info(title = "TUgood User Report Service", description = "This is the API for the User Report Service", contact = Contact(email = "e01634019@student.tuwien.ac.at"), version = "1.0.0"),
    externalDocs = ExternalDocumentation(description = "Project Repo", url = "https://reset.inso.tuwien.ac.at/repo/2020ss-ASE-group/ss20_ase_inso_02/-/tree/master/tg-user-service"),
    servers = [
        Server(url = "http://127.0.0.1:{port}", variables = [ServerVariable(name = "port", defaultValue = "8083")])
    ]
)
@Tag(name = "Login", description = "CRUD and attribute definitions for users")
class UserReportController(@Autowired private val userReportService: IUserReportService,
                           @Autowired private val notificationService: INotificationService) {
    @Operation(
        description = "Create and Get user report",
        method = "createUserReport",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "201", description = "User report created", content = [Content(schema = Schema(implementation = UserReportDTO::class))])])
    @RequestMapping(
        value = ["/report"],
        produces = ["application/json"],
        consumes = ["application/json"],
        method = [RequestMethod.POST])
    fun createUserReport(
        @RequestBody userReportDTO: UserReportDTO
    ): Mono<ResponseEntity<UserReportDTO>> {
        return userReportService
            .createUserReport(userReportDTO)
            .map { report ->
                notificationService.notify(report.userId,
                    "notification.report.user.title",
                    "notification.report.user.body",
                    ServiceNotificationDTO.NotificationType.BOTH)
                    .ignoreElement()
                    .subscribe()
                report
            }
            .map { ResponseEntity.status(HttpStatus.CREATED).body(it) }
    }

    @Operation(
        description = "Get user reports  by user id",
        method = "getUserReportsByUserId",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "List of user reports for specific user returned successfully", content = [Content(schema = Schema(implementation = UserReportDTO::class))])])
    @RequestMapping(
        value = ["/report/{id}"],
        produces = ["application/json"],
        method = [RequestMethod.GET])
    fun getUserReportsByUserId(
        @PathVariable("id") userId: String
    ): Mono<ResponseEntity<List<UserReportDTO>>> {
        return userReportService
            .getUserReportsByUser(userId)
            .collectList()
            .map { reports -> ResponseEntity.ok(reports) }
    }

}

