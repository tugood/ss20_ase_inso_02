package com.ase.tugood.userservice.application.data

import com.fasterxml.jackson.annotation.JsonIgnore
import java.time.Instant
import javax.validation.constraints.NotNull

data class UserReportDTO(
    var id: String? = null,

    @NotNull
    var userId: String,

    var user: UserDTO? = null,

    @NotNull
    var cause: UserReportCause,

    @NotNull
    var description: String,

    @JsonIgnore
    var createdDate: Instant? = null
)
