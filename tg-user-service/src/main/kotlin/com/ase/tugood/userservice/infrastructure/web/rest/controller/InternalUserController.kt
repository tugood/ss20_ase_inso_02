package com.ase.tugood.userservice.infrastructure.web.rest.controller

import com.ase.tugood.userservice.application.data.RatingDTO
import com.ase.tugood.userservice.application.data.UserDTO
import com.ase.tugood.userservice.domain.user.service.IUserService
import com.ase.tugood.userservice.infrastructure.web.rest.error.Error
import io.swagger.v3.oas.annotations.ExternalDocumentation
import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.info.Contact
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.servers.Server
import io.swagger.v3.oas.annotations.servers.ServerVariable
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono


@RestController
@RequestMapping("/internal/users")
@OpenAPIDefinition(
    info = Info(title = "TUgood User Service", description = "This is the internal API for the User Service", contact = Contact(email = "e1525631@student.tuwien.ac.at"), version = "1.0.0"),
    externalDocs = ExternalDocumentation(description = "Project Repo", url = "https://reset.inso.tuwien.ac.at/repo/2020ss-ASE-group/ss20_ase_inso_02/-/tree/master/tg-user-service"),
    servers = [
        Server(url = "http://127.0.0.1:{port}", variables = [ServerVariable(name = "port", defaultValue = "8083")])
    ]
)
class InternalUserController(@Autowired private val userService: IUserService) {
    @Operation(
        description = "Retrieve user by id",
        method = "getUserById",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "User found", content = [Content(schema = Schema(implementation = UserDTO::class))])])
    @RequestMapping(
        value = ["/accounts/{id}"],
        produces = ["application/json"],
        method = [RequestMethod.GET])
    fun getUserById(
        @PathVariable("id") id: String
    ): Mono<UserDTO> {
        return userService.getUserById(id)
    }

    @Operation(
        description = "Create a rating for user",
        method = "createRating",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "Rating added", content = [Content(schema = Schema(implementation = UserDTO::class))])])
    @RequestMapping(
        value = ["/ratings/{id}"],
        produces = ["application/json"],
        method = [RequestMethod.POST])
    fun createRating(
        @PathVariable("id") id: String,
        @RequestBody(required = true) ratingDTO: RatingDTO
    ): Mono<UserDTO> {
        return userService.createRating(id, ratingDTO)
    }


}
