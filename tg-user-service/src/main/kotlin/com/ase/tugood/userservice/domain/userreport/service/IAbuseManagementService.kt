package com.ase.tugood.userservice.domain.userreport.service

import com.ase.tugood.userservice.application.data.UserDTO
import com.ase.tugood.userservice.application.data.UserReportDTO
import org.springframework.data.domain.Pageable
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface IAbuseManagementService {
    /**
     * The aim of this function is to retrieve pages of a sorted list of all user reports
     *
     * @param pageable page and size of the page to fetch
     * @return the requested page
     */
    fun getOrderedUserReportPage(pageable: Pageable): Flux<UserReportDTO>

    /**
     * The aim of this function is to retrieve how many reports are managed in total
     *
     * @return the amount of reports in the database
     */
    fun countReports(): Mono<Long>

    /**
     * The aim of this function is to retrieve how many reports were issued for a single user
     *
     * @return the amount of reports in the database corresponding to a single user
     */
    fun countReportsByUser(userId: String): Mono<Long>

    /**
     * The aim of this function is to block the user and send them an email based on a report. The report is then
     * removed from the database.
     *
     * @return the blocked user as reactive stream
     */
    fun blockReportedUser(userReportId: String): Mono<UserDTO>

    /**
     * The aim of this function is to send the reported user an email with a warning based on a report.
     * The report is then removed from the database.
     *
     * @return the warned user as reactive stream
     */
    fun warnReportedUser(userReportId: String): Mono<UserDTO>

    /**
     * The aim of this function is to remove the report from the database.
     *
     * @return the deleted user report as reactive stream
     */
    fun deleteReport(userReportId: String): Mono<UserReportDTO>
}
