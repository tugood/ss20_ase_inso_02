package com.ase.tugood.userservice.domain.user.service.mapper

import com.ase.tugood.userservice.application.data.UserDTO
import com.ase.tugood.userservice.domain.user.model.User
import com.ase.tugood.userservice.infrastructure.web.model.request.UserInfo
import org.springframework.stereotype.Component
import java.time.Instant

/**
 * Mapper for the entity {@link User} and its DTO {@link UserDTO}.
 */
@Component
class UserMapper : EntityMapper<UserDTO, User> {

    override fun toEntity(dto: UserDTO, userInfo: UserInfo): User {
        return User().apply {
            id = userInfo.id
            email = userInfo.email
            lastName = userInfo.family_name
            firstName = userInfo.given_name
            langKey = userInfo.locale
            phoneNr = dto.phoneNr
            description = dto.description
            distance = dto.distance
            hasImage = dto.hasImage
            userState = dto.userState
            isWarned = dto.isWarned
            createdDate = Instant.now()
            lastModifiedDate = Instant.now()
        }
    }

    override fun merge(entity: User, dto: UserDTO): User {
        return entity.apply {
            firstName = dto.firstName
            lastName = dto.lastName
            phoneNr = dto.phoneNr
            description = dto.description
            distance = dto.distance
            hasImage = dto.hasImage
            langKey = dto.langKey
            isWarned = dto.isWarned
            lastModifiedDate = Instant.now()
        }
    }

    override fun toDto(entity: User): UserDTO {
        return UserDTO().apply {
            this.id = entity.id
            this.firstName = entity.firstName
            this.lastName = entity.lastName
            this.avgRating = if (entity.ratingCount == null || entity.ratingCount == 0L) null else entity.ratingSum!!.toDouble() / entity.ratingCount!!
            this.hasImage = entity.hasImage
            this.email = entity.email
            this.langKey = entity.langKey
            this.phoneNr = entity.phoneNr
            this.description = entity.description
            this.createdDate = entity.createdDate
            this.lastModifiedDate = entity.lastModifiedDate
            this.distance = entity.distance
            this.userState = entity.userState
            this.isWarned = entity.isWarned
        }
    }

    override fun toDto(entityList: List<User>): List<UserDTO> {
        return entityList.map {
            toDto(it)
        }
    }
}
