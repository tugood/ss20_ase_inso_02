package com.ase.tugood.userservice.application

import com.ase.tugood.userservice.domain.user.service.IImageUploadService
import com.ase.tugood.userservice.infrastructure.web.rest.error.ImageNotFoundException
import org.bson.types.ObjectId
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.gridfs.GridFsCriteria
import org.springframework.data.mongodb.gridfs.ReactiveGridFsTemplate
import org.springframework.http.codec.multipart.FilePart
import org.springframework.stereotype.Service
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono


@Service
class ImageUploadService(@Autowired private val gridFsTemplate: ReactiveGridFsTemplate) : IImageUploadService {

    override fun updateImage(userId: String, fileParts: Flux<DataBuffer>): Mono<ObjectId> {
        return gridFsTemplate.store(fileParts, userId)
    }

    override fun getImage(userId: String, exchange: ServerWebExchange): Flux<Void> {
        return gridFsTemplate.find(Query.query(GridFsCriteria.whereFilename().`is`(userId)))
            .last().onErrorMap { ImageNotFoundException(userId) }
            .log()
            .flatMap(gridFsTemplate::getResource)
            .flatMapMany { r -> exchange.response.writeWith(r.downloadStream) }
    }

}
