package com.ase.tugood.userservice.domain.user.service.mapper

import com.ase.tugood.userservice.application.data.UserDTO
import com.ase.tugood.userservice.application.data.UserState
import com.ase.tugood.userservice.infrastructure.web.model.request.UserInfo
import org.springframework.stereotype.Component

@Component
class UserInfoMapper {

    fun toDto(userInfo: UserInfo): UserDTO {

        return UserDTO().apply {
            email = userInfo.email
            firstName = userInfo.given_name
            lastName = userInfo.family_name
            id = userInfo.id
            userState = UserState.ACTIVE
            langKey = userInfo.locale
        }
    }
}
