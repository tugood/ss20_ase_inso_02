package com.ase.tugood.userservice.infrastructure.persistence

import com.ase.tugood.userservice.domain.userreport.model.UserReport
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Repository
interface UserReportRepository : ReactiveMongoRepository<UserReport, String> {
    fun findAllByUserId(userId: String): Flux<UserReport>
    fun findByOrderByCreatedDate(pageable: Pageable): Flux<UserReport>
    fun deleteAllByUserId(userId: String): Mono<Unit>
    fun countAllByUserId(userId: String): Mono<Long>
}
