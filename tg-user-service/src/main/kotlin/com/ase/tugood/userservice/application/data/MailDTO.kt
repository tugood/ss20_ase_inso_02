package com.ase.tugood.userservice.application.data

class MailDTO (val subject: String,
               val message: String)
