package com.ase.tugood.userservice.infrastructure.persistence

import com.ase.tugood.userservice.domain.user.model.User
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : ReactiveCrudRepository<User, String> {
    fun findOneByFirstName(firstName: String?): User?
}
