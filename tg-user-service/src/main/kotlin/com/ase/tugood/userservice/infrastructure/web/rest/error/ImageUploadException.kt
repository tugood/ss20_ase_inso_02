package com.ase.tugood.userservice.infrastructure.web.rest.error

import org.springframework.http.HttpStatus

class ImageUploadException(
    id: String,
    cause: Throwable? = null) :
    TUGoodException(
        "Error in file interaction process.",
        HttpStatus.INTERNAL_SERVER_ERROR,
        "File error",
        cause)
