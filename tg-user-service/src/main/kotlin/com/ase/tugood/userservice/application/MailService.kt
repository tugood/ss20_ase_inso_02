package com.ase.tugood.userservice.application

import com.ase.tugood.userservice.application.data.MailDTO
import com.ase.tugood.userservice.application.data.UserDTO
import com.ase.tugood.userservice.infrastructure.configuration.MailConfiguration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import javax.mail.*
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage


@Service
class MailService(@Autowired private val userService: UserService, @Autowired private val mailConfig: MailConfiguration) {

    fun sendMail(toUser:UserDTO, msg:MailDTO) {
        val toMail = toUser.email
        val fromMail = mailConfig.username
        val username = mailConfig.username
        val password = mailConfig.password
        val host = "smtp.gmail.com"

        val properties: Properties = System.getProperties()
        properties.put("mail.smtp.auth", "true")
        properties.put("mail.smtp.starttls.enable", "true")
        properties.put("mail.smtp.host", host)
        properties.put("mail.smtp.port", "587")

        val session:Session = Session.getDefaultInstance(properties, object : Authenticator() {
            override fun getPasswordAuthentication(): PasswordAuthentication {
                return PasswordAuthentication(username, password)
            }
        })

        val message = MimeMessage(session)
        message.setFrom(InternetAddress(fromMail))
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toMail))

        message.subject = msg.subject
        message.setText(msg.message)

        Transport.send(message)
    }
}
