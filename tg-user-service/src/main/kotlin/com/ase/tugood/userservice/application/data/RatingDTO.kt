package com.ase.tugood.userservice.application.data

import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.Max
import javax.validation.constraints.Min

data class RatingDTO(
    @field:Min(value = 0)
    @field:Max(value = 5)
    @JsonProperty("rating")
    var rating: Int
)
