package com.ase.tugood.userservice.domain.user.model

import com.ase.tugood.userservice.application.data.UserState
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import java.io.Serializable
import java.time.Instant
import javax.validation.constraints.Email
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size


@Document(collection = "tugood_user")
data class User @JvmOverloads constructor(

    @Id
    var id: String? = null,

    @field:Size(max = 50)
    @NotNull
    @Field("first_name")
    var firstName: String? = null,

    @field:Size(max = 50)
    @NotNull
    @Field("last_name")
    var lastName: String? = null,

    @field:Email
    @field:Size(min = 5, max = 254)
    @NotNull
    var email: String? = null,

    @field:Size(min = 4, max = 20)
    @Field("phone_nr")
    var phoneNr: String? = null,

    @field:Size(min = 2, max = 10)
    @Field("lang_key")
    var langKey: String? = null,

    var description: String? = null,

    var distance: Long? = 10L,   // set distance to 10km per default

    @Field("has_image")
    var hasImage: Boolean = false,

    @NotNull
    var userState: UserState? = UserState.ACTIVE,

    @NotNull
    @Field("rating_sum")
    var ratingSum: Long? = 0,

    @Field("is_warned")
    var isWarned: Boolean = false,

    @NotNull
    @Field("rating_count")
    var ratingCount: Long? = 0,

    @CreatedDate
    @Field("created_date")
    var createdDate: Instant? = Instant.now(),

    @LastModifiedDate
    @Field("last_modified_date")
    var lastModifiedDate: Instant? = Instant.now()

) : Serializable {

    companion object {
        private const val serialVersionUID = 1L
    }
}
