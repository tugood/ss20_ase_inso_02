package com.ase.tugood.userservice.infrastructure.web.rest.controller

import com.ase.tugood.userservice.application.AbuseManagementService
import com.ase.tugood.userservice.application.data.UserDTO
import com.ase.tugood.userservice.application.data.UserReportDTO
import com.ase.tugood.userservice.infrastructure.util.PaginationUtil
import com.ase.tugood.userservice.infrastructure.web.model.request.UserInfo
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.swagger.v3.oas.annotations.ExternalDocumentation
import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.info.Contact
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.servers.Server
import io.swagger.v3.oas.annotations.servers.ServerVariable
import io.swagger.v3.oas.annotations.tags.Tag
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.http.ResponseEntity
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.web.bind.annotation.*
import org.springframework.web.util.UriComponentsBuilder
import reactor.core.publisher.Mono
import java.util.*

@RestController
@RequestMapping("/api/users/abuse")
@OpenAPIDefinition(
    info = Info(title = "TUgood Abuse Management Service", description = "This is the API for the Abuse Management Service", contact = Contact(email = "silvio.vasiljevic@tuwien.ac.at"), version = "1.0.0"),
    externalDocs = ExternalDocumentation(description = "Project Repo", url = "https://reset.inso.tuwien.ac.at/repo/2020ss-ASE-group/ss20_ase_inso_02/-/tree/master/tg-user-service"),
    servers = [
        Server(url = "http://127.0.0.1:{port}", description = "local development", variables = [ServerVariable(name = "port", defaultValue = "8083")])
    ]
)
@Tag(name = "Login", description = "CRUD and attribute definitions for users")
class AbuseManagementController(@Autowired private val abuseManagementService: AbuseManagementService) {
    val logger: Logger = LoggerFactory.getLogger(javaClass)

    @Operation(
        description = "Get paged user reports",
        method = "getUserReports",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "Page of user reports returned successfully", content = [Content(schema = Schema(implementation = UserReportDTO::class))])])
    @RequestMapping(
        value = ["/reports"],
        produces = ["application/json"],
        method = [RequestMethod.GET])
    fun getUserReports(
        @RequestHeader("X-Userinfo") userInfo: String,
        @RequestParam(name = "page") page: Int,
        @RequestParam(name = "size") size: Int,
        request: ServerHttpRequest
    ): Mono<ResponseEntity<List<UserReportDTO>>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        if (info.realm_access?.get("roles")?.contains("tg-admin")?.not() != false) {
            throw Exception("not admin")
        }
        val pageable = PageRequest.of(page, size)
        return abuseManagementService.getOrderedUserReportPage(pageable)
            .collectList()
            .zipWith(abuseManagementService.countReports())
            .map { reportsAndCount ->
                val body = PageImpl(listOf<UserReportDTO>(), pageable, reportsAndCount.t2)
                val headers = PaginationUtil.generatePaginationHttpHeaders(UriComponentsBuilder.fromHttpRequest(request), body)

                ResponseEntity.ok()
                    .headers(headers)
                    .body(reportsAndCount.t1)
            }
    }

    @Operation(
        description = "Block user",
        method = "blockUser",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "User is blocked", content = [Content(schema = Schema(implementation = UserDTO::class))])])
    @RequestMapping(
        value = ["/block/{id}"],
        produces = ["application/json"],
        method = [RequestMethod.PUT])
    fun blockUser(
        @RequestHeader("X-Userinfo") userInfo: String,
        @PathVariable("id") userReportId: String
    ): Mono<ResponseEntity<UserDTO>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        if (info.realm_access?.get("roles")?.contains("tg-admin")?.not() != false) {
            throw Exception("not admin")
        }
        return abuseManagementService.blockReportedUser(userReportId).map { ResponseEntity.ok(it) }
    }

    @Operation(
        description = "Warn user",
        method = "warnUser",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "User is warned", content = [Content(schema = Schema(implementation = UserDTO::class))])])
    @RequestMapping(
        value = ["/warn/{id}"],
        produces = ["application/json"],
        method = [RequestMethod.PUT])
    fun warnUser(
        @RequestHeader("X-Userinfo") userInfo: String,
        @PathVariable("id") userReportId: String
    ): Mono<ResponseEntity<UserDTO>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        if (info.realm_access?.get("roles")?.contains("tg-admin")?.not() != false) {
            throw Exception("not admin")
        }
        return abuseManagementService.warnReportedUser(userReportId).map { ResponseEntity.ok(it) }
    }

    @Operation(
        description = "Delete report",
        method = "deleteReport",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "User is warned", content = [Content(schema = Schema(implementation = UserDTO::class))])])
    @RequestMapping(
        value = ["/reports/{id}"],
        produces = ["application/json"],
        method = [RequestMethod.DELETE])
    fun deleteReport(
        @RequestHeader("X-Userinfo") userInfo: String,
        @PathVariable("id") userReportId: String
    ): Mono<ResponseEntity<UserReportDTO>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        if (info.realm_access?.get("roles")?.contains("tg-admin")?.not() != false) {
            throw Exception("not admin")
        }
        return abuseManagementService.deleteReport(userReportId).map { ResponseEntity.ok(it) }
    }

}

