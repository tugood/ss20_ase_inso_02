package com.ase.tugood.userservice.domain.userreport.model

import com.ase.tugood.userservice.application.data.UserReportCause
import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import java.io.Serializable
import java.time.Instant
import javax.validation.constraints.NotNull

@Document(collection = "tugood_user_reports")
data class UserReport @JvmOverloads constructor(
    @Id
    var id: String? = null,

    @NotNull
    @Field("userId")
    var userId: String,

    @NotNull
    @Field("cause")
    var cause: UserReportCause,

    @NotNull
    @Field("description")
    var description: String,

    @JsonIgnore
    @Field("createdDate")
    var createdDate: Instant? = Instant.now()

) : Serializable {
    companion object {
        private const val serialVersionUID = 1L
    }
}
