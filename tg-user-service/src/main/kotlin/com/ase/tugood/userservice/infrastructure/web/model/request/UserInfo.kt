package com.ase.tugood.userservice.infrastructure.web.model.request

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonIgnoreProperties(ignoreUnknown = true)
data class UserInfo(
    @JsonProperty("id")
    var id: String,

    @JsonProperty("locale")
    var locale: String? = null,

    @JsonProperty("name")
    var name: String? = null,

    @JsonProperty("username")
    var username: String? = null,

    @JsonProperty("email")
    var email: String? = null,

    @JsonProperty("given_name")
    var given_name: String? = null,

    @JsonProperty("sub")
    var sub: String? = null,

    @JsonProperty("preferred_username")
    var preferred_username: String? = null,

    @JsonProperty("email_verified")
    var email_verified: Boolean,

    @JsonProperty("family_name")
    var family_name: String? = null,

    @JsonProperty("realm_access")
    val realm_access: Map<String, List<String>>? = null
) : Serializable
