package com.ase.tugood.userservice.infrastructure.web.model.request

import com.ase.tugood.userservice.application.data.UserDTO
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotNull

@JsonIgnoreProperties(ignoreUnknown = true)
data class TestUserDTO(

    @NotNull
    @JsonProperty("user")
    val userDTO: UserDTO,

    @NotNull
    @JsonProperty("userInfo")
    val userInfo: UserInfo

)