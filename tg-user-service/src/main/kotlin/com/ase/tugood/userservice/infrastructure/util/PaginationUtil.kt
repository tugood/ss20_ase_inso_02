package com.ase.tugood.userservice.infrastructure.util

import org.springframework.data.domain.Page
import org.springframework.http.HttpHeaders
import org.springframework.web.util.UriComponentsBuilder

/**
 * Util to generate links to the other pages.
 *
 * This is a Kotlin implementation of the PaginationUtil from Jhipster v6.9.1
 */
object PaginationUtil {

    private const val HEADER_X_TOTAL_COUNT = "X-Total-Count"

    /**
     * generate pagination information from the requested page
     *
     * @param uriBuilder uri components from the current request
     * @param page page with a fixed number of elements of a type <T>
     *
     * @return pagination header, e.g.
     * _links: {
     *              next:  { href: 'page=2&size=10' },
     *              prev:  { href: 'page=0&size=10' },
     *              last:  { href: 'page=5&size=10' },
     *              first: { href: 'page=0&size=10' }
     *         }
     */
    fun <T> generatePaginationHttpHeaders(uriBuilder: UriComponentsBuilder, page: Page<T>): HttpHeaders {
        val headers = HttpHeaders()
        headers.add(HEADER_X_TOTAL_COUNT, page.totalElements.toString())
        val pageNumber = page.number
        val pageSize = page.size
        val link = StringBuilder()
        if (pageNumber < page.totalPages - 1) {
            link.append(prepareLink(uriBuilder, pageNumber + 1, pageSize, "next")).append(",")
        }
        if (pageNumber > 0) {
            link.append(prepareLink(uriBuilder, pageNumber - 1, pageSize, "prev")).append(",")
        }
        link.append(prepareLink(uriBuilder, page.totalPages - 1, pageSize, "last")).append(",").append(prepareLink(uriBuilder, 0, pageSize, "first"))
        headers.add("_links", "{ $link }")
        return headers
    }

    private fun prepareLink(uriBuilder: UriComponentsBuilder, pageNumber: Int, pageSize: Int, relType: String): String {
        return "\"$relType\": { \"href\": \"${preparePageUri(uriBuilder, pageNumber, pageSize)}\" }"
    }

    private fun preparePageUri(uriBuilder: UriComponentsBuilder, pageNumber: Int, pageSize: Int): String {
        val link = uriBuilder.replaceQueryParam("page", pageNumber.toString()).replaceQueryParam("size", pageSize.toString()).toUriString().replace(",", "%2C").replace(";", "%3B")
        return link.substringAfter("?")
    }
}
