package com.ase.tugood.userservice.application

import com.ase.tugood.userservice.application.data.MailDTO
import com.ase.tugood.userservice.application.data.UserDTO
import com.ase.tugood.userservice.application.data.UserReportDTO
import com.ase.tugood.userservice.application.data.notification.ServiceNotificationDTO
import com.ase.tugood.userservice.domain.notification.service.INotificationService
import com.ase.tugood.userservice.domain.user.service.IUserService
import com.ase.tugood.userservice.domain.userreport.service.IAbuseManagementService
import com.ase.tugood.userservice.domain.userreport.service.mapper.UserReportMapper
import com.ase.tugood.userservice.infrastructure.persistence.UserReportRepository
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class AbuseManagementService(@Autowired private val userReportRepository: UserReportRepository,
                             @Autowired private val userService: IUserService,
                             @Autowired private val notificationService: INotificationService,
                             @Autowired private val userReportMapper: UserReportMapper,
                             @Autowired private val mailService: MailService) : IAbuseManagementService {
    val logger: Logger = LoggerFactory.getLogger(javaClass)

    override fun getOrderedUserReportPage(pageable: Pageable): Flux<UserReportDTO> {
        return userReportRepository.findByOrderByCreatedDate(pageable)
            .map(userReportMapper::toDto)
            .flatMap { userReportDTO ->
                userService.getUserById(userReportDTO.userId)
                    .flatMap { userDTO ->
                        countReportsByUser(userDTO.id!!)
                            .map { count ->
                                userDTO.apply { reportCount = count }
                            }
                    }
                    .map { userDTO ->
                        userReportDTO.apply {
                            user = userDTO
                        }
                    }
            }
    }

    override fun countReports(): Mono<Long> {
        return userReportRepository.count()
    }

    override fun countReportsByUser(userId: String): Mono<Long> {
        return userReportRepository.countAllByUserId(userId)
    }

    override fun blockReportedUser(userReportId: String): Mono<UserDTO> {
        return userReportRepository.findById(userReportId)
            .flatMap { report ->
                userService.blockUserById(report.userId, true)
                    .map { user ->
                        Pair(report, user)
                    }
            }
            .doOnNext {
                when(it.second.langKey) {
                    "en" -> sendEmailToUser(it.second, "TUgood: You have been blocked", "Cause of illicit behaviour you have been blocked from TUgood.")
                    "de" -> sendEmailToUser(it.second, "TUgood: Du wurdest blockiert", "Wegen widrigem Verhalten wurdest du in TUgood blockiert.")
                }

            }
            .doOnNext {
                userReportRepository.deleteAllByUserId(it.second.id!!).subscribe()
            }
            .map { it.second }
    }

    override fun warnReportedUser(userReportId: String): Mono<UserDTO> {
        return userReportRepository.findById(userReportId)
            .flatMap { report ->
                userService.warnUserById(report.userId, true)
                    .map { user ->
                        Pair(report, user)
                    }
            }
            .doOnNext {
                notificationService.notify(it.second.id!!,
                    Pair("notification.warning.title", null),
                    Pair("notification.warning.body", arrayOf(it.second.firstName, it.first.cause.userReportCause)),
                    ServiceNotificationDTO.NotificationType.PUSH).ignoreElement().subscribe()
            }
            .doOnNext {
                userReportRepository.deleteById(userReportId)
            }
            .map { it.second }
    }

    override fun deleteReport(userReportId: String): Mono<UserReportDTO> {
        return userReportRepository.findById(userReportId)
            .doOnNext {
                userReportRepository.delete(it).subscribe()
            }
            .map(userReportMapper::toDto)
    }

    private fun sendEmailToUser(user: UserDTO, subject: String, message: String) {
        logger.debug("sending email to user")
        val mail = MailDTO(subject, message)
        mailService.sendMail(user, mail)
    }

}
