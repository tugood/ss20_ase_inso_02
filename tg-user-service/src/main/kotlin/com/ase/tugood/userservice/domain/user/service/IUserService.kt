package com.ase.tugood.userservice.domain.user.service

import com.ase.tugood.userservice.application.data.RatingDTO
import com.ase.tugood.userservice.application.data.UserDTO
import com.ase.tugood.userservice.infrastructure.web.model.request.UserInfo
import reactor.core.publisher.Mono

/**
 * This interface aims in providing an API that performs user operations on
 * data storage triggered by the reactive web controller
 */
interface IUserService {

    /**
     * The aim of this function is to update the data of a given user in the data storage.
     * @param id the id of the user to be updated
     * @param userDTO the new data of the user
     * @return the new user as reactive type
     */
    fun updateUser(id: String, userDTO: UserDTO): Mono<UserDTO>

    /**
     * The aim of this function is to update the data of a given user in the data storage.
     * @param userInfo the userinfo of X-USERINFO
     * @param userDTO the new data of the user
     * @return the new user as reactive type
     */
    fun updateUser(userInfo: UserInfo, userDTO: UserDTO): Mono<UserDTO>

    /**
     * Creates a rating for a user, i.e. increments ratingCount and adds rating to ratingSum
     * @param id the id of the user that was rated
     * @param ratingDTO contains rating value 1-5
     */
    fun createRating(id: String, ratingDTO: RatingDTO): Mono<UserDTO>

    /**
     * The aim of this function is to return the given user by id.
     * @param id the id of the user to be returned
     * @return the new user as reactive type
     */
    fun getUserById(id: String): Mono<UserDTO>

    /**
     * The aim of this function is to return the user specified in X-USERINFO.
     * @param userInfo the id of the user to be returned
     * @return the new user as reactive type
     */
    fun getUser(userInfo: UserInfo): Mono<UserDTO>

    /**
     * The aim of this function is to delete an user.
     * @param id the id of the user to be deleted
     */
    fun deleteUserById(id: String): Mono<Void>

    /**
     * The aim of this function is to block/unblock a user
     * @param id the id of the user to be blocked/unblocked
     * @param block if the user should be blocked or unblocked
     * @return the blocked/unblocked user on success, specified exception otherwise
     */
    fun blockUserById(id: String, block: Boolean): Mono<UserDTO>

    /**
     * The aim of this function is to warn/unwarn a user
     * @param id the id of the user to be warned/unwarned
     * @param warn if the user should be warned/unwarned
     * @return the warned/unwarned user on success, specified exception otherwise
     */
    fun warnUserById(id: String, warn: Boolean): Mono<UserDTO>
}
