package com.ase.tugood.userservice.application.data

import com.fasterxml.jackson.annotation.JsonProperty

enum class UserReportCause(val userReportCause: String) {
    @JsonProperty("FAKE_PROFILE")
    FAKE_PROFILE("FAKE_PROFILE"),

    @JsonProperty("ILLEGAL_ACTIVITY")
    ILLEGAL_ACTIVITY("ILLEGAL_ACTIVITY"),

    @JsonProperty("DISCRIMINATION")
    DISCRIMINATION("DISCRIMINATION"),

    @JsonProperty("ABUSIVE_BEHAVIOUR")
    ABUSIVE_BEHAVIOUR("ABUSIVE_BEHAVIOUR")
}
