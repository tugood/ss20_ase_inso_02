package com.ase.tugood.userservice.infrastructure.web.rest.controller

import com.ase.tugood.userservice.infrastructure.web.rest.error.Error
import com.ase.tugood.userservice.infrastructure.web.rest.error.TUGoodException
import org.slf4j.LoggerFactory
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono

@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
@ControllerAdvice
class ExceptionTranslator {
    private val LOG = LoggerFactory.getLogger(javaClass)

    @ExceptionHandler(TUGoodException::class)
    fun handleTUGoodExceptions(
        ex: TUGoodException,
        request: ServerWebExchange
    ): Mono<ResponseEntity<Error>> {
        LOG.info("The request: ${request.logPrefix} on the path: ${request.request.path} failed  with the error: ${ex.message}")
        return Mono.just(ResponseEntity(ex.error, ex.status))
    }

    /*@ExceptionHandler(MethodArgumentNotValidException::class)
    fun handleMethodArgumentNotValidException(
        ex: MethodArgumentNotValidException,
        request: ServerWebExchange
    ): Mono<ResponseEntity<Error>> {
        LOG.info("The request: ${request.logPrefix} on the path: ${request.request.path} failed  with the error: ${ex.message}")
        val error: Error = Error(message = "Field validation failed", code = HttpStatus.BAD_REQUEST.value(), type = "Validation Error")

        ex.bindingResult.fieldErrors.forEach(action = { fieldError -> error.cause.add(Error(message = "field: ${fieldError.field} failed to validate: ${fieldError.defaultMessage}", type = "Validation Error")) })
        return Mono.just(ResponseEntity(error, HttpStatus.BAD_REQUEST))
    }*/
}
