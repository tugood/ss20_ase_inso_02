<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=false; section>
  <#if section = "title">
    ${msg((realm.displayName!''))?no_esc}
  <#elseif section = "header">
    <div>
      <img class="kc-logo-wrapper" src="${url.resourcesPath}/img/tg.svg" height="150px">
    </div>
  <#elseif section = "form">
    <div id="kc-error-message">
      <p class="instruction">${message.summary}</p>
      <#if client?? && client.baseUrl?has_content>
        <p><a id="backToApplication" href="${client.baseUrl}">${msg("backToApplication")}</a></p>
      </#if>
    </div>
  </#if>
</@layout.registrationLayout>
