import {Component, Inject, isDevMode, LOCALE_ID, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {SwUpdate} from '@angular/service-worker';
import {UserService} from './core/service/user.service';
import {NotificationService} from './core/service/notification.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {interval} from 'rxjs';

@Component({
  selector: 'tg-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'TUgood';

  constructor(private titleService: Title,
              private notificationService: NotificationService,
              private swUpdate: SwUpdate,
              private userService: UserService,
              private snackBar: MatSnackBar,
              @Inject(LOCALE_ID) protected localeId: string) {
  }

  checkForUpdate() {
    if (this.swUpdate.isEnabled) {

      this.swUpdate.available.subscribe(u => {
        this.swUpdate.activateUpdate().then(e => {
          // downloaded update
          const message = 'Application has been updated';
          const action = 'Ok, Reload!';

          // prompt the user to accept update
          this.snackBar.open(message, action).onAction().subscribe(
            () => location.reload()
          );
        });
      });

      interval(6 * 60 * 60).subscribe(() => this.swUpdate.checkForUpdate()
        .then());
    }
  }

  isAdmin() {
    return this.userService.getKeycloakRoles().includes('tg-admin');
  }

  ngOnInit(): void {
    this.titleService.setTitle(this.title);
    this.notificationService.subscribeToPushNotifications();
    this.notificationService.subscribeToWebSocket();
    this.checkForUpdate();

    if (!isDevMode()) {
      this.userService.getAccount().subscribe(res => {
        if (res.langKey === null) {
          window.location.href = '/de';
          res.langKey = 'de';
          this.userService.updateAccount(res).subscribe();
        } else if (this.localeId !== res.langKey) {
          window.location.href = '/' + res.langKey;
        }
      });
    }
  }
}
