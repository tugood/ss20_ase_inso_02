import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {requesterRoutes} from './requester/requester.module';
import {settingRoutes} from './settings/settings.module';
import {pageNotFoundRoutes} from './page-not-found/page-not-found.module';
import {createRequestRoutes} from './create-request/create-request.module';
import {notificationRoutes} from './notification/notification.module';
import {CanAuthenticationGuard} from './core/guards/auth.guard';
import {helperRoutes} from './helper/helper.module';
import {reportUserRoutes} from './report-user/report-user.module';
import {abuseRoutes} from './abuse-management/abuse-management.module';

const appRoutes: Routes = [
  ...abuseRoutes,
  ...createRequestRoutes,
  ...settingRoutes,
  ...helperRoutes,
  ...requesterRoutes,
  ...notificationRoutes,
  ...reportUserRoutes,
  ...pageNotFoundRoutes,
];

appRoutes.forEach(route => route.canActivate = [CanAuthenticationGuard]);

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule],
  providers: [CanAuthenticationGuard]
})
export class AppRoutingModule {
}
