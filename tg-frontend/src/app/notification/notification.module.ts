import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NotificationComponent} from './notification/notification.component';
import {Routes} from '@angular/router';
import {MaterialModule} from '../shared/material.module';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {NotificationMainComponent} from './notification-main.component';
import {KarmaComponent} from './karma/karma.component';
import {SharedModule} from '../shared/shared.module';

export const notificationRoutes: Routes = [
  {path: 'notification', component: NotificationMainComponent},
];

@NgModule({
  declarations: [NotificationComponent, NotificationMainComponent, KarmaComponent],
  imports: [CommonModule, MaterialModule, InfiniteScrollModule, SharedModule],
})
export class NotificationModule {
}
