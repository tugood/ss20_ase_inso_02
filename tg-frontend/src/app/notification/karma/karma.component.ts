import {Component, OnInit} from '@angular/core';
import {getKarmaChangeCauseString, KarmaSummary} from '../../core/model/karmaSummary';
import {Observable} from 'rxjs';
import {KarmaService} from '../../core/service/karma.service';
import {DateTimeService} from '../../core/service/date-time.service';

@Component({
  selector: 'tg-karma',
  templateUrl: './karma.component.html',
  styleUrls: ['./karma.component.scss']
})
export class KarmaComponent implements OnInit {

  public $karmaSummary: Observable<KarmaSummary>;
  public karmaSummary: KarmaSummary;
  public isLoading = false;

  constructor(private karmaService: KarmaService,
              private dateTimeService: DateTimeService) {
  }

  ngOnInit(): void {
    this.isLoading = true;
    this.karmaService.getKarmaSummary().subscribe(summary => {
      this.karmaSummary = summary;
      this.isLoading = false;
    });
  }

  getDateAsString(timestamp: Date) {
    timestamp = new Date(timestamp);
    return this.dateTimeService.getDateString(timestamp, new Date()) + ' ' + this.dateTimeService.getTimeString(timestamp);
  }

  getCauseAsString(type: string) {
    return getKarmaChangeCauseString(type);
  }

}
