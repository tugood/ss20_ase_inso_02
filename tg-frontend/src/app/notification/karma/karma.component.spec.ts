import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {KarmaComponent} from './karma.component';
import {KarmaService} from '../../core/service/karma.service';
import {DateTimeService} from '../../core/service/date-time.service';
import {of} from 'rxjs';
import SpyObj = jasmine.SpyObj;

describe('KarmaComponent', () => {
  let component: KarmaComponent;
  let fixture: ComponentFixture<KarmaComponent>;
  let karmaService: SpyObj<KarmaService>;
  let dateTimeServiceSpy: SpyObj<DateTimeService>;
  const mockDate = new Date();

  beforeEach(async(() => {
    karmaService = jasmine.createSpyObj(['getKarmaSummary']);

    karmaService.getKarmaSummary.and.returnValue(of({
      userId: 'user1',
      karmaPoints: 12,
      karmaChangeHistory: []
    }));

    dateTimeServiceSpy = jasmine.createSpyObj('dateTimeService', ['getTimeString', 'getDateString']);

    dateTimeServiceSpy.getDateString.and.returnValue('Heute');
    dateTimeServiceSpy.getTimeString.and.returnValue(mockDate.getHours() + ':' + mockDate.getMinutes());

    TestBed.configureTestingModule({
      declarations: [KarmaComponent],
      providers: [{provide: KarmaService, useValue: karmaService}, {
        provide: DateTimeService,
        useValue: dateTimeServiceSpy
      }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KarmaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get textual description', () => {
    expect(component.getCauseAsString('INTERNAL_CANCEL_REQUEST')).toBe('Sie haben einen Task storniert.');
  });

  it('should get date as string', () => {
    const expectedResult = 'Heute ' + mockDate.getHours() + ':' + mockDate.getMinutes();
    expect(component.getDateAsString(mockDate)).toBe(expectedResult);
  });
});
