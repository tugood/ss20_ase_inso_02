import {Component, OnInit} from '@angular/core';
import {HeaderService} from '../core/service/header.service';

@Component({
  selector: 'tg-notification-main',
  templateUrl: './notification-main.component.html',
  styleUrls: ['./notification-main.component.scss']
})
export class NotificationMainComponent implements OnInit {

  readonly notificationsTab = {
    name: $localize`:@@notification.main.text.notification:Benachrichtigungen`,
    type: 'notifications'
  };
  readonly karmaTab = {name: $localize`:@@notification.main.text.karma:Karma`, type: 'karma'};

  public viewTypes = [
    this.notificationsTab,
    this.karmaTab
  ];

  constructor(private headerService: HeaderService) {
  }

  ngOnInit(): void {
    this.headerService.setHeaderState({
      showBack: false,
      text: $localize`:@@header.notifications.text:Deine Nachrichten`,
      menu: []
    });
  }

}
