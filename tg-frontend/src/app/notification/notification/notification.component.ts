import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../../core/service/notification.service';
import { DateTimeService } from '../../core/service/date-time.service';
import { Notification } from 'src/app/core/model/notification';
import * as _ from 'lodash';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'tg-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss'],
})
export class NotificationComponent implements OnInit {
  public notifications: Notification[] = [];

  public $notifications: Observable<HttpResponse<Notification[]>>;

  private lockInfinityScroll = false;

  notificationsExhausted = false;
  isLoading = false;
  isInitialLoading = true;

  constructor(
    private notificationService: NotificationService,
    private dateTimeService: DateTimeService
  ) {}

  ngOnInit(): void {
    this.notifications = [];
    this.notificationService.deleteNotifications();
    this.notifications = this.notificationService.getNotifications();
    this.notificationService.setNextPageParameters(
      this.notificationService.getInitialPageParameter()
    );
    this.loadInitial();
  }

  loadInitial() {
    // load initial page with 10 notifications
    this.$notifications = this.notificationService.loadNextNotifications();
    this.loadNotifications(this.$notifications);
  }

  onScroll() {
    if (!this.isInitialLoading) {
      const $notificationsPage = this.notificationService.loadNextNotifications();
      this.loadNotifications($notificationsPage);
    }
  }

  close(notification: Notification) {
    notification.visibility = false;
    _.delay(() => this.deleteNotificationWithDelay(notification), 500);
  }

  private loadNotifications(
    $notifications: Observable<HttpResponse<Notification[]>>
  ) {
    if (!this.lockInfinityScroll && !this.notificationsExhausted) {
      this.isLoading = true;
      this.lockInfinityScroll = true;

      $notifications.subscribe((response: HttpResponse<Notification[]>) => {
        this.setNextLink(response);

        this.isLoading = false;
        this.isInitialLoading = false;
        this.addNotifications(response);

        this.lockInfinityScroll = false;
      });
    }
  }

  private addNotifications(response: HttpResponse<Notification[]>) {
    response.body.forEach((not, index) => {
      _.delay(() => this.showNotificationWithDelay(not), index * 100);
    });
  }

  private showNotificationWithDelay(notification: Notification) {
    this.notificationService.addNotification(
      notification,
      true,
      false,
      false
    );
  }

  setNextLink(response: HttpResponse<Notification[]>) {
    if (response.headers == null || response.headers.get('_links') == null) {
      this.notificationService.setNextPageParameters(null);
      this.notificationsExhausted = true;
      return;
    }

    const linkHeader = JSON.parse(response.headers.get('_links'));
    if (linkHeader.next == null || response.body.length === 0) {
      this.notificationService.setNextPageParameters(null);
      this.notificationsExhausted = true;
    } else {
      this.notificationService.setNextPageParameters(linkHeader.next.href);
      this.notificationsExhausted = false;
    }
  }

  private deleteNotificationWithDelay(notification: Notification) {
    this.notificationService.closeNotification(notification);
    this.notifications = this.notificationService.getNotifications();
  }

  getDateString(notification: Notification) {
    return this.dateTimeService.getDateString(
      notification.timestamp,
      new Date()
    );
  }

  getTimeString(notification: Notification) {
    return (
      this.dateTimeService.getTimeString(notification.timestamp) +
      $localize`:@@notification.date.clock: Uhr`
    );
  }
}
