import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationComponent } from './notification.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SwPush } from '@angular/service-worker';
import { environment } from '../../../environments/environment';
import { NotificationService } from '../../core/service/notification.service';
import { Notification } from '../../core/model/notification';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import SpyObj = jasmine.SpyObj;

describe('NotificationComponent', () => {
  let component: NotificationComponent;
  let fixture: ComponentFixture<NotificationComponent>;
  let notificationService: SpyObj<NotificationService>;

  const globalNotification = {
    body:
      /* tslint:disable */
      "You received a notification from me because I wanted to tell you something. I don't know quite how I should tell you...",
    /* tslint:enable */
    title: 'info for you',
    id: '48df1871-a5dc-4c58-b64d-1a133d0902eb',
    icon: 'pets',
    visibility: true,
    timestamp: new Date(),
  } as Notification;

  const httpResponse = new HttpResponse<Notification[]>({
    body: [globalNotification],
    headers: new HttpHeaders().append(
      '_links',
      '{\n' +
        '    "next": {\n' +
        '        "href": "page=2&size=3"\n' +
        '    },\n' +
        '    "prev": {\n' +
        '        "href": "page=0&size=3"\n' +
        '    },\n' +
        '    "last": {\n' +
        '        "href": "page=4&size=3"\n' +
        '    },\n' +
        '    "first": {\n' +
        '        "href": "page=0&size=3"\n' +
        '    }\n' +
        '}'
    ),
    status: 200,
    statusText: '',
    url: '',
  });

  beforeEach(async(() => {
    notificationService = jasmine.createSpyObj([
      'loadNextNotifications',
      'deleteNotifications',
      'getInitialPageParameter',
      'setNextPageParameters',
      'setNextPageLink',
      'addNotification',
      'addNewNotification',
      'closeNotification',
      'getNotifications',
    ]);
    notificationService.getNotifications.and.returnValue([]);
    notificationService.loadNextNotifications.and.returnValue(
      of<HttpResponse<Notification[]>>(httpResponse)
    );

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [NotificationComponent],
      providers: [
        { provide: SwPush, useValue: {} },
        {
          provide: 'WEBPUSH_VAPID_PUBLIC_KEY',
          useValue: environment.webPushPublicKey,
        },
        { provide: NotificationService, useValue: notificationService },
      ],
    }).compileComponents();
    TestBed.overrideProvider(NotificationService, {
      useValue: notificationService,
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should delay 500ms', async(() => {
    const notification = { title: 'title', visibility: true } as Notification;

    component.close(notification);

    expect(notification.visibility).toBeFalsy();
    expect(notificationService.closeNotification).toHaveBeenCalledTimes(0); // with delay
  }));

  it('should trigger scroll', () => {
    component.isInitialLoading = false;
    component.onScroll();

    expect(notificationService.loadNextNotifications).toHaveBeenCalledTimes(2);
  });

  it('should set next link to null due to no headers set', () => {
    const mockHttpResponse = new HttpResponse<Notification[]>({
      body: [globalNotification],
      headers: null,
      status: 200,
      statusText: '',
      url: '',
    });

    component.setNextLink(mockHttpResponse);

    expect(notificationService.setNextPageParameters).toHaveBeenCalledWith(
      null
    );
    expect(component.notificationsExhausted).toBeTruthy();
  });

  it('should set next link to null due to no body provided', () => {
    const mockHttpResponse = new HttpResponse<Notification[]>({
      body: [],
      headers: new HttpHeaders().append(
        '_links',
        '{\n' +
          '    "next": {\n' +
          '        "href": "page=2&size=3"\n' +
          '    },\n' +
          '    "prev": {\n' +
          '        "href": "page=0&size=3"\n' +
          '    },\n' +
          '    "last": {\n' +
          '        "href": "page=4&size=3"\n' +
          '    },\n' +
          '    "first": {\n' +
          '        "href": "page=0&size=3"\n' +
          '    }\n' +
          '}'
      ),
      status: 200,
      statusText: '',
      url: '',
    });

    component.setNextLink(mockHttpResponse);

    expect(notificationService.setNextPageParameters).toHaveBeenCalledWith(
      null
    );
    expect(component.notificationsExhausted).toBeTruthy();
  });

  it('should get date string from notification', () => {
    globalNotification.timestamp = new Date(2020, 10, 14, 10, 10);
    expect(component.getDateString(globalNotification)).toEqual('14.11.2020');
    expect(component.getTimeString(globalNotification)).toEqual(
      globalNotification.timestamp.getHours() +
        ':' +
        globalNotification.timestamp.getMinutes() +
        ' Uhr'
    );
  });
});
