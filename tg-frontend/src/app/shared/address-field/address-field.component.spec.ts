import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {AddressFieldComponent} from './address-field.component';
import {MaterialModule} from '../material.module';
import {SharedModule} from '../shared.module';
import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import {FormControl, FormGroup, ReactiveFormsModule} from '@angular/forms';

describe('AddressFieldComponent', () => {
  let component: AddressFieldComponent;
  let fixture: ComponentFixture<AddressFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddressFieldComponent ],
      imports: [SharedModule, MaterialModule, NoopAnimationsModule, ReactiveFormsModule, BrowserAnimationsModule],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressFieldComponent);
    component = fixture.componentInstance;
    component.addressFormGroup = new FormGroup({
      address: new FormControl()
    });
    component.addressFormFieldName = 'address';
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
