import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {FormGroup} from '@angular/forms';
import { } from 'googlemaps';

@Component({
  selector: 'tg-address-field',
  templateUrl: './address-field.component.html',
  styleUrls: ['./address-field.component.scss'],
})
export class AddressFieldComponent implements OnInit, AfterViewInit {
  addressType = 'address';
  @Input() addressFormGroup: FormGroup;
  @Input() addressFormFieldName: string;
  @Output() setAddress: EventEmitter<any> = new EventEmitter();
  @ViewChild('addresstext') addresstext: any;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.getPlaceAutocomplete();
  }

  private getPlaceAutocomplete() {
    const autocomplete = new google.maps.places.Autocomplete(this.addresstext.nativeElement,
      {
        componentRestrictions: { country: 'AT' },
        types: [this.addressType]  // 'establishment' / 'address' / 'geocode'
      });
    autocomplete.setFields(['address_component', 'geometry']);
    google.maps.event.addListener(autocomplete, 'place_changed', () => {
      const place = autocomplete.getPlace();
      this.invokeEvent(place);
    });
  }

  invokeEvent(place) {
    this.setAddress.emit(place);
  }
}
