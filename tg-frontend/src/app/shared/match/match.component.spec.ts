import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MatchComponent} from './match.component';
import {MaterialModule} from '../material.module';
import {DateTimeService} from '../../core/service/date-time.service';
import {Match, MatchState} from '../../core/model/match';
import {User} from '../../core/model/user';
import {RouterTestingModule} from '@angular/router/testing';
import {TimeFrame} from '../../core/model/timeFrame';
import {Category, HelpRequest} from '../../core/model/helpRequest';
import {Address} from '../../core/model/address';
import {of} from 'rxjs';
import {FileService} from '../../core/service/file.service';
import SpyObj = jasmine.SpyObj;

describe('MatchComponent', () => {
  let component: MatchComponent;
  let fixture: ComponentFixture<MatchComponent>;
  let dateTimeService: SpyObj<DateTimeService>;
  let fileService: SpyObj<FileService>;

  beforeEach(async(() => {
    dateTimeService = jasmine.createSpyObj('dateTimeService', ['getTimeString', 'getDateString']);
    fileService = jasmine.createSpyObj('fileService', ['loadImage']);
    fileService.loadImage.and.returnValue(of('lol'));

    TestBed.configureTestingModule({
      declarations: [MatchComponent],
      imports: [MaterialModule, RouterTestingModule],
      providers: [
        {provide: DateTimeService, useValue: dateTimeService},
        {provide: FileService, useValue: fileService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchComponent);
    component = fixture.componentInstance;

    const mockAddress: Address = {
      city: 'Wien',
      streetName: 'Schwedenplatz',
      streetNumber: '100/2/27',
      postalCode: '1030',
      country: 'Austria',
      location: {
        longitude: 16.3772,
        latitude: 48.2119
      }
    };

    const mockHelpRequest: HelpRequest = {
      id: 'this_is_a_task',
      category: Category.GROCERY_SHOPPING,
      timeFrame: new TimeFrame(new Date(), new Date(new Date().getTime() + 86400000)),
      description: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut ' +
        'labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea ' +
        'rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor ' +
        'sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna ' +
        'aliquyam erat, sed diam voluptua.',
      requester: {id: 'requester_id'} as User,
      address: mockAddress
    };

    component.match = {
      id: '111',
      helpRequest: mockHelpRequest,
      helper: {id: 'helper_id', hasImage: true} as User,
      status: MatchState.ACCEPTED,
      matchingScore: 122,
      createdDate: new Date(),
      lastModifiedDate: new Date(),
      completionInfo: {
        helperRating: {rating: 5, comment: 'nice one'}
      },
    } as Match;

    component.loggedInUser = {
      id: 'requester_id'
    } as User;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return helper', () => {

    component.loggedInUser = {
      id: 'requester_id'
    } as User;
    fixture.detectChanges();

    expect(component.getOtherUser()).toEqual({id: 'helper_id', hasImage: true} as User);
  });

  it('should return requester', () => {

    component.loggedInUser = {
      id: 'helper_id'
    } as User;
    fixture.detectChanges();

    expect(component.getOtherUser()).toEqual({id: 'requester_id'} as User);
  });

  it('should load image', () => {

    component.ngOnInit();

    expect(fileService.loadImage).toHaveBeenCalledWith('helper_id');
    expect(component.imagePath).toEqual('lol');
  });

});
