import {Component, Input, OnInit} from '@angular/core';
import {DateTimeService} from '../../core/service/date-time.service';
import {KeycloakUser} from '../../core/model/user';
import {getCategoryIcon} from '../../core/model/helpRequest';
import {getTaskStateName, Match, MatchState} from '../../core/model/match';
import {FileService} from '../../core/service/file.service';
import {SafeUrl} from '@angular/platform-browser';

@Component({
  selector: 'tg-match',
  templateUrl: './match.component.html',
  styleUrls: ['./match.component.scss']
})
export class MatchComponent implements OnInit {

  @Input() match: Match;
  @Input() loggedInUser: KeycloakUser;
  imagePath?: SafeUrl;

  taskStateChipColorMap = new Map<MatchState, string>([
    [MatchState.CREATED, 'chip-created'],
    [MatchState.ACCEPTED, 'chip-accepted'],
    [MatchState.FINISHED_BY_REQUESTER, 'chip-finished'],
    [MatchState.FINISHED_BY_HELPER, 'chip-finished'],
    [MatchState.CANCELLED_BY_REQUESTER, 'chip-cancelled'],
    [MatchState.CANCELLED_BY_HELPER, 'chip-cancelled'],
    [MatchState.EXPIRED, 'chip-expired']
  ]);

  getChipClass() {
    return this.taskStateChipColorMap.get(this.match.status);
  }

  constructor(private dateTimeService: DateTimeService, private fileService: FileService) {
  }

  ngOnInit(): void {

    const otherUser = this.getOtherUser();

    if (otherUser.hasImage) {
      this.fileService.loadImage(otherUser.id).subscribe(image => {
        this.imagePath = image;
      });
    }
  }

  getTime() {
    return this.dateTimeService.getTimeString(new Date(this.match.helpRequest.timeFrame.from));
  }

  getDate() {
    return this.dateTimeService.getDateString(new Date(this.match.helpRequest.timeFrame.from), new Date());
  }

  getTaskIcon() {
    return getCategoryIcon(this.match.helpRequest.category);
  }

  getTaskState() {
    return getTaskStateName(this.match.status);
  }

  getOtherUser() {
    if (this.loggedInUser.id === this.match.helper.id) {
      return this.match.helpRequest.requester;
    }
    return this.match.helper;
  }
}
