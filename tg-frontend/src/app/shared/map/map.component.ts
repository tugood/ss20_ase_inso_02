import {Component, Input, OnInit} from '@angular/core';
import {TgLocation} from '../../core/model/tgLocation';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'tg-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  @Input() tgLocation: TgLocation;
  public urlMap: string;
  private baseUrl = 'https://maps.googleapis.com/maps/api/staticmap?';
  private staticMapConfig = 'zoom=13&size=100x75';
  private urlOpenMap: string;

  constructor() {
  }

  ngOnInit(): void {
    this.urlMap = this.baseUrl + this.staticMapConfig
      + '&markers=size:small|' + this.tgLocation.latitude + ',' + this.tgLocation.longitude
      + '&key=' + environment.googleApiKey;
    this.urlOpenMap = 'https://www.google.com/maps/search/?api=1&query='
      + this.tgLocation.latitude + ',' + this.tgLocation.longitude;
  }

  onClick() {
    window.open(this.urlOpenMap);
  }
}
