import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MapComponent} from './map.component';

describe('MapComponent', () => {
  let component: MapComponent;
  let fixture: ComponentFixture<MapComponent>;
  let windowSpy: any;

  windowSpy = {paramMap: jasmine.createSpyObj('', ['open'])};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MapComponent],
      providers: [
        {provide: window, useValue: windowSpy}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MapComponent);
    component = fixture.componentInstance;

    component.tgLocation = {
      longitude: 16.3772,
      latitude: 48.2119
    };

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open new tab', () => {
    component.onClick();
    expect(component).toBeTruthy();
  });
});
