import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MenuComponent} from './menu.component';
import {MaterialModule} from '../material.module';
import {RouterTestingModule} from '@angular/router/testing';
import {NotificationService} from '../../core/service/notification.service';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconTestingModule} from '@angular/material/icon/testing';
import SpyObj = jasmine.SpyObj;

describe('MenuComponent', () => {
  let component: MenuComponent;
  let fixture: ComponentFixture<MenuComponent>;
  let notificationService: SpyObj<NotificationService>;
  let domSanitizer: SpyObj<DomSanitizer>;

  beforeEach(async(() => {
    notificationService = jasmine.createSpyObj(['getNotifications']);
    notificationService.getNotifications.and.returnValue([]);
    domSanitizer = jasmine.createSpyObj(['bypassSecurityTrustResourceUrl']);

    TestBed.configureTestingModule({
      declarations: [MenuComponent],
      imports: [MaterialModule, RouterTestingModule, MatIconTestingModule],
      providers: [
        {provide: NotificationService, useValue: notificationService},
        {provide: DomSanitizer, useValue: domSanitizer}
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
