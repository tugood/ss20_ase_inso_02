import {Component, OnInit} from '@angular/core';
import {NotificationService} from '../../core/service/notification.service';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'tg-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {
  constructor(private notificationService: NotificationService,
              private matIconRegistry: MatIconRegistry,
              private domSanitizer: DomSanitizer) {
    this.matIconRegistry.addSvgIcon(
      'tugood',
      this.domSanitizer.bypassSecurityTrustResourceUrl('../../assets/svgs/tg-monochrome-alt.svg')
    );
  }

  ngOnInit(): void {
  }

  notificationCount(): number | boolean {
    return this.notificationService.getNotifications().length > 0
      ? this.notificationService.getNotifications().length
      : false;
  }
}
