import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {HeaderService, HeaderState} from '../../core/service/header.service';

@Component({
  selector: 'tg-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  $headerState: Observable<HeaderState>;

  constructor(private headerService: HeaderService) {
  }

  ngOnInit(): void {
    this.$headerState = this.headerService.getHeaderState();
  }
}
