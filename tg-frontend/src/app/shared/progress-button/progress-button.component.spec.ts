import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProgressButtonComponent} from './progress-button.component';
import {MaterialModule} from '../material.module';
import {SimpleChange} from '@angular/core';

describe('ProgressButtonComponent', () => {
  let component: ProgressButtonComponent;
  let fixture: ComponentFixture<ProgressButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProgressButtonComponent],
      imports: [MaterialModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call setTimeout', () => {

    component.ngOnChanges({isLoading: {previousValue: true, currentValue: false} as SimpleChange});

    expect(component.isDone).toBeTruthy();
  });
});
