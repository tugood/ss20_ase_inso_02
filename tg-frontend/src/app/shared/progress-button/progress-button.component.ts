import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'tg-progress-button',
  templateUrl: './progress-button.component.html',
  styleUrls: ['./progress-button.component.scss']
})
export class ProgressButtonComponent implements OnInit, OnChanges {

  @Input() onClick: () => {};
  @Input() text!: string;
  @Input() isLoading!: boolean;
  isDone: boolean;

  constructor() {
  }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.isLoading) {
      if (changes.isLoading.previousValue && !changes.isLoading.currentValue) {
        this.isDone = true;
        setTimeout(() => this.isDone = false, 2000);
      }
    }

  }

}
