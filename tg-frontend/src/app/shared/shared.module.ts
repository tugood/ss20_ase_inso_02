import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './header/header.component';
import {MaterialModule} from './material.module';
import {MenuComponent} from './menu/menu.component';
import {RouterModule} from '@angular/router';
import {UserImageComponent} from './user-image/user-image.component';
import {MatchComponent} from './match/match.component';
import {StarRatingComponent} from './star-rating/star-rating.component';
import {MapComponent} from './map/map.component';
import {ProgressButtonComponent} from './progress-button/progress-button.component';
import {TugoodSpinnerComponent} from './tugood-spinner/tugood-spinner.component';
import {AddressFieldComponent} from './address-field/address-field.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatChipsModule} from '@angular/material/chips';
import {HelpRequestComponent} from './help-request/help-request.component';
import {MatchDetailsComponent} from './match-details/match-details.component';
import {MatchListComponent} from './match-list/match-list.component';
import {UserRatingComponent} from './user-rating/user-rating.component';
import {RatingModule} from 'ng-starrating';
import {LimitReachedComponent} from './limit-reached/limit-reached.component';

@NgModule({
  declarations: [
    HeaderComponent,
    MenuComponent,
    UserImageComponent,
    MatchComponent,
    StarRatingComponent,
    MapComponent,
    ProgressButtonComponent,
    TugoodSpinnerComponent,
    AddressFieldComponent,
    HelpRequestComponent,
    MatchDetailsComponent,
    MatchListComponent,
    UserRatingComponent,
    LimitReachedComponent
  ],
  exports: [
    HeaderComponent,
    MenuComponent,
    MatchComponent,
    UserImageComponent,
    ProgressButtonComponent,
    TugoodSpinnerComponent,
    AddressFieldComponent,
    HelpRequestComponent,
    MatchDetailsComponent,
    MatchListComponent,
    UserRatingComponent,
    LimitReachedComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatChipsModule,
    RatingModule
  ]
})
export class SharedModule {
}
