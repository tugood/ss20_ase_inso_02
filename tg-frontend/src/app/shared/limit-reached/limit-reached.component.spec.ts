import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LimitReachedComponent} from './limit-reached.component';
import {MatDialogRef} from '@angular/material/dialog';
import {UserRatingComponent} from '../user-rating/user-rating.component';
import {MaterialModule} from '../material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterTestingModule} from '@angular/router/testing';
import {of} from 'rxjs';
import SpyObj = jasmine.SpyObj;
import Any = jasmine.Any;

describe('LimitReachedComponent', () => {
  let component: LimitReachedComponent;
  let fixture: ComponentFixture<LimitReachedComponent>;
  let dialogRef: SpyObj<MatDialogRef<UserRatingComponent>>;

  beforeEach(async(() => {

    dialogRef = jasmine.createSpyObj('dialogRef', ['beforeClosed', 'close']);
    dialogRef.beforeClosed.and.returnValue(of({} as Promise<Any>));

    TestBed.configureTestingModule({
      declarations: [LimitReachedComponent],
      imports: [
        MaterialModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        RouterTestingModule],
      providers: [
        {provide: MatDialogRef, useValue: dialogRef}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LimitReachedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
