import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'tg-limit-reached',
  templateUrl: './limit-reached.component.html',
  styleUrls: ['./limit-reached.component.scss']
})
export class LimitReachedComponent implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<LimitReachedComponent>) {
  }

  ngOnInit(): void {
  }

  onOKClick() {
    this.dialogRef.close();
  }
}
