import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UserImageComponent} from './user-image.component';
import {MaterialModule} from '../material.module';

describe('UserImageComponent', () => {
  let component: UserImageComponent;
  let fixture: ComponentFixture<UserImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserImageComponent],
      imports: [MaterialModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return background url', () => {
    expect(component.getBackgroundImageUrl({changingThisBreaksApplicationSecurity: 'lel'}))
      .toEqual('url("lel")');
  });
});
