import {Component, Input, OnInit} from '@angular/core';
import {SafeUrl} from '@angular/platform-browser';

@Component({
  selector: 'tg-user-image',
  templateUrl: './user-image.component.html',
  styleUrls: ['./user-image.component.scss']
})
export class UserImageComponent implements OnInit {

  @Input() imagePath?: SafeUrl;

  constructor() {
  }

  ngOnInit(): void {
  }

  getBackgroundImageUrl(url) {
    return `url("${url.changingThisBreaksApplicationSecurity}")`;
  }
}
