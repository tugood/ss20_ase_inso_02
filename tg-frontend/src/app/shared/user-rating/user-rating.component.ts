import {Component, Inject, OnInit} from '@angular/core';
import {StarRatingComponent} from 'ng-starrating';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Rating} from '../../core/model/rating';
import {FormBuilder, FormGroup} from '@angular/forms';

export interface UserRatingDialogData {
  role: string;
}

@Component({
  selector: 'tg-user-rating',
  templateUrl: './user-rating.component.html',
  styleUrls: ['./user-rating.component.scss']
})
export class UserRatingComponent implements OnInit {
  checkedColor = 'gold';
  uncheckedColor = 'black';
  size = '40px';
  rating = {} as Rating;
  ratingForm: FormGroup;
  role: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: UserRatingDialogData,
              public dialogRef: MatDialogRef<UserRatingComponent>,
              private formbuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.role = this.data.role;
    this.ratingForm = this.formbuilder.group({
      description: ['']
    });
  }

  onRate($event: { oldValue: number, newValue: number, starRating: StarRatingComponent }) {
    this.rating.rating = $event.newValue;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  submitRating() {
    this.rating.comment = this.ratingForm.value.description;
    this.dialogRef.close(this.rating);
  }
}
