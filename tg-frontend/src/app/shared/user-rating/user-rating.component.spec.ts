import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UserRatingComponent} from './user-rating.component';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {MaterialModule} from '../material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatchRole} from '../../core/model/match';
import {ReactiveFormsModule} from '@angular/forms';
import SpyObj = jasmine.SpyObj;

describe('UserRatingComponent', () => {
  let component: UserRatingComponent;
  let fixture: ComponentFixture<UserRatingComponent>;
  let dialogRef: SpyObj<MatDialogRef<UserRatingComponent>>;

  beforeEach(async(() => {

    dialogRef = jasmine.createSpyObj('dialogRef', ['close']);

    TestBed.configureTestingModule({
      declarations: [UserRatingComponent],
      imports: [
        MaterialModule,
        BrowserAnimationsModule,
        ReactiveFormsModule],
      providers: [
        {provide: MatDialogRef, useValue: dialogRef},
        {provide: MAT_DIALOG_DATA, useValue: {role: MatchRole.HELPER}}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserRatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set rating', () => {

    component.onRate({oldValue: null, newValue: 3, starRating: null});

    expect(component.rating.rating).toEqual(3);
  });

  it('should close on no click', () => {

    component.onNoClick();

    expect(dialogRef.close).toHaveBeenCalled();
  });

  it('should close on submit', () => {

    component.submitRating();

    expect(dialogRef.close).toHaveBeenCalled();
  });
});
