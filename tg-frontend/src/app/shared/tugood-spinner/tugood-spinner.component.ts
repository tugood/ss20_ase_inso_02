import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs';

@Component({
  selector: 'tg-tugood-spinner',
  templateUrl: './tugood-spinner.component.html',
  styleUrls: ['./tugood-spinner.component.scss']
})
export class TugoodSpinnerComponent implements OnInit {

  @Input() $request?: Observable<any>;
  error: Error;

  constructor() {
  }

  ngOnInit(): void {

    if (this.$request) {
      this.$request.subscribe(() => {
      }, (error) => {
        this.error = error;
      });
    }

  }

}
