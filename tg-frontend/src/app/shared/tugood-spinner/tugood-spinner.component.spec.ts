import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TugoodSpinnerComponent} from './tugood-spinner.component';
import {MaterialModule} from '../material.module';
import {throwError} from 'rxjs';

describe('TugoodSpinnerComponent', () => {
  let component: TugoodSpinnerComponent;
  let fixture: ComponentFixture<TugoodSpinnerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TugoodSpinnerComponent],
      imports: [MaterialModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TugoodSpinnerComponent);
    component = fixture.componentInstance;
    component.$request = throwError({message: '2', name: '3'} as Error);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should subscribe to error', () => {

    component.ngOnInit();

    expect(component.error).toEqual({message: '2', name: '3'} as Error);
  });
});
