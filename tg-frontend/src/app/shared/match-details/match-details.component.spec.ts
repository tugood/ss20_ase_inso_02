import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MatchDetailsComponent} from './match-details.component';
import {of} from 'rxjs';
import {MatchService} from '../../core/service/match.service';
import {ActivatedRoute} from '@angular/router';
import {HeaderService} from '../../core/service/header.service';
import {UserService} from '../../core/service/user.service';
import {User} from '../../core/model/user';
import {Match, MatchRole, MatchState} from '../../core/model/match';
import {Category} from '../../core/model/helpRequest';
import {MatDialog} from '@angular/material/dialog';
import {Rating} from '../../core/model/rating';
import {RouterTestingModule} from '@angular/router/testing';
import SpyObj = jasmine.SpyObj;
import any = jasmine.any;

describe('MatchDetailsComponent', () => {
  let component: MatchDetailsComponent;
  let fixture: ComponentFixture<MatchDetailsComponent>;
  let matchServiceSpy: SpyObj<MatchService>;
  let headerServiceSpy: SpyObj<HeaderService>;
  let userServiceSpy: SpyObj<UserService>;
  let routeSpy: any;
  let matDialogSpy: SpyObj<MatDialog>;

  beforeEach(async(() => {

    const match = {
      id: 'match_id',
      matchingScore: 102,
      lastModifiedDate: new Date(),
      completionInfo: {
        helperRating: {rating: 3, comment: 'lol'}
      },
      status: MatchState.FINISHED_BY_HELPER,
      createdDate: new Date(),
      helpRequest: {
        address: {
          postalCode: '1220',
          streetNumber: '23',
          streetName: 'Hausstraße',
          country: 'Austria',
          city: 'Vienna',
          location: {
            longitude: 12.3,
            latitude: 40.2
          }
        },
        description: 'hilfe pls',
        id: 'request_id',
        category: Category.ANIMAL_SITTING,
        timeFrame: {
          from: new Date()
        },
        requester: {
          id: 'requester_id',
          firstName: 'first_name_requester',
          lastName: 'last_name_requester'
        } as User
      },
      helper: {
        id: 'helper_id',
        firstName: 'first_name_helper',
        lastName: 'last_name_helper'
      } as User
    };

    matchServiceSpy = jasmine.createSpyObj('matchService', ['getMatchById', 'finishMatch']);
    matchServiceSpy.getMatchById.withArgs(any(String)).and.returnValue(of(match));
    matchServiceSpy.finishMatch.and.returnValue(of({} as Match));

    headerServiceSpy = jasmine.createSpyObj('headerService', ['setHeaderState']);
    const paramsSpy = jasmine.createSpyObj('params', ['get']);
    paramsSpy.get.withArgs('id').and.returnValue('match_id');
    routeSpy = {paramMap: of(paramsSpy)};

    userServiceSpy = jasmine.createSpyObj('userService', ['getKeycloakProfile']);
    userServiceSpy.getKeycloakProfile.and.returnValue(of({
        id: 'helper_id',
        firstName: 'Lila',
        lastName: 'Lula',
        email: 'lila.lula@mail.com',
      } as User
    ));

    const dialogRefSpy = jasmine.createSpyObj('dialogRef', ['afterClosed']);
    dialogRefSpy.afterClosed.and.returnValue(of({rating: 1, comment: '2'}));
    matDialogSpy = jasmine.createSpyObj('dialog', ['open']);
    matDialogSpy.open.and.returnValue(dialogRefSpy);

    TestBed.configureTestingModule({
      declarations: [MatchDetailsComponent],
      imports: [RouterTestingModule],
      providers: [
        {provide: MatchService, useValue: matchServiceSpy},
        {provide: HeaderService, useValue: headerServiceSpy},
        {provide: ActivatedRoute, useValue: routeSpy},
        {provide: UserService, useValue: userServiceSpy},
        {provide: MatDialog, useValue: matDialogSpy}
      ]
    })
      .compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should get match from routeparams', () => {

    component.ngOnInit();
    expect(component.match.id).toEqual('match_id');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set headerState', () => {
    component.ngOnInit();
    expect(headerServiceSpy.setHeaderState).toHaveBeenCalled();
  });

  it('should return that i am helper', () => {

    expect(component.amIHelper()).toBeTruthy();
  });

  it('should return that i am requester', () => {

    expect(component.amIRequester()).toBeFalsy();
  });

  it('should return that requester can rate', () => {

    expect(component.canRequesterRate()).toBeTruthy();
  });

  it('should return that helper cannot rate', () => {

    expect(component.canHelperRate()).toBeFalsy();
  });

  it('should finish match upon dialog close as helper', () => {

    component.openRatingDialog();

    expect(matchServiceSpy.finishMatch)
      .toHaveBeenCalledWith('match_id', {rating: 1, comment: '2'} as Rating, MatchRole.HELPER);

  });

  it('should finish match upon dialog close as requester', () => {

    component.loggedInUser.id = 'requester_id';

    fixture.detectChanges();

    component.openRatingDialog();

    expect(matchServiceSpy.finishMatch)
      .toHaveBeenCalledWith('match_id', {rating: 1, comment: '2'} as Rating, MatchRole.REQUESTER);
  });

  it('should create menu items with cancel as helper', () => {
    component.match.status = MatchState.ACCEPTED;
    const menuItems = component.createMenuItems();

    expect(menuItems[0].name).toEqual('Benutzer melden');
    expect(menuItems[1].name).toEqual('Abbrechen');
  });

  it('should create menu items with cancel as requester', () => {
    component.match.status = MatchState.ACCEPTED;
    component.loggedInUser.id = 'requester_id';
    const menuItems = component.createMenuItems();

    expect(menuItems[0].name).toEqual('Benutzer melden');
    expect(menuItems[1].name).toEqual('Abbrechen');
  });

  it('shoul create menu items with revitalize as requester', () => {
    component.match.status = MatchState.CANCELLED_BY_HELPER;
    component.loggedInUser.id = 'requester_id';
    const menuItems = component.createMenuItems();

    expect(menuItems[0].name).toEqual('Benutzer melden');
    expect(menuItems[1].name).toEqual('Revitalisieren');
  });
});
