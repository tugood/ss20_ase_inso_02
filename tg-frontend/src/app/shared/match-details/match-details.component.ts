import {Component, OnInit} from '@angular/core';
import {HeaderService} from '../../core/service/header.service';
import {MatchService} from '../../core/service/match.service';
import {Match, MatchRole, MatchState} from '../../core/model/match';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {KeycloakUser} from '../../core/model/user';
import {UserService} from '../../core/service/user.service';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import {UserRatingComponent} from '../user-rating/user-rating.component';
import {Rating} from '../../core/model/rating';
import {CreateRequestService} from '../../core/service/create-request.service';
import {flatMap, map, tap} from 'rxjs/operators';

@Component({
  selector: 'tg-match-details',
  templateUrl: './match-details.component.html',
  styleUrls: ['./match-details.component.scss']
})
export class MatchDetailsComponent implements OnInit {

  private matchId: string;
  public loggedInUser: KeycloakUser;
  public match: Match;
  private rating: Rating;
  private dialogRef: MatDialogRef<UserRatingComponent>;

  constructor(private headerService: HeaderService,
              private matchService: MatchService,
              private userService: UserService,
              private route: ActivatedRoute,
              private router: Router,
              private location: Location,
              private dialog: MatDialog,
              private createRequestService: CreateRequestService
  ) {
  }

  ngOnInit(): void {
    this.route.paramMap.pipe(map(params => {
        this.matchId = params.get('id');
        return this.matchId;
      })
      , flatMap(matchId => {
        return this.matchService.getMatchById(matchId);
      })
      , tap(match => {
          this.match = match;
          this.headerService.setHeaderState({
            showBack: true, showBackCallback: (() => this.location.back()).bind(this),
            text: $localize`:@@header.match.details:Aufgaben-Details`,
            menu: this.createMenuItems()
          });
        }
      )
    ).subscribe();

    this.userService.getKeycloakProfile().subscribe(user => {
      this.loggedInUser = user;
    });
  }

  openRatingDialog() {
    let matchRole = MatchRole.HELPER;
    if (this.loggedInUser.id === this.match.helpRequest.requester.id) {
      matchRole = MatchRole.REQUESTER;
    }
    this.dialogRef = this.dialog.open(UserRatingComponent, {data: {role: matchRole}});
    this.dialogRef.afterClosed().subscribe(data => {
      if (data) {
        this.rating = data;
        this.matchService.finishMatch(this.matchId, this.rating, matchRole).subscribe(match =>
          this.match = match
        );
      }
    });
  }

  amIHelper() {
    return this.match.helper.id === this.loggedInUser.id;
  }

  amIRequester() {
    return this.match.helpRequest.requester.id === this.loggedInUser.id;
  }

  canHelperRate() {
    return this.match.status === MatchState.ACCEPTED;
  }

  canRequesterRate() {
    return this.match.status === MatchState.FINISHED_BY_HELPER;
  }

  canRequesterRevitalize() {
    return this.match.status === MatchState.CANCELLED_BY_HELPER;
  }

  createMenuItems() {
    const menuItems = [
      {
        name: $localize`:@@header.match.details.menu.report:Benutzer melden`,
        callback: () => {
          if (this.amIHelper()) {
            this.router.navigate(['/report-user/', this.match.helpRequest.requester.id]).then();
          } else {
            this.router.navigate(['/report-user/', this.match.helper.id]).then();
          }
        },
      }
    ];
    if (this.canCancel()) {
      menuItems.push({
        name: $localize`:@@header.match.details.menu.cancel:Abbrechen`,
        callback: () => {
          if (this.amIHelper()) {
            this.matchService.cancelMatch(this.matchId, MatchRole.HELPER).subscribe(res => {
              this.match = res;
            });
          } else {
            this.matchService.cancelMatch(this.matchId, MatchRole.REQUESTER).subscribe(res => {
              this.match = res;
            });
          }
        },
      });
    }
    if (this.canRequesterRevitalize() && this.amIRequester()) {
      menuItems.push(
        {
          name: $localize`:@@header.match.details.menu.cancel:Revitalisieren`,
          callback: () => {
            this.createRequestService.setHelpRequest({
              ...this.match.helpRequest,
              id: undefined,
              timeFrame: undefined
            });
            this.router.navigate(['/create-request']).then();
          },
        }
      );
    }
    return menuItems;
  }

  private canCancel() {
    return this.match.status === MatchState.ACCEPTED;
  }

}
