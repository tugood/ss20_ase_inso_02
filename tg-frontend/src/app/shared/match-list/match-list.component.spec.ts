import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MatchListComponent} from './match-list.component';
import {MaterialModule} from '../material.module';
import {Component, Input} from '@angular/core';
import {Match} from 'src/app/core/model/match';
import {RouterTestingModule} from '@angular/router/testing';
import {MatchService} from '../../core/service/match.service';
import {of} from 'rxjs';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {UserService} from '../../core/service/user.service';
import SpyObj = jasmine.SpyObj;

@Component({
  selector: 'tg-task',
  template: '<p>Mock Task</p>'
})
class TaskComponent {
  @Input() taskInfo: Match;
}

describe('MatchListComponent', () => {
  let component: MatchListComponent;
  let fixture: ComponentFixture<MatchListComponent>;
  let matchService: SpyObj<MatchService>;
  let userService: SpyObj<UserService>;

  beforeEach(async(() => {

    matchService = jasmine.createSpyObj('matchService', ['getMatchesByRole']);
    userService = jasmine.createSpyObj('userService', ['getKeycloakProfile']);

    matchService.getMatchesByRole.and.returnValue(of([]));
    userService.getKeycloakProfile.and.returnValue(of({
      id: 'helper_id',
      firstName: 'good',
      lastName: 'guy',
      email: 'lalals@l.com'
    }));

    TestBed.configureTestingModule({
      declarations: [MatchListComponent, TaskComponent],
      imports: [MaterialModule, RouterTestingModule, BrowserAnimationsModule],
      providers: [
        {provide: MatchService, useValue: matchService},
        {provide: UserService, useValue: userService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
