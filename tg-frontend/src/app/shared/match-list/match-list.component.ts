import {Component, Input, OnInit} from '@angular/core';
import {Match, MatchRole, MatchType} from '../../core/model/match';
import {MatchService} from '../../core/service/match.service';
import {Observable} from 'rxjs';
import {KeycloakUser} from '../../core/model/user';
import {UserService} from '../../core/service/user.service';

@Component({
  selector: 'tg-match-list',
  templateUrl: './match-list.component.html',
  styleUrls: ['./match-list.component.scss'],
})
export class MatchListComponent implements OnInit {

  @Input() matchRole: MatchRole;
  @Input() emptyText: string;
  @Input() historyText: string;

  public $loggedInUser: Observable<KeycloakUser>;
  public loggedInUser: KeycloakUser;
  public matchList: Match[];
  public $matchList: Observable<Match[]>;
  public historicList: Match[];
  public $historicList: Observable<Match[]>;

  constructor(private matchService: MatchService, private userService: UserService) {
  }

  ngOnInit(): void {
    this.$matchList = this.matchService.getMatchesByRole(this.matchRole, MatchType.ACTIVE, true);
    this.$matchList.subscribe((res) => {
      this.matchList = res;
    });

    this.$historicList = this.matchService.getMatchesByRole(this.matchRole, MatchType.HISTORIC, false);
    this.$historicList.subscribe((res) => {
      this.historicList = res;
    });

    this.$loggedInUser = this.userService.getKeycloakProfile();
    this.$loggedInUser.subscribe(user =>
      this.loggedInUser = user
    );
  }
}
