import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HelpRequestComponent} from './help-request.component';
import {SharedModule} from '../shared.module';
import {MaterialModule} from '../material.module';
import {DateTimeService} from '../../core/service/date-time.service';
import {RouterTestingModule} from '@angular/router/testing';
import {TimeFrame} from '../../core/model/timeFrame';
import {Category} from '../../core/model/helpRequest';
import {Address} from '../../core/model/address';
import {User} from '../../core/model/user';
import SpyObj = jasmine.SpyObj;

describe('HelpRequestComponent', () => {
  let component: HelpRequestComponent;
  let fixture: ComponentFixture<HelpRequestComponent>;
  let dateTimeService: SpyObj<DateTimeService>;

  beforeEach(async(() => {
    dateTimeService = jasmine.createSpyObj('dateTimeService', ['getTimeString', 'getDateString']);

    TestBed.configureTestingModule({
      declarations: [HelpRequestComponent],
      imports: [SharedModule, MaterialModule, RouterTestingModule],
      providers: [
        {provide: DateTimeService, useValue: dateTimeService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpRequestComponent);
    component = fixture.componentInstance;

    const mockAddress: Address = {
      city: 'Wien',
      streetName: 'Schwedenplatz',
      streetNumber: '100/2/27',
      postalCode: '1030',
      country: 'Austria',
      location: {
        longitude: 16.3772,
        latitude: 48.2119
      }
    };

    component.helpRequest = {
      id: 'this_is_a_task',
      category: Category.GROCERY_SHOPPING,
      timeFrame: new TimeFrame(new Date(), new Date(new Date().getTime() + 86400000)),
      description: 'bla bla',
      requester: {id: 'this_is_an_user_id'} as User,
      address: mockAddress
    };

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set imagePath correctly', () => {
    expect(component).toBeTruthy();
  });

});
