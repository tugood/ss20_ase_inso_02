import {Component, Input, OnInit} from '@angular/core';
import {DateTimeService} from '../../core/service/date-time.service';
import {getCategoryIcon, HelpRequest} from '../../core/model/helpRequest';

@Component({
  selector: 'tg-help-request',
  templateUrl: './help-request.component.html',
  styleUrls: ['./help-request.component.scss']
})
export class HelpRequestComponent implements OnInit {

  @Input() helpRequest: HelpRequest;

  constructor(private dateTimeService: DateTimeService) {
  }

  ngOnInit(): void {
  }

  getTime() {
    return this.dateTimeService.getTimeString(new Date(this.helpRequest.timeFrame.from));
  }

  getDate() {
    return this.dateTimeService.getDateString(new Date(this.helpRequest.timeFrame.from), new Date());
  }

  getTaskIcon() {
    return getCategoryIcon(this.helpRequest.category);
  }

}
