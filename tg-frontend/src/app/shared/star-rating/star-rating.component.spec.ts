import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {StarRatingComponent} from './star-rating.component';
import {MaterialModule} from '../material.module';

describe('StarRatingComponent', () => {
  let component: StarRatingComponent;
  let fixture: ComponentFixture<StarRatingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StarRatingComponent],
      imports: [MaterialModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StarRatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show 3.5 stars', () => {

    component.rating = 3.5;
    fixture.detectChanges();
    const nativeElement: HTMLElement = fixture.debugElement.nativeElement;
    expect(nativeElement.querySelectorAll('.star').length).toEqual(3);
    expect(nativeElement.querySelector('#halfStar')).toBeTruthy();
  });

  it('should show 1.5 stars', () => {

    component.rating = 1.1;
    fixture.detectChanges();
    const nativeElement: HTMLElement = fixture.debugElement.nativeElement;
    expect(nativeElement.querySelectorAll('.star').length).toEqual(1);
    expect(nativeElement.querySelector('#halfStar')).toBeTruthy();
  });

  it('should show 5 stars', () => {

    component.rating = 5;
    fixture.detectChanges();
    const nativeElement: HTMLElement = fixture.debugElement.nativeElement;
    expect(nativeElement.querySelectorAll('.star').length).toEqual(5);
    expect(nativeElement.querySelector('#halfStar')).toBeFalsy();
  });

  it('should show 0 stars', () => {

    component.rating = 0;
    fixture.detectChanges();
    const nativeElement: HTMLElement = fixture.debugElement.nativeElement;
    expect(nativeElement.querySelectorAll('.star').length).toEqual(0);
    expect(nativeElement.querySelector('#halfStar')).toBeFalsy();
  });

  it('should show 0.5 stars', () => {

    component.rating = 0.9;
    fixture.detectChanges();
    const nativeElement: HTMLElement = fixture.debugElement.nativeElement;
    expect(nativeElement.querySelectorAll('.star').length).toEqual(0);
    expect(nativeElement.querySelector('#halfStar')).toBeTruthy();
  });

  it('should show 5 stars with', () => {

    component.rating = 9.9;
    fixture.detectChanges();
    const nativeElement: HTMLElement = fixture.debugElement.nativeElement;
    expect(nativeElement.querySelectorAll('.star').length).toEqual(5);
    expect(nativeElement.querySelector('#halfStar')).toBeFalsy();
  });

});
