import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'tg-star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss']
})
export class StarRatingComponent implements OnInit {

  @Input() rating?: number;
  readonly integerThresholds = [1, 2, 3, 4, 5];
  readonly halfStarThreshold = 0.1;

  constructor() {
  }

  ngOnInit(): void {
  }

  getRatingsLarger() {
    return this.integerThresholds.filter(threshold => threshold <= this.rating);
  }

  getRatingString() {
    return (Math.round(this.rating * 10) / 10).toLocaleString();
  }
}
