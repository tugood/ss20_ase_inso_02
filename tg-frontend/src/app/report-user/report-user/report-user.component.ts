import {Component, OnInit} from '@angular/core';
import {HeaderService} from '../../core/service/header.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {getUserReportCauseName, UserReport, UserReportCause} from '../../core/model/userReport';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../core/service/user.service';

interface Cause {
  value: UserReportCause;
  viewValue: string;
}

@Component({
  selector: 'tg-report-user',
  templateUrl: './report-user.component.html',
  styleUrls: ['./report-user.component.scss']
})
export class ReportUserComponent implements OnInit {

  userReportCauses: Cause[] = [
    {value: UserReportCause.FAKE_PROFILE, viewValue: getUserReportCauseName(UserReportCause.FAKE_PROFILE)},
    {value: UserReportCause.ILLEGAL_ACTIVITY, viewValue: getUserReportCauseName(UserReportCause.ILLEGAL_ACTIVITY)},
    {value: UserReportCause.DISCRIMINATION, viewValue: getUserReportCauseName(UserReportCause.DISCRIMINATION)},
    {value: UserReportCause.ABUSIVE_BEHAVIOUR, viewValue: getUserReportCauseName(UserReportCause.ABUSIVE_BEHAVIOUR)}
  ];

  reportUserForm: FormGroup;
  userId: string;

  constructor(private headerService: HeaderService,
              private userService: UserService,
              private formBuilder: FormBuilder,
              private location: Location,
              private route: ActivatedRoute) {

    this.headerService.setHeaderState({
      showBack: true,
      showBackCallback: (() => this.location.back()).bind(this),
      text: $localize`:@@header.report.user.text:Benutzer melden`,
      menu: []
    });

    this.reportUserForm = this.formBuilder.group({
      reportCause: ['', Validators.required],
      reportDescription: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.userId = params.get('id');
    });
  }

  submitUserReport() {
    const userReport = {
      userId: this.userId,
      cause: this.reportUserForm.value.reportCause,
      description: this.reportUserForm.value.reportDescription
    } as UserReport;

    this.userService.reportUser(userReport).subscribe(report => {
      console.log(report);
    });
  }

}
