import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ReportUserComponent} from './report-user.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../../shared/material.module';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {UserService} from '../../core/service/user.service';
import {of} from 'rxjs';
import {UserReport, UserReportCause} from '../../core/model/userReport';
import {ActivatedRoute} from '@angular/router';
import SpyObj = jasmine.SpyObj;

describe('ReportUserComponent', () => {
  let component: ReportUserComponent;
  let fixture: ComponentFixture<ReportUserComponent>;
  let userServiceSpy: SpyObj<UserService>;
  let routeSpy: any;

  beforeEach(async(() => {

    userServiceSpy = jasmine.createSpyObj('userService', ['reportUser']);

    const paramsSpy = jasmine.createSpyObj('params', ['get']);
    paramsSpy.get.withArgs('id').and.returnValue('user_id');
    routeSpy = {paramMap: of(paramsSpy)};

    userServiceSpy.reportUser.and.returnValue(of({
      id: 'user_report_id',
      userId: 'user_id',
      cause: UserReportCause.FAKE_PROFILE,
      description: 'This profile is unreal'
    } as UserReport));

    TestBed.configureTestingModule({
      declarations: [ReportUserComponent],
      imports: [ReactiveFormsModule, MaterialModule, NoopAnimationsModule],
      providers: [{provide: UserService, useValue: userServiceSpy},
        {provide: ActivatedRoute, useValue: routeSpy}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should submit user report', () => {
    component.submitUserReport();

    expect(userServiceSpy.reportUser).toHaveBeenCalled();
  });
});
