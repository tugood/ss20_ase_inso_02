import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReportUserComponent} from './report-user/report-user.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../shared/shared.module';
import {MaterialModule} from '../shared/material.module';
import {ReactiveFormsModule} from '@angular/forms';

export const reportUserRoutes: Routes = [
  {path: 'report-user/:id', component: ReportUserComponent},
];

@NgModule({
  declarations: [ReportUserComponent],
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    ReactiveFormsModule,
    RouterModule,
  ]
})
export class ReportUserModule {
}
