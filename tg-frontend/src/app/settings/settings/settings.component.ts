import {Component, OnInit} from '@angular/core';
import {HeaderService} from '../../core/service/header.service';
import {MatDialog} from '@angular/material/dialog';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../core/service/user.service';
import {User} from '../../core/model/user';
import {Observable, of} from 'rxjs';
import {FileService} from '../../core/service/file.service';
import {KeycloakService} from 'keycloak-angular';
import {NotificationService} from '../../core/service/notification.service';
import {MatSliderChange} from '@angular/material/slider';
import {Router} from '@angular/router';

@Component({
  selector: 'tg-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {
  accountForm: FormGroup;
  user$: Observable<User>;
  user: User;
  isLoading = false;
  minDistance = 1;
  maxDistance = 30;
  defaultDistance = 10;
  selectedCountryCode = 'at';
  countryCodes = ['at', 'gb'];
  languageChanged = false;

  constructor(
    private headerService: HeaderService,
    private accountService: UserService,
    private fileService: FileService,
    public dialog: MatDialog,
    private formBuilder: FormBuilder,
    private keycloak: KeycloakService,
    private router: Router,
    private notificationService: NotificationService
  ) {
    this.accountForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phoneNr: [
        '',
        Validators.pattern('^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$'),
      ],
      description: [''],
    });
  }

  ngOnInit(): void {
    this.headerService.setHeaderState({
      showBack: false,
      text: $localize`:@@header.settings:Einstellungen`,
      menu: [{
        name: $localize`:@@menu.logout:Abmelden`,
        callback: () => this.keycloak.logout(window.location.protocol + '//' + window.location.host + '/')
      }],
    });

    this.user$ = this.accountService.getAccount();

    this.user$.subscribe((user) => {
      this.user = user;
      this.accountForm.patchValue({
        firstName: user.firstName,
        lastName: user.lastName,
        phoneNr: user.phoneNr,
        description: user.description,
      });
      if (user.distance) {
        this.user.distance = user.distance;
      } else {
        this.user.distance = this.defaultDistance;
      }

      this.selectedCountryCode = this.user.langKey === 'en' ? 'gb' : 'at';
    });
  }

  changeSelectedCountryCode(value: string): void {
    if (this.selectedCountryCode === value) {
      return;
    }
    this.selectedCountryCode = value;
    let langKeyRoute: string;
    if (value === 'gb') {
      langKeyRoute = 'en';
    } else {
      langKeyRoute = 'de';
    }

    this.user.langKey = langKeyRoute;
    this.languageChanged = true;
  }

  updateUser() {
    this.isLoading = true;

    if (this.fileService.wasImageSelected()) {
      this.user.hasImage = true;
    }

    this.user.firstName = this.accountForm.value.firstName;
    this.user.lastName = this.accountForm.value.lastName;
    this.user.phoneNr = this.accountForm.value.phoneNr;
    this.user.description = this.accountForm.value.description;

    this.accountService.updateAccount(this.user).subscribe((res) => {
      this.uploadImage().subscribe((_) => {
        this.isLoading = false;

        if (this.languageChanged) {
          window.location.href = '/' + this.user.langKey + this.router.url;
          this.languageChanged = false;
        }

        this.notificationService.showSnackbar(
          {
            id: this.notificationService.noId(),
            title: $localize`:@@notification.settings.profile.title:Profil aktualisiert`,
            body: $localize`:@@notification.settings.profile:Profil erfolgreich aktualisiert`,
            timestamp: new Date(),
            visibility: true,
          }
        );
      });
    });
  }

  uploadImage() {
    if (this.fileService.wasImageSelected()) {
      return this.fileService.uploadSelectedImage();
    }
    return of({});
  }

  distanceLabel(value: number) {
    return value + 'km';
  }

  onSlideChange(event: MatSliderChange) {
    this.user.distance = event.value;
  }
}
