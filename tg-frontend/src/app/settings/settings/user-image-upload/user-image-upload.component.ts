import {Component, Input, OnInit} from '@angular/core';
import {FileService} from '../../../core/service/file.service';
import {User} from '../../../core/model/user';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';

@Component({
  selector: 'tg-user-image-upload',
  templateUrl: './user-image-upload.component.html',
  styleUrls: ['./user-image-upload.component.scss']
})
export class UserImageUploadComponent implements OnInit {

  @Input() user: User;
  imagePath: SafeUrl;
  isLoading = false;

  constructor(private fileService: FileService, private sanitizer: DomSanitizer) {
  }

  ngOnInit(): void {
    if (this.user.hasImage) {
      this.fileService.loadImage(this.user.id).subscribe(image => {
        this.imagePath = image;
      });
    }
  }

  processFile(imageInput: any) {
    const file: File = imageInput.files[0];
    const reader = new FileReader();

    this.isLoading = true;

    reader.addEventListener('load', (event: any) => this.handleLoad(event, file));

    reader.readAsDataURL(file);
  }

  handleLoad(event: any, file: File) {
    this.fileService.setSelectedImage(file.name, event.target.result);
    this.imagePath = this.sanitizer.bypassSecurityTrustResourceUrl(event.target.result); // base64 encoded preview
    this.isLoading = false;
  }
}
