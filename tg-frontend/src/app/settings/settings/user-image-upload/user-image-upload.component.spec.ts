import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UserImageUploadComponent} from './user-image-upload.component';
import {MaterialModule} from '../../../shared/material.module';
import {SharedModule} from '../../../shared/shared.module';
import {FileService} from '../../../core/service/file.service';
import {Authority} from '../../../core/model/authority';
import {User, UserState} from '../../../core/model/user';
import {of} from 'rxjs';
import SpyObj = jasmine.SpyObj;

describe('ImageUploadComponent', () => {
  let component: UserImageUploadComponent;
  let fixture: ComponentFixture<UserImageUploadComponent>;
  let fileServiceSpy: SpyObj<FileService>;

  beforeEach(async(() => {

    fileServiceSpy = jasmine.createSpyObj('fileService', ['setSelectedImage', 'loadImage']);
    fileServiceSpy.loadImage.and.returnValue(of('lel'));

    TestBed.configureTestingModule({
      declarations: [UserImageUploadComponent],
      imports: [MaterialModule, SharedModule],
      providers: [{provide: FileService, useValue: fileServiceSpy}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserImageUploadComponent);
    component = fixture.componentInstance;
    component.user = {
      id: '100',
      authorities: [Authority.USER],
      firstName: 'Lila',
      lastName: 'Lula',
      phoneNr: '067678456546',
      email: 'lila.lula@mail.com',
      userState: UserState.ACTIVE,
      hasImage: false
    } as User;
    fixture.detectChanges();
  });

  it('should create', () => {

    expect(component).toBeTruthy();
  });

  it('should set image path correctly', () => {
    component.user.hasImage = true;
    component.ngOnInit();
    expect(fileServiceSpy.loadImage).toHaveBeenCalledWith(component.user.id);
    expect(component.imagePath).toEqual('lel');
  });

  it('should handle file change correctly', () => {

    component.isLoading = true;
    const event = {target: {result: 'lol'}};
    const file = {name: 'name1'} as File;

    component.handleLoad(event, file);

    expect(fileServiceSpy.setSelectedImage).toHaveBeenCalledWith('name1', 'lol');
    expect(component.isLoading).toBeFalsy();
  });

});
