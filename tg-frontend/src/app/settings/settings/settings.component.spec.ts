import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {SettingsComponent} from './settings.component';
import {SharedModule} from '../../shared/shared.module';
import {MatDialog} from '@angular/material/dialog';
import {MaterialModule} from '../../shared/material.module';
import {ReactiveFormsModule} from '@angular/forms';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {UserService} from '../../core/service/user.service';
import {of} from 'rxjs';
import {Authority} from '../../core/model/authority';
import {User, UserState} from '../../core/model/user';
import {FileService} from '../../core/service/file.service';
import {Component, Input} from '@angular/core';
import {KeycloakService} from 'keycloak-angular';
import {NotificationService} from '../../core/service/notification.service';
import {RouterTestingModule} from '@angular/router/testing';
import SpyObj = jasmine.SpyObj;

@Component({
  selector: 'tg-user-image-upload',
  template: '<p>Mock User Image Upload</p>'
})
class UserImageUploadComponent {
  @Input() user: User;
}

describe('SettingsComponent', () => {
  let component: SettingsComponent;
  let fixture: ComponentFixture<SettingsComponent>;
  let userService: SpyObj<UserService>;
  let fileService: SpyObj<FileService>;
  let notificationService: SpyObj<NotificationService>;

  beforeEach(async(() => {

    userService = jasmine.createSpyObj('userService', ['getAccount', 'updateAccount']);
    userService.getAccount.and.returnValue(of({
      id: '100',
      authorities: [Authority.USER],
      description: 'aa',
      firstName: 'Lila',
      lastName: 'Lula',
      phoneNr: '067678456546',
      email: 'lila.lula@mail.com',
      distance: 10,
      userState: UserState.ACTIVE,
      hasImage: false
    } as User));

    userService.updateAccount.and.returnValue(of());

    fileService = jasmine.createSpyObj('fileService', ['wasImageSelected']);
    notificationService = jasmine.createSpyObj(['addNewNotification']);

    fileService.wasImageSelected.and.returnValue(false);

    TestBed.configureTestingModule({
      declarations: [SettingsComponent, UserImageUploadComponent],
      imports: [SharedModule, MaterialModule, NoopAnimationsModule, ReactiveFormsModule, RouterTestingModule],
      providers: [
        {provide: MatDialog, useValue: {}},
        {provide: UserService, useValue: userService},
        {provide: FileService, useValue: fileService},
        {provide: NotificationService, useValue: notificationService},
        KeycloakService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('updateUser should get the form values', () => {

    component.accountForm.patchValue({
      firstName: 'user.firstName',
      lastName: 'user.lastName',
      phoneNr: 'user.phoneNr',
      description: 'user.description',
      distance: 10
    });

    component.updateUser();

    expect(userService.updateAccount).toHaveBeenCalledWith({
      id: '100',
      authorities: [Authority.USER],
      firstName: 'user.firstName',
      lastName: 'user.lastName',
      phoneNr: 'user.phoneNr',
      description: 'user.description',
      email: 'lila.lula@mail.com',
      distance: 10,
      hasImage: false,
      userState: UserState.ACTIVE
    } as User);
  });

  it('updateUser should set hasImage', () => {

    fileService.wasImageSelected.and.returnValue(true);

    component.updateUser();

    expect(userService.updateAccount).toHaveBeenCalledWith({
      id: '100',
      authorities: [Authority.USER],
      firstName: 'Lila',
      lastName: 'Lula',
      phoneNr: '067678456546',
      email: 'lila.lula@mail.com',
      description: 'aa',
      distance: 10,
      userState: UserState.ACTIVE,
      hasImage: true
    } as User);
  });

  it('should change selected country code', () => {
    component.changeSelectedCountryCode('gb');

    expect(component.languageChanged).toBeTruthy();
    expect(component.user.langKey).toBe('en');
  });

  it('should change distance label', () => {
    expect(component.distanceLabel(10)).toBe('10km');
  });

  it('upload image with no image selected', () => {
    component.uploadImage().subscribe(result => {
      expect(result).toEqual({});
    });
  });

});
