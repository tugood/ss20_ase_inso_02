import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SettingsComponent} from './settings/settings.component';
import {Routes} from '@angular/router';
import {SharedModule} from '../shared/shared.module';
import {MaterialModule} from '../shared/material.module';
import {UserImageUploadComponent} from './settings/user-image-upload/user-image-upload.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatSliderModule} from '@angular/material/slider';
import {NgxFlagPickerModule} from 'ngx-flag-picker';

export const settingRoutes: Routes = [
  {path: 'settings', component: SettingsComponent}
];

@NgModule({
  declarations: [SettingsComponent, UserImageUploadComponent],
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    ReactiveFormsModule,
    MatSliderModule,
    NgxFlagPickerModule
  ]
})
export class SettingsModule {
}
