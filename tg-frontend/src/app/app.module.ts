import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppComponent} from './app.component';
import {CoreModule} from './core/core.module';
import {AppRoutingModule} from './app-routing.module';
import {RequesterModule} from './requester/requester.module';
import {BrowserModule} from '@angular/platform-browser';
import {SharedModule} from './shared/shared.module';
import {SettingsModule} from './settings/settings.module';
import {PageNotFoundModule} from './page-not-found/page-not-found.module';
import {CreateRequestModule} from './create-request/create-request.module';
import {KeycloakAngularModule, KeycloakService} from 'keycloak-angular';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {initializer} from './core/keycloak.init';
import {ServiceWorkerModule} from '@angular/service-worker';
import {NotificationService} from './core/service/notification.service';
import {environment} from '../environments/environment';
import {registerLocaleData} from '@angular/common';
import localeEn from '@angular/common/locales/en';
import localeDe from '@angular/common/locales/de';
import {ReportUserModule} from './report-user/report-user.module';
import {AbuseManagementModule} from './abuse-management/abuse-management.module';

registerLocaleData(localeEn);
registerLocaleData(localeDe);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    AppRoutingModule,
    CoreModule,
    RequesterModule,
    SettingsModule,
    CreateRequestModule,
    PageNotFoundModule,
    SharedModule,
    ServiceWorkerModule.register('ngsw-worker.js', {enabled: environment.production}),
    KeycloakAngularModule,
    ReportUserModule,
    AbuseManagementModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      multi: true,
      deps: [KeycloakService]
    },
    {
      provide: 'WEBPUSH_VAPID_PUBLIC_KEY',
      useValue: environment.webPushPublicKey
    },
    NotificationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
