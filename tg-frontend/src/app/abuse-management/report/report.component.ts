import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {getUserReportCauseName, UserReport, UserReportCause} from '../../core/model/userReport';
import {AbuseManagementService} from '../../core/service/abuse-management.service';
import {Observable} from 'rxjs';
import {User} from '../../core/model/user';

@Component({
  selector: 'tg-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {

  @Input() report: UserReport;
  @Output() reportChanged = new EventEmitter<any>();

  private $warned: Observable<User>;
  private $banned: Observable<User>;
  private $deleted: Observable<UserReport>;

  reportCauseChipColorMap = new Map<UserReportCause, string>([
    [UserReportCause.ABUSIVE_BEHAVIOUR, 'chip-abusive'],
    [UserReportCause.DISCRIMINATION, 'chip-discrimination'],
    [UserReportCause.FAKE_PROFILE, 'chip-fake'],
    [UserReportCause.ILLEGAL_ACTIVITY, 'chip-illegal'],
  ]);

  constructor(
    private abuseManagementService: AbuseManagementService,
  ) {
  }

  ngOnInit(): void {
    console.log(this.report);
  }

  chipClass(): string {
    return this.reportCauseChipColorMap.get(this.report.cause);
  }

  getReportCause(): string {
    return getUserReportCauseName(this.report.cause);
  }

  warnUser() {
    this.$warned = this.abuseManagementService.warnUser(this.report.id);
    this.$warned.subscribe(res => {
      this.reportChanged.emit(res);
    });
  }

  banUser() {
    this.$banned = this.abuseManagementService.banUser(this.report.id);
    this.$banned.subscribe(res => {
      this.reportChanged.emit(res);
    });
  }

  deleteReport() {
    this.$deleted = this.abuseManagementService.deleteReport(this.report.id);
    this.$deleted.subscribe(res => {
      this.reportChanged.emit(res);
    });
  }

}
