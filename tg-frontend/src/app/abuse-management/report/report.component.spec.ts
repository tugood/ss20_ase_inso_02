import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ReportComponent} from './report.component';
import {UserReport, UserReportCause} from '../../core/model/userReport';
import {Authority} from '../../core/model/authority';
import {User, UserState} from '../../core/model/user';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AbuseManagementService} from '../../core/service/abuse-management.service';
import {of} from 'rxjs';
import SpyObj = jasmine.SpyObj;

describe('ReportComponent', () => {
  let component: ReportComponent;
  let fixture: ComponentFixture<ReportComponent>;
  let abuseManagementService: SpyObj<AbuseManagementService>;

  const mockUser = {
    id: 'this_is_an_id',
    firstName: 'Max',
    lastName: 'Mustermann',
    email: 'max.muster@mail.com',
    phoneNr: '+431231213123',
    authorities: [Authority.USER],
    userState: UserState.ACTIVE,
    hasImage: false,
    distance: 10,
    isWarned: false
  } as User;

  const mockUserReport = {
    id: 'user_report_id',
    userId: 'user_id',
    cause: UserReportCause.FAKE_PROFILE,
    description: 'This profile is unreal',
    user: mockUser
  } as UserReport;

  beforeEach(async(() => {
    abuseManagementService = jasmine.createSpyObj(['warnUser', 'banUser', 'deleteReport']);

    abuseManagementService.warnUser.and.returnValue(of(mockUser));
    abuseManagementService.banUser.and.returnValue(of(mockUser));
    abuseManagementService.deleteReport.and.returnValue(of(mockUserReport));

    TestBed.configureTestingModule({
      declarations: [ReportComponent],
      imports: [HttpClientTestingModule],
      providers: [{provide: AbuseManagementService, useValue: abuseManagementService}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportComponent);
    component = fixture.componentInstance;
    component.report = mockUserReport;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get right chip class', () => {
    expect(component.chipClass()).toEqual('chip-fake');
  });

  it('should get right report cause', () => {
    expect(component.getReportCause()).toEqual('Fake-Profil');
  });

  it('should warn user', () => {
    spyOn(component.reportChanged, 'emit');
    component.warnUser();

    expect(abuseManagementService.warnUser).toHaveBeenCalledWith(mockUserReport.id);
    expect(component.reportChanged.emit).toHaveBeenCalled();
  });

  it('should ban user', () => {
    spyOn(component.reportChanged, 'emit');
    component.banUser();

    expect(abuseManagementService.banUser).toHaveBeenCalledWith(mockUserReport.id);
    expect(component.reportChanged.emit).toHaveBeenCalled();
  });

  it('should delete user report', () => {
    spyOn(component.reportChanged, 'emit');
    component.deleteReport();

    expect(abuseManagementService.deleteReport).toHaveBeenCalledWith(mockUserReport.id);
    expect(component.reportChanged.emit).toHaveBeenCalled();
  });
});
