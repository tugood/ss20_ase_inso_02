import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AbuseListComponent} from './abuse-list/abuse-list.component';
import {ReportComponent} from './report/report.component';
import {MaterialModule} from '../shared/material.module';
import {SharedModule} from '../shared/shared.module';
import {MatChipsModule} from '@angular/material/chips';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatTooltipModule} from '@angular/material/tooltip';
import {Routes} from '@angular/router';

export const abuseRoutes: Routes = [
  {path: 'abuse-management', component: AbuseListComponent}
];

@NgModule({
  declarations: [AbuseListComponent, ReportComponent],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule,
    MatChipsModule,
    MatPaginatorModule,
    MatTooltipModule
  ]
})
export class AbuseManagementModule {
}
