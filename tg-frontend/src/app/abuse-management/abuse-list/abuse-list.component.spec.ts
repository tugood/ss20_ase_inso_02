import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AbuseListComponent} from './abuse-list.component';
import {AbuseManagementService} from '../../core/service/abuse-management.service';
import {KeycloakService} from 'keycloak-angular';
import {of} from 'rxjs';
import {HttpHeaders, HttpResponse} from '@angular/common/http';
import {UserReport, UserReportCause} from '../../core/model/userReport';
import SpyObj = jasmine.SpyObj;

describe('AbuseListComponent', () => {
  let component: AbuseListComponent;
  let fixture: ComponentFixture<AbuseListComponent>;
  let abuseManagementService: SpyObj<AbuseManagementService>;
  let keycloakService: SpyObj<KeycloakService>;

  const mockLinks = '{\n' +
    '    "next": {\n' +
    '        "href": "page=2&size=4"\n' +
    '    },\n' +
    '    "prev": {\n' +
    '        "href": "page=0&size=4"\n' +
    '    },\n' +
    '    "last": {\n' +
    '        "href": "page=4&size=4"\n' +
    '    },\n' +
    '    "first": {\n' +
    '        "href": "page=0&size=4"\n' +
    '    }\n' +
    '}';

  const mockResponse = new HttpResponse<UserReport[]>({
    body: [{
      id: 'user_report_id_1',
      userId: 'user_id_1',
      cause: UserReportCause.FAKE_PROFILE,
      description: 'This profile is unreal'
    } as UserReport,
      {
        id: 'user_report_id_2',
        userId: 'user_id_2',
        cause: UserReportCause.DISCRIMINATION,
        description: 'This user is nasty.'
      } as UserReport],
    headers: new HttpHeaders().append('_links',
      mockLinks),
    status: 200,
    statusText: '',
    url: ''
  });

  beforeEach(async(() => {
    abuseManagementService = jasmine.createSpyObj(['loadNextReports', 'getInitialPageParameter', 'setNextPageParameters']);
    keycloakService = jasmine.createSpyObj(['logout']);

    abuseManagementService.loadNextReports.and.returnValue(of(mockResponse));

    TestBed.configureTestingModule({
      declarations: [AbuseListComponent],
      providers: [
        {provide: AbuseManagementService, useValue: abuseManagementService},
        {provide: KeycloakService, useValue: keycloakService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbuseListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize correctly', () => {
    component.ngOnInit();
    expect(abuseManagementService.loadNextReports).toHaveBeenCalledTimes(2);
  });

  it('should reload reports', () => {
    component.reloadReports();
    expect(component.links).toEqual(JSON.parse(mockResponse.headers.get('_links')));
  });

  it('should have next in links', () => {
    component.links = JSON.parse(mockLinks);
    expect(component.hasNext()).toBeTruthy();
  });

  it('should have previous in links', () => {
    component.links = JSON.parse(mockLinks);
    expect(component.hasPrevious()).toBeTruthy();
  });

  it('should navigate as expected', () => {
    component.links = JSON.parse(mockLinks);

    component.goNext();
    expect(abuseManagementService.setNextPageParameters).toHaveBeenCalledWith('page=2&size=4');

    component.goPrevious();
    expect(abuseManagementService.setNextPageParameters).toHaveBeenCalledWith('page=0&size=4');

    component.goFirst();
    expect(abuseManagementService.setNextPageParameters).toHaveBeenCalledWith('page=0&size=4');

    component.goLast();
    expect(abuseManagementService.setNextPageParameters).toHaveBeenCalledWith('page=4&size=4');
  });

});
