import {Component, OnInit} from '@angular/core';
import {UserReport} from '../../core/model/userReport';
import {HeaderService} from '../../core/service/header.service';
import {KeycloakService} from 'keycloak-angular';
import {Observable} from 'rxjs';
import {AbuseManagementService} from '../../core/service/abuse-management.service';
import {HttpResponse} from '@angular/common/http';

@Component({
  selector: 'tg-abuse-list',
  templateUrl: './abuse-list.component.html',
  styleUrls: ['./abuse-list.component.scss']
})
export class AbuseListComponent implements OnInit {
  public $reportList: Observable<HttpResponse<UserReport[]>>;
  public reportList: UserReport[];
  public links;

  constructor(private headerService: HeaderService,
              private abuseManagementService: AbuseManagementService,
              private keycloak: KeycloakService) {
  }

  ngOnInit(): void {
    this.setHeaderState();
    this.abuseManagementService.setNextPageParameters(this.abuseManagementService.getInitialPageParameter());
    this.reloadReports();
  }

  setHeaderState() {
    this.headerService.setHeaderState({
      showBack: false,
      text: $localize`:@@header.abuse.text:Missbrauchsverwaltung`,
      menu: [
        {
          name: $localize`:@@header.abuse.menu.logout:Abmelden`,
          callback: () => this.keycloak.logout(),
        },
      ],
    });
  }

  reloadReports() {
    this.$reportList = this.abuseManagementService.loadNextReports();
    this.$reportList.subscribe((response) => {
        this.reportList = response.body;
        if (response.headers == null) {
          this.links = null;
        } else {
          this.links = JSON.parse(response.headers.get('_links'));
        }
      }
    );
  }

  hasNext(): boolean {
    return 'next' in this.links;
  }

  goNext() {
    this.abuseManagementService.setNextPageParameters(this.links.next.href);
    this.reloadReports();
  }

  hasPrevious(): boolean {
    return 'prev' in this.links;
  }

  goPrevious() {
    this.abuseManagementService.setNextPageParameters(this.links.prev.href);
    this.reloadReports();
  }

  goFirst() {
    this.abuseManagementService.setNextPageParameters(this.links.first.href);
    this.reloadReports();
  }

  goLast() {
    this.abuseManagementService.setNextPageParameters(this.links.last.href);
    this.reloadReports();
  }
}
