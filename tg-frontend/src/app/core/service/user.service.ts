import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {from, Observable} from 'rxjs';
import {KeycloakUser, User} from '../model/user';
import {map} from 'rxjs/operators';
import {KeycloakService} from 'keycloak-angular';
import {UserReport} from '../model/userReport';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private usersUrl = '/users';
  private accountsUrl = `${this.usersUrl}/accounts`;
  private accountsSelfUrl = `${this.accountsUrl}/self`;
  private reportUserUrl = `${this.usersUrl}/report`;

  constructor(
    private http: HttpClient,
    private keycloak: KeycloakService) {
  }

  public getKeycloakProfile(): Observable<KeycloakUser> {
    return from(this.keycloak.loadUserProfile()).pipe(
      map(profile => {
        profile.id = this.keycloak.getKeycloakInstance().idTokenParsed?.sub;
        return {
          id: profile.id,
          firstName: profile.firstName,
          lastName: profile.lastName,
          email: profile.email
        } as User;
      })
    );
  }

  public getKeycloakRoles(): string[] {
    return this.keycloak.getUserRoles(true);
  }

  public getAccount(): Observable<User> {
    return this.http.get<User>(this.accountsSelfUrl);
  }

  public updateAccount(user: User) {
    return this.http.put<User>(this.accountsSelfUrl, user);
  }

  public reportUser(userReport: UserReport) {
    return this.http.post<UserReport>(this.reportUserUrl, userReport);
  }

}
