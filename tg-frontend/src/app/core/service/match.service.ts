import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Match, MatchRole, MatchType} from '../model/match';
import {Observable, of} from 'rxjs';
import {flatMap, tap} from 'rxjs/operators';
import {MatchRequest} from '../model/matchRequest';
import {HelpRequestService} from './help-request.service';
import {Rating} from '../model/rating';
import {RateLimit} from '../model/rateLimit';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root',
})
export class MatchService {
  private readonly baseUrl = '/requests';
  private readonly tasksUrl = `${this.baseUrl}/tasks`;
  private readonly matchesUrl = `${this.baseUrl}/matches`;
  private readonly acceptableMatchesUrl = `${this.matchesUrl}/acceptable`;
  public location = null;
  private watchNr = -1;

  constructor(private http: HttpClient, private helpRequestService: HelpRequestService) {
    if (navigator.geolocation) {
      this.watchNr = navigator.geolocation.watchPosition(this.setLocation.bind(this), this.showLocationError.bind(this));
    }
  }

  private setLocation(position) {
    this.location = position.coords;
  }

  private showLocationError(error) {
        switch (error.code) {
          case error.PERMISSION_DENIED:
            console.log('LOCATION: PERMISSION_DENIED');
            break;
          case error.POSITION_UNAVAILABLE:
            console.log('LOCATION: POSITION_UNAVAILABLE');
            break;
          case error.TIMEOUT:
            console.log('LOCATION: TIMEOUT');
            break;
          case error.UNKNOWN_ERROR:
            console.log('LOCATION: UNKNOWN_ERROR');
            break;
        }
        alert(
          $localize`:@@geolocation.error:Du musst die Location aktivieren um diese Webapp verwenden zu können.`
        );
  }

  // returns the current state in regards to the rate limit for accepting new matches
  getRemainingTasksCount(): Observable<RateLimit> {
    return this.http.get<RateLimit>(`${this.tasksUrl}/remaining`);
  }

  // hier kommen matches zurück die ein helper akzeptieren kann absteigend sortiert nach matchingscore
  getAcceptableMatches(): Observable<Match[]> {
    return this.http.get<Match[]>(this.acceptableMatchesUrl).pipe(
      flatMap((matches) => {
        while (this.watchNr === -1) {
          continue;
        }
        if (matches.length === 0 && this.location) {
          return this.requestMatch({
            location: {
              // {longitude: 16.368953, latitude: 48.198724}
              longitude: this.location.longitude,
              latitude: this.location.latitude,
            },
          });
        }
        return of(matches);
      }),
      tap(
        (matches: Match[]) =>
          matches.sort((a, b) => b.matchingScore - a.matchingScore) // descending
      )
    );
  }

  // liefert alle acceptable matches und lädt neue nach wenn es keine mehr gibt

  acceptMatch(matchId: string) {
    return this.http.put<Match>(`${this.matchesUrl}/${matchId}/accept`, {});
  }

  rejectMatch(matchId: string) {
    return this.http.put<Match>(`${this.matchesUrl}/${matchId}/reject`, {});
  }

  // liefert alle matches die nicht mehr acceptable sind nach der from zeit sortiert
  // (requester, active) -> liefert alle aktiven matches wo ich als requester eingetragen bin
  // (requester, historic) -> liefert alle matches die abgeschlossen und gecancelled sind wo ich als requester eingetragen bin
  // (helper, active) -> liefert alle matches wo ich als helper eingetagen bin und die nicht abgeschlossen/cancelled sind
  // (helper, historic) -> liefert alle matches die abgeschlossen sind und wo ich als helper eingetragen bin
  getMatchesByRole(
    matchRole: MatchRole,
    matchType: MatchType,
    ascending: boolean
  ): Observable<Match[]> {
    return this.http
      .get<Match[]>(`${this.tasksUrl}/${matchRole}/${matchType}`)
      .pipe(
        tap((matches: Match[]) =>
          matches.sort((a, b) => (a.createdDate < b.createdDate ? 1 : -1))
        )
      );
  }

  getMatchById(id: string): Observable<Match> {
    return this.http.get<Match>(`${this.matchesUrl}/${id}`);
  }

  finishMatch(matchId: string, rating: Rating, matchRole: MatchRole) {
    if (navigator.geolocation) {
      navigator.geolocation.clearWatch(this.watchNr);
    }
    return this.http.put<Match>(
      `${this.tasksUrl}/${matchRole}/${matchId}/finish`,
      rating
    );
  }

  cancelMatch(matchId: string, matchRole: MatchRole) {
    return this.http.put<Match>(
      `${this.tasksUrl}/${matchRole}/${matchId}/cancel`,
      null
    );
  }

  private requestMatch(matchRequest: MatchRequest): Observable<any> {
    return this.http.put<MatchRequest>(this.matchesUrl, matchRequest);
  }
}
