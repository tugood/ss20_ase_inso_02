import { TestBed } from '@angular/core/testing';

import { MatchService } from './match.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { Match, MatchRole, MatchState, MatchType } from '../model/match';
import { Rating } from '../model/rating';
import { User } from '../model/user';

describe('MatchService', () => {
  let service: MatchService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(MatchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getAcceptable should trigger matching algorithm if no matches available', () => {
    const mockMatch = {
      id: '111',
      helpRequest: null,
      helper: null,
      status: MatchState.CREATED,
      matchingScore: 122,
      createdDate: new Date(),
      lastModifiedDate: new Date(),
      completionInfo: null,
    } as Match;

    service.location = { longitude: 16.368953, latitude: 48.198724 };

    service.getAcceptableMatches().subscribe((result) => {
      expect(result.length).toEqual(1);
    });

    const getRequest = httpTestingController.expectOne(
      '/requests/matches/acceptable'
    );
    expect(getRequest.request.method).toBe('GET');
    getRequest.flush([]);

    const putRequest = httpTestingController.expectOne('/requests/matches');
    expect(putRequest.request.method).toBe('PUT');
    putRequest.flush([mockMatch]);
  });

  it('should trigger get requester historic matches', () => {
    service
      .getMatchesByRole(MatchRole.REQUESTER, MatchType.HISTORIC, false)
      .subscribe();

    const getRequest = httpTestingController.expectOne(
      '/requests/tasks/requester/historic'
    );
    expect(getRequest.request.method).toBe('GET');
    getRequest.flush([]);
  });

  it('should trigger get helper active matches', () => {
    service
      .getMatchesByRole(MatchRole.HELPER, MatchType.ACTIVE, false)
      .subscribe();

    const getRequest = httpTestingController.expectOne(
      '/requests/tasks/helper/active'
    );
    expect(getRequest.request.method).toBe('GET');
    getRequest.flush([]);
  });

  it('should get match by id', () => {
    const mockMatch = {
      id: 'this_is_a_match',
      helpRequest: null,
      helper: null,
      status: MatchState.CREATED,
      matchingScore: 122,
      createdDate: new Date(),
      lastModifiedDate: new Date(),
      completionInfo: null,
    } as Match;

    service.getMatchById(mockMatch.id).subscribe((match) => {
      expect(match.id).toEqual(mockMatch.id);
    });

    const putRequest = httpTestingController.expectOne(
      `/requests/matches/${mockMatch.id}`
    );
    expect(putRequest.request.method).toBe('GET');
    putRequest.flush(mockMatch);
  });

  it('should finish match by helper', () => {
    const mockMatch = {
      id: 'this_is_a_match',
      helpRequest: null,
      helper: { id: 'this_is_a_helper' } as User,
      status: MatchState.ACCEPTED,
      matchingScore: 122,
      createdDate: new Date(),
      lastModifiedDate: new Date(),
      completionInfo: null,
    } as Match;

    const mockRating = {
      rating: 5,
      comment: 'This was very well done!',
    } as Rating;

    service
      .finishMatch(mockMatch.id, mockRating, MatchRole.HELPER)
      .subscribe((match) => {
        expect(match.status).toEqual(MatchState.FINISHED_BY_HELPER);
      });

    mockMatch.status = MatchState.FINISHED_BY_HELPER;
    const putRequest = httpTestingController.expectOne(
      `/requests/tasks/helper/${mockMatch.id}/finish`
    );
    expect(putRequest.request.method).toBe('PUT');
    putRequest.flush(mockMatch);
  });

  it('should cancel match by helper', () => {
    const mockMatch = {
      id: 'this_is_a_match',
      helpRequest: null,
      helper: { id: 'this_is_a_helper' } as User,
      status: MatchState.ACCEPTED,
      matchingScore: 122,
      createdDate: new Date(),
      lastModifiedDate: new Date(),
      completionInfo: null,
    } as Match;

    service.cancelMatch(mockMatch.id, MatchRole.HELPER).subscribe((match) => {
      expect(match.status).toEqual(MatchState.CANCELLED_BY_HELPER);
    });

    mockMatch.status = MatchState.CANCELLED_BY_HELPER;
    const putRequest = httpTestingController.expectOne(
      `/requests/tasks/helper/${mockMatch.id}/cancel`
    );
    expect(putRequest.request.method).toBe('PUT');
    putRequest.flush(mockMatch);
  });

  it('should finish match optional rating', () => {
    const mockMatch = {
      id: 'this_is_a_match',
      helpRequest: null,
      helper: { id: 'this_is_a_helper' } as User,
      status: MatchState.ACCEPTED,
      matchingScore: 122,
      createdDate: new Date(),
      lastModifiedDate: new Date(),
      completionInfo: null,
    } as Match;

    const mockRating = null;

    service
      .finishMatch(mockMatch.id, mockRating, MatchRole.HELPER)
      .subscribe((match) => {
        expect(match.status).toEqual(MatchState.FINISHED_BY_HELPER);
      });

    mockMatch.status = MatchState.FINISHED_BY_HELPER;
    const putRequest = httpTestingController.expectOne(
      `/requests/tasks/helper/${mockMatch.id}/finish`
    );
    expect(putRequest.request.method).toBe('PUT');
    putRequest.flush(mockMatch);
  });

  it('should accept match', () => {
    service.acceptMatch('match1').subscribe((res) => {
      const putRequest = httpTestingController.expectOne(
        `/requests/tasks/match1/accept`
      );
      expect(putRequest.request.method).toBe('PUT');
    });
  });

  it('should reject match', () => {
    service.rejectMatch('match1').subscribe((res) => {
      const putRequest = httpTestingController.expectOne(
        `/requests/tasks/match1/reject`
      );
      expect(putRequest.request.method).toBe('PUT');
    });
  });
});
