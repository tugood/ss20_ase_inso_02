import {TestBed} from '@angular/core/testing';

import {AbuseManagementService} from './abuse-management.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {UserReport, UserReportCause} from '../model/userReport';
import {Authority} from '../model/authority';
import {User, UserState} from '../model/user';

describe('AbuseManagementService', () => {
  let service: AbuseManagementService;
  let httpTestingController: HttpTestingController;

  const mockUser = {
    id: 'this_is_an_id',
    firstName: 'Max',
    lastName: 'Mustermann',
    email: 'max.muster@mail.com',
    phoneNr: '+431231213123',
    authorities: [Authority.USER],
    userState: UserState.ACTIVE,
    hasImage: false,
    distance: 10,
    isWarned: false
  } as User;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(AbuseManagementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get initial parameters', () => {
    expect(service.getInitialPageParameter()).toEqual('page=0&size=5');
  });

  it('should set next page to load', () => {
    service.setNextPageParameters('page=1&size=5');
    expect(service.getNextPageParameters()).toEqual('/users/abuse/reports?page=1&size=5');
  });

  it('should get page successfully', () => {
    const mockUserReports = [{
      id: 'user_report_id_1',
      userId: 'user_id_1',
      cause: UserReportCause.FAKE_PROFILE,
      description: 'This profile is unreal'
    } as UserReport,
      {
        id: 'user_report_id_2',
        userId: 'user_id_2',
        cause: UserReportCause.DISCRIMINATION,
        description: 'This user is nasty.'
      } as UserReport];

    service.setNextPageParameters('page=1&size=5');
    service.loadNextReports().subscribe(pageResponse => {
      expect(pageResponse.body.length).toEqual(2);
    });

    const getRequest = httpTestingController.expectOne('/users/abuse/reports?page=1&size=5');
    expect(getRequest.request.method).toBe('GET');
    getRequest.flush(mockUserReports);
  });

  it('should warn user', () => {
    const mockUserReport = {
      id: 'user_report_id',
      userId: 'user_id',
      cause: UserReportCause.FAKE_PROFILE,
      description: 'This profile is unreal'
    } as UserReport;

    service.warnUser(mockUserReport.id).subscribe(warnedUser => {
      expect(warnedUser).toEqual(mockUser);
    });

    const putRequest = httpTestingController.expectOne(`/users/abuse/warn/${mockUserReport.id}`);
    expect(putRequest.request.method).toBe('PUT');
    putRequest.flush(mockUser);
  });

  it('should ban user', () => {
    const mockUserReport = {
      id: 'user_report_id_1',
      userId: 'user_id_1',
      cause: UserReportCause.FAKE_PROFILE,
      description: 'This profile is unreal'
    } as UserReport;

    service.banUser(mockUserReport.id).subscribe(bannedUser => {
      expect(bannedUser).toEqual(mockUser);
    });

    const putRequest = httpTestingController.expectOne(`/users/abuse/block/${mockUserReport.id}`);
    expect(putRequest.request.method).toBe('PUT');
    putRequest.flush(mockUser);
  });

});
