import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateTimeService {
  constructor() { }

  getTimeString(date: Date) {
    const hours = this.buildDoubleDigitString(date.getHours());
    const minutes = this.buildDoubleDigitString(date.getMinutes());

    return hours + ':' + minutes;
  }

  getDateString(date: Date, today: Date) {

    if (this.isSameDay(date, today)) {
      return $localize`:@@date.time.service.today:Heute`;
    }

    const day = this.buildDoubleDigitString(date.getDate());
    const month = this.buildDoubleDigitString(date.getMonth() + 1);
    const year = date.getFullYear();

    return day + '.' + month + '.' + year;
  }

  getDateWithTimeSet(date: Date, time: string) {

    const newDate = new Date(date.toDateString());
    const timeParts = time.split(':');
    newDate.setHours(Number(timeParts[0]));
    newDate.setMinutes(Number(timeParts[1]));

    return newDate;
  }

  getExpirationDate(date: Date, expirationLimitMinutes: number) {
    return new Date(date.valueOf() - expirationLimitMinutes * 60000);
  }

  private isSameDay(date1: Date, date2: Date) {
    return date1.getDate() === date2.getDate()
      && date1.getMonth() === date2.getMonth()
      && date1.getFullYear() === date2.getFullYear();
  }

  private buildDoubleDigitString(value: number) {
    return (value < 10 ? '0' : '') + value.toLocaleString();
  }
}
