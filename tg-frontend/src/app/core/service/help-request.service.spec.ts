import {TestBed} from '@angular/core/testing';

import {HelpRequestService} from './help-request.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {Address} from '../model/address';
import {TimeFrame} from '../model/timeFrame';
import {Category, HelpRequest} from '../model/helpRequest';
import {User} from '../model/user';

describe('HelpRequestService', () => {
  let service: HelpRequestService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(HelpRequestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should post a request', () => {
    const mockAddress: Address = {
      city: 'Wien',
      streetName: 'Schwedenplatz',
      streetNumber: '100/2/27',
      postalCode: '1030',
      country: 'Austria',
      location: {
        longitude: 16.3772,
        latitude: 48.2119
      }
    };

    const mockHelpRequest: HelpRequest = {
      id: 'this_is_a_task',
      category: Category.GROCERY_SHOPPING,
      timeFrame: new TimeFrame(new Date(), new Date(new Date().getTime() + 86400000)),
      description: 'bla bla',
      requester: {id: 'this_is_an_user_id'} as User,
      address: mockAddress
    };

    service.createRequest(mockHelpRequest).subscribe();

    const req = httpTestingController.expectOne('/requests');
    expect(req.request.method).toBe('POST');
    req.flush(mockHelpRequest);
  });

  it('should get all helpRequests', () => {
    const mockAddress: Address = {
      city: 'Wien',
      streetName: 'Schwedenplatz',
      streetNumber: '100/2/27',
      postalCode: '1030',
      country: 'Austria',
      location: {
        longitude: 16.3772,
        latitude: 48.2119
      }
    };

    const mockHelpRequest: HelpRequest = {
      id: 'this_is_a_helpRequest',
      category: Category.GROCERY_SHOPPING,
      timeFrame: new TimeFrame(new Date(), new Date(new Date().getTime() + 86400000)),
      description: 'bla bla',
      requester: {id: 'this_is_an_user_id'} as User,
      address: mockAddress
    };

    service.getAllHelpRequests(false).subscribe(helpRequests => {
      expect(helpRequests[0].id).toEqual('this_is_a_helpRequest');
    });

    const req = httpTestingController.expectOne('/requests');
    expect(req.request.method).toBe('GET');
    req.flush([mockHelpRequest]);
  });

  it('should get helpRequest by id', () => {
    const mockAddress: Address = {
      city: 'Wien',
      streetName: 'Schwedenplatz',
      streetNumber: '100/2/27',
      postalCode: '1030',
      country: 'Austria',
      location: {
        longitude: 16.3772,
        latitude: 48.2119
      }
    };

    const mockHelpRequest: HelpRequest = {
      id: 'this_is_a_helpRequest',
      category: Category.GROCERY_SHOPPING,
      timeFrame: new TimeFrame(new Date(), new Date(new Date().getTime() + 86400000)),
      description: 'bla bla',
      requester: {id: 'this_is_an_user_id'} as User,
      address: mockAddress
    };

    service.getHelpRequestById('this_is_a_helpRequest').subscribe(helpRequest => {
      expect(helpRequest).toBeTruthy();
      expect(helpRequest.id).toEqual('this_is_a_helpRequest');
      expect(helpRequest.requester).toEqual({id: 'this_is_an_user_id'} as User);
    });

    const req = httpTestingController.expectOne('/requests/this_is_a_helpRequest');
    expect(req.request.method).toBe('GET');
    req.flush(mockHelpRequest);
  });

  it('should compare help requests correctly', () => {

    const mockHelpRequest1: HelpRequest = {
      id: 'this_is_a_helpRequest',
      category: Category.GROCERY_SHOPPING,
      timeFrame: new TimeFrame(new Date('Sun, 24 May 2020 11:12:29 GMT'), new Date(new Date().getTime() + 86400000)),
      description: 'bla bla',
      requester: {id: 'this_is_an_user_id'} as User,
      address: null
    };

    const mockHelpRequest2: HelpRequest = {
      id: 'this_is_a_helpRequest',
      category: Category.GROCERY_SHOPPING,
      timeFrame: new TimeFrame(new Date('Sun, 24 May 2020 10:12:29 GMT'), new Date(new Date().getTime() + 86400000)),
      description: 'bla bla',
      requester: {id: 'this_is_an_user_id'} as User,
      address: null
    };

    expect(service.compareHelpRequests(mockHelpRequest1, mockHelpRequest2, true)).toBeGreaterThan(0);
    expect(service.compareHelpRequests(mockHelpRequest1, mockHelpRequest2, false)).toBeLessThan(0);

  });

  it('should delete help request by id', () => {
    const mockHelpRequest: HelpRequest = {
      id: 'this_is_a_helpRequest',
      category: Category.GROCERY_SHOPPING,
      timeFrame: new TimeFrame(new Date('Sun, 24 May 2020 11:12:29 GMT'), new Date(new Date().getTime() + 86400000)),
      description: 'bla bla',
      requester: {id: 'this_is_an_user_id'} as User,
      address: null
    };

    service.deleteHelpRequest(mockHelpRequest.id).subscribe();

    const req = httpTestingController.expectOne(`/requests/${mockHelpRequest.id}/delete`);
    expect(req.request.method).toBe('DELETE');
    req.flush([]);
  });
});
