import {Injectable} from '@angular/core';
import {HelpRequest} from '../model/helpRequest';

@Injectable({
  providedIn: 'root'
})
export class CreateRequestService {
  private helpRequest: HelpRequest = null;

  constructor() {
  }

  setHelpRequest(helpRequest: HelpRequest) {
    this.helpRequest = helpRequest;
  }

  getHelpRequest(): HelpRequest {
    return this.helpRequest;
  }
}
