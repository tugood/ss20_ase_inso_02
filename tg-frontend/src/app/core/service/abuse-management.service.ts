import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {UserReport} from '../model/userReport';
import {Observable} from 'rxjs';
import {User} from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class AbuseManagementService {
  private readonly baseUrl = '/users/abuse';
  private readonly reportUrl = `${this.baseUrl}/reports`;
  private readonly banUrl = `${this.baseUrl}/block`;
  private readonly warnUrl = `${this.baseUrl}/warn`;
  private nextPageLink: string;

  constructor(private http: HttpClient) {
  }

  getInitialPageParameter(): string {
    return `page=0&size=5`;
  }

  setNextPageParameters(parameters: string) {
    this.nextPageLink = `${this.reportUrl}?${parameters}`;
  }

  getNextPageParameters() {
    return this.nextPageLink;
  }

  loadNextReports(): Observable<HttpResponse<UserReport[]>> {
    return this.http.get<UserReport[]>(this.nextPageLink, {observe: 'response'});
  }

  warnUser(reportId: string): Observable<User> {
    return this.http.put<User>(`${this.warnUrl}/${reportId}`, {});
  }

  banUser(reportId: string): Observable<User> {
    return this.http.put<User>(`${this.banUrl}/${reportId}`, {});
  }

  deleteReport(reportId: string): Observable<UserReport> {
    return this.http.delete<UserReport>(`${this.reportUrl}/${reportId}`, {});
  }

}
