import {TestBed} from '@angular/core/testing';

import {AddressService} from './address.service';
import PlaceResult = google.maps.places.PlaceResult;

describe('AddressService', () => {
  let service: AddressService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AddressService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should set Address correctly', () => {

    const place = {
      address_components: [
        {
          long_name: '1',
          short_name: '1',
          types: [
            'street_number'
          ]
        },
        {
          long_name: 'Karlsplatz',
          short_name: 'Karlsplatz',
          types: [
            'route'
          ]
        },
        {
          long_name: 'Innere Stadt',
          short_name: 'Innere Stadt',
          types: [
            'sublocality_level_1',
            'sublocality',
            'political'
          ]
        },
        {
          long_name: 'Wien',
          short_name: 'Wien',
          types: [
            'locality',
            'political'
          ]
        },
        {
          long_name: 'Wien',
          short_name: 'Wien',
          types: [
            'administrative_area_level_1',
            'political'
          ]
        },
        {
          long_name: 'Austria',
          short_name: 'AT',
          types: [
            'country',
            'political'
          ]
        },
        {
          long_name: '1010',
          short_name: '1010',
          types: [
            'postal_code'
          ]
        }
      ],
      geometry: {
        location: {lat: () => 48.1996970197085, lng: () => 16.36807766970849},
      },
      html_attributions: []
    };

    const address = service.getAddress(place as unknown as PlaceResult);

    expect(address.city).toEqual('Wien');
    expect(address.country).toEqual('Austria');
    expect(address.streetName).toEqual('Karlsplatz');
    expect(address.streetNumber).toEqual('1');
    expect(address.postalCode).toEqual('1010');
    expect(address.location.latitude).toEqual(48.1996970197085);
    expect(address.location.longitude).toEqual(16.36807766970849);

  });
});
