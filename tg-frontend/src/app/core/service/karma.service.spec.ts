import {TestBed} from '@angular/core/testing';

import {KarmaService} from './karma.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {KarmaChange, KarmaSummary} from '../model/karmaSummary';

describe('KarmaService', () => {
  let service: KarmaService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(KarmaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get karma summary', () => {
    const mockKarmaChangeHistory = [{
      type: 'INTERNAL_CANCEL_REQUEST',
      value: -100,
      timestamp: new Date()
    } as KarmaChange,
      {
        type: 'INTERNAL_FINISH_REQUEST',
        value: 150,
        timestamp: new Date(new Date().setDate(new Date().getDate() - 1))
      } as KarmaChange,
      {
        type: 'INTERNAL_COMPLETE_RATING',
        value: 35,
        timestamp: new Date(new Date().setDate(new Date().getDate() - 1))
      } as KarmaChange];

    const mockKarmaSummary = {
      userId: 'test_user_id',
      karmaPoints: 100,
      karmaChangeHistory: mockKarmaChangeHistory
    } as KarmaSummary;

    service.getKarmaSummary().subscribe(summary => {
      expect(summary.karmaChangeHistory.length).toEqual(3);
    });

    const getRequest = httpTestingController.expectOne('/karma');
    expect(getRequest.request.method).toBe('GET');
    getRequest.flush(mockKarmaSummary);
  });
});
