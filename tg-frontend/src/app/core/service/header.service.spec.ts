import {TestBed} from '@angular/core/testing';

import {HeaderService, HeaderState} from './header.service';

describe('HeaderService', () => {
  let service: HeaderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HeaderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should set state', () => {

    service.setHeaderState({
      showBack: true, showBackCallback: () => ({}),
      text: 'test', menu: [{name: 'name', callback: undefined}]
    } as HeaderState);

    service.getHeaderState().subscribe(state => {
      expect(state.showBack).toBeTruthy();
      expect(state.text).toEqual('test');
      expect(state.menu).toEqual([{name: 'name', callback: undefined}]);
    });

  });
});
