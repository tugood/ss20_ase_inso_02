import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

export interface HeaderMenuItem {
  name: string;
  callback: () => void;
}

export interface HeaderState {
  showBack: boolean;
  showBackCallback?: () => {};
  text: string;
  menu: HeaderMenuItem[];
}

@Injectable({
  providedIn: 'root'
})
export class HeaderService {

  private headerState: BehaviorSubject<HeaderState>;

  constructor() {
    this.headerState = new BehaviorSubject<HeaderState>({
      showBack: false,
      text: '',
      menu: []
    } as HeaderState);
  }

  setHeaderState(headerState: HeaderState) {
    this.headerState.next(headerState);
  }

  getHeaderState() {
    return this.headerState.asObservable();
  }

}
