import {async, TestBed} from '@angular/core/testing';

import {UserService} from './user.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {User, UserState} from '../model/user';
import {Authority} from '../model/authority';
import {KeycloakService} from 'keycloak-angular';
import {KeycloakInstance} from 'keycloak-js';
import {UserReport, UserReportCause} from '../model/userReport';
import SpyObj = jasmine.SpyObj;

describe('UserService', () => {
  let service: UserService;
  let httpTestingController: HttpTestingController;
  let keycloakSpy: SpyObj<KeycloakService>;

  beforeEach(() => {

    keycloakSpy = jasmine.createSpyObj('userService', ['loadUserProfile', 'getKeycloakInstance']);

    const keycloakInstance = {
      idTokenParsed: {
        sub: 'this_is_a_helper_id'
      }
    } as KeycloakInstance;

    keycloakSpy.getKeycloakInstance.and.returnValue(keycloakInstance);

    keycloakSpy.loadUserProfile.and.returnValue(Promise.resolve({
        id: undefined,
        firstName: 'Lila',
        lastName: 'Lula',
        email: 'lila.lula@mail.com',
      }
    ));

    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{
        provide: KeycloakService, useValue: keycloakSpy
      }],
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(UserService);
  });

  it('should get keycloak account', async(() => {

    service.getKeycloakProfile().subscribe(user => {
      expect(user.id).toEqual('this_is_a_helper_id');
    });

  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get account', () => {

    const mockUser: User = {
      id: 'this_is_an_id',
      firstName: 'Max',
      lastName: 'Mustermann',
      email: 'max.muster@mail.com',
      phoneNr: '+431231213123',
      authorities: [Authority.USER],
      userState: UserState.ACTIVE,
      hasImage: false,
      distance: 10,
      isWarned: false
    };

    service.getAccount().subscribe(user =>
      expect(user.id).toEqual('this_is_an_id')
    );

    const req = httpTestingController.expectOne('/users/accounts/self');
    expect(req.request.method).toBe('GET');
    req.flush(mockUser);
  });

  it('should put a user', () => {

    const mockUser: User = {
      id: 'this_is_an_id',
      firstName: 'Max',
      lastName: 'Mustermann',
      email: 'max.muster@mail.com',
      phoneNr: '+431231213123',
      authorities: [Authority.USER],
      userState: UserState.ACTIVE,
      hasImage: false,
      distance: 10,
      isWarned: false
    };

    service.updateAccount(mockUser).subscribe();

    const req = httpTestingController.expectOne('/users/accounts/self');
    expect(req.request.method).toBe('PUT');
    req.flush(mockUser);
  });

  it('should post a user report', () => {
    const userReportMock = {
      userId: 'id_of_user_to_be_reported',
      cause: UserReportCause.DISCRIMINATION,
      description: 'He wrote that I am an ugly skunk!'
    } as UserReport;

    service.reportUser(userReportMock).subscribe();

    const req = httpTestingController.expectOne('/users/report');
    expect(req.request.method).toBe('POST');
    req.flush(userReportMock);
  });
});
