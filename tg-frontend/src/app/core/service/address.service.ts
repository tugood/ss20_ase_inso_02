import {Injectable} from '@angular/core';
import {Address} from '../model/address';
import PlaceResult = google.maps.places.PlaceResult;

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  constructor() {
  }

  getAddress(place: PlaceResult) {
    const address = {city: '', country: '', location: undefined, postalCode: ''} as Address;

    const addressComponentIndex = {
      street_number: 'short_name',
      route: 'long_name',
      locality: 'long_name',
      administrative_area_level_1: 'short_name',
      country: 'long_name',
      postal_code: 'short_name'
    };

    // Get each component of the address from the place details,
    // and then fill-in the corresponding field on the form.
    for (const elem of place.address_components) {
      const addressType = elem.types[0];

      if (addressComponentIndex[addressType]) {
        const val = elem[addressComponentIndex[addressType]];

        switch (addressType) {
          case 'street_number':
            address.streetNumber = val;
            break;
          case 'route':
            address.streetName = val;
            break;
          case 'country':
            address.country = val;
            break;
          case 'postal_code':
            address.postalCode = val;
            break;
          case 'locality':
            address.city = val;
            break;
        }
      }
    }

    address.location = {
      longitude: place.geometry.location.lng(),
      latitude: place.geometry.location.lat()
    };

    return address;
  }
}
