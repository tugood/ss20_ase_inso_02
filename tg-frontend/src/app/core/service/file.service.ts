import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';
import {Observable, of, Subject} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  public readonly POST_FILE_URL = '/users/accounts/self/image';
  public readonly GET_FILE_URL = '/users/image/';

  private readonly maxSize = 256;
  private selectedDataUrl: string; // data url
  private selectedFileName: string; // filename

  constructor(private http: HttpClient, private domSanitizer: DomSanitizer) {
  }

  setSelectedImage(fileName: string, dataUrl: string) {

    if (!dataUrl.startsWith('data:image')) {
      throw new Error('Not an image');
    }

    this.selectedDataUrl = dataUrl;
    this.selectedFileName = fileName;
  }

  wasImageSelected(): boolean {
    return !!this.selectedFileName;
  }

  loadImage(id: string): Observable<SafeUrl> {
    return this.http
      .get(this.GET_FILE_URL + id, {responseType: 'blob'})
      .pipe(
        map(e => this.domSanitizer.bypassSecurityTrustUrl(URL.createObjectURL(e)))
      );
  }

  uploadSelectedImage(): Observable<any> {
    if (!this.wasImageSelected()) {
      console.error('No image selected');
      return of({});
    }

    const image = new Image();
    image.src = this.selectedDataUrl;
    const canvas = document.createElement('canvas');

    const newDimensions = this.getImageDimensions(image.width, image.height);

    console.log('Resize image to ', newDimensions.width + 'x' + newDimensions.height);

    canvas.width = newDimensions.width;
    canvas.height = newDimensions.height;
    canvas.getContext('2d').drawImage(image, 0, 0, newDimensions.width, newDimensions.height);

    const subject = new Subject();

    canvas.toBlob((blob: Blob) => this.uploadBlob(blob, subject), 'image/jpeg', 0.9);

    return subject.asObservable();
  }

  private uploadBlob(blob: Blob, subject: Subject<any>) {
    if (!blob) {
      console.error('Could not resize image');
      return;
    }
    const formData: FormData = new FormData();
    formData.append('fileParts', blob, this.selectedFileName);
    this.http.put(this.POST_FILE_URL, formData).subscribe(res => subject.next(res));
  }

  public getImageDimensions(width: number, height: number): any {

    let newHeight = height;
    let newWidth = width;

    if (width > height) {
      if (width > this.maxSize) {
        newHeight *= this.maxSize / width;
        newWidth = this.maxSize;
      }
    } else {
      if (height > this.maxSize) {
        newWidth *= this.maxSize / height;
        newHeight = this.maxSize;
      }
    }

    return {width: newWidth, height: newHeight};

  }

}
