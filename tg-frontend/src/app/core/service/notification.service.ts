import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { SwPush } from '@angular/service-worker';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Notification } from '../model/notification';
import * as _ from 'lodash';
import { UserService } from './user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  private notificationRoot = '/notifications';
  private notifications: Notification[] = [];
  private myWebSocket: WebSocketSubject<any>;
  nextPageLink: string;

  constructor(
    private http: HttpClient,
    private swPush: SwPush,
    private snackBar: MatSnackBar,
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    @Inject('WEBPUSH_VAPID_PUBLIC_KEY') private vapidPublicKey: string
  ) {}

  getInitialPageParameter(): string {
    return `page=0&size=10`;
  }

  setNextPageParameters(value: string) {
    this.nextPageLink = `${this.notificationRoot}?${value}`;
  }

  subscribeToPushNotifications() {
    if (this.swPush.isEnabled) {
      this.swPush
        .requestSubscription({
          serverPublicKey: this.vapidPublicKey,
        })
        .then((sub) => this.addPushSubscriber(sub).subscribe())
        .catch((err) =>
          console.error('[App] Could not subscribe to notifications', err)
        );
    }
  }

  subscribeToWebSocket() {
    this.myWebSocket = webSocket(this.getWebsocketUrl());

    // maybe https://itnext.io/websocket-error-handling-with-rxjs-17125c6f2159

    this.myWebSocket.subscribe((data: Notification) => {
      this.addNewNotification(data);
    }, this.websocketErrorFunction);

    this.userService
      .getAccount()
      .pipe(take(1))
      .subscribe((client) => {
        this.myWebSocket.next(client.id);
      }, this.websocketErrorFunction);
  }

  websocketErrorFunction(error) {
    const sec = 3;
    console.log(
      `clould not connect to websocket. try again in ${sec} seconds ...`
    );
    _.delay(() => this.subscribeToWebSocket, sec * 1000);
  }

  loadNextNotifications(): Observable<HttpResponse<Notification[]>> {
    return this.http.get<Notification[]>(this.nextPageLink, {
      observe: 'response',
    });
  }

  getWebsocketUrl() {
    const loc = window.location;
    let newUri;
    if (loc.protocol === 'https:') {
      newUri = 'wss:';
    } else {
      newUri = 'ws:';
    }
    newUri += '//' + loc.host;
    newUri += '/socket-notifications/notifications-emitter';
    return newUri;
  }

  public addPushSubscriber(sub: any) {
    return this.http.put(this.notificationRoot.concat('/subscribe'), sub);
  }

  /**
   * adds notification popup and to the notification menue
   */
  addNewNotification(msg: Notification): Notification {
    return this.addNotification(msg);
  }

  addNotification(
    msg: Notification,
    transform: boolean = true,
    showSnackbar: boolean = true,
    unshift: boolean = true
  ): Notification {
    if (showSnackbar) {
      const snackBarRef = this.snackBar.open(
        msg.title,
        $localize`:@@snackbar.action:anzeigen`,
        {
          duration: 5000,
        }
      );
      snackBarRef.onAction().subscribe(() => {
        this.router.navigate(['notification']).then();
      });
    }

    msg.visibility = false;
    if (transform) {
      msg.timestamp = new Date(msg.timestamp);
    }

    if (this.notifications.indexOf(msg) === -1) {
      if (unshift) {
        this.notifications.unshift(msg);
      } else {
        this.notifications.push(msg);
      }
      _.delay(() => (msg.visibility = true), 250);
    }
    return msg;
  }

  showSnackbar(msg: Notification) {
    this.snackBar.open(msg.title, null, {
      duration: 2000,
    });
  }

  public getNotifications(): Notification[] {
    return this.notifications;
  }

  public deleteNotifications(): Notification[] {
    return (this.notifications = []);
  }

  public closeNotification(notification: Notification) {
    this.notifications.splice(this.notifications.indexOf(notification), 1);
    if (notification.id !== this.noId()) {
      // otherwise it is not persisted and doesn't have to be deleted
      this.http
        .delete(this.notificationRoot.concat('/').concat(notification.id))
        .subscribe();
    }
  }

  /** in case of GUI notifications only.
   * Those don't leave the gui and will not be transmitted to the server.
   * Hence these notifications don't need an id for persistence'
   */
  public noId(): string {
    return 'noId';
  }
}
