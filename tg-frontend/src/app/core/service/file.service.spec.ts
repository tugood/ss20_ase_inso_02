import {async, TestBed} from '@angular/core/testing';

import {FileService} from './file.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('FileService', () => {
  let service: FileService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(FileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('setSelectedFile should set member', () => {

    service.setSelectedImage('1', 'data:image');
    expect(service.wasImageSelected()).toBeTruthy();
  });

  it('should make put call', async(() => {

    service.setSelectedImage('kleines bild', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAA' +
      'AABCAYAAAAfFcSJAAAADUlEQVR42mNk+P+/HgAFhAJ/wlseKgAAAABJRU5ErkJggg==');

    expect(service.wasImageSelected()).toBeTruthy();

    service.uploadSelectedImage().subscribe(_ => {
      const req = httpTestingController.expectOne('/users/accounts/self/image');
      expect(req.request.method).toBe('PUT');
    });
  }));

  it('should load file', () => {

    service.loadImage('helper_id').subscribe(res => {
      const req = httpTestingController.expectOne('/users/accounts/self/image');
      expect(req.request.method).toBe('GET');
    });
  });

  it('should resize correctly 1', () => {

    expect(service.getImageDimensions(1500, 3000)).toEqual({width: 128, height: 256});
  });

  it('should resize correctly 2', () => {

    expect(service.getImageDimensions(3000, 3000)).toEqual({width: 256, height: 256});
  });

  it('should resize correctly 3', () => {

    expect(service.getImageDimensions(3000, 1500)).toEqual({width: 256, height: 128});
  });

});
