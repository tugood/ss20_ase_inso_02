import {TestBed} from '@angular/core/testing';

import {DateTimeService} from './date-time.service';

describe('DateTimeService', () => {
  let service: DateTimeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DateTimeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should format time to 03:03', () => {
    const date = new Date(2020, 4, 22, 3, 3, 21);
    expect(service.getTimeString(date)).toEqual('03:03');
  });

  it('should format time to 13:43', () => {
    const date = new Date(2020, 4, 22, 13, 43, 21);
    expect(service.getTimeString(date)).toEqual('13:43');
  });

  it('should format date to Heute', () => {
    const date1 = new Date(2020, 4, 22, 13, 43, 21);
    const date2 = new Date(2020, 4, 22, 13, 43, 21);
    expect(service.getDateString(date1, date2)).toEqual('Heute');
  });

  it('should format date to 14.10.2020', () => {
    const date1 = new Date(2020, 9, 14);
    const date2 = new Date(2020, 4, 22);
    expect(service.getDateString(date1, date2)).toEqual('14.10.2020');
  });

  it('should set date to 14.10.2020 11:11', () => {
    const time = '11:11';
    const date1 = new Date(2020, 10, 14, 10, 10);
    const date2 = new Date(2020, 10, 14, 11, 11);
    expect(service.getDateWithTimeSet(date1, time).toString()).toEqual(date2.toString());
  });

  it('should set date to 14.10.2020 00:11', () => {
    const time = '0:11';
    const date1 = new Date(2020, 10, 14, 10, 10);
    const date2 = new Date(2020, 10, 14, 0, 11);
    expect(service.getDateWithTimeSet(date1, time).toString()).toEqual(date2.toString());
  });

  it('should set date to 14.10.2020 11:00', () => {
    const date1 = new Date(2020, 10, 14, 11);
    const date2 = new Date(2020, 10, 14, 11, 30);
    expect(service.getExpirationDate(date2, 30).toString).toEqual(date1.toString);
  });

});
