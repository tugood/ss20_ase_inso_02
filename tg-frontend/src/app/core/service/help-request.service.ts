import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {HelpRequest} from '../model/helpRequest';
import {tap} from 'rxjs/operators';
import {RateLimit} from '../model/rateLimit';

@Injectable({
  providedIn: 'root'
})
export class HelpRequestService {

  private requestUrl = '/requests';

  constructor(private http: HttpClient) {
  }

  // erstellt einen neuen HelpRequest ohne den matching algorithmus zu triggern
  public createRequest(helpRequest: HelpRequest): Observable<HelpRequest> {
    return this.http.post<HelpRequest>(this.requestUrl, helpRequest);
  }

  // liefert alle help requests die noch nicht gematched wurden, aber nicht die abgelaufenen sind nach from zeit sortiert
  public getAllHelpRequests(ascending: boolean): Observable<HelpRequest[]> {
    return this.http.get<HelpRequest[]>(this.requestUrl).pipe(
      tap((helpRequests: HelpRequest[]) =>
        helpRequests.sort(((a, b) => this.compareHelpRequests(a, b, ascending)))
      )
    );
  }

  getHelpRequestById(id: string): Observable<HelpRequest> {
    return this.http.get<HelpRequest>(`${this.requestUrl}/${id}`);
  }

  // optional (r: getHistoricHelpRequests -> liefert help requests die nicht gematched werden konnten)

  compareHelpRequests(helpRequest1: HelpRequest, helpRequest2: HelpRequest, ascending: boolean) { // ascending

    const a = helpRequest1.timeFrame.from != null ? new Date(helpRequest1.timeFrame.from).getTime() : 0;
    const b = helpRequest2.timeFrame.from != null ? new Date(helpRequest2.timeFrame.from).getTime() : 0;
    if (ascending) {
      return a - b;
    }
    return b - a;
  }

  // returns the current state in regards to the rate limit for creating requests
  getRemainingRequestsCount(): Observable<RateLimit> {
    return this.http.get<RateLimit>(`${this.requestUrl}/remaining`);
  }

  deleteHelpRequest(helpRequestId: string) {
    return this.http.delete(`${this.requestUrl}/${helpRequestId}/delete`);
  }

}
