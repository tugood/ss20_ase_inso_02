import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {KarmaChange, KarmaSummary} from '../model/karmaSummary';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class KarmaService {

  private karmaUrl = '/karma';

  constructor(private http: HttpClient) {
  }

  public getKarmaSummary(): Observable<KarmaSummary> {
    return this.http.get<KarmaSummary>(this.karmaUrl).pipe(
      map(value => {
        const history = value.karmaChangeHistory;
        history.sort((a, b) => this.compareKarmaChange(a, b, false));
        value.karmaChangeHistory = history;
        return value;
      })
    );
  }

  compareKarmaChange(karmaChangeA: KarmaChange, karmaChangeB: KarmaChange, ascending: boolean) {
    const a = karmaChangeA.timestamp != null ? new Date(karmaChangeA.timestamp).getTime() : 0;
    const b = karmaChangeB.timestamp != null ? new Date(karmaChangeB.timestamp).getTime() : 0;

    if (ascending) {
      return a - b;
    }

    return b - a;
  }

}
