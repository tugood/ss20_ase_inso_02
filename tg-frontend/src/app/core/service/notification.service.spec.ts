import { TestBed } from '@angular/core/testing';

import { NotificationService } from './notification.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { SwPush } from '@angular/service-worker';
import { environment } from '../../../environments/environment';
import {
  MatSnackBar,
  MatSnackBarRef,
  SimpleSnackBar,
} from '@angular/material/snack-bar';
import { MaterialModule } from '../../shared/material.module';
import { UserService } from './user.service';
import { RouterTestingModule } from '@angular/router/testing';
import { Notification } from '../model/notification';
import { of } from 'rxjs';
import SpyObj = jasmine.SpyObj;

describe('NotificationService', () => {
  let service: NotificationService;
  let httpTestingController: HttpTestingController;

  let mockSwPush: SpyObj<SwPush>;
  let mockSnackBar: SpyObj<MatSnackBar>;
  let mockSnackBarRef: SpyObj<MatSnackBarRef<SimpleSnackBar>>;
  let userService: SpyObj<UserService>;

  const globalNotification = {
    body:
      'You received a notification from me because I wanted to tell you something. I don\'t know quite how I should tell you...',
    title: 'info for you',
    id: '48df1871-a5dc-4c58-b64d-1a133d0902eb',
    icon: 'pets',
    visibility: true,
    timestamp: new Date(),
  } as Notification;

  beforeEach(() => {
    mockSwPush = jasmine.createSpyObj('subscribe', ['requestSubscription']);
    mockSnackBar = jasmine.createSpyObj(['open', 'onAction']);
    mockSnackBarRef = jasmine.createSpyObj(['onAction']);
    userService = jasmine.createSpyObj(['getAccount']);

    mockSnackBar.open.and.returnValue(mockSnackBarRef);
    mockSnackBarRef.onAction.and.returnValue(of());

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        MaterialModule,
        RouterTestingModule.withRoutes([]),
      ],
      providers: [
        { provide: SwPush, useValue: mockSwPush },
        {
          provide: 'WEBPUSH_VAPID_PUBLIC_KEY',
          useValue: environment.webPushPublicKey,
        },
        { provide: MatSnackBar, useValue: mockSnackBar },
        { provide: UserService, useValue: userService },
        { provide: MatSnackBarRef, useValue: mockSnackBarRef },
      ],
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(NotificationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should close notification', () => {
    service.closeNotification(globalNotification);

    const deleteRequest = httpTestingController.expectOne(
      `/notifications/${globalNotification.id}`
    );
    expect(deleteRequest.request.method).toBe('DELETE');
    deleteRequest.flush([]);
  });

  it('should add notification', () => {
    const result = service.addNewNotification(globalNotification);
    const notifications = service.getNotifications();

    expect(mockSnackBar.open).toHaveBeenCalledWith(
      globalNotification.title,
      'anzeigen',
      { duration: 5000 }
    );
    expect(result.visibility).toBe(false);
  });

  it('should get initial parameter', () => {
    expect(service.getInitialPageParameter()).toEqual(`page=0&size=10`);
  });

  it('should set next page link', () => {
    service.setNextPageParameters('test');
    expect(service.nextPageLink).toEqual('/notifications?test');
  });

  it('should get websocket url', () => {
    const location = window.location;
    expect(service.getWebsocketUrl()).toEqual(
      'ws://' + location.host + '/socket-notifications/notifications-emitter'
    );
  });

  it('should load next notification', () => {
    const mockNotifications = [globalNotification, globalNotification];
    service.nextPageLink = '/notifications?test';

    service.loadNextNotifications().subscribe();

    const getRequest = httpTestingController.expectOne('/notifications?test');
    expect(getRequest.request.method).toBe('GET');
    getRequest.flush(mockNotifications);
  });

  it('should add publisher', () => {
    const sub = 'any';

    service.addPushSubscriber(sub).subscribe();

    const putRequest = httpTestingController.expectOne(
      '/notifications/subscribe'
    );
    expect(putRequest.request.method).toBe('PUT');
    putRequest.flush([]);
  });

  it('should get no notifications initially', () => {
    expect(service.getNotifications().length).toBe(0);
  });

  it('should delete notifications', () => {
    expect(service.deleteNotifications().length).toBe(0);
  });
});
