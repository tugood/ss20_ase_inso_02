import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';

@NgModule({
  imports: [CommonModule, HttpClientModule],
  providers: [MatSnackBar],
})
export class CoreModule {}
