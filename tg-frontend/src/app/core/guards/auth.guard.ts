import {Injectable} from '@angular/core';
import {KeycloakAuthGuard, KeycloakService} from 'keycloak-angular';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CanAuthenticationGuard extends KeycloakAuthGuard implements CanActivate {
  constructor(protected router: Router, protected keycloakAngular: KeycloakService) {
    super(router, keycloakAngular);
  }

  isAccessAllowed(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    return new Promise((resolve, reject) => {
      if (!this.authenticated) {
        this.keycloakAngular.login()
          .catch(e => console.error(e));
        return reject(false);
      }

      const isAdmin = this.roles.includes('tg-admin');
      const isAbuseManagementRoute = this.getResolvedUrl(route) === '/abuse-management';

      if (isAdmin && !isAbuseManagementRoute) {
        this.router.navigate(['/abuse-management']).then();
        resolve(false);
      }

      if (!isAdmin && isAbuseManagementRoute) {
        this.router.navigate(['/']).then();
        resolve(false);
      }

      resolve(true);
      // ROLES HANDLING
      // const requiredRoles: string[] = route.data.requiredRoles;
      // if (!requiredRoles || requiredRoles.length === 0) {
      //   return resolve(true);
      // } else {
      //   if (!this.roles || this.roles.length === 0) {
      //     resolve(false);
      //   }
      //   resolve(requiredRoles.every(role => this.roles.indexOf(role) > -1));
      // }
    });
  }

  getResolvedUrl(route: ActivatedRouteSnapshot): string {
    return route.pathFromRoot
      .map(v => v.url.map(segment => segment.toString()).join('/'))
      .join('/');
  }
}
