import {Authority} from './authority';

export interface User extends KeycloakUser {
  phoneNr?: string;
  langKey?: string;
  description?: string;
  hasImage: boolean;
  distance: number;
  userState: UserState;
  authorities: Authority[];
  avgRating?: number;
  isWarned?: boolean;
  reportCount?: number;
}

export interface KeycloakUser {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
}

export enum UserState {
  ACTIVE, INACTIVE, BLOCKED
}
