import {TgLocation} from './tgLocation';

export interface MatchRequest {
  location: TgLocation;
}
