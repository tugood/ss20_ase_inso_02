import {TgLocation} from './tgLocation';

export interface Address {
  streetName?: string;
  streetNumber?: string;
  postalCode: string;
  city: string;
  country: string;
  location: TgLocation;
}
