export interface KarmaSummary {
  userId: string;
  karmaPoints: number;
  karmaChangeHistory: KarmaChange[];
}

export interface KarmaChange {
  value: number;
  timestamp: Date;
  type: 'THIRD_PARTY_TRANSACTION' | 'INTERNAL_CANCEL_REQUEST' | 'INTERNAL_FINISH_REQUEST' | 'INTERNAL_COMPLETE_RATING';
  thirdPartyClient: string;
}

const karmaChangeTypeStringMap = new Map<string, string>([
  ['THIRD_PARTY_TRANSACTION', $localize`:@@karmachange.type.third.party.transaction:Transaktion durch externen Händler:`],
  ['INTERNAL_CANCEL_REQUEST', $localize`:@@karmachange.type.cancel:Sie haben einen Task storniert.`],
  ['INTERNAL_FINISH_REQUEST', $localize`:@@karmachange.type.finished:Sie haben einen Task erfolgreich abgeschlossen.`],
  ['INTERNAL_COMPLETE_RATING', $localize`:@@karmachange.type.complete.rating:Sie haben einen Benutzer bewertet und somit geholfen TUgood zu verbessern. Danke :).`]
]);

export function getKarmaChangeCauseString(type: string) {
  return karmaChangeTypeStringMap.get(type);
}
