export interface RateLimit {
  remaining?: number;
  current?: number;
  max?: number;
}
