export class TimeFrame {
  from?: Date;
  to?: Date;

  constructor(from?: Date, to?: Date) {
    this.from = from; // starting date of task
    this.to = to;     // date until task should be done. note: might be used in the future
  }
}
