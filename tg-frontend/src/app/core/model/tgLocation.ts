export interface TgLocation {
  longitude: number;
  latitude: number;
}
