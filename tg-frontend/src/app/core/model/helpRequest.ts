import {TimeFrame} from './timeFrame';
import {Address} from './address';
import {User} from './user';

export interface HelpRequest {
  id: string;
  category: Category;
  timeFrame: TimeFrame;
  description: string;
  requester: User;
  address: Address;
}

export enum Category {
  GROCERY_SHOPPING = 'GROCERY_SHOPPING',
  ANIMAL_SITTING = 'ANIMAL_SITTING',
  TRANSPORT = 'TRANSPORT',
  CHILD_CARE = 'CHILD_CARE',
  PERSONAL_TRAINING = 'PERSONAL_TRAINING',
  OTHER = 'OTHER'
}

const categoryIconMap = new Map<Category, string>([
  [Category.GROCERY_SHOPPING, 'shopping_cart'],
  [Category.ANIMAL_SITTING, 'pets'],
  [Category.TRANSPORT, 'emoji_transportation'],
  [Category.CHILD_CARE, 'child_care'],
  [Category.PERSONAL_TRAINING, 'sports'],
  [Category.OTHER, 'emoji_objects']
]);

const categoryHumanReadableStringMap = new Map<Category, string>([
  [Category.GROCERY_SHOPPING, $localize`:@@task.category.shopping:Einkaufen`],
  [Category.ANIMAL_SITTING, $localize`:@@task.category.petsitting:Tierbetreuung`],
  [Category.TRANSPORT, $localize`:@@task.category.transport:Transport`],
  [Category.CHILD_CARE, $localize`:@@task.category.childcare:Kinderbetreuung`],
  [Category.PERSONAL_TRAINING, $localize`:@@task.category.personaltraining:Personaltraining`],
  [Category.OTHER, $localize`:@@task.category.other:Andere`]
]);

export function getCategoryName(category: Category) {
  return categoryHumanReadableStringMap.get(category);
}

export function getCategoryIcon(category: Category) {
  return categoryIconMap.get(category);
}
