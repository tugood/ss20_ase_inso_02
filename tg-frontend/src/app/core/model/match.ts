import {HelpRequest} from './helpRequest';
import {CompletionInfo} from './completionInfo';
import {User} from './user';

// a task is always matched
export interface Match {
  id: string;
  helpRequest: HelpRequest;
  helper: User;
  status: MatchState;
  matchingScore: number;
  createdDate: Date;
  lastModifiedDate: Date;
  completionInfo?: CompletionInfo;
}

export enum MatchState {
  CREATED = 'CREATED',
  REJECTED = 'REJECTED',
  ACCEPTED = 'ACCEPTED',
  EXPIRED = 'EXPIRED',
  CANCELLED_BY_HELPER = 'CANCELLED_BY_HELPER',
  CANCELLED_BY_REQUESTER = 'CANCELLED_BY_REQUESTER',
  FINISHED_BY_HELPER = 'FINISHED_BY_HELPER',
  FINISHED_BY_REQUESTER = 'FINISHED_BY_REQUESTER'
}

export enum MatchRole {
  REQUESTER = 'requester',
  HELPER = 'helper'
}

export enum MatchType {
  HISTORIC = 'historic',
  ACTIVE = 'active'
}

const taskStateStringMap = new Map<MatchState, string>([
  [MatchState.CREATED, $localize`:@@task.state.created:Angelegt`],
  [MatchState.ACCEPTED, $localize`:@@task.state.assigned:Angenommen`],
  [MatchState.CANCELLED_BY_HELPER, $localize`:@@task.state.cancelled:Storniert`],
  [MatchState.CANCELLED_BY_REQUESTER, $localize`:@@task.state.cancelled:Storniert`],
  [MatchState.EXPIRED, $localize`:@@task.state.expired:Abgelaufen`],
  [MatchState.FINISHED_BY_HELPER, $localize`:@@task.state.finished:Abgeschlossen`],
  [MatchState.FINISHED_BY_REQUESTER, $localize`:@@task.state.finished:Abgeschlossen`]
]);

export function getTaskStateName(taskState: MatchState) {
  return taskStateStringMap.get(taskState);
}
