export interface Notification {
  id: string;
  title?: string;
  body: string;
  timestamp: Date;
  visibility: boolean;
  icon?: string;
}
