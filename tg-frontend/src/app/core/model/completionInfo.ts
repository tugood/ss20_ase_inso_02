import {Rating} from './rating';

export interface CompletionInfo {
  helperRating?: Rating;
  requesterRating?: Rating;
}
