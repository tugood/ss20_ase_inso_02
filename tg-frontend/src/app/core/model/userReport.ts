import {User} from './user';

export interface UserReport {
  id: string;
  userId: string;
  user: User;
  cause: UserReportCause;
  description: string;
}

export enum UserReportCause {
  FAKE_PROFILE = 'FAKE_PROFILE',
  ILLEGAL_ACTIVITY = 'ILLEGAL_ACTIVITY',
  DISCRIMINATION = 'DISCRIMINATION',
  ABUSIVE_BEHAVIOUR = 'ABUSIVE_BEHAVIOUR'
}

const userReportCauseStringMap = new Map<UserReportCause, string>([
  [UserReportCause.FAKE_PROFILE, $localize`:@@user.report.cause.fake:Fake-Profil`],
  [UserReportCause.ILLEGAL_ACTIVITY, $localize`:@@user.report.cause.illegal:Illegale Aktivitäten`],
  [UserReportCause.DISCRIMINATION, $localize`:@@user.report.cause.discrimination:Diskriminierung`],
  [UserReportCause.ABUSIVE_BEHAVIOUR, $localize`:@@user.report.cause.abuse:Missbrauch`]
]);

export function getUserReportCauseName(userReportCause: UserReportCause) {
  return userReportCauseStringMap.get(userReportCause);
}
