import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RequesterComponent} from './requester.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../shared/shared.module';
import {MaterialModule} from '../shared/material.module';
import {HelpRequestListComponent} from './help-request-list/help-request-list.component';
import {HelpRequestDetailsComponent} from './help-request-details/help-request-details.component';

export const requesterRoutes: Routes = [
  {path: '', pathMatch: 'full', component: RequesterComponent},
  {path: 'help-request/:id', component: HelpRequestDetailsComponent},
];

@NgModule({
  declarations: [RequesterComponent, HelpRequestListComponent, HelpRequestDetailsComponent],
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    RouterModule
  ]
})
export class RequesterModule {
}
