import {Component, OnInit} from '@angular/core';
import {HeaderService} from '../core/service/header.service';
import {MatchRole} from '../core/model/match';

@Component({
  selector: 'tg-requester',
  templateUrl: './requester.component.html',
  styleUrls: ['./requester.component.scss']
})
export class RequesterComponent implements OnInit {

  readonly matchTab = {name: $localize`:@@request.text.accepted.requests:Akzeptierte Anfragen`, type: 'acceptedMatches'};
  readonly requestTab = {name: $localize`:@@request.text.open.requests:Offene Anfragen`, type: 'openRequests'};
  public emptyText = $localize`:@@request.text.emptytext:Keine akzeptierten Anfragen vorhanden`;
  public historicText = $localize`:@@request.text.historictext:Frühere Anfragen`;
  role: MatchRole;

  public viewTypes = [
    this.matchTab,
    this.requestTab
  ];

  constructor(private headerService: HeaderService) {
  }

  ngOnInit(): void {
    this.headerService.setHeaderState({showBack: false, text: $localize`:@@header.request.text:Meine Anfragen`, menu: []});
    this.role = MatchRole.REQUESTER;
  }
}
