import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RequesterComponent} from './requester.component';
import {MaterialModule} from '../shared/material.module';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {RouterTestingModule} from '@angular/router/testing';

describe('RequesterComponent', () => {
  let component: RequesterComponent;
  let fixture: ComponentFixture<RequesterComponent>;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [RequesterComponent],
      imports: [MaterialModule, NoopAnimationsModule, RouterTestingModule],
      providers: []
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequesterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
