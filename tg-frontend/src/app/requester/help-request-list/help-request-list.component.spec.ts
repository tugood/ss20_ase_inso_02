import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HelpRequestListComponent} from './help-request-list.component';
import {MaterialModule} from '../../shared/material.module';
import {Component, Input} from '@angular/core';
import {Match} from 'src/app/core/model/match';
import {RouterTestingModule} from '@angular/router/testing';
import {HelpRequestService} from '../../core/service/help-request.service';
import {of} from 'rxjs';
import SpyObj = jasmine.SpyObj;

@Component({
  selector: 'tg-task',
  template: '<p>Mock Task</p>'
})
class TaskComponent {
  @Input() taskInfo: Match;
}

describe('HelpRequestListComponent', () => {
  let component: HelpRequestListComponent;
  let fixture: ComponentFixture<HelpRequestListComponent>;
  let helpRequestService: SpyObj<HelpRequestService>;

  beforeEach(async(() => {

    helpRequestService = jasmine.createSpyObj('helpRequestService', ['getAllHelpRequests']);

    helpRequestService.getAllHelpRequests.and.returnValue(of([]));

    TestBed.configureTestingModule({
      declarations: [HelpRequestListComponent, TaskComponent],
      imports: [MaterialModule, RouterTestingModule],
      providers: [{provide: HelpRequestService, useValue: helpRequestService}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpRequestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
