import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {HelpRequest} from '../../core/model/helpRequest';
import {HelpRequestService} from '../../core/service/help-request.service';

@Component({
  selector: 'tg-help-request-list',
  templateUrl: './help-request-list.component.html',
  styleUrls: ['./help-request-list.component.scss'],
})
export class HelpRequestListComponent implements OnInit {
  public helpRequestList: HelpRequest[] = [];
  public $helpRequestList: Observable<HelpRequest[]>;
  public isLoading = false;

  constructor(private helpRequestService: HelpRequestService) {
  }

  ngOnInit(): void {
    this.isLoading = true;

    this.$helpRequestList = this.helpRequestService.getAllHelpRequests(false);

    this.$helpRequestList.subscribe((res) => {
      if (res) {
        this.helpRequestList = res;
      }
      this.isLoading = false;
    });
  }

}
