import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {HelpRequestDetailsComponent} from './help-request-details.component';
import {HelpRequestService} from '../../core/service/help-request.service';
import {RouterTestingModule} from '@angular/router/testing';
import SpyObj = jasmine.SpyObj;

describe('HelpRequestDetailsComponent', () => {
  let component: HelpRequestDetailsComponent;
  let fixture: ComponentFixture<HelpRequestDetailsComponent>;
  let helpRequestService: SpyObj<HelpRequestService>;

  beforeEach(async(() => {

    helpRequestService = jasmine.createSpyObj('helpRequestService', ['getHelpRequestById', 'deleteHelpRequest']);

    TestBed.configureTestingModule({
      declarations: [HelpRequestDetailsComponent],
      imports: [RouterTestingModule],
      providers: [
        {provide: HelpRequestService, useValue: helpRequestService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HelpRequestDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
