import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {HeaderService} from '../../core/service/header.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {HelpRequestService} from '../../core/service/help-request.service';
import {HelpRequest} from '../../core/model/helpRequest';

@Component({
  selector: 'tg-help-request-details',
  templateUrl: './help-request-details.component.html',
  styleUrls: ['./help-request-details.component.scss'],
})
export class HelpRequestDetailsComponent implements OnInit {
  private helpRequestId: string;
  public $helpRequest: Observable<HelpRequest>;

  constructor(
    private headerService: HeaderService,
    private helpRequestService: HelpRequestService,
    private route: ActivatedRoute,
    private router: Router,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.headerService.setHeaderState({
      showBack: true,
      showBackCallback: (() => this.location.back()).bind(this),
      text: $localize`:@@header.request.details.text:Anfrage-Details`,
      menu: [
        {
          name: $localize`:@@header.request.details.menu:Löschen`,
          callback: () => {
            this.helpRequestService.deleteHelpRequest(this.helpRequestId).subscribe(_ => {
              this.router.navigate(['/']).then();
            });
          },
        },
      ],
    });

    this.route.paramMap.subscribe((params) => {
      this.helpRequestId = params.get('id');
      this.$helpRequest = this.helpRequestService.getHelpRequestById(
        this.helpRequestId
      );
    });
  }
}
