import {Component, OnInit, ViewChild} from '@angular/core';
import {HeaderService} from '../core/service/header.service';
import {MatchRole} from '../core/model/match';
import {MatTabGroup} from '@angular/material/tabs';

@Component({
  selector: 'tg-helper',
  templateUrl: './helper.component.html',
  styleUrls: ['./helper.component.scss']
})
export class HelperComponent implements OnInit {
  @ViewChild('tabGroup', {static: false}) tabGroup: MatTabGroup;

  readonly givingHelpTab = {name: $localize`:@@helper.tab.giving:Hilfe leisten`, type: 'givingHelp'};
  readonly activeMatchesTab = {name: $localize`:@@helper.tab.active:Aktive Aufgaben`, type: 'activeMatches'};
  role: MatchRole;
  public emptyText = $localize`:@@helper.text.empty:Keine aktiven Aufgaben vorhanden`;
  public historyText = $localize`:@@helper.text.history:Frühere Aufgaben`;
  public viewTypes = [
    this.activeMatchesTab,
    this.givingHelpTab
  ];

  constructor(private headerService: HeaderService) {
  }

  ngOnInit(): void {
    this.headerService.setHeaderState({
      showBack: false,
      text: $localize`:@@header.helper.text:Helfer Dashboard`,
      menu: []
    });
    this.role = MatchRole.HELPER;
  }

}
