import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {Routes} from '@angular/router';
import {MatchDetailsComponent} from '../shared/match-details/match-details.component';
import {HelperComponent} from './helper.component';
import {MaterialModule} from '../shared/material.module';
import {SharedModule} from '../shared/shared.module';
import {AcceptMatchComponent} from './accept-match/accept-match.component';

export const helperRoutes: Routes = [
  {path: 'helper', component: HelperComponent},
  {path: 'match/:id', component: MatchDetailsComponent}
];

@NgModule({
  declarations: [HelperComponent, AcceptMatchComponent],
  imports: [
    CommonModule,
    MaterialModule,
    SharedModule
  ]
})
export class HelperModule {
}
