import {Component, Input, OnInit} from '@angular/core';
import {MatchService} from '../../core/service/match.service';
import {Observable, Subscription} from 'rxjs';
import {Match} from '../../core/model/match';
import {KeycloakUser} from '../../core/model/user';
import {UserService} from '../../core/service/user.service';
import {RateLimit} from '../../core/model/rateLimit';
import {LimitReachedComponent} from '../../shared/limit-reached/limit-reached.component';
import {MatDialog} from '@angular/material/dialog';
import {MatTabGroup} from '@angular/material/tabs';

@Component({
  selector: 'tg-accept-match',
  templateUrl: './accept-match.component.html',
  styleUrls: ['./accept-match.component.scss']
})
export class AcceptMatchComponent implements OnInit {

  @Input() tabGroup: MatTabGroup;

  $remaining: Observable<RateLimit>;
  lastSubscription: Subscription;
  matches: Match[];
  $loggedInUser: Observable<KeycloakUser>;
  isLoadingMatches = false;
  index = 0;

  constructor(
    private matchService: MatchService,
    private userService: UserService,
    private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.$loggedInUser = this.userService.getKeycloakProfile();
    this.loadMatches();
  }

  acceptMatch() {
    this.matchService.acceptMatch(this.matches[this.index++].id).subscribe(_ => {
      this.tabGroup.selectedIndex = 1;
    });
  }

  rejectMatch() {
    this.matchService.rejectMatch(this.matches[this.index++].id).subscribe(_ => {
      if (this.index >= this.matches.length) {
        this.loadMatches();
      }
    });

  }

  private loadMatches() {
    this.$remaining = this.matchService.getRemainingTasksCount();

    this.isLoadingMatches = true;
    this.index = 0;

    if (this.lastSubscription) {
      this.lastSubscription.unsubscribe();
      this.lastSubscription = null;
    }

    this.lastSubscription = this.$remaining.subscribe(limit => {
      if (limit.remaining === 0) {
        this.dialog.open(LimitReachedComponent).beforeClosed().subscribe(() => {
          this.tabGroup.selectedIndex = 1;
        });
      } else {
        this.lastSubscription = this.matchService.getAcceptableMatches().subscribe(matches => {
          this.matches = matches;
          this.isLoadingMatches = false;
        });
      }
    });
  }
}
