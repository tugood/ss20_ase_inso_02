import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AcceptMatchComponent} from './accept-match.component';
import {MatchService} from '../../core/service/match.service';
import {of} from 'rxjs';
import {Match} from '../../core/model/match';
import {UserService} from '../../core/service/user.service';
import {MatDialog} from '@angular/material/dialog';
import {RouterTestingModule} from '@angular/router/testing';
import {RateLimit} from '../../core/model/rateLimit';
import {MatTabGroup} from '@angular/material/tabs';
import SpyObj = jasmine.SpyObj;

describe('AcceptMatchComponent', () => {
  let component: AcceptMatchComponent;
  let fixture: ComponentFixture<AcceptMatchComponent>;
  let matchServiceSpy: SpyObj<MatchService>;
  let userServiceSpy: SpyObj<UserService>;
  let tabGroupSpy: SpyObj<MatTabGroup>;

  beforeEach(async(() => {

    tabGroupSpy = jasmine.createSpyObj([], ['selectedIndex']);

    matchServiceSpy = jasmine.createSpyObj('matchService',
      [
        'getAcceptableMatches',
        'acceptMatch',
        'rejectMatch',
        'getRemainingTasksCount'
      ]);
    userServiceSpy = jasmine.createSpyObj('userService', ['getKeycloakProfile']);
    matchServiceSpy.getRemainingTasksCount.and.returnValue(of({remaining: 3, current: 0, max: 0} as RateLimit));

    userServiceSpy.getKeycloakProfile.and.returnValue(
      of({id: 'helper_id', firstName: 'a', lastName: 'b', email: 'c'})
    );

    matchServiceSpy.getAcceptableMatches.and.returnValue(of([]));
    matchServiceSpy.acceptMatch.and.returnValue(of({} as Match));
    matchServiceSpy.rejectMatch.and.returnValue(of({} as Match));

    TestBed.configureTestingModule({
      declarations: [AcceptMatchComponent],
      imports: [
        RouterTestingModule
      ],
      providers: [
        {provide: MatchService, useValue: matchServiceSpy},
        {provide: UserService, useValue: userServiceSpy},
        {provide: MatDialog, useValue: {}}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptMatchComponent);
    component = fixture.componentInstance;
    component.tabGroup = tabGroupSpy;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('accept match should increase index', () => {

    component.matches = [{id: '1'} as Match, {id: '2'} as Match, {id: '3'} as Match];

    component.acceptMatch();

    expect(component.index).toEqual(1);

    component.rejectMatch();

    expect(component.index).toEqual(2);

  });
});
