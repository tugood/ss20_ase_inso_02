import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateRequestComponent} from './create-request/create-request.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../shared/shared.module';
import {MaterialModule} from '../shared/material.module';
import {ReactiveFormsModule} from '@angular/forms';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {CategorySelectComponent} from './create-request/category-select/category-select.component';
import {MainCreateRequestComponent} from './create-request/main-create-request/main-create-request.component';
import {CreateRequestSummaryComponent} from './create-request/create-request-summary/create-request-summary.component';

export const createRequestRoutes: Routes = [
  {path: 'create-request', component: CreateRequestComponent}
];

@NgModule({
  declarations: [
    CreateRequestComponent,
    CategorySelectComponent,
    MainCreateRequestComponent,
    CreateRequestSummaryComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    ReactiveFormsModule,
    NgxMaterialTimepickerModule,
    RouterModule
  ]
})
export class CreateRequestModule {
}
