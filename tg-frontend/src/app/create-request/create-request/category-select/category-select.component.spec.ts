import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CategorySelectComponent} from './category-select.component';
import {Category} from '../../../core/model/helpRequest';

describe('CategorySelectComponent', () => {
  let component: CategorySelectComponent;
  let fixture: ComponentFixture<CategorySelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CategorySelectComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategorySelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return pet item', () => {
    const icon = component.getIcon(Category.ANIMAL_SITTING);
    expect(icon).toEqual('pets');
  });

  it('should return category name', () => {
    const categoryName = component.getName(Category.PERSONAL_TRAINING);
    expect(categoryName).toEqual('Personaltraining');
  });
});
