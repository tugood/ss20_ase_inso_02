import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Category, getCategoryIcon, getCategoryName} from '../../../core/model/helpRequest';

@Component({
  selector: 'tg-category-select',
  templateUrl: './category-select.component.html',
  styleUrls: ['./category-select.component.scss']
})
export class CategorySelectComponent implements OnInit {

  @Output() categoryEmitter = new EventEmitter<Category>();

  categories = [
    Category.GROCERY_SHOPPING,
    Category.ANIMAL_SITTING,
    Category.TRANSPORT,
    Category.CHILD_CARE,
    Category.PERSONAL_TRAINING,
    Category.OTHER
  ];

  constructor() {
  }

  ngOnInit(): void {
  }

  onCategoryChange(category: Category) {
    this.categoryEmitter.emit(category);
  }

  getIcon(category: Category) {
    return getCategoryIcon(category);
  }

  getName(category: Category) {
    return getCategoryName(category);
  }
}
