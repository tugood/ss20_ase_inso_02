import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {DateTimeService} from '../../../core/service/date-time.service';
import {Address} from '../../../core/model/address';
import {TimeFrame} from '../../../core/model/timeFrame';
import {HelpRequest} from '../../../core/model/helpRequest';
import {AddressService} from '../../../core/service/address.service';
import {CreateRequestService} from '../../../core/service/create-request.service';
import PlaceResult = google.maps.places.PlaceResult;

@Component({
  selector: 'tg-main-create-request',
  templateUrl: './main-create-request.component.html',
  styleUrls: ['./main-create-request.component.scss']
})
export class MainCreateRequestComponent implements OnInit {

  createRequestForm: FormGroup;
  minDate: Date;
  maxDate: Date;
  address: Address;
  isAddressSetByGoogleService: boolean;

  @Input() currentHelpRequest: HelpRequest;
  @Output() helpRequestEmitter = new EventEmitter<HelpRequest>();

  constructor(private dateTimeService: DateTimeService,
              private formBuilder: FormBuilder,
              private addressService: AddressService,
              private createRequestService: CreateRequestService) {
    this.minDate = new Date();
    this.maxDate = new Date();
    this.address = {city: '', country: '', location: undefined, postalCode: ''};
    this.maxDate.setDate(this.maxDate.getDate() + 14);

    this.createRequestForm = this.formBuilder.group({
      startingDate: [this.minDate, Validators.required],
      startingTime: [this.getCurrentTime(), Validators.required],
      requestLocation: this.formBuilder.group({address: ['', [Validators.required]]}),
      requestDescription: [''],
    }, {
      validator: Validators.compose(
        [
          this.validateAddressField(),
          this.validateSelectedTime(this.minDate, 'startingDate'),
          Validators.required
        ]
      )
    });

  }

  ngOnInit(): void {
    this.isAddressSetByGoogleService = false;

    const revitalized = this.createRequestService.getHelpRequest();

    if (revitalized) {
      this.createRequestForm.patchValue({
        requestLocation: {address: this.toGoogleAddressString(revitalized.address)},
        requestDescription: revitalized.description,
      });
    } else if (this.currentHelpRequest.timeFrame) {
      this.createRequestForm.patchValue({
        startingDate: this.currentHelpRequest.timeFrame.from,
        startingTime: this.currentHelpRequest.timeFrame.from.getHours() + ':' +
          this.currentHelpRequest.timeFrame.from.getMinutes(),
        requestLocation: {address: this.toGoogleAddressString(this.currentHelpRequest.address)},
        requestDescription: this.currentHelpRequest.description
      });
      this.address = this.currentHelpRequest.address;
      this.isAddressSetByGoogleService = true;
    }
  }

  getCurrentTime(): string {
    const now = new Date();
    return now.getHours() + ':' + now.getMinutes();
  }

  toGoogleAddressString(address: Address): string {
    return address.streetName.concat(' ', address.streetNumber, ', ', address.city, ', ', address.country);
  }

  validateAddressField(): ValidatorFn {
    return (): { isAddressValid: boolean } | null => {
      return this.isAddressSetByGoogleService ? null : {isAddressValid: false};
    };
  }

  validateSelectedTime(minDate: Date, startingDate: string): ValidatorFn {
    return (group: FormGroup): { isTimeValid: boolean } | null => {
      if (minDate === group.get(startingDate).value) {
        const minTime = minDate.getHours() + ':' + minDate.getMinutes();
        return group.get('startingTime').value >= minTime ? null : {isTimeValid: true};
      }
    };
  }

  emitRequest() {

    const from = this.dateTimeService.getDateWithTimeSet(this.createRequestForm.value.startingDate,
      this.createRequestForm.value.startingTime.toString());

    const helpRequest = {
      timeFrame: new TimeFrame(from, new Date(from.getTime() + 86400000)),  // add 1 day to to
      address: this.address,
      description: this.createRequestForm.value.requestDescription,
    } as HelpRequest;

    this.helpRequestEmitter.emit(helpRequest);
  }

  getAddress(place: PlaceResult) {
    this.address = this.addressService.getAddress(place);

    // Used to trigger validation after event in address component was emitted
    this.isAddressSetByGoogleService = true;
    this.createRequestForm.updateValueAndValidity();
  }

  onAddressChange() {
    this.isAddressSetByGoogleService = false;
  }
}
