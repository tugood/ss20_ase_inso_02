import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {MainCreateRequestComponent} from './main-create-request.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../../../shared/material.module';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {Address} from '../../../core/model/address';
import {DateTimeService} from '../../../core/service/date-time.service';
import {Category, HelpRequest} from '../../../core/model/helpRequest';
import {AddressService} from '../../../core/service/address.service';
import {CreateRequestService} from '../../../core/service/create-request.service';
import {User} from '../../../core/model/user';
import {TimeFrame} from '../../../core/model/timeFrame';
import SpyObj = jasmine.SpyObj;
import PlaceResult = google.maps.places.PlaceResult;

describe('MainCreateRequestComponent', () => {
  let component: MainCreateRequestComponent;
  let fixture: ComponentFixture<MainCreateRequestComponent>;
  let dateTimeService: SpyObj<DateTimeService>;
  let addressService: SpyObj<AddressService>;
  let createRequestService: SpyObj<CreateRequestService>;

  const mockAddress = {
    city: 'Wien',
    country: 'Austria',
    location: {
      longitude: 16.3695113,
      latitude: 48.2011792
    },
    postalCode: '1010',
    streetNumber: '1',
    streetName: 'Karlsplatz'
  } as Address;

  const requester = {
    id: '100',
    firstName: 'Lila',
    lastName: 'Lula',
    email: 'lila.lula@mail.com'
  } as User;

  const mockHelpRequest = {
    id: 'help_request_id',
    category: Category.ANIMAL_SITTING,
    description: 'I need help!',
    address: mockAddress,
    requester
  } as HelpRequest;

  beforeEach(async(() => {

    dateTimeService = jasmine.createSpyObj('dateTimeService', ['getDateWithTimeSet']);
    addressService = jasmine.createSpyObj(['getAddress']);
    createRequestService = jasmine.createSpyObj(['getHelpRequest']);

    dateTimeService.getDateWithTimeSet.and.returnValue(new Date('2020-05-11T19:48:00.000Z'));
    addressService.getAddress.and.returnValue(mockAddress);

    TestBed.overrideProvider(DateTimeService, {useValue: dateTimeService});

    TestBed.configureTestingModule({
      declarations: [MainCreateRequestComponent],
      imports: [ReactiveFormsModule, MaterialModule, NoopAnimationsModule, NgxMaterialTimepickerModule],
      providers: [
        {provide: DateTimeService, useValue: dateTimeService},
        {provide: AddressService, useValue: addressService},
        {provide: CreateRequestService, useValue: createRequestService}
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainCreateRequestComponent);
    component = fixture.componentInstance;
    component.currentHelpRequest = {} as HelpRequest;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('validator should be true for startTime', () => {
    const now = new Date('2020-05-11T19:47:19.491Z');
    component.createRequestForm.patchValue({
      startingDate: now,
      startingTime: now.getHours() + ':' + (now.getMinutes() + 1)
    });

    fixture.detectChanges();

    const res = component.validateSelectedTime(now, 'startingDate')(component.createRequestForm);
    expect(res).toEqual(null);
  });

  it('validator should be false for startTime', () => {
    const now = new Date('2020-05-11T19:47:19.491Z');
    component.createRequestForm.patchValue({
      startingDate: now,
      startingTime: now.getHours() + ':' + (now.getMinutes() - 1)
    });

    fixture.detectChanges();

    const res = component.validateSelectedTime(now, 'startingDate')(component.createRequestForm);
    expect(res).toEqual({isTimeValid: true});
  });

  it('validator should be false for address field', () => {
    component.isAddressSetByGoogleService = false;

    const res = component.validateAddressField()(component.createRequestForm);
    expect(res).toEqual({isAddressValid: false});
  });

  it('validator should be null for address field', () => {
    component.isAddressSetByGoogleService = true;

    const res = component.validateAddressField()(component.createRequestForm);
    expect(res).toEqual(null);
  });

  it('emitter should emit correctly', () => {

    component.address = mockAddress;

    component.helpRequestEmitter.subscribe(request => {
      expect(request.address).toEqual(component.address);
      expect(request.timeFrame.from).toBeTruthy();
      expect(dateTimeService.getDateWithTimeSet).toHaveBeenCalled();
    });

    component.emitRequest();

  });

  it('should convert address to google address string', () => {
    expect(component.toGoogleAddressString(mockAddress)).toEqual('Karlsplatz 1, Wien, Austria');
  });

  it('should get address from place result', () => {
    component.getAddress({} as PlaceResult);

    expect(component.address).toEqual(mockAddress);
    expect(component.isAddressSetByGoogleService).toBeTruthy();
  });

  it('should reset google address flag', () => {
    component.onAddressChange();

    expect(component.isAddressSetByGoogleService).toBeFalsy();
  });

  it('should patch form on revitalize help request', () => {
    createRequestService.getHelpRequest.and.returnValue(mockHelpRequest);
    component.ngOnInit();

    expect(component.createRequestForm.value.requestLocation.address).toEqual('Karlsplatz 1, Wien, Austria');
    expect(component.createRequestForm.value.requestDescription).toEqual(mockHelpRequest.description);
  });

  it('should patch form after back on create request summary', () => {
    mockHelpRequest.timeFrame = new TimeFrame(new Date(), new Date(new Date().getTime() + 86400000));
    component.currentHelpRequest = mockHelpRequest;
    component.ngOnInit();

    expect(component.createRequestForm.value.startingDate).toEqual(mockHelpRequest.timeFrame.from);
    expect(component.createRequestForm.value.startingTime).toEqual(mockHelpRequest.timeFrame.from.getHours() + ':' +
      mockHelpRequest.timeFrame.from.getMinutes());

    expect(component.createRequestForm.value.requestLocation.address).toEqual('Karlsplatz 1, Wien, Austria');
    expect(component.createRequestForm.value.requestDescription).toEqual(mockHelpRequest.description);
    expect(component.address).toEqual(mockHelpRequest.address);
    expect(component.isAddressSetByGoogleService).toBeTruthy();
  });

});
