import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {HelpRequest} from '../../../core/model/helpRequest';

@Component({
  selector: 'tg-create-request-summary',
  templateUrl: './create-request-summary.component.html',
  styleUrls: ['./create-request-summary.component.scss']
})
export class CreateRequestSummaryComponent implements OnInit {

  @Output() confirmTaskEmitter = new EventEmitter<void>();

  @Input() helpRequest: HelpRequest;

  constructor() {
  }

  ngOnInit(): void {
  }

  onConfirm() {
    this.confirmTaskEmitter.emit();
  }

}
