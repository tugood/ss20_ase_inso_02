import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CreateRequestSummaryComponent} from './create-request-summary.component';

describe('CreateRequestSummaryComponent', () => {
  let component: CreateRequestSummaryComponent;
  let fixture: ComponentFixture<CreateRequestSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CreateRequestSummaryComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateRequestSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit', () => {

    component.confirmTaskEmitter.subscribe(_ => {
    });

    component.onConfirm();
  });
});
