import {Component, OnInit} from '@angular/core';
import {UserService} from '../../core/service/user.service';
import {HeaderService} from '../../core/service/header.service';
import {HelpRequestService} from '../../core/service/help-request.service';
import {Category, getCategoryIcon, getCategoryName, HelpRequest} from '../../core/model/helpRequest';
import {NotificationService} from '../../core/service/notification.service';
import {CreateRequestService} from '../../core/service/create-request.service';
import {MatDialog} from '@angular/material/dialog';
import {LimitReachedComponent} from '../../shared/limit-reached/limit-reached.component';
import {Observable} from 'rxjs';
import {RateLimit} from '../../core/model/rateLimit';
import {Router} from '@angular/router';

@Component({
  selector: 'tg-create-request',
  templateUrl: './create-request.component.html',
  styleUrls: ['./create-request.component.scss'],
})
export class CreateRequestComponent implements OnInit {
  createRequestProcessStep = 1;
  helpRequest: HelpRequest = {} as HelpRequest;
  $remaining: Observable<RateLimit>;
  isLoadingLimit = false;

  constructor(
    private userService: UserService,
    private headerService: HeaderService,
    private requestService: HelpRequestService,
    private notificationService: NotificationService,
    private createRequestService: CreateRequestService,
    private dialog: MatDialog,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.$remaining = this.requestService.getRemainingRequestsCount();
    this.initializeExistingHelpRequest();
    this.setHeaderState();

    this.userService.getAccount().subscribe((user) => {
      this.helpRequest.requester = user;
    });
    this.loadLimit();
  }

  setCategory(category: Category) {
    this.helpRequest.category = category;
    this.goForward();
  }

  setMainFields(request: HelpRequest) {
    this.helpRequest.address = request.address;
    this.helpRequest.description = request.description;
    this.helpRequest.timeFrame = request.timeFrame;
    this.goForward();
  }

  goBack() {
    this.createRequestProcessStep -= 1;
    this.setHeaderState();
  }

  goForward() {
    this.createRequestProcessStep += 1;
    this.setHeaderState();
  }

  confirmTask() {
    this.requestService.createRequest(this.helpRequest).subscribe(() => {
      this.notificationService.showSnackbar(
        {
          id: this.notificationService.noId(),
          title:
            $localize`:@@request.create.message.title:Neuer Request wurde erstellt`,
          body:
            $localize`:@@request.create.message:Der Request wurde erstellt. Du hörst von uns wenn wir jemanden gefunden haben.`,
          timestamp: new Date(),
          visibility: true,
          icon: getCategoryIcon(this.helpRequest.category),
        }
      );
    });
  }

  setHeaderState() {
    switch (this.createRequestProcessStep) {
      case 1:
        this.headerService.setHeaderState({
          showBack: false,
          text: $localize`:@@request.create.choose.category:Wähle Kategorie`,
          menu: [],
        });
        break;
      case 2:
        this.headerService.setHeaderState({
          showBack: true,
          showBackCallback: (() => this.goBack()).bind(this),
          text: getCategoryName(this.helpRequest.category),
          menu: [],
        });
        break;
      case 3:
        this.headerService.setHeaderState({
          showBack: true,
          showBackCallback: (() => this.goBack()).bind(this),
          text: $localize`:@@request.create.choose.preview:Vorschau`,
          menu: [],
        });
        break;
    }
  }

  initializeExistingHelpRequest() {
    const tmpHelpRequest = this.createRequestService.getHelpRequest();
    if (tmpHelpRequest) {
      this.helpRequest = tmpHelpRequest;
      this.createRequestProcessStep = 2;
    }
  }

  private loadLimit() {
    this.isLoadingLimit = true;

    this.$remaining.subscribe(limit => {
      if (limit.remaining === 0) {
        this.dialog.open(LimitReachedComponent).beforeClosed().subscribe(() => {
          this.router.navigateByUrl('/').then();
        });
      }
      this.isLoadingLimit = false;
    });
  }
}
