import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {CreateRequestComponent} from './create-request.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../../shared/material.module';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {UserService} from '../../core/service/user.service';
import {HelpRequestService} from '../../core/service/help-request.service';
import {of} from 'rxjs';
import {User} from '../../core/model/user';
import {Category, HelpRequest} from '../../core/model/helpRequest';
import {HeaderService} from '../../core/service/header.service';
import {NotificationService} from '../../core/service/notification.service';
import {CreateRequestService} from '../../core/service/create-request.service';
import {RateLimit} from '../../core/model/rateLimit';
import {RouterTestingModule} from '@angular/router/testing';
import SpyObj = jasmine.SpyObj;

describe('CreateRequestComponent', () => {
  let component: CreateRequestComponent;
  let fixture: ComponentFixture<CreateRequestComponent>;
  let userService: SpyObj<UserService>;
  let requestService: SpyObj<HelpRequestService>;
  let headerService: SpyObj<HeaderService>;
  let notificationService: SpyObj<NotificationService>;
  let createRequestService: SpyObj<CreateRequestService>;

  const requester = {
    id: '100',
    firstName: 'Lila',
    lastName: 'Lula',
    email: 'lila.lula@mail.com'
  } as User;

  const mockHelpRequest = {
    id: 'help_request_id',
    category: Category.ANIMAL_SITTING,
    description: 'I need help!',
    requester
  } as HelpRequest;

  beforeEach(async(() => {

    userService = jasmine.createSpyObj('userService', ['getAccount']);
    requestService = jasmine.createSpyObj('requestService', ['createRequest', 'getRemainingRequestsCount']);
    headerService = jasmine.createSpyObj('headerService', ['setHeaderState', 'getHeaderState']);
    notificationService = jasmine.createSpyObj(['addNewNotification']);
    createRequestService = jasmine.createSpyObj(['getHelpRequest']);

    userService.getAccount.and.returnValue(of({
      id: '100',
      firstName: 'Lila',
      lastName: 'Lula',
      email: 'lila.lula@mail.com'
    } as User));

    requestService.createRequest.and.returnValue(of({} as HelpRequest));
    requestService.getRemainingRequestsCount.and.returnValue(of({} as RateLimit));

    createRequestService.getHelpRequest.and.returnValue(mockHelpRequest);

    TestBed.overrideProvider(HeaderService, {useValue: headerService});

    TestBed.configureTestingModule({
      declarations: [CreateRequestComponent],
      imports: [
        ReactiveFormsModule,
        MaterialModule,
        NoopAnimationsModule,
        NgxMaterialTimepickerModule,
        HttpClientTestingModule,
        RouterTestingModule],
      providers: [
        {provider: UserService, useValue: userService},
        {provider: HelpRequestService, useValue: requestService},
        {provide: NotificationService, useValue: notificationService},
        {provide: CreateRequestService, useValue: createRequestService}
      ]
    }).compileComponents();
    TestBed.overrideProvider(UserService, {useValue: userService});
    TestBed.overrideProvider(HelpRequestService, {useValue: requestService});
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set header state correctly', () => {

    component.helpRequest.category = Category.ANIMAL_SITTING;

    component.createRequestProcessStep = 1;

    component.setHeaderState();

    expect(headerService.setHeaderState.calls.mostRecent().args[0].text).toEqual('Wähle Kategorie');

    component.createRequestProcessStep = 2;

    component.setHeaderState();

    expect(headerService.setHeaderState.calls.mostRecent().args[0].text).toEqual('Tierbetreuung');

    component.createRequestProcessStep = 3;

    component.setHeaderState();

    expect(headerService.setHeaderState.calls.mostRecent().args[0].text).toEqual('Vorschau');

  });

  it('set main fields should set correctly', () => {

    const request = {
      requesterId: '4113fa46-bfd0-4639-83e7-dc79245bc8a3',
      category: 'ANIMAL_SITTING',
      address: {
        city: 'Wien',
        country: 'Austria',
        location: {
          longitude: 16.3702533,
          latitude: 48.2009114
        },
        postalCode: '1010',
        streetNumber: '3',
        streetName: 'Karlsplatz'
      },
      description: 'Das ist ein Text',
      timeFrame: {
        from: new Date('2020-05-11T19:55:00.000Z'),
        to: new Date('2020-05-12T19:55:00.000Z')
      }
    };

    component.setMainFields(request as unknown as HelpRequest);

    expect(component.helpRequest.timeFrame).toEqual(request.timeFrame);
    expect(component.helpRequest.description).toEqual(request.description);
    expect(component.helpRequest.address).toEqual(request.address);

  });

  it('should set category correctly', () => {
    component.createRequestProcessStep = 1;
    component.setCategory(Category.ANIMAL_SITTING);
    expect(component.helpRequest.category).toEqual(Category.ANIMAL_SITTING);
    expect(component.createRequestProcessStep).toEqual(2);
  });

  it('should go one step back', () => {
    component.createRequestProcessStep = 3;
    component.goBack();

    expect(component.createRequestProcessStep).toEqual(2);
  });

  it('should initialize existing help request', () => {
    component.initializeExistingHelpRequest();

    expect(component.helpRequest).toEqual(mockHelpRequest);
    expect(component.createRequestProcessStep).toEqual(2);
  });

});
