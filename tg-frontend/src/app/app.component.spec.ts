import {async, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {Title} from '@angular/platform-browser';
import {RouterTestingModule} from '@angular/router/testing';
import {SharedModule} from './shared/shared.module';
import {SwUpdate} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {NotificationService} from './core/service/notification.service';
import {UserService} from './core/service/user.service';
import {of} from 'rxjs';
import {Authority} from './core/model/authority';
import {User, UserState} from './core/model/user';
import {MatSnackBar} from '@angular/material/snack-bar';
import SpyObj = jasmine.SpyObj;

describe('AppComponent', () => {

  let titleService: SpyObj<Title>;
  let notificationService: SpyObj<NotificationService>;
  let userService: SpyObj<UserService>;
  let mockSnackBar: SpyObj<MatSnackBar>;

  // let swUpdate: SpyObj<SwUpdate>;

  beforeEach(async(() => {

    titleService = jasmine.createSpyObj('titleService', ['setTitle']);
    notificationService = jasmine.createSpyObj(['loadNextNotifications', 'getInitialPageParameter', 'setNextPageParameters', 'deleteNotifications', 'subscribeToPushNotifications', 'subscribeToWebSocket']);
    mockSnackBar = jasmine.createSpyObj(['open', 'onAction']);

    userService = jasmine.createSpyObj('userService', ['getAccount', 'updateAccount']);
    userService.getAccount.and.returnValue(of({
      id: '100',
      authorities: [Authority.USER],
      description: 'aa',
      firstName: 'Lila',
      lastName: 'Lula',
      phoneNr: '067678456546',
      email: 'lila.lula@mail.com',
      distance: 10,
      langKey: 'en',
      userState: UserState.ACTIVE,
      hasImage: false
    } as User));

    userService.updateAccount.and.returnValue(of());
    notificationService.loadNextNotifications.and.returnValue(of());
    notificationService.deleteNotifications.and.returnValue([]);

    // swUpdate = jasmine.createSpyObj('swUpdate');

    TestBed.configureTestingModule({
      imports: [RouterTestingModule, SharedModule],
      declarations: [
        AppComponent,
      ],
      providers: [
        {provide: Title, useValue: titleService},
        {provide: SwUpdate, useValue: {}},
        {
          provide: 'WEBPUSH_VAPID_PUBLIC_KEY',
          useValue: environment.webPushPublicKey
        },
        {provide: UserService, useValue: userService},
        {provide: MatSnackBar, useValue: mockSnackBar},
        {provide: NotificationService, useValue: notificationService}
      ]
    }).compileComponents();
    TestBed.overrideProvider(NotificationService, {useValue: notificationService});

  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'TUgood'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('TUgood');
  });

});
