import { KeycloakConfig } from 'keycloak-angular';

const keycloakConfig: KeycloakConfig = {
  url: 'https://auth.dev.tugood.team/auth',
  realm: 'tugood',
  clientId: 'angular',
};

export const environment = {
  production: true,
  webPushPublicKey: '${WEBPUSH_VAPID_PUBLIC_KEY}',
  keycloak: keycloakConfig,
  googleApiKey: 'AIzaSyAfiwDDeZ0q7ASa-39k-LIOkJ8SYmnELWk',
};
