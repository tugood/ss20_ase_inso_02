package com.ase.tugood.notificationservice.infrastructure.configuration

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.web.reactive.HandlerMapping
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping
import org.springframework.web.reactive.socket.WebSocketHandler
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter


@Configuration
@Profile("generatedata")
class TestNotificationSocketConfig {

    val ENDPOINT: String = "/api/socket-notifications/test-notifications-emitter" // ws://localhost:<port>/api/notifications/test-notifications-emitter

    @Autowired
    @Qualifier("TestReactiveWebSocketHandler")
    private val webSocketHandler: WebSocketHandler? = null

    @Bean
    @Qualifier("testWebSocketHandlerMapping")
    fun weebSocketHandlerMapping() : HandlerMapping {
        val map: MutableMap<String, WebSocketHandler?> = HashMap()
        map[ENDPOINT] = webSocketHandler
        val handlerMapping = SimpleUrlHandlerMapping()
        handlerMapping.order = 1
        handlerMapping.urlMap = map
        return handlerMapping
    }

    @Bean
    @Qualifier("testHandlerAdapter")
    fun haandlerAdapter(): WebSocketHandlerAdapter? {
        return WebSocketHandlerAdapter()
    }

}
