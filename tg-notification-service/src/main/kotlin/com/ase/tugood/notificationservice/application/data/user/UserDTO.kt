package com.ase.tugood.notificationservice.application.data.user

import com.fasterxml.jackson.annotation.JsonIgnore
import java.time.Instant
import javax.validation.constraints.Email
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

data class UserDTO(

    var id: String = "",

    @field:Size(max = 50)
    @NotNull
    val firstName: String? = null,

    @field:Size(max = 50)
    @NotNull
    val lastName: String? = null,

    @field:Email
    @field:Size(min = 5, max = 254)
    @NotNull
    val email: String? = null,

    @field:Size(min = 4, max = 20)
    val phoneNr: String? = null,

    @field:Size(min = 2, max = 10)
    val langKey: String? = null,

    val description: String? = null,

    val hasImage: Boolean = false,

    val userState: UserState? = null,

    val distance: Long? = null,

    val avgRating: Double? = null,

    @JsonIgnore
    val createdDate: Instant? = null,

    @JsonIgnore
    val lastModifiedDate: Instant? = null
)
