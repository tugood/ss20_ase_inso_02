package com.ase.tugood.notificationservice.application.data.notification

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

@JsonInclude(JsonInclude.Include.NON_NULL)
data class ServiceNotificationDTO(

    @NotNull
    @NotEmpty
    @JsonProperty("title")
    val title: String = "",

    @JsonProperty("badge")
    val badge: String? = null,

    @NotNull
    @NotEmpty
    @JsonProperty("body")
    val body: String = "",

    @JsonProperty("icon")
    val icon: String? = null,

    @NotNull
    @JsonProperty("userId")
    val userId: String = "",

    @NotNull
    @JsonProperty("userEmail")
    val userEmail: String,

    @NotNull
    @JsonProperty("tag")
    val tag: String = "request",

    @NotNull
    @JsonProperty("notificationType")
    val notificationType: NotificationType = NotificationType.PLAIN_OLD
) {
    enum class NotificationType {
        PUSH,
        PLAIN_OLD,
        BOTH // if not online send a push notification
    }
}
