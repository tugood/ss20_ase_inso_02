package com.ase.tugood.notificationservice.domain.notification.service.mapper

import com.ase.tugood.notificationservice.application.data.notification.ServiceNotificationDTO
import com.ase.tugood.notificationservice.domain.notification.model.PoNotification
import com.ase.tugood.notificationservice.infrastructure.mapper.EntityMapper
import org.springframework.stereotype.Component
import java.time.Instant

@Component
class ServiceNotificationMapper : EntityMapper<ServiceNotificationDTO, PoNotification> {
    override fun toEntity(dto: ServiceNotificationDTO): PoNotification {
        return PoNotification(
            userId = dto.userId,
            timestamp = Instant.now(),
            title = dto.title,
            body = dto.body,
            icon = dto.icon
        )
    }

    override fun toDto(entity: PoNotification): ServiceNotificationDTO {
        throw UnsupportedOperationException("Entities should not be converted to service notifications")
    }


}
