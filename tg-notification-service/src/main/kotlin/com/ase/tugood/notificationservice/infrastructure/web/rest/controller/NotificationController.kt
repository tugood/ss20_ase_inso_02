package com.ase.tugood.notificationservice.infrastructure.web.rest.controller


import com.ase.tugood.notificationservice.application.data.notification.GUINotificationDTO
import com.ase.tugood.notificationservice.application.data.notification.NotificationDTO
import com.ase.tugood.notificationservice.domain.notification.service.IPoNotificationService
import com.ase.tugood.notificationservice.domain.notification.service.IPushNotificationService
import com.ase.tugood.notificationservice.domain.subscription.service.ISubscriptionService
import com.ase.tugood.notificationservice.infrastructure.util.PaginationUtil
import com.ase.tugood.notificationservice.infrastructure.web.model.request.UserInfo
import com.ase.tugood.notificationservice.infrastructure.web.rest.error.TUGoodException
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.swagger.v3.oas.annotations.ExternalDocumentation
import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.info.Contact
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.annotations.servers.Server
import io.swagger.v3.oas.annotations.servers.ServerVariable
import nl.martijndwars.webpush.Subscription
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.web.bind.annotation.*
import org.springframework.web.util.UriComponentsBuilder
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.switchIfEmpty
import reactor.util.function.Tuple2
import java.util.*


@RestController
@RequestMapping("/api/notifications")
@OpenAPIDefinition(
    info = Info(title = "TUgood Notification Service", description = "This is the API for the Notification Service", contact = Contact(email = "e1525631@student.tuwien.ac.at"), version = "1.0.0"),
    externalDocs = ExternalDocumentation(description = "Project Repo", url = "https://reset.inso.tuwien.ac.at/repo/2020ss-ASE-group/ss20_ase_inso_02/-/tree/master/tg-notification-service"),
    servers = [
        Server(url = "http://127.0.0.1:{port}", description = "tbd", variables = [ServerVariable(name = "port", defaultValue = "8083")])
    ]
)
class NotificationController(@Autowired private val pushNotificationService: IPushNotificationService,
                             @Autowired private val subscriptionService: ISubscriptionService,
                             @Autowired private val poNotificationService: IPoNotificationService) {

    @PutMapping(path = ["/subscribe"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun subscribe(
        @RequestHeader("X-Userinfo") userInfo: String,
        @RequestBody subscription: Subscription
    ): Mono<ResponseEntity<Map<String, String>>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))

        return subscriptionService
            .subscribe(subscription, info)
            .flatMap { sub -> pushNotificationService.sendEnabledPushNotificationsMessage(sub, info) }
            .map { httpResponse -> ResponseEntity.status(httpResponse).body(mapOf("subscribe" to "done")) }
            .switchIfEmpty { Mono.just(ResponseEntity.ok(mapOf("subscribe" to "not executed or already subscribed"))) }
    }

    // use post instead of delete, since for delete, there is no body
    @PostMapping(path = ["/unsubscribe"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun unsubscribe(
        @RequestHeader("X-Userinfo") userInfo: String,
        @RequestBody subscription: Subscription
    ): Mono<ResponseEntity<Map<String, String>>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))

        return subscriptionService
            .unsubscribe(subscription, info.id)
            .map { ResponseEntity.ok().body(mapOf("unsubscribe" to "done")) }
    }

    /**
     * `GET /api/notifications` : get all notifications from newest to oldest
     *
     * @param userInfo the information of the authenticated user
     * @return the [ResponseEntity] with status `200 (OK)` and with all notifications
     */
    @GetMapping(path = [""], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getAllNotifications(
        @RequestHeader("X-Userinfo") userInfo: String,
        @RequestParam(name = "page") page: Int,
        @RequestParam(name = "size") size: Int,
        request: ServerHttpRequest
    ): Mono<ResponseEntity<List<GUINotificationDTO>>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        val pageable = PageRequest.of(page, size)

        return poNotificationService
            .getNotificationsByUserId(info.id, pageable)
            .collectList().zipWith(poNotificationService.countNotificationsByUserId(info.id))
            .map { notificationsAndCount: Tuple2<List<GUINotificationDTO>, Long> ->
                // we don't want the page information in the body, thus we only provide an empty list
                val body = PageImpl(listOf<NotificationDTO>(), pageable, notificationsAndCount.t2)
                // the headers have links, which refer to the next pages
                val headers = PaginationUtil.generatePaginationHttpHeaders(UriComponentsBuilder.fromHttpRequest(request), body)

                ResponseEntity.ok()
                    .headers(headers)
                    .body(notificationsAndCount.t1)
            }
            .onErrorMap {
                throw TUGoodException(
                    message = "Cannot fetch notifications",
                    status = HttpStatus.INTERNAL_SERVER_ERROR,
                    type = "FetchNotifications",
                    cause = it)
            }


    }

    @DeleteMapping(path = ["/{id}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun deleteNotification(
        @RequestHeader("X-Userinfo") userInfo: String,
        @PathVariable("id") notificationId: String
    ): Mono<ResponseEntity<Map<String, String>>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))

        return poNotificationService
            .deleteNotification(info.id, notificationId)
            .map { ResponseEntity.ok().body(mapOf("delete" to "success")) }
            .onErrorMap {
                throw TUGoodException(
                    message = "Cannot delete notifications",
                    status = HttpStatus.INTERNAL_SERVER_ERROR,
                    type = "DeleteNotifications",
                    cause = it)
            }
    }
}
