package com.ase.tugood.notificationservice.application

import com.ase.tugood.notificationservice.application.data.notification.GUINotificationDTO
import com.ase.tugood.notificationservice.application.data.notification.ServiceNotificationDTO
import com.ase.tugood.notificationservice.domain.notification.service.IPoNotificationService
import com.ase.tugood.notificationservice.domain.notification.service.mapper.PoNotificationMapper
import com.ase.tugood.notificationservice.domain.notification.service.mapper.ServiceNotificationMapper
import com.ase.tugood.notificationservice.infrastructure.persistence.PoNotificationRepository
import com.ase.tugood.notificationservice.infrastructure.web.socket.ReactiveWebSocketHandler
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class PoNotificationService(@Autowired private val webSocketHandler: ReactiveWebSocketHandler,
                            @Autowired private val repository: PoNotificationRepository,
                            @Autowired private val poNotificationMapper: PoNotificationMapper,
                            @Autowired private val serviceNotificationMapper: ServiceNotificationMapper) : IPoNotificationService {

    val logger: Logger = LoggerFactory.getLogger(javaClass)

    override fun sendNotification(serviceNotificationDTO: ServiceNotificationDTO): Mono<Void> {
        return repository.save(serviceNotificationMapper.toEntity(serviceNotificationDTO))
            .flatMap { entity -> webSocketHandler.reactiveSend(serviceNotificationDTO.userId, poNotificationMapper.toDto(entity)) }
    }

    override fun countNotificationsByUserId(userId: String): Mono<Long> {
        return repository.countAllByUserId(userId)
    }

    override fun getNotificationsByUserId(userId: String, pageable: Pageable): Flux<GUINotificationDTO> {
        return repository.findAllByUserIdOrderByTimestampDesc(userId, pageable)
            .map { poNotificationMapper.toDto(it) }
    }

    override fun deleteNotification(userId: String, notificationId: String): Mono<Void> {
        return repository.deleteAllByIdAndUserId(notificationId, userId)
    }
}
