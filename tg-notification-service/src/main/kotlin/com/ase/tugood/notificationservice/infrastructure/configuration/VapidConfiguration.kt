package com.ase.tugood.notificationservice.infrastructure.configuration

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.PropertySource

@Configuration
@PropertySource("classpath:application.yml")
class VapidConfiguration {

    @Value("\${webpush.vapid.public-key}")
    lateinit var publicKey: String

    @Value("\${webpush.vapid.private-key}")
    lateinit var privateKey: String
}
