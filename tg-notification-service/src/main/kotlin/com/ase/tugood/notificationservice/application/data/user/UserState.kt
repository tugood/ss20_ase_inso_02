package com.ase.tugood.notificationservice.application.data.user

import com.fasterxml.jackson.annotation.JsonProperty

enum class UserState(val userState: String) {
    @JsonProperty("ACTIVE")
    ACTIVE("ACTIVE"),

    @JsonProperty("BLOCKED")
    BLOCKED("BLOCKED");
}
