package com.ase.tugood.notificationservice.infrastructure.configuration

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.HandlerMapping
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping
import org.springframework.web.reactive.socket.WebSocketHandler
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter


@Configuration
class NotificationSocketConfig {

    val ENDPOINT: String = "/api/socket-notifications/notifications-emitter" // ws://localhost:<port>/api/notifications/notifications-emitter

    @Autowired
    @Qualifier("ReactiveWebSocketHandler")
    private val webSocketHandler: WebSocketHandler? = null

    @Bean
    fun webSocketHandlerMapping() : HandlerMapping {
        val map: MutableMap<String, WebSocketHandler?> = HashMap()
        map[ENDPOINT] = webSocketHandler
        val handlerMapping = SimpleUrlHandlerMapping()
        handlerMapping.order = 1
        handlerMapping.urlMap = map
        return handlerMapping
    }

    @Bean
    fun handlerAdapter(): WebSocketHandlerAdapter? {
        return WebSocketHandlerAdapter()
    }

}
