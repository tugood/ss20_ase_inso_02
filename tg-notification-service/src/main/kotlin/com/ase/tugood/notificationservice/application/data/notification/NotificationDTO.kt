package com.ase.tugood.notificationservice.application.data.notification

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@JsonInclude(JsonInclude.Include.NON_NULL)
data class NotificationDTO(

    @NotNull
    @NotBlank
    @JsonProperty("title")
    val title: String,

    @JsonProperty("data")
    val data: Any? = null,

    @JsonProperty("badge")
    val badge: String? = null,

    @NotNull
    @NotBlank
    @JsonProperty("body")
    val body: String,

    @JsonProperty("dir")
    val dir: Direction? = null,

    @JsonProperty("icon")
    val icon: String? = null,

    @JsonProperty("image")
    val image: String? = null,

    @JsonProperty("lang")
    val lang: String? = null,

    @JsonProperty("renotify")
    val renotify: Boolean? = null,

    @JsonProperty("requireInteraction")
    val requireInteraction: Boolean? = null,

    @JsonProperty("silent")
    val silent: Boolean? = null,

    @JsonProperty("tag")
    val tag: String? = null,

    @JsonProperty("vibrate")
    val vibrate: List<Int>? = null,

    @JsonProperty("timestamp")
    val timestamp // millis since 1970-01-01
    : Long? = null,

    @JsonProperty("actions")
    val actions: List<NotificationActionDTO>? = null
) {
    enum class Direction {
        auto, ltr, rtl
    }

    @JsonIgnore
    fun getPushNotificationJson(): String {
        return jacksonObjectMapper().writeValueAsString(mapOf("notification" to this))
    }
}
