package com.ase.tugood.notificationservice.domain.notification.service

import com.ase.tugood.notificationservice.application.data.notification.ServiceNotificationDTO
import com.ase.tugood.notificationservice.infrastructure.web.model.request.UserInfo
import nl.martijndwars.webpush.Subscription
import org.springframework.http.HttpStatus
import reactor.core.publisher.Mono

/**
 * Provide several functions for sending push notifications to the user
 */
interface IPushNotificationService {

    /**
     * send a push notification to the user who just enabled the subscription for the push notifications
     *
     * @param subscription subscription for a specific device for a specific user
     * @param userInfo info for the user who just subscribed
     * @return http status code of the platform specific push API
     */
    fun sendEnabledPushNotificationsMessage(subscription: Subscription, userInfo: UserInfo): Mono<HttpStatus>

    /**
     * send an arbitrary push notification to the user
     *
     * @param serviceNotificationDTO: push notification to send
     * @return http status code of the platform specific push API
     */
    fun sendServicePushNotification(serviceNotificationDTO: ServiceNotificationDTO): Mono<HttpStatus>
}
