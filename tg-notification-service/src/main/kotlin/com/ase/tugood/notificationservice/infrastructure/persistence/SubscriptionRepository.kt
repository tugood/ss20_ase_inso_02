package com.ase.tugood.notificationservice.infrastructure.persistence

import com.ase.tugood.notificationservice.domain.subscription.model.TUGoodNotificationSubscription
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Repository
interface SubscriptionRepository : ReactiveMongoRepository<TUGoodNotificationSubscription, String> {
    fun existsByUserIdAndEndpoint(userId: String, endpoint: String): Mono<Boolean>

    fun findAllByUserId(userId: String): Flux<TUGoodNotificationSubscription>

    fun deleteAllByUserIdAndEndpoint(userId: String, endpoint: String): Mono<Void>
}
