package com.ase.tugood.notificationservice.infrastructure.web.socket

import com.ase.tugood.notificationservice.application.data.notification.GUINotificationDTO
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.web.reactive.socket.WebSocketHandler
import org.springframework.web.reactive.socket.WebSocketSession
import reactor.core.publisher.EmitterProcessor
import reactor.core.publisher.Mono

@Component("ReactiveWebSocketHandler")
class ReactiveWebSocketHandler : WebSocketHandler {

    private var clientFluxSinks: MutableMap<String, EmitterProcessor<String>> = mutableMapOf()
    private var webSocketSessions: MutableMap<String, String> = mutableMapOf()

    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    override fun handle(webSocketSession: WebSocketSession): Mono<Void> {
        return webSocketSession.receive().map { message ->
            val userId = message.payloadAsText.replace("\"", "")
            logger.info("User with id $userId subscribed to websocket session ${webSocketSession.id}")

            webSocketSessions.putIfAbsent(webSocketSession.id, userId)

            val processor = EmitterProcessor.create<String>()
            clientFluxSinks[userId] = processor

            webSocketSession.send(
                clientFluxSinks[userId]!!.map { s ->
                    webSocketSession.textMessage(s!!)
                }
            ).subscribe()

        }.doFinally { sig ->
            // The cancellation is the last inbound message
            val userId = webSocketSessions[webSocketSession.id]
            logger.info("Terminating websocket session with id ${webSocketSession.id} for user $userId. Reason: $sig")
            webSocketSession.close()

            webSocketSessions.remove(webSocketSession.id)

            if (userId != null) {
                clientFluxSinks.remove(userId)
            }
        }.then()
    }

    /**
     * reactive wrapper function for the send function
     */
    fun reactiveSend(userId: String, notification: GUINotificationDTO): Mono<Void> {
        return Mono.create { sink ->
            try {
                logger.info("Sending websocket notification with id ${notification.id} to user $userId")
                send(userId, notification)
                sink.success()
            } catch (e: Exception) {
                sink.error(e)
            }
        }
    }

    private fun send(userId: String, notification: GUINotificationDTO) {
        val emitter: EmitterProcessor<String> = clientFluxSinks[userId] ?: throw IllegalArgumentException()
        emitter.onNext(json.writeValueAsString(notification))
    }

    companion object {
        private val json = ObjectMapper()

        init {
            val module = JavaTimeModule()
            json.registerModule(module)
            json.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        }
    }
}
