package com.ase.tugood.notificationservice.domain.subscription.service.mapper

import com.ase.tugood.notificationservice.domain.subscription.model.TUGoodNotificationSubscription
import com.ase.tugood.notificationservice.infrastructure.configuration.DateTimeProvider
import com.ase.tugood.notificationservice.infrastructure.mapper.EntityMapper
import com.ase.tugood.notificationservice.infrastructure.mapper.EntityMapperWithUserInfo
import com.ase.tugood.notificationservice.infrastructure.web.model.request.UserInfo
import nl.martijndwars.webpush.Subscription
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.util.*

/**
 * Mapper for the entity {@link Match} and its DTO {@link MatchDTO}.
 */
@Component
class SubscriptionMapper(@Autowired var dateTimeProvider: DateTimeProvider) : EntityMapperWithUserInfo<Subscription, TUGoodNotificationSubscription> {
    override fun toEntity(dto: Subscription, userInfo: UserInfo): TUGoodNotificationSubscription {
        return merge(TUGoodNotificationSubscription(), dto, userInfo)
    }

    override fun merge(entity: TUGoodNotificationSubscription, dto: Subscription, userInfo: UserInfo): TUGoodNotificationSubscription {
        val now = dateTimeProvider.now()

        return entity.apply {
            endpoint = dto.endpoint
            userId = userInfo.id
            p256dh = dto.keys.p256dh
            auth = dto.keys.auth
            createdBy = userInfo.email
            createdDate = now
            lastModifiedBy = userInfo.email
            lastModifiedDate = now
        }
    }

    override fun toDto(entity: TUGoodNotificationSubscription): Subscription {
        return Subscription().apply {
            endpoint = entity.endpoint
            keys = Subscription.Keys().apply {
                p256dh = entity.p256dh
                auth = entity.auth
            }
        }
    }
}
