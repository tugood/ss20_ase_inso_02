package com.ase.tugood.notificationservice.domain.subscription.model

import org.springframework.data.annotation.*
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import java.io.Serializable
import java.time.Instant
import javax.validation.constraints.NotNull


@Document(collection = "tugood_subscription")
data class TUGoodNotificationSubscription @JvmOverloads constructor(

    @Id
    var id: String? = null,

    @NotNull
    @Field("userId")
    var userId: String? = null,

    @NotNull
    @Field("endpoint")
    var endpoint: String? = null,

    @NotNull
    @Field("p256dh")
    var p256dh: String? = null,

    @NotNull
    @Field("auth")
    var auth: String? = null,

    @CreatedBy
    @Field("createdBy")
    var createdBy: String? = null,

    @CreatedDate
    @Field("createdDate")
    var createdDate: Instant? = Instant.now(),

    @LastModifiedBy
    @Field("lastModifiedBy")
    var lastModifiedBy: String? = null,

    @LastModifiedDate
    @Field("lastModifiedDate")
    var lastModifiedDate: Instant? = Instant.now()

) : Serializable {

    companion object {
        private const val serialVersionUID = 1L
    }
}
