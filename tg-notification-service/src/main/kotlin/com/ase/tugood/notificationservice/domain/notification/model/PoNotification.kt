package com.ase.tugood.notificationservice.domain.notification.model

import org.springframework.data.annotation.*
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import java.io.Serializable
import java.time.Instant
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size


@Document(collection = "tugood_notification")
data class PoNotification @JvmOverloads constructor(

    @Id
    val id: String? = null,

    @NotNull
    @Field("userId")
    val userId: String,

    @CreatedDate
    @Field("timestamp")
    val timestamp: Instant,

    @NotNull
    @NotBlank
    @Field("title")
    val title: String,

    @NotNull
    @NotBlank
    @Field("body")
    val body: String,

    @field:Size(min = 1, max = 255)
    @NotNull
    @Field("icon")
    val icon: String? = null
) : Serializable {

    companion object {
        private const val serialVersionUID = 1L
    }
}
