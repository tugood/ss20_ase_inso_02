package com.ase.tugood.notificationservice.domain.notification.service.mapper

import com.ase.tugood.notificationservice.application.data.notification.GUINotificationDTO
import com.ase.tugood.notificationservice.domain.notification.model.PoNotification
import com.ase.tugood.notificationservice.infrastructure.mapper.EntityMapper
import org.springframework.stereotype.Component

@Component
class PoNotificationMapper: EntityMapper<GUINotificationDTO, PoNotification> {
    override fun toEntity(dto: GUINotificationDTO): PoNotification {
        throw UnsupportedOperationException("GUINotifications should not be converted to entities")
    }

    override fun toDto(entity: PoNotification): GUINotificationDTO {
        return GUINotificationDTO(
                id = entity.id!!,
                timestamp = entity.timestamp,
                title = entity.title,
                body = entity.body,
                icon = entity.icon
        )
    }

}
