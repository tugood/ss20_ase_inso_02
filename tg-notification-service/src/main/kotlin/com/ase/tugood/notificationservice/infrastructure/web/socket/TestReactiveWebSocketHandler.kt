package com.ase.tugood.notificationservice.infrastructure.web.socket

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component
import org.springframework.web.reactive.socket.WebSocketHandler
import org.springframework.web.reactive.socket.WebSocketMessage
import org.springframework.web.reactive.socket.WebSocketSession
import reactor.core.publisher.EmitterProcessor
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Duration


@Component("TestReactiveWebSocketHandler")
@Profile("generatedata")
class TestReactiveWebSocketHandler : WebSocketHandler {

    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    override fun handle(webSocketSession: WebSocketSession): Mono<Void> {
        return webSocketSession.send(Flux
                .interval(Duration.ofMillis(500))
                .map { t: Long? -> webSocketSession.textMessage(t.toString())  }
            )
            .and(webSocketSession.receive().map { msg: WebSocketMessage? ->  logger.debug(msg?.payloadAsText)})
            .then()
    }
    companion object {
        private val json = ObjectMapper()

        init {
            val module = JavaTimeModule()
            json.registerModule(module)
            json.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        }
    }
}
