package com.ase.tugood.notificationservice.application.data.notification

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

@JsonInclude(JsonInclude.Include.NON_NULL)
data class NotificationActionDTO(

    @JsonProperty("action")
    val action: String = "",

    @JsonProperty("title")
    val title: String = "",

    @JsonProperty("icon")
    val icon: String? = null
)


