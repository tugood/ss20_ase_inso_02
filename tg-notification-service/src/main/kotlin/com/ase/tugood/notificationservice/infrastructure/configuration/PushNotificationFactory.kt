package com.ase.tugood.notificationservice.infrastructure.configuration

import nl.martijndwars.webpush.PushService
import nl.martijndwars.webpush.Utils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

@Component
class PushNotificationFactory(@Autowired final var vapidConfiguration: VapidConfiguration) {

    // Base64 string server public/private key
    private val vapidPrivateKey: String = vapidConfiguration.privateKey
    val vapidPublicKey: String = vapidConfiguration.publicKey


    fun getPushService(email: String): PushService {
        return PushService().apply {
            setSubject("mailto:$email")
            publicKey = Utils.loadPublicKey(vapidPublicKey)
            privateKey = Utils.loadPrivateKey(vapidPrivateKey)
        }
    }
}
