package com.ase.tugood.notificationservice.infrastructure.persistence

import com.ase.tugood.notificationservice.domain.notification.model.PoNotification
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.data.repository.reactive.ReactiveCrudRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Repository
interface PoNotificationRepository : ReactiveMongoRepository<PoNotification, String> {
    fun countAllByUserId(userId: String): Mono<Long>

    fun findAllByUserIdOrderByTimestampDesc(userId: String, pageable: Pageable): Flux<PoNotification>

    fun deleteAllByIdAndUserId(notificationId: String, userId: String): Mono<Void>
}
