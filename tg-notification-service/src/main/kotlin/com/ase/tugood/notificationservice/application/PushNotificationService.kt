package com.ase.tugood.notificationservice.application

import com.ase.tugood.notificationservice.application.data.notification.NotificationDTO
import com.ase.tugood.notificationservice.application.data.notification.ServiceNotificationDTO
import com.ase.tugood.notificationservice.application.data.user.UserDTO
import com.ase.tugood.notificationservice.domain.notification.service.IPushNotificationService
import com.ase.tugood.notificationservice.infrastructure.configuration.PushNotificationFactory
import com.ase.tugood.notificationservice.infrastructure.web.model.request.UserInfo
import nl.martijndwars.webpush.Notification
import nl.martijndwars.webpush.PushService
import nl.martijndwars.webpush.Subscription
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.*

@Service
class PushNotificationService(@Autowired var pushNotificationFactory: PushNotificationFactory,
                              @Autowired var subscriptionService: SubscriptionService,
                              @Autowired var userService: UserService,
                              @Autowired var messageSource: MessageSource) : IPushNotificationService {

    val logger: Logger = LoggerFactory.getLogger(javaClass)

    override fun sendEnabledPushNotificationsMessage(subscription: Subscription, userInfo: UserInfo): Mono<HttpStatus> {
        return userService
            .getUserForId(userInfo.id)
            .flatMap {
                val userNotificationDTO = ServiceNotificationDTO(
                    title = messageSource.getMessage("notification.subscription.new.title", null, getLang(it)),
                    body = messageSource.getMessage("notification.subscription.new.message", null, getLang(it)),
                    tag = "new subscription",
                    userId = userInfo.id,
                    userEmail = userInfo.email ?: ""
                )

                sendPushNotification(subscription, userNotificationDTO)
            }
    }

    fun getLang(userDTO: UserDTO): Locale {
        if (userDTO.langKey == null || userDTO.langKey.isEmpty()) {
            logger.error("No language defined for user ${userDTO.id}")
            return Locale.GERMAN
        }

        return try {
            Locale(userDTO.langKey)
        } catch (e: Exception) {
            logger.error("Cannot convert language key ${userDTO.langKey} for user ${userDTO.id}")
            Locale.GERMAN
        }
    }

    override fun sendServicePushNotification(serviceNotificationDTO: ServiceNotificationDTO): Mono<HttpStatus> {
        return subscriptionService
            .getSubscriptions(serviceNotificationDTO.userId)
            .flatMap { sub ->
                sendPushNotification(sub, serviceNotificationDTO)
                    .map { status ->
                        if (HttpStatus.GONE == status || HttpStatus.FORBIDDEN == status) {
                            subscriptionService.unsubscribe(sub, serviceNotificationDTO.userId)
                                .subscribe(
                                    { logger.info("Unsubscribed user ${serviceNotificationDTO.userId} due to status $status"); },
                                    { cause -> logger.error("Cannot unsubscribe. Exception: ${cause.message}") }
                                ) // just throw away the result, as we don't want to disturb the whole process
                        }

                        // return the status again
                        status
                    }
            }
            .onErrorResume { Flux.just(HttpStatus.INTERNAL_SERVER_ERROR) }
            .switchIfEmpty(Flux.just(HttpStatus.OK))
            .collectList()
            .map { httpCodes: List<HttpStatus> ->
                val statusCode = httpCodes.stream()
                    .filter { !it.is2xxSuccessful }
                    .findAny()

                // check if there was any unsuccessful send notification operation
                if (statusCode.isPresent) {
                    statusCode.get()
                } else {
                    HttpStatus.OK
                }
            }
    }

    /**
     * send a push notification to a user for several active subscriptions
     *
     * @param subscription active subscriptions for a user
     * @param serviceNotificationDTO notification to send to the user (this notification has been created by another service)
     * @return http status code of the platform specific push API
     */
    fun sendPushNotification(subscription: Subscription, serviceNotificationDTO: ServiceNotificationDTO): Mono<HttpStatus> {

        val notificationDTO = NotificationDTO(
            title = serviceNotificationDTO.title,
            body = serviceNotificationDTO.body,
            tag = serviceNotificationDTO.tag
        )

        // Construct notification
        val notification = Notification(subscription, notificationDTO.getPushNotificationJson())

        // Construct push service
        val pushService: PushService? = pushNotificationFactory.getPushService(serviceNotificationDTO.userEmail)

        // Send notification!
        return Mono
            .fromSupplier { pushService!!.send(notification) }
            .map { httpResponse ->
                logger.info("Sent notification for user ${serviceNotificationDTO.userId} and received ${httpResponse.statusLine.statusCode} from the Push API")
                HttpStatus.valueOf(httpResponse.statusLine.statusCode)
            }
            .onErrorResume {
                logger.error("Could not send push notification for user ${serviceNotificationDTO.userId}. Exception: ${it.message}")
                Mono.just(HttpStatus.INTERNAL_SERVER_ERROR)
            }
    }
}
