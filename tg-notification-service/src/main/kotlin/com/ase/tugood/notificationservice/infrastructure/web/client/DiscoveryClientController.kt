package com.ase.tugood.notificationservice.infrastructure.web.client

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.cloud.client.ServiceInstance
import org.springframework.cloud.client.discovery.DiscoveryClient
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import java.net.URI
import javax.naming.ServiceUnavailableException


@RestController
class DiscoveryClientController {
    @Autowired
    private val discoveryClient: DiscoveryClient? = null

    fun userService(): Mono<URI> {
        return Mono.justOrEmpty(
            discoveryClient!!.getInstances("tg-user-service")
                .stream()
                .findFirst()
                .map { si: ServiceInstance -> si.uri }
                .orElseThrow { ServiceUnavailableException() }
        )
    }
}
