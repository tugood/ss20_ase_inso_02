package com.ase.tugood.notificationservice.domain.subscription.service

import com.ase.tugood.notificationservice.infrastructure.web.model.request.UserInfo
import nl.martijndwars.webpush.Subscription
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

/**
 * Provide several functions for managing subscriptions for push notifications
 */
interface ISubscriptionService {

    /**
     * subscribes a user with their browser specific parameters to receive push notifications
     * If the subscription already exists, an empty Mono publisher is being returned
     *
     * @param subscription: browser and user specific subscription values for the Push API
     * @param userInfo: user related fields
     *
     * @return created subscription or empty publisher, if it already existed
     */
    fun subscribe(subscription: Subscription, userInfo: UserInfo): Mono<Subscription>

    /**
     * get all subscriptions for a specified user
     *
     * @param userId for the user who should receive push notifications
     *
     * @return all subscriptions for the specified user
     */
    fun getSubscriptions(userId: String): Flux<Subscription>

    /**
     * unsubscribe a user by deleting the entry for the specified user information and
     * platform specific subscription information
     *
     * @param subscription platform specific subscription details
     * @param userId for the user to unsubscribe
     * @return publisher signal for the save and send operation (can be used to check if there were errors)
     */
    fun unsubscribe(subscription: Subscription, userId: String): Mono<Void>
}
