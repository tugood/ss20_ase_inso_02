package com.ase.tugood.notificationservice.infrastructure.mapper

import com.ase.tugood.notificationservice.infrastructure.web.model.request.UserInfo

/**
 * Contract for a generic dto to entity mapper.
 *
 * @param <D> - DTO type parameter.
 * @param <E> - Entity type parameter.
 */
interface EntityMapperWithUserInfo<D, E> {

    /**
     * This function maps a generic DTO to the generic entity.
     *
     * @param dto the DTO to be mapped to an entity
     * @param userInfo the userInfo object that holds user specific information to be mapped
     * @return the mapped entity instance
     */
    fun toEntity(dto: D, userInfo: UserInfo): E

    /**
     * The aim of this function is to set merge fields of the DTO into the given entity.
     *
     * @param entity the entity to merge the fields into
     * @param dto the DTO providing the fields to merge into the entity
     * @return the merged entity
     */
    fun merge(entity: E, dto: D, userInfo: UserInfo): E

    /**
     * The aim of this function is to map a generic entity to a generic DTO.
     *
     * @param entity the entity to be mapped to a DTO
     * @return the mapped DTO
     */
    fun toDto(entity: E): D
}
