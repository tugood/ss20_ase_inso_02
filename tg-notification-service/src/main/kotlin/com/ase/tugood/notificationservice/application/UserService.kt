package com.ase.tugood.notificationservice.application

import com.ase.tugood.notificationservice.application.data.user.UserDTO
import com.ase.tugood.notificationservice.infrastructure.web.client.UserServiceClient
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class UserService(@Autowired private val userServiceClient: UserServiceClient) {

    fun getUserForId(userId: String): Mono<UserDTO> {
        return userServiceClient
            .getUserData(userId)
    }
}
