package com.ase.tugood.notificationservice.infrastructure.web.rest.controller


import com.ase.tugood.notificationservice.application.data.notification.ServiceNotificationDTO
import com.ase.tugood.notificationservice.domain.notification.service.IPoNotificationService
import com.ase.tugood.notificationservice.domain.notification.service.IPushNotificationService
import io.swagger.v3.oas.annotations.ExternalDocumentation
import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.info.Contact
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.servers.Server
import io.swagger.v3.oas.annotations.servers.ServerVariable
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono


@RestController
@RequestMapping("/internal/notifications")
@OpenAPIDefinition(
    info = Info(title = "TUgood Notification Service",
        description = "This is the internal API for the Notification Service",
        contact = Contact(email = "e1525631@student.tuwien.ac.at"),
        version = "1.0.0"),
    externalDocs = ExternalDocumentation(description = "Project Repo", url = "https://reset.inso.tuwien.ac.at/repo/2020ss-ASE-group/ss20_ase_inso_02/-/tree/master/tg-notification-service"),
    servers = [
        Server(url = "http://127.0.0.1:{port}", variables = [ServerVariable(name = "port", defaultValue = "8087")])
    ]
)
class InternalNotificationController(@Autowired private val pushNotificationService: IPushNotificationService,
                                     @Autowired private val poNotificationService: IPoNotificationService) {

    private val logger: Logger = LoggerFactory.getLogger(javaClass)

    @Operation(
        description = "Trigger a new notification for a specific user.",
        method = "notify",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "Notification was sent.")]
    )
    @PostMapping(path = ["/notify"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun notify(
        @RequestBody serviceNotificationDTO: ServiceNotificationDTO
    ): Mono<ResponseEntity<Map<String, String>>> {
        logger.info("Create notification for user: {}", serviceNotificationDTO.userId)

        when (serviceNotificationDTO.notificationType) {
            ServiceNotificationDTO.NotificationType.BOTH -> {
                // try to send a 'plain-old' notification and if it fails send a push notification
                return poNotificationService.sendNotification(serviceNotificationDTO)
                    .map { ResponseEntity.ok(mapOf("notify" to "succeeded")) }
                    .onErrorResume {
                        logger.error("Cannot send notification to user ${serviceNotificationDTO.userId}. Exception: ${it.message}")
                        sendToPushAPI(serviceNotificationDTO)
                    }
            }
            ServiceNotificationDTO.NotificationType.PUSH -> {
                return sendToPushAPI(serviceNotificationDTO)
            }
            ServiceNotificationDTO.NotificationType.PLAIN_OLD -> {
                return poNotificationService.sendNotification(serviceNotificationDTO)
                    .map { ResponseEntity.ok(mapOf("notify" to "succeeded")) }
                    .onErrorResume { cause ->
                        logger.error("Cannot send notification to user ${serviceNotificationDTO.userId}. Exception: ${cause.message}")

                        // We send OK, because the notification should be saved.
                        // However, there is currently no active websocket session
                        Mono.just(ResponseEntity.ok(mapOf("notify" to "failed")))
                    }
            }
        }
    }

    private fun sendToPushAPI(serviceNotificationDTO: ServiceNotificationDTO): Mono<ResponseEntity<Map<String, String>>> {
        return pushNotificationService
            .sendServicePushNotification(serviceNotificationDTO)
            .map { statusCode: HttpStatus ->
                if (!statusCode.is2xxSuccessful) {
                    ResponseEntity.status(statusCode).body(mapOf("notify" to "failed"))
                } else {
                    ResponseEntity.ok(mapOf("notify" to "succeeded"))
                }
            }
            .onErrorResume { Mono.just(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(mapOf("notify" to "failed"))) }
    }
}
