package com.ase.tugood.notificationservice.infrastructure.mapper

/**
 * Contract for a generic dto to entity mapper.
 *
 * @param <D> - DTO type parameter.
 * @param <E> - Entity type parameter.
 */
interface EntityMapper<D, E> {

    /**
     * This function maps a generic DTO to the generic entity.
     *
     * @param dto the DTO to be mapped to an entity
     * @return the mapped entity instance
     */
    fun toEntity(dto: D): E

    /**
     * The aim of this function is to map a generic entity to a generic DTO.
     *
     * @param entity the entity to be mapped to a DTO
     * @return the mapped DTO
     */
    fun toDto(entity: E): D
}
