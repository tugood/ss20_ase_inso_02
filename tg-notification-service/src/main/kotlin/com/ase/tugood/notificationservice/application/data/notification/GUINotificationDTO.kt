package com.ase.tugood.notificationservice.application.data.notification

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.time.Instant
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

@JsonInclude(JsonInclude.Include.NON_NULL)
data class GUINotificationDTO(

    @NotNull
    @NotEmpty
    @JsonProperty("id")
    val id: String = "",

    @NotNull
    @NotEmpty
    @JsonProperty("timestamp")
    val timestamp: Instant = Instant.now(),

    @NotNull
    @NotBlank
    @JsonProperty("title")
    val title: String = "",

    @NotNull
    @NotBlank
    @JsonProperty("body")
    val body: String = "",

    @JsonProperty("icon")
    val icon: String? = null
)
