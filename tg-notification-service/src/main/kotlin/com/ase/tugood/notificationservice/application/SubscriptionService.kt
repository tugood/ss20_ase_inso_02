package com.ase.tugood.notificationservice.application

import com.ase.tugood.notificationservice.domain.subscription.service.ISubscriptionService
import com.ase.tugood.notificationservice.domain.subscription.service.mapper.SubscriptionMapper
import com.ase.tugood.notificationservice.infrastructure.persistence.SubscriptionRepository
import com.ase.tugood.notificationservice.infrastructure.web.model.request.UserInfo
import nl.martijndwars.webpush.Subscription
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono


@Service
class SubscriptionService(@Autowired var subscriptionRepository: SubscriptionRepository,
                          @Autowired var subscriptionMapper: SubscriptionMapper) : ISubscriptionService {

    private val logger: Logger = LoggerFactory.getLogger(javaClass)

    override fun subscribe(subscription: Subscription, userInfo: UserInfo): Mono<Subscription> {
        return subscriptionRepository
            .existsByUserIdAndEndpoint(userInfo.id, subscription.endpoint)
            .filter {exists ->
                logger.debug("Subscription for user ${userInfo.id} exists $exists")
                !exists
            }
            .flatMap { _ ->
                val entity = subscriptionMapper.toEntity(subscription, userInfo)
                logger.info("Save subscription for ${entity.userId}")
                subscriptionRepository.save(entity)
            }
            .map { sub -> subscriptionMapper.toDto(sub) }
    }

    override fun getSubscriptions(userId: String): Flux<Subscription> {
        return subscriptionRepository
            .findAllByUserId(userId)
            .map { sub -> subscriptionMapper.toDto(sub) }
    }

    override fun unsubscribe(subscription: Subscription, userId: String): Mono<Void> {
        logger.info("Unsubscribe endpoint ${subscription.endpoint} for user $userId")
        return subscriptionRepository
            .deleteAllByUserIdAndEndpoint(userId, subscription.endpoint)
    }
}
