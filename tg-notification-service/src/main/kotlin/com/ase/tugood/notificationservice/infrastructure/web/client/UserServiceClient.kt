package com.ase.tugood.notificationservice.infrastructure.web.client

import com.ase.tugood.notificationservice.application.data.user.UserDTO
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.ClientResponse
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono


@Component
class UserServiceClient {
    val logger: Logger = LoggerFactory.getLogger(javaClass)

    @Autowired
    private val webClientBuilder: WebClient.Builder? = null

    @Autowired
    private val discoveryClientController: DiscoveryClientController? = null

    fun getUserData(userId: String): Mono<UserDTO> {
        logger.info("receiving user data for: id={}", userId)

        if (discoveryClientController == null) {
            throw RuntimeException("Cannot initialize user service client")
        }

        return discoveryClientController
            .userService()
            .flatMap { url ->
                webClientBuilder!!.build()
                    .get().uri { builder ->
                        url.resolve(builder.path("/internal/users/accounts/{id}").build(userId))
                    }
                    .exchange()
            }
            .flatMap { t: ClientResponse? ->
                t?.bodyToMono(UserDTO::class.java)
            }
            .switchIfEmpty(Mono.just(UserDTO(id = userId)))
    }

}
