package com.ase.tugood.notificationservice.domain.notification.service

import com.ase.tugood.notificationservice.application.data.notification.GUINotificationDTO
import com.ase.tugood.notificationservice.application.data.notification.ServiceNotificationDTO
import org.springframework.data.domain.Pageable
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

/**
 * Provide several functions for sending and managing GUI notifications for the user
 */
interface IPoNotificationService {

    /**
     * save the notification in the database and send it as an in-app notification to the user
     *
     * @param serviceNotificationDTO notification to save and send
     * @return publisher signal for the save and send operation (can be used to check if there were errors)
     */
    fun sendNotification(serviceNotificationDTO: ServiceNotificationDTO): Mono<Void>

    /**
     * get the number of notifications for a user
     *
     * @param userId owner of the notifications
     * @return number of notifications for a user
     */
    fun countNotificationsByUserId(userId: String): Mono<Long>

    /**
     * get all unread notifications from the database for a particular user
     *
     * @param userId owner of the notifications
     * @param pageable page and size of the page to fetch
     * @return publisher for all unread notifications of a user
     */
    fun getNotificationsByUserId(userId: String, pageable: Pageable): Flux<GUINotificationDTO>

    /**
     * delete the notification with a certain id for a particular user
     *
     * @param userId for the user to delete the notification for
     * @param notificationId for the notification to delete
     * @return publisher signal for the save and send operation (can be used to check if there were errors)
     */
    fun deleteNotification(userId: String, notificationId: String): Mono<Void>

}
