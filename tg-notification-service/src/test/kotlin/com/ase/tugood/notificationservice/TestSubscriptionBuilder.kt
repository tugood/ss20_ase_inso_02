package com.ase.tugood.notificationservice


import com.ase.tugood.notificationservice.domain.subscription.model.TUGoodNotificationSubscription
import com.ase.tugood.notificationservice.infrastructure.configuration.DateTimeProvider
import nl.martijndwars.webpush.Subscription
import java.time.Instant

class TestSubscriptionBuilder(dateTimeProvider: DateTimeProvider = DateTimeProvider()) {
    private val vapidPrivateKey: String = "tZ9kly49ai8KYQQU_11mVSk6VLjzUSHK-xH6vgncKak"


    private val defaultId: String = "AAAAAAAAAAAAAAAAAAAA"
    private val defaultTime: Instant = dateTimeProvider.now()
    private val defaultEndpoint: String = "fcm.googleapis.com"
    private val defaultp256dh: String = "BNttdJM1mImaf3ZVWcG3VT69kRL17rCgbiDrpI_Jprw9WCsB1T0TkWXuo9wrhWmpTFjZWf4ZNp1QUBC6M_F4svg"
    private val defaultAuth: String = "6yUFUsMVvuGgzdiCl2z2SA"
    private val defaultUserInfo = TestUserInfoBuilder().validDefaultUserInfo()

    private val updatedId: String = "BBBBBBBBBBBBBBBBBBB"
    private val updatedTime: Instant = dateTimeProvider.now()
    private val updatedEndpoint: String = "push.mozilla.com"
    private val updatedp256dh: String = "BNttdJM1mImaf3ZVWcG3VT69kRL17rCgbiDrpI_Jprw9WCsB1T0TkWXuo9wrhWmpTFjZWf4ZNp1QUBC6M_F4svg"
    private val updatedAuth: String = "6yUFUsMVvuGgzdiCl2z2SA"
    private val updatedUserInfo = TestUserInfoBuilder().validUpdatedUserInfo()

    fun getPrivateTestKey() = vapidPrivateKey;

    fun validDefaultSubscriptionDTO() = Subscription().apply {
        endpoint = defaultEndpoint
        keys = Subscription.Keys().apply {
            p256dh = defaultp256dh
            auth = defaultAuth
        }
    }
    fun validDefaultTUGoodNotificationSubscription() = TUGoodNotificationSubscription(
        id = defaultId,
        userId = defaultUserInfo.id,
        endpoint = defaultEndpoint,
        p256dh = defaultp256dh,
        auth = defaultAuth,
        createdBy = defaultUserInfo.email,
        createdDate =  defaultTime,
        lastModifiedBy = defaultUserInfo.email,
        lastModifiedDate = defaultTime
    )

    fun validUpdatedSubscriptionDTO() = Subscription().apply {
        endpoint = updatedEndpoint
        keys = Subscription.Keys().apply {
            p256dh = updatedp256dh
            auth = updatedAuth
        }
    }

    fun validUpdatedTUGoodNotificationSubscription() = TUGoodNotificationSubscription(
        id = updatedId,
        userId = updatedUserInfo.id,
        endpoint = updatedEndpoint,
        p256dh = updatedp256dh,
        auth = updatedAuth,
        createdBy = updatedUserInfo.email,
        createdDate = updatedTime,
        lastModifiedBy = updatedUserInfo.email,
        lastModifiedDate = updatedTime
    )
}
