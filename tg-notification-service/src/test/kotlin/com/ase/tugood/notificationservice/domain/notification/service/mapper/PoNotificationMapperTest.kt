package com.ase.tugood.notificationservice.domain.notification.service.mapper

import com.ase.tugood.notificationservice.TestGuiNotificationBuilder
import com.ase.tugood.notificationservice.application.data.notification.GUINotificationDTO
import com.ase.tugood.notificationservice.domain.notification.model.PoNotification
import io.mockk.every
import io.mockk.mockkStatic
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Instant

internal class PoNotificationMapperTest {
    private lateinit var poNotificationMapper: PoNotificationMapper

    private lateinit var poNotification: PoNotification

    private lateinit var guiNotificationDTO: GUINotificationDTO

    @BeforeEach
    fun setUp() {
        mockkStatic(Instant::class)
        every { Instant.now() } returns Instant.ofEpochSecond(1587910583) // Sun, 26 Apr 2020 14:16:23 GMT

        poNotificationMapper = PoNotificationMapper()
        guiNotificationDTO = TestGuiNotificationBuilder().getDefaultGUINotificationDTO()
        poNotification = TestGuiNotificationBuilder().getDefaultPoNotification()
    }

    @Test
    fun `test entity to dto`() {
        val goiNotificationDTOMapped = poNotificationMapper.toDto(poNotification)

        Assertions.assertNotNull(goiNotificationDTOMapped)
        Assertions.assertEquals(guiNotificationDTO.id, goiNotificationDTOMapped.id)
        Assertions.assertEquals(guiNotificationDTO.timestamp, goiNotificationDTOMapped.timestamp)
        Assertions.assertEquals(guiNotificationDTO.body, goiNotificationDTOMapped.body)
        Assertions.assertEquals(guiNotificationDTO.icon, goiNotificationDTOMapped.icon)
    }

    @Test
    fun `test dto to entity should throw UnsupportedOperationException`() {
        assertThrows(UnsupportedOperationException::class.java) { poNotificationMapper.toEntity(guiNotificationDTO) }
    }
}
