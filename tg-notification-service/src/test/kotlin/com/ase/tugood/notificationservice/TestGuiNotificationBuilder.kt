package com.ase.tugood.notificationservice

import com.ase.tugood.notificationservice.application.data.notification.GUINotificationDTO
import com.ase.tugood.notificationservice.domain.notification.model.PoNotification
import java.time.Instant

class TestGuiNotificationBuilder() {

    private val defaultUserInfo = TestUserInfoBuilder().validDefaultUserInfo()

    private val defaultId: String = "AAAAAAAAAAAAAAAAA"
    private val defaultUserId: String = defaultUserInfo.id
    private val defaultTitle: String = "My perfect Title"
    private val defaultBody: String = "Let's not bicker and argue over who killed who."
    private val defaultIcon: String = "/path/to/icon"

    private val updatedUserInfo = TestUserInfoBuilder().validUpdatedUserInfo()

    private val updatedId: String = "BBBBBBBBBBBBBBBB"
    private val updatedUserId: String = updatedUserInfo.id
    private val updatedTitle: String = "My perfect updated Title"
    private val updatedBody: String = "Strange women lying in ponds, distributing swords, is no basis for a system of government!"
    private val updatedIcon: String = "/path/to/icon2"

    fun getDefaultGUINotificationDTO() = GUINotificationDTO(
            id = defaultId,
            timestamp = Instant.now(),
            body = defaultBody,
            icon = defaultIcon,
            title = defaultTitle
    )

    fun getDefaultPoNotification() = PoNotification(
        id = defaultId,
        timestamp = Instant.now(),
        title = defaultTitle,
        body = defaultBody,
        userId = defaultUserId,
        icon = defaultIcon
    )

    fun getUpdatedGUINotificationDTO() = GUINotificationDTO(
            id = updatedId,
            timestamp = Instant.now(),
            body = updatedBody,
            icon = updatedIcon,
            title = updatedTitle
    )

    fun getUpdatedPoNotification() = PoNotification(
        id = updatedId,
        timestamp = Instant.now(),
        title = updatedTitle,
        body = updatedBody,
        userId = updatedUserId,
        icon = updatedIcon
    )
}
