package com.ase.tugood.notificationservice.domain.subscription.service.mapper

import com.ase.tugood.notificationservice.TestSubscriptionBuilder
import com.ase.tugood.notificationservice.TestUserInfoBuilder
import com.ase.tugood.notificationservice.domain.subscription.model.TUGoodNotificationSubscription
import com.ase.tugood.notificationservice.infrastructure.configuration.DateTimeProvider
import com.ase.tugood.notificationservice.infrastructure.web.model.request.UserInfo
import io.mockk.every
import io.mockk.mockk
import nl.martijndwars.webpush.Subscription
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Instant
import kotlin.test.junit5.JUnit5Asserter.assertNull

internal class SubscriptionMapperTest {

    private lateinit var subscriptionMapper: SubscriptionMapper

    private lateinit var dateTimeProvider: DateTimeProvider

    private lateinit var subscriptionDTO: Subscription

    private lateinit var subscription: TUGoodNotificationSubscription

    private lateinit var userInfo: UserInfo

    @BeforeEach
    fun setUp() {
        dateTimeProvider = mockk()
        every { dateTimeProvider.now() } returns Instant.ofEpochSecond(1587910583) // Sun, 26 Apr 2020 14:16:23 GMT

        subscriptionMapper = SubscriptionMapper(dateTimeProvider = dateTimeProvider)
        subscriptionDTO = TestSubscriptionBuilder().validDefaultSubscriptionDTO()
        subscription = TestSubscriptionBuilder().validDefaultTUGoodNotificationSubscription()
        userInfo = TestUserInfoBuilder().validDefaultUserInfo()
    }

    @Test
    fun `test dto to entity`() {
        val subscriptionMapped = subscriptionMapper.toEntity(subscriptionDTO, userInfo)

        assertNotNull(subscriptionMapped)
        assertNull("The external subscription does not have an id", subscriptionMapped.id)
        assertEquals(subscription.endpoint, subscriptionMapped.endpoint)
        assertEquals(subscription.p256dh, subscriptionMapped.p256dh)
        assertEquals(subscription.auth, subscriptionMapped.auth)
        assertEquals(userInfo.email, subscriptionMapped.createdBy)
        assertEquals(dateTimeProvider.now(), subscriptionMapped.createdDate)
        assertEquals(userInfo.email, subscriptionMapped.lastModifiedBy)
        assertEquals(dateTimeProvider.now(), subscriptionMapped.lastModifiedDate)
    }

    @Test
    fun `test entity to dto`() {
        val subscriptionDTOMapped = subscriptionMapper.toDto(subscription)

        assertNotNull(subscriptionDTOMapped)
        assertEquals(subscriptionDTO.endpoint, subscriptionDTOMapped.endpoint)
        assertEquals(subscriptionDTO.keys.auth, subscriptionDTOMapped.keys.auth)
        assertEquals(subscriptionDTO.keys.p256dh, subscriptionDTOMapped.keys.p256dh)
    }
}
