package com.ase.tugood.notificationservice.application

import com.ase.tugood.notificationservice.TestSubscriptionBuilder
import com.ase.tugood.notificationservice.TestUserInfoBuilder
import com.ase.tugood.notificationservice.domain.subscription.model.TUGoodNotificationSubscription
import com.ase.tugood.notificationservice.domain.subscription.service.mapper.SubscriptionMapper
import com.ase.tugood.notificationservice.infrastructure.persistence.SubscriptionRepository
import com.ase.tugood.notificationservice.infrastructure.web.model.request.UserInfo
import com.ninjasquad.springmockk.SpykBean
import io.mockk.Called
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.slot
import io.mockk.verify
import nl.martijndwars.webpush.Subscription
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@SpringBootTest
@ExtendWith(MockKExtension::class)
internal class SubscriptionServiceTest {

    @Autowired
    private lateinit var subscriptionService: SubscriptionService

    @SpykBean
    private lateinit var mockRepository: SubscriptionRepository

    private lateinit var subscription: TUGoodNotificationSubscription
    private lateinit var updatedSubscription: TUGoodNotificationSubscription

    private lateinit var subscriptionDTO: Subscription

    private lateinit var userInfo: UserInfo

    @BeforeEach
    fun setup() {
        subscriptionDTO = TestSubscriptionBuilder().validDefaultSubscriptionDTO()
        subscription = TestSubscriptionBuilder().validDefaultTUGoodNotificationSubscription()
        updatedSubscription = TestSubscriptionBuilder().validUpdatedTUGoodNotificationSubscription()
        userInfo = TestUserInfoBuilder().validDefaultUserInfo()
    }

    @Test
    fun `test subscribe for unsubscribed user should save`() {

        val uuid = UUID.randomUUID().toString()

        subscriptionDTO.endpoint = uuid
        val subscriptionDTOSaved = subscriptionService.subscribe(subscriptionDTO, userInfo)

        StepVerifier
            .create<Subscription>(subscriptionDTOSaved)
            .consumeNextWith { dto ->
                assertNotNull(dto)
                assertEquals(uuid, dto.endpoint)
                assertEquals(subscriptionDTO.keys.p256dh, dto.keys.p256dh)
                assertEquals(subscriptionDTO.keys.auth, dto.keys.auth)
            }
            .expectComplete()
            .verify()
    }

    @Test
    fun `test subscribe for subscribed user should return empty`() {
        // save a new entry in the database
        subscriptionService
            .subscribe(subscriptionDTO, userInfo)
            .subscribe()

        // save again
        StepVerifier
            .create<Any>(subscriptionService.subscribe(subscriptionDTO, userInfo))
            .expectComplete()  // for empty only check if complete immediately
            .verify()

        verify { mockRepository.save(any<TUGoodNotificationSubscription>()) wasNot Called }
    }

    @Test
    fun `test unsubscribe user should delete database entry`() {

        // save a new entry in the database
        subscriptionService
            .subscribe(subscriptionDTO, userInfo)
            .subscribe()

        // check if it is being deleted
        StepVerifier
            .create<Any>(subscriptionService.unsubscribe(subscriptionDTO, userInfo.id))
            .expectComplete()  // for empty only check if complete immediately
            .verify()

        verify(exactly = 1) { mockRepository.deleteAllByUserIdAndEndpoint(userId = userInfo.id, endpoint = subscriptionDTO.endpoint) }
    }

    @Test
    fun `test getSubscriptions should return all subscriptions for a user`() {

        val userId = slot<String>()
        every {
            mockRepository.findAllByUserId(capture(userId))
        } answers {
            Flux.just(subscription.apply { this.userId = userId.captured }, updatedSubscription.apply { this.userId = userId.captured })
        }

        val subs = subscriptionService.getSubscriptions(userInfo.id)
        verify(exactly = 1) { mockRepository.findAllByUserId(userId = userInfo.id) }

        StepVerifier
            .create<Subscription>(subs)
            .consumeNextWith { dto ->
                assertNotNull(dto)
                assertEquals(subscription.endpoint, dto.endpoint)
            }
            .consumeNextWith { dto ->
                assertNotNull(dto)
                assertEquals(updatedSubscription.endpoint, dto.endpoint)
            }
            .expectComplete()
            .verify()

    }
}
