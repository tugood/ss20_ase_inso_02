package com.ase.tugood.notificationservice.configuration

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import org.springframework.boot.test.util.TestPropertyValues
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ApplicationEvent
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.annotation.Profile
import org.springframework.context.event.ContextClosedEvent
import org.springframework.test.context.ActiveProfiles

@Profile("!websocket")
class WireMockInitializer : ApplicationContextInitializer<ConfigurableApplicationContext> {

    override fun initialize(configurableApplicationContext: ConfigurableApplicationContext) {
        val wireMockServer = WireMockServer(WireMockConfiguration().dynamicPort())
        wireMockServer.start()
        configurableApplicationContext.beanFactory.registerSingleton("wireMockServer", wireMockServer)
        configurableApplicationContext.addApplicationListener { applicationEvent: ApplicationEvent? ->
            if (applicationEvent is ContextClosedEvent) {
                wireMockServer.stop()
            }
        }
        TestPropertyValues
            .of("tg-user-service=http://localhost:" + wireMockServer.port())
            .applyTo(configurableApplicationContext)
    }
}
