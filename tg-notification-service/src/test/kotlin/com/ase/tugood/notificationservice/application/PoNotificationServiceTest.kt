package com.ase.tugood.notificationservice.application

import com.ase.tugood.notificationservice.TestGuiNotificationBuilder
import com.ase.tugood.notificationservice.TestServiceNotificationBuilder
import com.ase.tugood.notificationservice.application.data.notification.GUINotificationDTO
import com.ase.tugood.notificationservice.application.data.notification.ServiceNotificationDTO
import com.ase.tugood.notificationservice.domain.notification.model.PoNotification
import com.ase.tugood.notificationservice.infrastructure.persistence.PoNotificationRepository
import com.ase.tugood.notificationservice.infrastructure.web.socket.ReactiveWebSocketHandler
import com.ninjasquad.springmockk.MockkBean
import com.ninjasquad.springmockk.SpykBean
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.domain.Pageable
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertNotNull

@SpringBootTest
@ExtendWith(MockKExtension::class)
internal class PoNotificationServiceTest {

    @Autowired
    private lateinit var poNotificationService: PoNotificationService

    @MockkBean
    @Qualifier("ReactiveWebSocketHandler")
    private lateinit var webSocketHandler: ReactiveWebSocketHandler

    @SpykBean
    private lateinit var poNotificationRepository: PoNotificationRepository

    private lateinit var serviceNotificationDTO: ServiceNotificationDTO

    private lateinit var defaultPoNotification: PoNotification

    private lateinit var updatedPoNotification: PoNotification

    @BeforeEach
    fun setUp() {
        serviceNotificationDTO = TestServiceNotificationBuilder().getDefaultServiceNotificationDTO()
        defaultPoNotification = TestGuiNotificationBuilder().getDefaultPoNotification()
        updatedPoNotification = TestGuiNotificationBuilder().getUpdatedPoNotification()

        StepVerifier
            .create<Any>(poNotificationRepository.deleteAll())
            .expectComplete()
            .verify()

        clearAllMocks()

    }

    @Test
    fun `test send notification successful should save and then send notification`() {
        every { webSocketHandler.reactiveSend(any(), any()) } returns Mono.empty()

        StepVerifier
            .create<Any>(poNotificationService.sendNotification(serviceNotificationDTO))
            .expectComplete()
            .verify()

        verify(exactly = 1) { poNotificationRepository.save(any<PoNotification>()) }
        verify(exactly = 1) { webSocketHandler.reactiveSend(serviceNotificationDTO.userId, any<GUINotificationDTO>()) }
    }

    @Test
    fun `test get all notifications by user should return all notifications for user`() {
        val userId = defaultPoNotification.userId

        val notifications = listOf(defaultPoNotification, updatedPoNotification)

        assertNotEquals(defaultPoNotification.userId, updatedPoNotification.userId)

        // insert test data for the same user
        StepVerifier
            .create<Any>(poNotificationRepository.saveAll(notifications))
            .expectNextCount(2)
            .verifyComplete()

        // verify that only one notification is received by this method
        StepVerifier
            .create<GUINotificationDTO>(poNotificationService.getNotificationsByUserId(userId, Pageable.unpaged()))
            .consumeNextWith { notification ->
                assertNotNull(notification)
                assertEquals(defaultPoNotification.id, notification.id)
            }
            .expectComplete()
            .verify()
    }

    @Test
    fun `test get all unread notification should return all unread notifications`() {
        val userId = defaultPoNotification.userId

        val newUpdatedPoNotification = updatedPoNotification.copy(
            userId = userId
        )

        val notifications = listOf(defaultPoNotification, newUpdatedPoNotification)

        // insert test data for the same user
        poNotificationRepository
            .saveAll(notifications)
            .subscribe()

        assertEquals(2, poNotificationRepository.count().block())

        // delete the entry in the database
        StepVerifier
            .create<Void>(poNotificationService.deleteNotification(userId, defaultPoNotification.id!!))
            .expectComplete()
            .verify()

        // verify that only one notification is received by this method
        StepVerifier
            .create<PoNotification>(poNotificationRepository.findAll())
            .consumeNextWith { notification ->
                assertNotNull(notification)
                assertEquals(newUpdatedPoNotification.id, notification.id)
            }
            .expectComplete()
            .verify()
    }
}
