package com.ase.tugood.notificationservice.infrastructure.web.rest.controller

import com.ase.tugood.notificationservice.TestServiceNotificationBuilder
import com.ase.tugood.notificationservice.TestSubscriptionBuilder
import com.ase.tugood.notificationservice.application.PushNotificationService
import com.ase.tugood.notificationservice.application.SubscriptionService
import com.ase.tugood.notificationservice.application.data.notification.ServiceNotificationDTO
import com.ase.tugood.notificationservice.infrastructure.web.socket.ReactiveWebSocketHandler
import com.ninjasquad.springmockk.MockkBean
import com.ninjasquad.springmockk.SpykBean
import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.slot
import nl.martijndwars.webpush.Subscription
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.web.reactive.function.BodyInserters
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.test.publisher.TestPublisher


@AutoConfigureWebTestClient
@SpringBootTest
@ExtendWith(MockKExtension::class)
internal class InternalNotificationControllerTest {

    @SpykBean
    private lateinit var pushNotificationService: PushNotificationService

    @MockkBean
    private lateinit var webSocketHandler: ReactiveWebSocketHandler

    @MockkBean
    private lateinit var mockSubscriptionService: SubscriptionService

    @Autowired
    private lateinit var webTestClient: WebTestClient

    private lateinit var serviceNotificationDTO: ServiceNotificationDTO

    private lateinit var subscriptionDTO: Subscription
    private lateinit var updatedSubscriptionDTO: Subscription

    @BeforeEach
    fun setup() {
        subscriptionDTO = TestSubscriptionBuilder().validDefaultSubscriptionDTO()
        updatedSubscriptionDTO = TestSubscriptionBuilder().validUpdatedSubscriptionDTO()
        serviceNotificationDTO = TestServiceNotificationBuilder().getDefaultServiceNotificationDTO()
    }

    @Test
    fun `POST receive valid push serviceNotificationDTO should send notification`() {

        val newTestServiceNotificationDTO = serviceNotificationDTO.copy(
            notificationType = ServiceNotificationDTO.NotificationType.PUSH
        )

        val userIdMock = slot<String>()
        every {
            mockSubscriptionService.getSubscriptions(capture(userIdMock))
        } returns Flux.just(subscriptionDTO)

        every {
            pushNotificationService.sendPushNotification(any(), any())
        } returns Mono.just(HttpStatus.OK)

        webTestClient.post().uri("/internal/notifications/notify")
            .contentType(MediaType.APPLICATION_JSON)
            .body(BodyInserters.fromValue(newTestServiceNotificationDTO))
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("notify").isEqualTo("succeeded")
    }

    @Test
    fun `POST receive valid push serviceNotificationDTO for invalid second subscription should return notify failed`() {
        val newTestServiceNotificationDTO = serviceNotificationDTO.copy(
            notificationType = ServiceNotificationDTO.NotificationType.PUSH
        )

        val userIdMock = slot<String>()
        every {
            mockSubscriptionService.getSubscriptions(capture(userIdMock))
        } returns TestPublisher.createCold<Subscription>().emit(subscriptionDTO, updatedSubscriptionDTO).flux()

        every {
            pushNotificationService.sendPushNotification(any(), any())
        } returns TestPublisher.createCold<HttpStatus>().emit(HttpStatus.OK).mono() andThen
            TestPublisher.createCold<HttpStatus>().emit(HttpStatus.BAD_REQUEST).mono()

        webTestClient.post().uri("/internal/notifications/notify")
            .contentType(MediaType.APPLICATION_JSON)
            .body(BodyInserters.fromValue(newTestServiceNotificationDTO))
            .exchange()
            .expectStatus().isBadRequest
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("notify").isEqualTo("failed")
    }

    @Test
    fun `POST receive serviceNotificationDTO with mode BOTH fails for plain-old notification should send as push notification`() {
        val newTestServiceNotificationDTO = serviceNotificationDTO.copy(
            notificationType = ServiceNotificationDTO.NotificationType.BOTH
        )

        // return an error from the websocket method
        every {
            webSocketHandler.reactiveSend(any(), any())
        } returns TestPublisher.createCold<Void>().error(IllegalArgumentException("This is illegal, it's unfair")).mono()

        // return success for sending a push notification
        every {
            mockSubscriptionService.getSubscriptions(any())
        } returns TestPublisher.createCold<Subscription>().emit(subscriptionDTO).flux()

        every {
            pushNotificationService.sendPushNotification(any(), any())
        } returns TestPublisher.createCold<HttpStatus>().emit(HttpStatus.OK).mono()

        webTestClient.post().uri("/internal/notifications/notify")
            .contentType(MediaType.APPLICATION_JSON)
            .body(BodyInserters.fromValue(newTestServiceNotificationDTO))
            .exchange()
            .expectStatus().is2xxSuccessful
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("notify").isEqualTo("succeeded")
    }

    @Test
    fun `POST receive serviceNotificationDTO with mode PLAIN_OLD should send notification`() {
        val newTestServiceNotificationDTO = serviceNotificationDTO.copy(
            notificationType = ServiceNotificationDTO.NotificationType.PLAIN_OLD
        )

        // return an error from the websocket method
        every {
            webSocketHandler.reactiveSend(any(), any())
        } returns TestPublisher.createCold<Void>().complete().mono()

        webTestClient.post().uri("/internal/notifications/notify")
            .contentType(MediaType.APPLICATION_JSON)
            .body(BodyInserters.fromValue(newTestServiceNotificationDTO))
            .exchange()
            .expectStatus().is2xxSuccessful
    }
}
