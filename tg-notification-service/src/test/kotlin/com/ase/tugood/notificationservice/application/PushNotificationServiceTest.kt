package com.ase.tugood.notificationservice.application

import com.ase.tugood.notificationservice.TestServiceNotificationBuilder
import com.ase.tugood.notificationservice.TestSubscriptionBuilder
import com.ase.tugood.notificationservice.TestUserBuilder
import com.ase.tugood.notificationservice.TestUserInfoBuilder
import com.ase.tugood.notificationservice.application.data.notification.ServiceNotificationDTO
import com.ase.tugood.notificationservice.application.data.user.UserDTO
import com.ase.tugood.notificationservice.infrastructure.configuration.PushNotificationFactory
import com.ase.tugood.notificationservice.infrastructure.web.model.request.UserInfo
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.slot
import io.mockk.verify
import nl.martijndwars.webpush.Notification
import nl.martijndwars.webpush.PushService
import nl.martijndwars.webpush.Subscription
import org.apache.http.ProtocolVersion
import org.apache.http.message.BasicHttpResponse
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.MessageSource
import org.springframework.http.HttpStatus
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import reactor.test.publisher.TestPublisher
import java.security.Security


@SpringBootTest
@ExtendWith(MockKExtension::class)
internal class PushNotificationServiceTest {

    private lateinit var pushNotificationService: PushNotificationService

    @MockK
    private lateinit var mockPushNotificationFactory: PushNotificationFactory

    @MockK
    private lateinit var mockMessageSource: MessageSource

    @MockK
    private lateinit var mockSubscriptionService: SubscriptionService

    @MockK
    private lateinit var pushService: PushService

    @MockK
    private lateinit var userService: UserService

    private lateinit var subscription: Subscription

    private lateinit var serviceNotificationDTO: ServiceNotificationDTO

    private lateinit var userInfo: UserInfo

    private lateinit var userDTO: UserDTO

    fun responseOK(body: String): BasicHttpResponse {
        return BasicHttpResponse(ProtocolVersion("v1", 1,1), 200, "reason")
    }

    fun responseBadRequest(body: String): BasicHttpResponse {
        return BasicHttpResponse(ProtocolVersion("v1", 1,1), 400, "reason")
    }

    fun responseGone(body: String): BasicHttpResponse {
        return BasicHttpResponse(ProtocolVersion("v1", 1,1), 410, "reason")
    }

    @BeforeEach
    fun setup() {
        Security.addProvider(BouncyCastleProvider())

        pushNotificationService = PushNotificationService(
            pushNotificationFactory = mockPushNotificationFactory,
            subscriptionService = mockSubscriptionService,
            userService = userService,
            messageSource = mockMessageSource
        )
        subscription = TestSubscriptionBuilder().validDefaultSubscriptionDTO()
        userInfo = TestUserInfoBuilder().validDefaultUserInfo()
        userDTO = TestUserBuilder().validDefaultUserDTO()
        serviceNotificationDTO = TestServiceNotificationBuilder().getDefaultServiceNotificationDTO()

        val mockUserId = slot<String>()
        every {
            userService.getUserForId(capture(mockUserId))
        } answers {
            TestPublisher.createCold<UserDTO>().emit(userDTO.apply { id = mockUserId.captured }).mono()
        }

        every { mockMessageSource.getMessage(any(), any(), any()) } answers { "Make it work, make it right, make it fast." }

        val mockPushServiceSlot = slot<String>()
        every { mockPushNotificationFactory.getPushService(capture(mockPushServiceSlot)) } answers { pushService }
    }

    @Test
    fun `test send subscription successful`() {
        val resp = responseOK("return from notification")

        // for this message return
        val pushServiceSlot = slot<Notification>()
        every { pushService.send(capture(pushServiceSlot)) } answers { resp }

        val httpResponseSource = pushNotificationService.sendEnabledPushNotificationsMessage(subscription, userInfo)

        StepVerifier
            .create<HttpStatus>(httpResponseSource)
            .expectNext(HttpStatus.OK)
            .expectComplete()
            .verify()
    }

    @Test
    fun `test send subscription with push service returning failure http code returns push service http code`() {
        val resp = responseBadRequest("return from notification")

        // for this message return
        val pushServiceSlot = slot<Notification>()
        every { pushService.send(capture(pushServiceSlot)) } answers { resp }

        val httpResponseSource = pushNotificationService.sendEnabledPushNotificationsMessage(subscription, userInfo)

        StepVerifier
            .create<HttpStatus>(httpResponseSource)
            .expectNext(HttpStatus.BAD_REQUEST)
            .expectComplete()
            .verify()
    }

    @Test
    fun `test send subscription message returns exception in push library`() {
        val pushServiceSlot = slot<Notification>()
        every { pushService.send(capture(pushServiceSlot)) } throws Exception()  // just throw general exception

        val httpResponseSource = pushNotificationService.sendEnabledPushNotificationsMessage(subscription, userInfo)

        StepVerifier
            .create<HttpStatus>(httpResponseSource)
            .expectNext(HttpStatus.INTERNAL_SERVER_ERROR)
            .expectComplete()
            .verify()
    }

    @Test
    fun `test send subscription with push service returning gone should unsubscribe user`() {
        val resp = responseGone("return from notification")

        // for this message return
        val pushServiceSlot = slot<Notification>()
        every { pushService.send(capture(pushServiceSlot)) } answers { resp }

        every { mockSubscriptionService.unsubscribe(subscription, userInfo.id) } answers {
            TestPublisher.createCold<Void>().complete().mono()
        }

        every { mockSubscriptionService.getSubscriptions(userInfo.id) } answers {
            TestPublisher.createCold<Subscription>().emit(subscription).flux()
        }

        val httpResponseSource = pushNotificationService.sendServicePushNotification(serviceNotificationDTO)

        StepVerifier
            .create<HttpStatus>(httpResponseSource)
            .expectNext(HttpStatus.GONE)
            .expectComplete()
            .verify()

        verify(exactly = 1) { mockSubscriptionService.unsubscribe(subscription, userInfo.id) }
    }


}



