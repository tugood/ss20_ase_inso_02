package com.ase.tugood.notificationservice.infrastructure.web.socket

import com.ase.tugood.notificationservice.TestGuiNotificationBuilder
import com.ase.tugood.notificationservice.TestUserInfoBuilder
import com.ase.tugood.notificationservice.application.data.notification.GUINotificationDTO
import com.ase.tugood.notificationservice.infrastructure.web.model.request.UserInfo
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Timeout
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.test.context.ActiveProfiles
import org.springframework.web.reactive.socket.WebSocketMessage
import org.springframework.web.reactive.socket.client.JettyWebSocketClient
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.publisher.ReplayProcessor
import java.net.URI
import java.net.URISyntaxException
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.AtomicLong

@AutoConfigureWebTestClient
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(MockKExtension::class)
@ActiveProfiles("generatedata")
internal class TestReactiveWebSocketHandlerTest {

    @LocalServerPort
    private val port: String? = null

    @Autowired
    @Qualifier("TestReactiveWebSocketHandler")
    private lateinit var reactiveWebSocketHandler: TestReactiveWebSocketHandler

    private lateinit var userInfo: UserInfo

    private lateinit var guiNotificationDTO: GUINotificationDTO

    private val client: JettyWebSocketClient = JettyWebSocketClient()

    @BeforeEach
    fun setup() {
        userInfo = TestUserInfoBuilder().validDefaultUserInfo()
        guiNotificationDTO = TestGuiNotificationBuilder().getDefaultGUINotificationDTO()
        client.start()
    }

    @AfterEach
    fun after() {
        client.stop()
    }

    @Throws(URISyntaxException::class)
    private fun getWebsocketURL(): URI {
        return URI("ws://localhost:$port/api/socket-notifications/test-notifications-emitter")
    }

    @Test
    @Throws(URISyntaxException::class, InterruptedException::class)
    @Timeout(value = 10000, unit = TimeUnit.MILLISECONDS)
    fun `test subscribe to websocket and create request should receive values`() {
        val count = 4
        val latch = CountDownLatch(count)

        // define a replay subscriber/publisher for the output of the websocket
        val output = ReplayProcessor.create<String>(count)

        val websessionMono = client.execute(getWebsocketURL()) { session ->
            session
                .send(Mono.just(session.textMessage(userInfo.id)))
                .thenMany(
                    session.receive()
                        .take(count.toLong())
                        .map(WebSocketMessage::getPayloadAsText)
                        .log()
                        .subscribeWith(output)
                        .then())
                .then()
        }

        // if the receive connection is established, subscribeWith must have subscribed to the output
        output
            .doOnSubscribe {
                websessionMono.subscribe()
            }
            .log()
            .subscribe { num: String ->
                kotlin.test.assertNotNull(num)
                latch.countDown()
            }

        // let the whole thing process
        Thread.sleep(2000)

        // send messages from the server and check if the messages can be received by the client
        Flux.range(1, count)
            .map { index: Int ->
                guiNotificationDTO.copy(
                    id = index.toString()
                )
            }.subscribe()


        latch.await()
    }
}
