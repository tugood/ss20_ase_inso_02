package com.ase.tugood.notificationservice.infrastructure.web.rest.controller

import com.ase.tugood.notificationservice.TestGuiNotificationBuilder
import com.ase.tugood.notificationservice.TestSubscriptionBuilder
import com.ase.tugood.notificationservice.TestUserInfoBuilder
import com.ase.tugood.notificationservice.application.PoNotificationService
import com.ase.tugood.notificationservice.application.PushNotificationService
import com.ase.tugood.notificationservice.application.SubscriptionService
import com.ase.tugood.notificationservice.application.data.notification.GUINotificationDTO
import com.ase.tugood.notificationservice.domain.subscription.model.TUGoodNotificationSubscription
import com.ase.tugood.notificationservice.domain.subscription.service.mapper.SubscriptionMapper
import com.ase.tugood.notificationservice.infrastructure.web.model.request.UserInfo
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.verify
import nl.martijndwars.webpush.Subscription
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toFlux
import reactor.kotlin.core.publisher.toMono
import reactor.test.publisher.TestPublisher
import java.util.*

@AutoConfigureWebTestClient
@SpringBootTest
@ExtendWith(MockKExtension::class)
internal class NotificationControllerTest {

    @MockkBean
    private lateinit var mockPushNotificationService: PushNotificationService

    @MockkBean
    private lateinit var mockSubscriptionService: SubscriptionService

    @Autowired
    private lateinit var subscriptionMapper: SubscriptionMapper

    @MockkBean
    private lateinit var mockPoNotificationService: PoNotificationService

    @Autowired
    private lateinit var webTestClient: WebTestClient

    private lateinit var subscriptionDTO: Subscription

    private lateinit var guiNotificationDTO: GUINotificationDTO

    private lateinit var subscription: TUGoodNotificationSubscription

    private lateinit var userInfo: UserInfo

    private lateinit var userInfoB64: String

    @BeforeEach
    fun setup() {
        subscriptionDTO = TestSubscriptionBuilder().validDefaultSubscriptionDTO()
        subscription = TestSubscriptionBuilder().validDefaultTUGoodNotificationSubscription()
        userInfo = TestUserInfoBuilder().validDefaultUserInfo()

        guiNotificationDTO = TestGuiNotificationBuilder().getDefaultGUINotificationDTO()

        userInfoB64 = Base64.getEncoder().encodeToString(jacksonObjectMapper().writeValueAsBytes(userInfo))
    }


    @Test
    fun `PUT subscribe with no previous subscription should subscribe`() {
        val entity = subscriptionMapper.toEntity(subscriptionDTO, userInfo)
        val afterSubscribeDTO = subscriptionMapper.toDto(entity)

        every {
            mockSubscriptionService.subscribe(any(), any())
        } returns Mono.just(afterSubscribeDTO)

        every {
            mockPushNotificationService.sendEnabledPushNotificationsMessage(any(), any())
        } returns Mono.just(HttpStatus.OK)

        webTestClient.put().uri("/api/notifications/subscribe")
            .contentType(MediaType.APPLICATION_JSON)
            .header("X-Userinfo", userInfoB64)
            .body(Mono.just(subscriptionDTO), Subscription::class.java)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("subscribe").isEqualTo("done")
    }

    @Test
    fun `PUT subscribe with previous subscription should not subscribe again`() {
        val entity = subscriptionMapper.toEntity(subscriptionDTO, userInfo)
        val afterSubscribeDTO = subscriptionMapper.toDto(entity)

        every {
            mockSubscriptionService.subscribe(any(), any())
        } returns Mono.empty()

        every {
            mockPushNotificationService.sendEnabledPushNotificationsMessage(any(), any())
        } returns Mono.just(HttpStatus.OK)

        webTestClient.put().uri("/api/notifications/subscribe")
            .contentType(MediaType.APPLICATION_JSON)
            .header("X-Userinfo", userInfoB64)
            .body(Mono.just(subscriptionDTO), Subscription::class.java)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("subscribe").isEqualTo("not executed or already subscribed")
    }

    @Test
    fun `POST unsubscribe should delete database entry`() {

        every {
            mockSubscriptionService.unsubscribe(any(), any())
        } returns Mono.empty()

        webTestClient.post().uri("/api/notifications/unsubscribe")
            .contentType(MediaType.APPLICATION_JSON)
            .header("X-Userinfo", userInfoB64)
            .body(Mono.just(subscriptionDTO), Subscription::class.java)
            .exchange()
            .expectStatus().isOk

        verify(exactly = 1) {
            mockSubscriptionService.unsubscribe(any(), any())
        }
    }

    @Test
    fun `DELETE notifications should delete notification entry`() {

        val notificationId = guiNotificationDTO.id
        val userId = userInfo.id

        every {
            mockPoNotificationService.deleteNotification(userId, notificationId)
        } returns TestPublisher.createCold<Void>().complete().mono()

        webTestClient.delete().uri("/api/notifications/$notificationId")
            .header("X-Userinfo", userInfoB64)
            .exchange()
            .expectStatus().isOk

        verify(exactly = 1) {
            mockPoNotificationService.deleteNotification(userId, notificationId)
        }
    }

    @Test
    fun `DELETE notification throws error should return error message`() {

        val userId = userInfo.id
        val notificationId = guiNotificationDTO.id

        every {
            mockPoNotificationService.deleteNotification(userId, notificationId)
        } returns TestPublisher.createCold<Void>().error(RuntimeException("Something went wrong")).toMono()

        webTestClient.delete().uri("/api/notifications/$notificationId")
            .header("X-Userinfo", userInfoB64)
            .exchange()
            .expectStatus().is5xxServerError
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.message").isEqualTo("Cannot delete notifications")
    }

    @Test
    fun `GET notifications should return all notifications`() {

        val userId = userInfo.id

        every {
            mockPoNotificationService.getNotificationsByUserId(userId, any())
        } returns TestPublisher.createCold<GUINotificationDTO>().emit(guiNotificationDTO).flux()

        every {
            mockPoNotificationService.countNotificationsByUserId(userId)
        } returns TestPublisher.createCold<Long>().emit(1L).mono()

        webTestClient.get().uri("/api/notifications?page=0&size=10")
                .header("X-Userinfo", userInfoB64)
                .exchange()
                .expectStatus().isOk
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$[0].id").isEqualTo(guiNotificationDTO.id)
                .jsonPath("$[0].title").isEqualTo(guiNotificationDTO.title)
                .jsonPath("$[0].body").isEqualTo(guiNotificationDTO.body)

        verify(exactly = 1) {
            mockPoNotificationService.getNotificationsByUserId(userId, any())
        }
    }

    @Test
    fun `GET notifications throws error should return error message`() {

        val userId = userInfo.id

        every {
            mockPoNotificationService.getNotificationsByUserId(userId, any())
        } returns TestPublisher.createCold<GUINotificationDTO>().error(RuntimeException("Something went wrong")).toFlux()

        every {
            mockPoNotificationService.countNotificationsByUserId(userId)
        } returns TestPublisher.createCold<Long>().emit(1L).mono()

        webTestClient.get().uri("/api/notifications?page=0&size=10")
            .header("X-Userinfo", userInfoB64)
            .exchange()
            .expectStatus().is5xxServerError
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.message").isEqualTo("Cannot fetch notifications")

        verify(exactly = 1) {
            mockPoNotificationService.getNotificationsByUserId(userId, any())
        }
    }
}
