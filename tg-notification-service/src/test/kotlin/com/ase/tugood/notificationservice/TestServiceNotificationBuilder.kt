package com.ase.tugood.notificationservice

import com.ase.tugood.notificationservice.application.data.notification.ServiceNotificationDTO

class TestServiceNotificationBuilder() {

    private val defaultUserInfo = TestUserInfoBuilder().validDefaultUserInfo()

    private val defaultTitle: String = "AAAAAAAAAAAAAAAAA"
    private val defaultBadge: String = "My special badge"
    private val defaultBody: String = "Deeds will not be less valiant because they are unpraised."
    private val defaultIcon: String = "/path/to/icon"
    private val defaultUserId: String = defaultUserInfo.id
    private val defaultUserEmail: String? = defaultUserInfo.email
    private val defaultTag: String = "some lovely tag"
    private val defaultNotificationType: ServiceNotificationDTO.NotificationType = ServiceNotificationDTO.NotificationType.PLAIN_OLD

    private val updatedUserInfo = TestUserInfoBuilder().validUpdatedUserInfo()

    private val updatedTitle: String = "AAAAAAAAAAAAAAAAA"
    private val updatedBadge: String = "just a plain badge"
    private val updatedBody: String = "All we have to decide is what to do with the time that is given us."
    private val updatedIcon: String = "/path/to/icon"
    private val updatedUserId: String = updatedUserInfo.id
    private val updatedUserEmail: String? = updatedUserInfo.email
    private val updatedTag: String = "another lovely tag"
    private val updatedNotificationType: ServiceNotificationDTO.NotificationType = ServiceNotificationDTO.NotificationType.PUSH


    fun getDefaultServiceNotificationDTO() = ServiceNotificationDTO(
            title = defaultTitle,
            badge = defaultBadge,
            body = defaultBody,
            icon = defaultIcon,
            userId = defaultUserId,
            userEmail = defaultUserEmail!!,
            tag = defaultTag,
            notificationType = defaultNotificationType
    )

    fun getUpdatedServiceNotificationDTO() = ServiceNotificationDTO(
            title = updatedTitle,
            badge = updatedBadge,
            body = updatedBody,
            icon = updatedIcon,
            userId = updatedUserId,
            userEmail = updatedUserEmail!!,
            tag = updatedTag,
            notificationType = updatedNotificationType
    )
}
