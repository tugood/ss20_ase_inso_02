package com.ase.tugood.notificationservice.domain.notification.service.mapper

import com.ase.tugood.notificationservice.TestGuiNotificationBuilder
import com.ase.tugood.notificationservice.TestServiceNotificationBuilder
import com.ase.tugood.notificationservice.application.data.notification.ServiceNotificationDTO
import com.ase.tugood.notificationservice.domain.notification.model.PoNotification
import io.mockk.every
import io.mockk.mockkStatic
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Instant

internal class ServiceNotificationMapperTest {
    private lateinit var serviceNotificationMapper: ServiceNotificationMapper

    private lateinit var poNotification: PoNotification

    private lateinit var serviceNotificationDTO: ServiceNotificationDTO

    @BeforeEach
    fun setUp() {
        mockkStatic(Instant::class)
        every { Instant.now() } returns Instant.ofEpochSecond(1587910583) // Sun, 26 Apr 2020 14:16:23 GMT

        serviceNotificationMapper = ServiceNotificationMapper()
        serviceNotificationDTO = TestServiceNotificationBuilder().getDefaultServiceNotificationDTO()
        poNotification = TestGuiNotificationBuilder().getDefaultPoNotification()
    }

    @Test
    fun `test dto to entity`() {
        val poNotificationMapped = serviceNotificationMapper.toEntity(serviceNotificationDTO)

        assertNotNull(poNotificationMapped)
        assertNull(poNotificationMapped.id)
        assertEquals(serviceNotificationDTO.userId, poNotificationMapped.userId)
        assertEquals(Instant.now(), poNotificationMapped.timestamp)
        assertEquals(serviceNotificationDTO.title, poNotificationMapped.title)
        assertEquals(serviceNotificationDTO.body, poNotificationMapped.body)
        assertEquals(serviceNotificationDTO.icon, poNotificationMapped.icon)
    }

    @Test
    fun `test entity to dto should throw UnsupportedOperationException`() {
        assertThrows(UnsupportedOperationException::class.java) { serviceNotificationMapper.toDto(poNotification) }
    }
}
