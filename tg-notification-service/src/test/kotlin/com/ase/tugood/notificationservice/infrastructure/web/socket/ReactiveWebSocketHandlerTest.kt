package com.ase.tugood.notificationservice.infrastructure.web.socket

import com.ase.tugood.notificationservice.TestGuiNotificationBuilder
import com.ase.tugood.notificationservice.TestUserInfoBuilder
import com.ase.tugood.notificationservice.application.data.notification.GUINotificationDTO
import com.ase.tugood.notificationservice.infrastructure.web.model.request.UserInfo
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Timeout
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.test.context.ActiveProfiles
import org.springframework.web.reactive.socket.WebSocketMessage
import org.springframework.web.reactive.socket.client.JettyWebSocketClient
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient
import org.springframework.web.reactive.socket.client.WebSocketClient
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.publisher.ReplayProcessor
import java.lang.Thread.sleep
import java.net.URI
import java.net.URISyntaxException
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger
import kotlin.test.assertEquals
import kotlin.test.assertNotNull


@AutoConfigureWebTestClient
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(MockKExtension::class)
@ActiveProfiles("websocket")
internal class ReactiveWebSocketHandlerTest {


    companion object {
        private val json = ObjectMapper()

        init {
            val module = JavaTimeModule()
            json.registerModule(module)
            json.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        }
    }


    @LocalServerPort
    private val port: String? = null

    @Autowired
    @Qualifier("ReactiveWebSocketHandler")
    private lateinit var reactiveWebSocketHandler: ReactiveWebSocketHandler

    private lateinit var userInfo: UserInfo

    private lateinit var guiNotificationDTO: GUINotificationDTO

    private val client: JettyWebSocketClient = JettyWebSocketClient()

    @BeforeEach
    fun setup() {
        userInfo = TestUserInfoBuilder().validDefaultUserInfo()
        guiNotificationDTO = TestGuiNotificationBuilder().getDefaultGUINotificationDTO()
        client.start()
    }

    @AfterEach
    fun after() {
        client.stop()
    }

    @Throws(URISyntaxException::class)
    private fun getWebsocketURL(): URI {
        return URI("ws://localhost:$port/api/socket-notifications/notifications-emitter")
    }

    @Test
    @Throws(URISyntaxException::class, InterruptedException::class)
    @Timeout(value = 10000, unit = TimeUnit.MILLISECONDS)
    fun `test subscribe to websocket and create request should receive values`() {
        val count = 4
        val latch = CountDownLatch(count)

        // define a replay subscriber/publisher for the output of the websocket
        val output = ReplayProcessor.create<GUINotificationDTO>(count)

        val websessionMono = client.execute(getWebsocketURL()) { session ->
            session
                .send(Mono.just(session.textMessage(userInfo.id)))
                .thenMany(
                    session.receive()
                        .take(count.toLong())
                        .map(WebSocketMessage::getPayloadAsText)
                        .log()
                        .map { msg -> json.readValue(msg, GUINotificationDTO::class.java) }
                        .subscribeWith(output)
                        .then())
                .then()
        }

        // if the receive connection is established, subscribeWith must have subscribed to the output
        val i: AtomicInteger = AtomicInteger(0)
        output
            .doOnSubscribe {
                websessionMono.subscribe()
            }
            .log()
            .subscribe { dto: GUINotificationDTO ->
                assertNotNull(dto)
                assertEquals(i.incrementAndGet().toString(), dto.id)
                assertEquals(guiNotificationDTO.timestamp, dto.timestamp)
                assertEquals(guiNotificationDTO.title, dto.title)
                assertEquals(guiNotificationDTO.body, dto.body)
                assertEquals(guiNotificationDTO.icon, dto.icon)
                latch.countDown()
            }

        // let the whole thing process
        sleep(2000)

        // send messages from the server and check if the messages can be received by the client
        Flux.range(1, count)
            .map { index: Int ->
                guiNotificationDTO.copy(
                    id = index.toString()
                )
            }
            .flatMap {dto ->
                reactiveWebSocketHandler.reactiveSend(userInfo.id, dto)
            }.subscribe()


        latch.await()
    }



}
