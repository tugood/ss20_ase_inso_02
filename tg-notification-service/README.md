# TUGood notification service

## Kotlin Client for testing websockets
```kotlin
object ReactiveJavaClientWebSocket {
    @Throws(InterruptedException::class)
    @JvmStatic
    fun main(args: Array<String>) {
        val client: WebSocketClient = ReactorNettyWebSocketClient()
        client.execute(
            URI.create("http://localhost:8000/event-emitter")
        ) { session ->
            session.send(
                Mono.just(session.textMessage("event-spring-reactive-client-websocket")))
                .thenMany(session.receive()
                    .map(WebSocketMessage::getPayloadAsText)
                    .log())
                .then()
        }
            .block(Duration.ofSeconds(10L))
    }
}
```

## Special thanks to
[AngularPwaMessenger](https://github.com/Angular2Guy/AngularPwaMessenger)
