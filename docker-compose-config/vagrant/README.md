# Vagrant setup for Windows users

Download and install vagrant.

## Virtualbox setup
For Virtualbox install the following plugins

```shell script
vagrant plugin install vagrant-vbguest
vagrant plugin install vagrant-cachier
vagrant plugin install vagrant-faster
```

## Connect to Vagrant
```shell script
vagrant ssh
```

## Execute the following to setup the codebase
```shell script
git clone https://reset.inso.tuwien.ac.at/repo/2020ss-ASE-group/ss20_ase_inso_02.git
cd ./ss20_ase_inso_02
git remote set-url --push origin no-pushing
docker-compose up --build
```
