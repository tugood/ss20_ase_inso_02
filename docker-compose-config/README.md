# Docker compose configuration for the TUgood project

This README provides some helpful commands for working with docker images.

## Pull all images
```
docker-compose pull
```

## Create docker system
```
docker-compose up
```
or for rebuilding the docker system
```
docker-compose up --build
```

## Connecting to a docker container

```
docker exec -it <container id> sh
```
starts bash in the docker container

## Kill the docker system
```
 docker kill $(docker ps -q)
```

## Recreate docker system
```
docker system prune
docker system prune --volumes
docker network prune
```
or
```
docker system prune -a
```

Deletes all docker containers and volumes and thus databases to start anew.
(Use -a with caution)

or just the volumes
```
docker volume ls
docker volume rm <volume_name>
```

## Stop and start containers with rebuild
```
docker-compose stop <container id>
docker-compose up --build --force-recreate --no-deps [-d] [<service_name>..]
```


# Deregistering services via consul
Use this as an example on how to deregister services
```bash
curl -v -X PUT http://localhost:8500/v1/agent/service/deregister/tg-notification-service-338118
```
