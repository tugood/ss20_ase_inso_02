In order to run `npm run dev` you have to set the environment variable for keycloak credentials with

```bash
export LC_USERSERVICE_SECRET=0a2b51a7-....
```

receive the full secret from: https://vault.dev.tugood.team/ui/vault/secrets/secret/show/keycloak/admin-userservice
