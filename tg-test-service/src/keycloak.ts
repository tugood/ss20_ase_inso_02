import KeycloakAdminClient from 'keycloak-admin'
import UserRepresentation from 'keycloak-admin/lib/defs/userRepresentation'
import express from 'express'
import {
    defaultCatch,
    clone
} from './utils'
import {getKcAdminClient, validRealms} from './keycloak-utils'

const faker = require('faker');

export function setupKeycloak(app: express.Application) {
    const kcAdminClient: KeycloakAdminClient = getKcAdminClient()

    app.get('/api/tests/keycloak/users/', (req, res) => {
        let users = createKcUsers(req);

        res.contentType('application/json');
        res.send(JSON.stringify(users));
    })


    app.get('/api/tests/keycloak/:realm/users/fill', (req, res) => {

        if (!(validRealms.includes(req.params.realm))) {
            res.status(403); // forbidden
            res.send();
            return;
        }

        kcAdminClient.setConfig({
            realmName: req.params.realm,
        })

        let users = createKcUsers(req);
        let userIdList: String[] = [];

        users.forEach((el) => {
            console.log(`save ${JSON.stringify(el)}`)
            kcAdminClient.users
                .create(el)
                .then((resultId) => {
                    userIdList.push(resultId.id)
                    if(userIdList.length === users.length) {
                      res.json(userIdList);
                    }
                })
                // .then((result) => {
                //     // console.log(`created users: ${userIdList.toString()}`);
                // })
                .catch((reason) => {
                  defaultCatch(reason);
                  res.send("Not all or non of the users have been transmitted.");
                  return;
                });
        });
    })

    app.get('/api/tests/keycloak/:realm/users/delete', (req, res) => {

        if (!(validRealms.includes(req.params.realm))) {
            res.status(403); // forbidden
            res.send();
            return;
        }

        kcAdminClient.setConfig({
            realmName: req.params.realm,
        })

        kcAdminClient.users.find().then((kcUsers: UserRepresentation[]) =>
            kcUsers.forEach((user: UserRepresentation) =>
                kcAdminClient.users
                    .del({
                        id: user.id as string,
                        realm: req.params.realm,
                    })
                    .catch(defaultCatch)
            )
        ).catch(defaultCatch)
        res.send('OK')
    })
}

function createKcUsers(req): UserRepresentation[] {
    let maxString = req.query.max
    let max = 0

    if (req.query.max == undefined) {
        max = 10
    } else {
        max = Number(maxString);
    }

    faker.locale = "de";
    let users: UserRepresentation[] = []

    for (let i = 0; i < max; i++) {
        // delete all special characters and spaces! So the name can be used as email
        const firstName = faker.name.firstName().replace(/[^a-zA-Z ]/g, "");
        const lastName = faker.name.lastName().replace(/[^a-zA-Z ]/g, "");
        const email = `${firstName}${lastName}@discardmail.com`;
        console.log(`Create user with email ${email} and name ${firstName}`)
        const user: UserRepresentation = clone({
            credentials: [{
                temporary: false,
                type: "password",
                value: firstName
            }],
            email: email,
            emailVerified: true,
            enabled: true,
            firstName: firstName,
            lastName: lastName,
            username: email
        });

        users.push(user);
    }

    return users;
}
