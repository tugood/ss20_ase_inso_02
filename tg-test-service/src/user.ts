import express from 'express'
import {getKcAdminClient, getKcUsers, validRealms} from './keycloak-utils';
import {clone} from './utils';
import {KeycloakAdminClient} from 'keycloak-admin/lib/client';
import * as httpm from 'typed-rest-client/HttpClient';
import {HttpClient} from 'typed-rest-client/HttpClient';

const base64 = require('node-base64-image');

const { Faker } = require('fakergem');

interface UserAll {
    user: UserDTO,
    userInfo: UserInfo
}

interface UserDTO {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    phoneNr?: string;
    langKey?: string;
    description?: string;
    hasImage: boolean;
    distance: number;
    userState: string;
    authorities: string[];
}

interface UserInfo {
    id: string,
    email: string,
    given_name: string,
    family_name: string,
    username: string,
    email_verified: true,
    locale: string
}

const faker = require('faker');

const userState = ["ACTIVE", "BLOCKED"]
const langKeys = ["de", "en"]

export function setupUser(app: express.Application) {
    let kcAdminClient: KeycloakAdminClient = getKcAdminClient()

    app.get('/api/tests/users/:realm', (req, res) => {

        if (!(validRealms.includes(req.params.realm))) {
            res.status(403); // forbidden
            res.send();
            return;
        }

        kcAdminClient.setConfig({
            realmName: req.params.realm,
        })

        createUsers(req, kcAdminClient).then(async users => {
            res.contentType('application/json');
            res.send(JSON.stringify(users));
        });
    })

    app.get('/api/tests/users/:realm/fill', (req, res) => {

        if (!(validRealms.includes(req.params.realm))) {
            res.status(403); // forbidden
            res.send();
            return;
        }

        kcAdminClient.setConfig({
            realmName: req.params.realm,
        })

        createUsers(req, kcAdminClient).then( async users => {
            const httpc: httpm.HttpClient = new httpm.HttpClient('tg-user-service', [], {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            });

            const response = httpc.post(`${process.env.USER_SERVICE}/generate/users/accounts`, JSON.stringify(users))

            response.then((result) => {

                result.readBody().then(res => {

                    const resultArray: string[] = JSON.parse(res);
                    console.log(`UserResult: ${res}`)

                    users.forEach(userall => {
                        console.log(`check for user: ${userall.userInfo.id}`)

                        if (resultArray.includes(userall.userInfo.id)) {
                            getUserPicture(userall.user, httpc).then(async picture => {
                                // console.log(`pic: ${picture}`);

                                if (picture === '') {
                                    return;
                                }


                                const body: JSON = clone({
                                    picture
                                });
                                const pictureResponse = await httpc.put(`${process.env.USER_SERVICE}/generate/users/accounts/pictures/${userall.userInfo.id}`,
                                    JSON.stringify(body));

                                pictureResponse.readBody().then(p => {
                                    console.log(`Got: ${p}`)
                                });
                            });
                        }
                    });
                });

                res.contentType('application/json');

                if (typeof result.message.statusCode !== 'undefined') {
                    res.status(result.message.statusCode);
                } else {
                    res.status(500);
                }
                res.send("done");
            }).catch(cause => {
                console.log(`${cause}`);
                res.status(500);
                res.send("failure");
            });
        });

    })
}

async function getUserPicture(user: UserDTO, httpc: HttpClient): Promise<string> {

    if (!user.hasImage) {
        return Promise.resolve('');
    }

    let userPicture: string = '';


    if (user.hasImage) {
        let pictureMap: Map<string, string> = new Map<string, string>();

        const pictureURL = 'https://placekitten.com/256/256?image=' + (faker.random.number(12));

        if (!pictureMap.has(pictureURL)) {

            const options = {
                string: true
            };

            const image = await base64.encode(pictureURL, options);

            if (image !== undefined) {
                // console.log(`body: ${byteString}`);
                userPicture = `data:image/jpeg;base64,${image}`;
                pictureMap.set(pictureURL, userPicture);

            }
        } else {
            userPicture = pictureMap.get(pictureURL)!!;
        }
    }
    return userPicture;
}

async function createUsers(req, kcAdminClient): Promise<UserAll[]> {
    faker.locale = "de";
    let users: any[] = []

    const maxString = req.query.max
    let max = 0

    if (req.query.max == undefined) {
        max = 10
    } else {
        max = Number(maxString);
    }

    const kcUsers = await getKcUsers(kcAdminClient, max);

    console.log(`got ${kcUsers.length} users from keycloak`);

    for (const kcUser of kcUsers) {

        const userInfo: UserInfo = clone({
            id: kcUser.id,
            email: kcUser.email,
            given_name: kcUser.firstName,
            family_name: kcUser.lastName,
            username: kcUser.username,
            email_verified: true,
            locale: langKeys[faker.random.number(langKeys.length - 1)],
        });

        const user: UserDTO = clone({
            id: kcUser.id,
            email: kcUser.email,
            firstName: kcUser.firstName,
            lastName: kcUser.lastName,
            username: kcUser.username,
            langKey: langKeys[faker.random.number(langKeys.length - 1)],
            phoneNr: faker.phone.phoneNumber(),
            description: Faker.HarryPotter.quote(),
            userState: userState[faker.random.number(userState.length - 1)],
            distance: 25,  // use always nearly the max so that the matching algorithm can do its magic
            hasImage: faker.random.boolean()
        });

        const userall: UserAll = clone({
            userInfo,
            user
        });

        users.push(userall);
    }

    return users;
}
