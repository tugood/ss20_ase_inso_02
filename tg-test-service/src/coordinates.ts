// source: https://gitlab.com/snippets/28591

const EARTH_RADIUS = 6371000 /* meters  */
const DEG_TO_RAD =  Math.PI / 180.0
const THREE_PI = Math.PI*3
const TWO_PI = Math.PI*2

function isFloat(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function recursiveConvert(input, callback){
    if (input instanceof Array) {
        return input.map((el) => recursiveConvert(el, callback))
    }
    if  (input instanceof Object) {
        input = JSON.parse(JSON.stringify(input))
        for (let key in input) {
            if( input.hasOwnProperty(key) ) {
                input[key] = recursiveConvert(input[key], callback)
            }
        }
        return input
    }
    if (isFloat(input)) { return callback(input) }
}

function toRadians(input){
    return recursiveConvert(input, (val) => val * DEG_TO_RAD)
}

function toDegrees(input){
    return recursiveConvert(input, (val) => val / DEG_TO_RAD)
}


/*
coords is an object: {latitude: y, longitude: x}
toRadians() and toDegrees() convert all values of the object
*/
function pointAtDistance(inputCoords, distance) {
    const result = {
        latitude: 0.0,
        longitude: 0.0
    }
    const coords = toRadians(inputCoords)
    const sinLat = 	Math.sin(coords.latitude)
    const cosLat = 	Math.cos(coords.latitude)

    /* go fixed distance in random direction*/
    const bearing = Math.random() * TWO_PI
    const theta = distance/EARTH_RADIUS
    const sinBearing = Math.sin(bearing)
    const cosBearing = 	Math.cos(bearing)
    const sinTheta = Math.sin(theta)
    const cosTheta = 	Math.cos(theta)

    result.latitude = Math.asin(sinLat*cosTheta+cosLat*sinTheta*cosBearing);
    result.longitude = coords.longitude +
        Math.atan2( sinBearing*sinTheta*cosLat, cosTheta-sinLat*Math.sin(result.latitude )
        );
    /* normalize -PI -> +PI radians */
    result.longitude = ((result.longitude+THREE_PI)%TWO_PI)-Math.PI

    return toDegrees(result)
}

/* haversine*/
function distanceBetween (start, end) {
    const startPoint = toRadians(start)
    const endPoint = toRadians(end)

    const delta = {
        latitude:  Math.sin((endPoint.latitude -startPoint.latitude)/2),
        longitude: Math.sin((endPoint.longitude -startPoint.longitude)/2)
    }

    const A = delta.latitude * delta.latitude  +
        delta.longitude *  delta.longitude * Math.cos(startPoint.latitude) * Math.cos(endPoint.latitude)

    return EARTH_RADIUS*2 * Math.atan2(Math.sqrt(A), Math.sqrt(1-A))

}

export function pointInCircle(coord, distance) {
    const rnd =  Math.random()
    /*use square root of random number to avoid high density at the center*/
    const randomDist = Math.sqrt(rnd) * distance
    return pointAtDistance(coord, randomDist)
}

/* Test the above functions and compare at different places opn the earth */
export function doit(callback, place){
    let count = 0
    let min = 100000
    let max = 0
    let avgError = 0
    const radius = 1000

    const places = {
        sweden: {	 "latitude":67.498454, "longitude":   21.040181} ,
        london: {longitude: -0.059389,latitude: 51.408693},
        panama: {		"latitude": 8.5417461,	"longitude": -79.8867635}
    }

    for (let i = 0; i < 1000; i++) {


        const result = callback(places[place], radius);

        const dist = distanceBetween(places[place], result);

        avgError += Math.abs(dist - radius);

        if (dist > max) {
        }
        max = dist;
        if (dist < min) {
        }
        min = dist;
        count++;
    }
    avgError = (avgError*100 / radius) / count

    console.log("radius: "+radius+" meters")
    console.log("samples: "+count)
    console.log("max: "+max)
    console.log("min: "+min)
    console.log("mean error: "+ avgError +"%")
}
