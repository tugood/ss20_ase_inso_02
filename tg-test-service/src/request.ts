import express from 'express'
import {getKcAdminClient, getKcUsers, validRealms} from './keycloak-utils';
import {KeycloakAdminClient} from 'keycloak-admin/lib/client';
import {pointInCircle} from './coordinates';
import UserRepresentation from 'keycloak-admin/lib/defs/userRepresentation';
import * as httpm from 'typed-rest-client/HttpClient';
import Consulite from 'consulite'
import {clone} from './utils';

const faker = require('faker');
const { Faker } = require('fakergem');


interface HelpRequest {
    id: string;
    category: string;
    timeFrame: TimeFrame;
    description: string;
    requesterId: string;
    address: Address;
}

class TimeFrame {
    from?: Date;
    to?: Date;

    constructor(from?: Date, to?: Date) {
        this.from = from; // starting date of task
        this.to = to;     // date until task should be done. note: might be used in the future
    }
}

interface Address {
    streetName?: string;
    streetNumber?: string;
    postalCode: string;
    city: string;
    country: string;
    location: TgLocation;
}

interface TgLocation {
    longitude: number;
    latitude: number;
}



const radiusInMeters = 25000

const places = {
    vienna: {"latitude": 48.210033, "longitude": 16.363449},
    graz: {"latitude": 	47.076668, "longitude": 15.421371},
}

const categories = ["GROCERY_SHOPPING", "ANIMAL_SITTING", "CHILD_CARE",  "PERSONAL_TRAINING",  "OTHER"]

export function setupRequest(app: express.Application) {
    let kcAdminClient: KeycloakAdminClient = getKcAdminClient()

    app.get('/api/tests/requests/:realm', (req, res) => {

        if (!(validRealms.includes(req.params.realm))) {
            res.status(403); // forbidden
            res.send();
            return;
        }

        kcAdminClient.setConfig({
            realmName: req.params.realm,
        })

        createRequests(req, kcAdminClient).then(requests => {
            res.contentType('application/json');
            res.send(JSON.stringify(requests));
        })


    })

    app.get('/api/tests/requests/:realm/fill', (req, res) => {

        if (!(validRealms.includes(req.params.realm))) {
            res.status(403); // forbidden
            res.send();
            return;
        }

        kcAdminClient.setConfig({
            realmName: req.params.realm,
        })

        createRequests(req, kcAdminClient).then( async reqests => {

            const httpc: httpm.HttpClient = new httpm.HttpClient('tg-request-matching-service', [], {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            });

            const response = httpc.post(`${process.env.REQUEST_SERVICE}/generate/requests`, JSON.stringify(reqests))

            response.then((result) => {

                result.readBody().then(res => {
                    console.log(`RequestResult: ${res}`)
                })


                res.contentType('application/json');

                if (typeof result.message.statusCode !== 'undefined') {
                    res.status(result.message.statusCode);
                } else {
                    res.status(500);
                }
                res.send("done");
            }).catch(cause => {
                console.log(`${cause}`);
                res.status(500);
                res.send("failure");
            });
        });

    })
}

async function createRequests(req, kcAdminClient): Promise<JSON[]> {
    const maxString = req.query.max
    let max = 0

    if (req.query.max == undefined) {
        max = 10
    } else {
        max = Number(maxString);
    }

    let kcUserList: UserRepresentation[] = await getKcUsers(kcAdminClient, max);

    if (kcUserList.length <= 1) {
        return []
    }

    faker.locale = "de";
    let requests: any[] = []

    for (let i = 0; i < max; i++) {
        const randPlace = places[getRandomPlace()];

        if (randPlace == null) {
            console.log("error got unknown place");
            break;
        }

        const coordinates = pointInCircle(randPlace, radiusInMeters);

        const startDays = 1;
        const date = new Date()
        const dateFrom = new Date(date.setTime(date.getTime() + startDays * 86400000));

        const timeSpan = 5;
        const dateTo = new Date(date.setTime(date.getTime() + timeSpan * 86400000));

        const kcUser = kcUserList[faker.random.number(kcUserList.length - 1)];

        const request: HelpRequest = clone({
            address: {
                location: {
                    latitude: coordinates.latitude,
                    longitude: coordinates.longitude
                },
                streetName: faker.address.streetName(),
                streetNumber: faker.random.number(100),
                postalCode: faker.address.zipCode(),
                city: faker.address.city(),
                country: 'AT'
            },
            category: categories[faker.random.number(categories.length - 1)],
            timeFrame: {
                from: dateFrom,
                to: dateTo
            },
            description: Faker.ChuckNorris.fact(),
            requester: {
                id: kcUser.id,
                email: kcUser.email,
                firstName: kcUser.firstName,
                lastName: kcUser.lastName,
                username: kcUser.username
            },
            asap: faker.random.boolean()
        });

        requests.push(request);
    }

    return requests
}

function getRandomPlace() {
    let randplace = '';

    switch (faker.random.number(1)) {
        case 0:
            randplace = 'vienna';
            break;
        case 1:
            randplace = 'graz';
            break;
    }
    return randplace;

}
