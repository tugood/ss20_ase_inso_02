import KeycloakAdminClient from 'keycloak-admin'
import { Client, Issuer, TokenSet } from 'openid-client'
import { defaultCatch } from './utils'
import UserRepresentation from 'keycloak-admin/lib/defs/userRepresentation';
import {UserQuery} from 'keycloak-admin/lib/resources/users';

const keycloakUrl = 'https://auth.dev.tugood.team/auth'

const kcAdminClient = new KeycloakAdminClient({
    baseUrl: keycloakUrl,
    realmName: 'master',
})

export const validRealms = ['test', 'tugood'];

let isAuth: Boolean = false;

export async function getKcUsers(kcRealmClient: KeycloakAdminClient, max: number): Promise<UserRepresentation[]> {
    let users: UserRepresentation[] = [];

    const userQuery: UserQuery = {
        max: max
    }

    await kcRealmClient.users.find(userQuery).then((kcUsers: UserRepresentation[]) =>
        kcUsers.forEach((user: UserRepresentation) =>
            users.push(user)
        )
    ).catch(defaultCatch);

    return users;
}

export function getKcAdminClient(): KeycloakAdminClient {

    if (!isAuth) {

        kcAdminClient
            .auth({
                username: '',
                password: '',
                clientSecret: process.env.LC_USERSERVICE_SECRET,
                grantType: 'client_credentials',
                clientId: 'admin-userservice',
            })
            .catch(defaultCatch);

        setupReissue();
        isAuth = true;
    }

    kcAdminClient.setConfig({
        realmName: 'test',
    })
    return kcAdminClient
}

function setupReissue() {
    Issuer.discover(keycloakUrl + '/realms/master').then(
        (keycloakIssuer: Issuer<Client>) => {
            let client: Client = new keycloakIssuer.Client({
                // Same as `clientId` passed to client.auth()
                client_id: 'admin-userservice',
                client_secret: process.env.LC_USERSERVICE_SECRET,
            })
            client
                .grant({
                    grant_type: 'client_credentials',
                })
                .then((tokenSet: TokenSet) => {
                    // Periodically using refresh_token grant flow to get new access token here
                    setInterval(async () => {
                        const refreshToken: string = tokenSet.refresh_token as string
                        tokenSet = await client.refresh(refreshToken)
                        kcAdminClient.setAccessToken(
                            tokenSet.access_token as string
                        )
                    }, 30 * 1000) // 30 seconds
                })
                .catch((reason) => console.log(reason))
        }
    )
}
