import express from 'express'
import {getKcAdminClient, getKcUsers, validRealms} from './keycloak-utils';
import {KeycloakAdminClient} from 'keycloak-admin/lib/client';
import UserRepresentation from 'keycloak-admin/lib/defs/userRepresentation';
import * as httpm from 'typed-rest-client/HttpClient';
import {clone} from './utils';

const faker = require('faker');
const {Faker} = require('fakergem');


interface Notification {
  body: string;
  email: string;
  notificationType: string;
  tag: string;
  title: string;
  userId: string;
  badge: string;
  icon: string;
  userEmail: string;
}

const notBodyQuotes = [Faker.StarWars.quote(), Faker.TwinPeaks.quote(), Faker.HarryPotter.quote()]

export function setupNotification(app: express.Application) {
  let kcAdminClient: KeycloakAdminClient = getKcAdminClient()

  app.get('/api/tests/notifications/:realm', (req, res) => {

    if (!(validRealms.includes(req.params.realm))) {
      res.status(403); // forbidden
      res.send();
      return;
    }

    kcAdminClient.setConfig({
      realmName: req.params.realm,
    })

    createNotifications(req, kcAdminClient).then(requests => {
      res.contentType('application/json');
      res.send(JSON.stringify(requests));
    })
  })

  app.get('/api/tests/notifications/:realm/fill', (req, res) => {

    if (!(validRealms.includes(req.params.realm))) {
      res.status(403); // forbidden
      res.send();
      return;
    }

    kcAdminClient.setConfig({
      realmName: req.params.realm,
    })

    createNotifications(req, kcAdminClient).then(async notifications => {

      const httpc: httpm.HttpClient = new httpm.HttpClient('tg-notification-service', [], {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        }
      });

      let notResponses: string[] = []

      notifications.forEach(notification => {
        const response = httpc.post(`${process.env.NOTIFICATION_SERVICE}/internal/notifications/notify`, JSON.stringify(notification))

        response.then((result) => {
          result.readBody().then(res => {
            console.log(`RequestResult: ${res}`)
            notResponses.push("OK");
          })
        }).catch(cause => {
          console.log(`${cause}`);
          notResponses.push("Failure");
        });
      });

      res.contentType('application/json');
      res.send(notResponses);


    });

  })
}

async function createNotifications(req, kcAdminClient): Promise<Notification[]> {
  const maxString = req.query.max
  let max = 0

  if (req.query.max == undefined) {
    max = 10
  } else {
    max = Number(maxString);
  }

  let kcUserList: UserRepresentation[] = await getKcUsers(kcAdminClient, max);

  if (kcUserList.length <= 1) {
    return []
  }

  let notifications: Notification[] = []

  for (let i = 0; i < max; i++) {
    const kcUser = kcUserList[faker.random.number(kcUserList.length - 1)];

    const notification: Notification = clone({
      body: notBodyQuotes[faker.random.number(notBodyQuotes.length - 1)],
      notificationType: "PLAIN_OLD",
      tag: "useful stuff",
      title: Faker.Hacker.ingverb(),
      userId: kcUser.id,
      userEmail: kcUser.email
    });

    notifications.push(notification);
  }

  return notifications;
}
