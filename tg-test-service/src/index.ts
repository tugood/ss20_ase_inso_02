import express from 'express'
import { setupRequest } from './request'
import { setupKeycloak } from './keycloak'
import { setupUser } from './user'
import { setupNotification } from './notifications'

const app: express.Application = express()

const port = process.env.PORT || 9000

mountRoutes()

function mountRoutes() {
    app.get('/', (req, res) => res.send('Hello World!'))

    setupKeycloak(app)
    setupUser(app)
    setupRequest(app)
    setupNotification(app)

    app.listen(port, () =>
        console.log(`Example app listening at http://localhost:${port}`)
    )
}
