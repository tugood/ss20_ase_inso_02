
export function clone(data) {
    return JSON.parse(JSON.stringify(data))
}

export function defaultCatch(reason) {
    if (reason.response && reason.response.data)
        console.log(reason.response.data)
    else console.log(reason)
}
