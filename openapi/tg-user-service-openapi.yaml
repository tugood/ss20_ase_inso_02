openapi: 3.0.1
info:
  title: TUgood Abuse Management Service
  description: This is the API for the Abuse Management Service
  contact:
    email: silvio.vasiljevic@tuwien.ac.at
  version: 1.0.0
externalDocs:
  description: Project Repo
  url: https://reset.inso.tuwien.ac.at/repo/2020ss-ASE-group/ss20_ase_inso_02/-/tree/master/tg-user-service
servers:
- url: http://127.0.0.1:{port}
  description: local development
  variables:
    port:
      default: "8083"
tags:
- name: Login
  description: CRUD and attribute definitions for users
paths:
  /api/users/abuse/reports/{id}:
    delete:
      tags:
      - Login
      description: Delete report
      operationId: deleteReport
      parameters:
      - name: X-Userinfo
        in: header
        required: true
        schema:
          type: string
      - name: id
        in: path
        required: true
        schema:
          type: string
      responses:
        "400":
          description: The JSON is not valid
          content:
            application/json:
              schema:
                type: string
        "401":
          description: The request requires an user authentication
          content:
            application/json:
              schema:
                type: string
        "404":
          description: The resource could not be found
          content:
            application/json:
              schema:
                type: string
        "500":
          description: An unexpected error occurred.
          content:
            application/json:
              schema:
                type: string
        "200":
          description: User is warned
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserDTO'
  /api/users/abuse/reports:
    get:
      tags:
      - Login
      description: Get paged user reports
      operationId: getUserReports
      parameters:
      - name: X-Userinfo
        in: header
        required: true
        schema:
          type: string
      - name: page
        in: query
        required: true
        schema:
          type: integer
          format: int32
      - name: size
        in: query
        required: true
        schema:
          type: integer
          format: int32
      responses:
        "400":
          description: The JSON is not valid
          content:
            application/json:
              schema:
                type: string
        "401":
          description: The request requires an user authentication
          content:
            application/json:
              schema:
                type: string
        "404":
          description: The resource could not be found
          content:
            application/json:
              schema:
                type: string
        "500":
          description: An unexpected error occurred.
          content:
            application/json:
              schema:
                type: string
        "200":
          description: Page of user reports returned successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserReportDTO'
  /api/users/abuse/block/{id}:
    put:
      tags:
      - Login
      description: Block user
      operationId: blockUser
      parameters:
      - name: X-Userinfo
        in: header
        required: true
        schema:
          type: string
      - name: id
        in: path
        required: true
        schema:
          type: string
      responses:
        "400":
          description: The JSON is not valid
          content:
            application/json:
              schema:
                type: string
        "401":
          description: The request requires an user authentication
          content:
            application/json:
              schema:
                type: string
        "404":
          description: The resource could not be found
          content:
            application/json:
              schema:
                type: string
        "500":
          description: An unexpected error occurred.
          content:
            application/json:
              schema:
                type: string
        "200":
          description: User is blocked
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserDTO'
  /api/users/abuse/warn/{id}:
    put:
      tags:
      - Login
      description: Warn user
      operationId: warnUser
      parameters:
      - name: X-Userinfo
        in: header
        required: true
        schema:
          type: string
      - name: id
        in: path
        required: true
        schema:
          type: string
      responses:
        "400":
          description: The JSON is not valid
          content:
            application/json:
              schema:
                type: string
        "401":
          description: The request requires an user authentication
          content:
            application/json:
              schema:
                type: string
        "404":
          description: The resource could not be found
          content:
            application/json:
              schema:
                type: string
        "500":
          description: An unexpected error occurred.
          content:
            application/json:
              schema:
                type: string
        "200":
          description: User is warned
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserDTO'
  /api/users/accounts/self/image:
    put:
      tags:
      - image-upload-controller
      description: Upload an new image as a user picture.
      operationId: uploadImage
      parameters:
      - name: X-Userinfo
        in: header
        required: true
        schema:
          type: string
      requestBody:
        content:
          multipart/form-data:
            schema:
              type: object
              properties:
                fileParts:
                  type: array
                  items:
                    type: string
                    format: binary
      responses:
        "400":
          description: The JSON is not valid
          content:
            application/stream+json:
              schema:
                $ref: '#/components/schemas/Error'
        "401":
          description: The request requires an user authentication
          content:
            application/stream+json:
              schema:
                $ref: '#/components/schemas/Error'
        "404":
          description: The resource could not be found
          content:
            application/stream+json:
              schema:
                $ref: '#/components/schemas/Error'
        "500":
          description: An unexpected error occurred.
          content:
            application/stream+json:
              schema:
                $ref: '#/components/schemas/Error'
        "200":
          description: Id of uploaded image returned
          content:
            application/stream+json:
              schema:
                $ref: '#/components/schemas/ObjectId'
  /api/users/image/{userId}:
    get:
      tags:
      - image-upload-controller
      description: Fetch user image
      operationId: getUserImage
      parameters:
      - name: userId
        in: path
        required: true
        schema:
          type: string
      responses:
        "400":
          description: The JSON is not valid
          content:
            application/octet-stream:
              schema:
                $ref: '#/components/schemas/Error'
        "401":
          description: The request requires an user authentication
          content:
            application/octet-stream:
              schema:
                $ref: '#/components/schemas/Error'
        "404":
          description: The resource could not be found
          content:
            application/octet-stream:
              schema:
                $ref: '#/components/schemas/Error'
        "500":
          description: An unexpected error occurred.
          content:
            application/octet-stream:
              schema:
                $ref: '#/components/schemas/Error'
        "200":
          description: Image object returned
  /internal/users/accounts/{id}:
    get:
      tags:
      - internal-user-controller
      description: Retrieve user by id
      operationId: getUserById
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: string
      responses:
        "400":
          description: The JSON is not valid
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "401":
          description: The request requires an user authentication
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "404":
          description: The resource could not be found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "500":
          description: An unexpected error occurred.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "200":
          description: User found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserDTO'
  /internal/users/ratings/{id}:
    post:
      tags:
      - internal-user-controller
      description: Create a rating for user
      operationId: createRating
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/RatingDTO'
        required: true
      responses:
        "400":
          description: The JSON is not valid
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "401":
          description: The request requires an user authentication
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "404":
          description: The resource could not be found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "500":
          description: An unexpected error occurred.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "200":
          description: Rating added
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserDTO'
  /api/users/accounts/self:
    get:
      tags:
      - Login
      description: Retrieve current active user
      operationId: getUser
      parameters:
      - name: X-Userinfo
        in: header
        required: true
        schema:
          type: string
      responses:
        "400":
          description: The JSON is not valid
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "401":
          description: The request requires an user authentication
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "404":
          description: The resource could not be found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "500":
          description: An unexpected error occurred.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "200":
          description: User found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserDTO'
    put:
      tags:
      - Login
      description: Update a user
      operationId: updateUser
      parameters:
      - name: X-Userinfo
        in: header
        required: true
        schema:
          type: string
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UserDTO'
        required: true
      responses:
        "400":
          description: The JSON is not valid
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "401":
          description: The request requires an user authentication
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "404":
          description: The resource could not be found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "500":
          description: An unexpected error occurred.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "200":
          description: User updated
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserDTO'
  /api/users/accessToken:
    get:
      tags:
      - Login
      description: Fetch an access token
      operationId: getAccessToken
      parameters:
      - name: X-Access-Token
        in: header
        required: true
        schema:
          type: string
      responses:
        "400":
          description: The JSON is not valid
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "401":
          description: The request requires an user authentication
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "404":
          description: The resource could not be found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "500":
          description: An unexpected error occurred.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "200":
          description: Access token returned
          content:
            application/json:
              schema:
                type: string
  /api/users/idToken:
    get:
      tags:
      - Login
      description: Fetch the corresponding id token.
      operationId: getIdToken
      parameters:
      - name: X-Id-Token
        in: header
        required: true
        schema:
          type: string
      responses:
        "400":
          description: The JSON is not valid
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "401":
          description: The request requires an user authentication
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "404":
          description: The resource could not be found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "500":
          description: An unexpected error occurred.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "200":
          description: ID token returned
          content:
            application/json:
              schema:
                type: string
  /api/users/printHeader:
    get:
      tags:
      - Login
      description: Fetch the corresponding id tokens.
      operationId: getIdToken_1
      responses:
        "400":
          description: The JSON is not valid
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "401":
          description: The request requires an user authentication
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "404":
          description: The resource could not be found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "500":
          description: An unexpected error occurred.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "200":
          description: ID tokens returned
          content:
            application/json:
              schema:
                type: object
                additionalProperties:
                  type: string
  /api/users/userinfo:
    get:
      tags:
      - Login
      description: Retrieve user info
      operationId: getUserInfo
      parameters:
      - name: X-Userinfo
        in: header
        required: true
        schema:
          type: string
      responses:
        "400":
          description: The JSON is not valid
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "401":
          description: The request requires an user authentication
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "404":
          description: The resource could not be found
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "500":
          description: An unexpected error occurred.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
        "200":
          description: User info returned
          content:
            application/json:
              schema:
                type: string
  /api/users/report:
    post:
      tags:
      - Login
      description: Create and Get user report
      operationId: createUserReport
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UserReportDTO'
        required: true
      responses:
        "400":
          description: The JSON is not valid
          content:
            application/json:
              schema:
                type: string
        "401":
          description: The request requires an user authentication
          content:
            application/json:
              schema:
                type: string
        "404":
          description: The resource could not be found
          content:
            application/json:
              schema:
                type: string
        "500":
          description: An unexpected error occurred.
          content:
            application/json:
              schema:
                type: string
        "201":
          description: User report created
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserReportDTO'
  /api/users/report/{id}:
    get:
      tags:
      - Login
      description: Get user reports  by user id
      operationId: getUserReportsByUserId
      parameters:
      - name: id
        in: path
        required: true
        schema:
          type: string
      responses:
        "400":
          description: The JSON is not valid
          content:
            application/json:
              schema:
                type: string
        "401":
          description: The request requires an user authentication
          content:
            application/json:
              schema:
                type: string
        "404":
          description: The resource could not be found
          content:
            application/json:
              schema:
                type: string
        "500":
          description: An unexpected error occurred.
          content:
            application/json:
              schema:
                type: string
        "200":
          description: List of user reports for specific user returned successfully
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/UserReportDTO'
components:
  schemas:
    UserDTO:
      type: object
      properties:
        id:
          type: string
        firstName:
          maxLength: 50
          minLength: 0
          type: string
        lastName:
          maxLength: 50
          minLength: 0
          type: string
        email:
          maxLength: 254
          minLength: 5
          type: string
        phoneNr:
          maxLength: 20
          minLength: 4
          type: string
        langKey:
          maxLength: 10
          minLength: 2
          type: string
        description:
          type: string
        distance:
          type: integer
          format: int64
        hasImage:
          type: boolean
        userState:
          type: string
          enum:
          - ACTIVE
          - BLOCKED
        avgRating:
          type: number
          format: double
        reportCount:
          type: integer
          format: int64
        warned:
          type: boolean
    UserReportDTO:
      type: object
      properties:
        id:
          type: string
        userId:
          type: string
        user:
          $ref: '#/components/schemas/UserDTO'
        cause:
          type: string
          enum:
          - FAKE_PROFILE
          - ILLEGAL_ACTIVITY
          - DISCRIMINATION
          - ABUSIVE_BEHAVIOUR
        description:
          type: string
    Error:
      type: object
      properties:
        message:
          type: string
        type:
          type: string
        code:
          type: integer
          format: int32
        cause:
          type: array
          items:
            $ref: '#/components/schemas/Error'
    ObjectId:
      type: object
      properties:
        timestamp:
          type: integer
          format: int32
        counter:
          type: integer
          format: int32
        machineIdentifier:
          type: integer
          format: int32
        processIdentifier:
          type: integer
          format: int32
        timeSecond:
          type: integer
          format: int32
        time:
          type: integer
          format: int64
        date:
          type: string
          format: date-time
    RatingDTO:
      type: object
      properties:
        rating:
          maximum: 5
          minimum: 0
          type: integer
          format: int32
