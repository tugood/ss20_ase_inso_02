#!/bin/bash

export $(cat .env | sed 's/#.*//g' | xargs)

cd ..
echo "---------------Disabling ufw firewall---------------"
ssh root@$toolsDroplet 'ufw disable'
echo "---------------Creating tugood folder---------------"
ssh root@$toolsDroplet 'mkdir -p tugood'
echo "---------------Syncing vault files---------------"
rsync -avuz vault root@$toolsDroplet:tugood/
echo "---------------Syncing keycloak files---------------"
rsync -avuz keycloak root@$toolsDroplet:tugood/
echo "---------------Syncing docker-compose file---------------"
rsync -avuz docker-compose.yml root@$toolsDroplet:tugood/
echo "---------------Running docker-compose up---------------"
ssh -o SendEnv=LC_* root@$toolsDroplet 'cd tugood && docker-compose up -d'
echo "---------------Getting pritunl password---------------"
ssh root@$toolsDroplet 'docker exec tugood_pritunl-zero_1 pritunl-zero default-password'
