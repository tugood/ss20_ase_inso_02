#!/bin/bash

export $(cat .env | sed 's/#.*//g' | xargs)

echo "---------------Adding TMP Admin---------------"
ssh root@$toolsDroplet 'docker exec tugood_keycloak_1 /opt/jboss/keycloak/bin/add-user-keycloak.sh -u keycloak -p keycloak'
echo "---------------Restarting Keycloak---------------"
ssh root@$toolsDroplet 'docker restart tugood_keycloak_1'
