#!/bin/bash

export $(cat .env | sed 's/#.*//g' | xargs)

ssh root@$toolsDroplet 'docker exec tugood_keycloak_1 /opt/jboss/keycloak/bin/standalone.sh -Djboss.socket.binding.port-offset=100 -Dkeycloak.migration.action=import -Dkeycloak.migration.provider=singleFile -Dkeycloak.migration.file=/tmp/keycloak_export.json -Dkeycloak.migration.strategy=OVERWRITE_EXISTING'
