#!/bin/bash

export $(cat .env | sed 's/#.*//g' | xargs)
export VAULT_ADDR=$LC_VAULT_ADDR

cd ..
if [ -f "vault-bin" ]; then
    echo "---------------vault cli already exists---------------"
else
    echo "---------------Installing vault cli---------------"
    mkdir -p tmp
    cd tmp
    wget https://releases.hashicorp.com/vault/1.4.0/vault_1.4.0_linux_amd64.zip
    unzip vault_1.4.0_linux_amd64.zip
    chmod +x vault
    mv vault ../vault-bin
    cd ..
    rm -rf tmp
fi
echo "Setting up vault at $VAULT_ADDR"
echo "---------------Logging into vault---------------"
./vault-bin login token=$VAULT_TOKEN
echo "---------------Setting up policies---------------"
./vault-bin policy write admin setup/policies/admin.hcl
./vault-bin policy write tugood-ci setup/policies/tugood-ci.hcl
echo "---------------Creating the OIDC Login---------------"
./vault-bin auth enable oidc
./vault-bin write auth/oidc/config \
        oidc_discovery_url="https://gitlab.com" \
        oidc_client_id="$GITLAB_CLIENT_ID" \
        oidc_client_secret="$GITLAB_CLIENT_SECRET" \
        default_role="admin"
./vault-bin write auth/oidc/role/admin -<<EOF
{
  "user_claim": "sub",
  "allowed_redirect_uris":"$VAULT_ADDR/ui/vault/auth/oidc/oidc/callback,http://localhost:8250/oidc/callback",
  "bound_audiences":"$GITLAB_CLIENT_ID",
  "role_type":"oidc",
  "oidc_scopes":"openid",
  "policies":"default",
  "ttl": "1h",
  "bound_claims": {
    "groups": "tugood"
  },
  "groups_claim":"groups"
}
EOF
echo "---------------Creating group---------------"
./vault-bin write identity/group -format=json name="tugood-team" type="external" \
        policies="admin" \
        metadata=responsibility="Manage K/V Secrets"
export GROUP_ID="$(./vault-bin list -format=json identity/group/id | jq -r '.[0]')"
./vault-bin auth list -format=json  \
        | jq -r '."oidc/".accessor' > accessor.txt
./vault-bin write identity/group-alias name="tugood" \
        mount_accessor=$(cat accessor.txt) \
        canonical_id="$GROUP_ID"
rm accessor.txt
echo "---------------Creating the JWT Gilab runner auth---------------"
./vault-bin auth enable jwt
./vault-bin write auth/jwt/config \
    jwks_url="https://gitlab.com/-/jwks" \
    bound_issuer="gitlab.com"
./vault-bin write auth/jwt/role/tugood-ci -<<EOF
{
  "role_type": "jwt",
  "policies":["tugood-ci"],
  "token_explicit_max_ttl":60,
  "user_claim":"user_email",
  "bound_claims": {
    "project_id": "17814329"
  }
}
EOF
echo "---------------Creating secrets---------------"
./vault-bin secrets enable -path=secret kv
./vault-bin kv put secret/keycloak/db username=$LC_KEYCLOAK_DB_USERNAME password=$LC_KEYCLOAK_DB_PASSWORD
./vault-bin kv put secret/keycloak/admin username=$LC_ADMIN_USERNAME password=$LC_ADMIN_PASSWORD
./vault-bin kv put secret/keycloak/kong client_secret=$LC_KEYCLOAK_KONG_SECRET
./vault-bin kv put secret/keycloak/admin-userservice client_secret=$LC_KEYCLOAK_USERSERVICE_SECRET
./vault-bin kv put secret/microservice/tg-user-service/mongo username=$LC_USERSERVICE_MONGO_USERNAME password=$LC_USERSERVICE_MONGO_PASSWORD
./vault-bin kv put secret/microservice/tg-notification-service/mongo username=$LC_NOTIFICATIONSERVICE_MONGO_USERNAME password=$LC_NOTIFICATIONSERVICE_MONGO_PASSWORD
./vault-bin kv put secret/microservice/tg-notification-service/vapid public_key=$LC_NOTIFICATIONSERVICE_VAPID_PUBLIC_KEY private_key=$LC_NOTIFICATIONSERVICE_VAPID_PRIVATE_KEY
./vault-bin kv put secret/kong/pg username=$LC_KONG_PG_USERNAME password=$LC_KONG_PG_PASSWORD
echo "---------------Setting up the vault unsealer---------------"
export LC_VAULT_KEY1=$VAULT_KEY1
ssh -o SendEnv=LC_VAULT_* root@$toolsDroplet 'envsubst < tugood/vault/vault-unseal.json > vault-unseal.tmp'
ssh root@$toolsDroplet 'mv vault-unseal.tmp tugood/vault/vault-unseal.json'
ssh -o SendEnv=LC_* root@$toolsDroplet 'cd tugood && docker-compose up -d vault-unseal'
