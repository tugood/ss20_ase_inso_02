#!/bin/bash

echo "---------------Getting secrets from the vault---------------"
export VAULT_ADDR=https://vault.dev.tugood.team
export LC_VAULT_ADDR=$VAULT_ADDR
export LC_KEYCLOAK_KONG_SECRET="$(vault kv get -field=client_secret secret/keycloak/kong)"
export LC_KEYCLOAK_USERSERVICE_SECRET="$(vault kv get -field=client_secret secret/keycloak/admin-userservice)"
export LC_USERSERVICE_SECRET="$(vault kv get -field=client_secret secret/keycloak/admin-userservice)"
export LC_KEYCLOAK_DB_USERNAME="$(vault kv get -field=username secret/keycloak/db)"
export LC_KEYCLOAK_DB_PASSWORD="$(vault kv get -field=password secret/keycloak/db)"
export LC_KEYCLOAK_USERNAME="$(vault kv get -field=username secret/keycloak/admin)"
export LC_KEYCLOAK_PASSWORD="$(vault kv get -field=password secret/keycloak/admin)"
export LC_USERSERVICE_MONGO_USERNAME="$(vault kv get -field=username secret/microservice/tg-user-service/mongo)"
export LC_USERSERVICE_MONGO_PASSWORD="$(vault kv get -field=password secret/microservice/tg-user-service/mongo)"
export LC_NOTIFICATIONSERVICE_MONGO_USERNAME="$(vault kv get -field=username secret/microservice/tg-notification-service/mongo)"
export LC_NOTIFICATIONSERVICE_MONGO_PASSWORD="$(vault kv get -field=password secret/microservice/tg-notification-service/mongo)"
export LC_NOTIFICATIONSERVICE_VAPID_PUBLIC_KEY="$(vault kv get -field=public_key /secret/microservice/tg-notification-service/vapid)"
export LC_NOTIFICATIONSERVICE_VAPID_PRIVATE_KEY="$(vault kv get -field=private_key secret/microservice/tg-notification-service/vapid)"
export LC_KONG_PG_USERNAME="$(vault kv get -field=username secret/kong/pg)"
export LC_KONG_PG_PASSWORD="$(vault kv get -field=password secret/kong/pg)"
echo "---------------Updating keycloak files---------------"
rsync -avuz keycloak root@64.225.105.4:tugood/
echo "---------------Updating docker-compose file---------------"
rsync -avuz docker-compose.yml root@64.225.105.4:tugood/
echo "---------------Running docker-compose pull---------------"
ssh -o SendEnv=LC_* root@64.225.105.4 'cd tugood && docker-compose pull'
echo "---------------Running docker-compose up---------------"
ssh -o SendEnv=LC_* root@64.225.105.4 'cd tugood && docker-compose up -d'
