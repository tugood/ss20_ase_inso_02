#!/bin/bash

echo "---------------Building-Pushing new Image---------------"
docker build --tag registry.gitlab.com/tugood/ss20_ase_inso_02/shoepping:latest ../../third-party-client
docker push registry.gitlab.com/tugood/ss20_ase_inso_02/shoepping:latest
echo "---------------Running tools deploy---------------"
./deploy.sh
