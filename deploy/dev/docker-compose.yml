version: "3.4"

volumes:
  kong_data: {}
  consul_data: {}
  pritunl_db:

  tg_mongo_data:

networks:
  kong-net:
    external: false
    ipam:
      config:
        - subnet: 172.18.1.0/16
  api-management-net:
    external: false
    ipam:
      config:
        - subnet: 172.177.0.0/16
  management-uis:
    external: false
  mongo-db-net:
    external: false

services:
  #QOL
  pritunl-zero:
    image: pritunl/pritunl-zero:latest
    environment:
      MONGO_URI: "mongodb://pritunl-db:27017/pritunl-zero"
      NODE_ID: "5b8e11e4610f990034635e98"
      TZ: Europe/Vienna
    restart: always
    ports:
      - "80:80"
      - "443:443"
    depends_on:
      - pritunl-db
    networks:
      management-uis:
    tmpfs:
      - /var/log

  pritunl-db:
    image: mongo:latest
    restart: always
    volumes:
      - pritunl_db:/data/db
    networks:
      - management-uis

  portainer-agent:
    image: portainer/agent
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
      - "/var/lib/docker/volumes:/var/lib/docker/volumes"
    environment:
      TZ: Europe/Vienna
    restart: always
    ports:
      - target: 9001
        published: 9001
        protocol: tcp
        mode: host
  #QOL END

  # general project network with API gateway and service discovery

  kong-migrations:
    image: registry.gitlab.com/tugood/ss20_ase_inso_02/kong-oidc:latest
    command: kong migrations bootstrap
    depends_on:
      - kong-db
    environment:
      KONG_DATABASE: postgres
      KONG_PG_DATABASE: kong
      KONG_PG_HOST: 172.18.1.5
      KONG_PG_USER: ${LC_KONG_PG_USERNAME}
      KONG_PG_PASSWORD: ${LC_KONG_PG_PASSWORD}
      KONG_PLUGINS: bundled,oidc
    networks:
      kong-net:
        ipv4_address: 172.18.1.2
    restart: on-failure

  kong-migrations-up:
    image: registry.gitlab.com/tugood/ss20_ase_inso_02/kong-oidc:latest
    command: kong migrations up && kong migrations finish
    depends_on:
      - kong-migrations
      - kong-db
    environment:
      KONG_DATABASE: postgres
      KONG_PG_DATABASE: kong
      KONG_PG_HOST: 172.18.1.5
      KONG_PG_USER: ${LC_KONG_PG_USERNAME}
      KONG_PG_PASSWORD: ${LC_KONG_PG_PASSWORD}
      KONG_PLUGINS: bundled,oidc
    networks:
      kong-net:
        ipv4_address: 172.18.1.3
    restart: on-failure

  kong-init:
    image: "${KONG_INIT_DOCKER_TAG:-hbagdi/deck:latest}"
    depends_on:
      - kong
    volumes:
      - ./docker-compose-config/kong/kong.yml:/etc/kong-init/kong.yml
    command: sync --kong-addr="http://kong:8001" -s=/etc/kong-init/kong.yml --verbose 1
    networks:
      kong-net:
        ipv4_address: 172.18.1.7
    restart: on-failure

  kong:
    image: registry.gitlab.com/tugood/ss20_ase_inso_02/kong-oidc:latest
    user: kong
    depends_on:
      - kong-migrations-up
      - kong-migrations
      - kong-db
      - consul
    environment:
      KONG_ADMIN_ACCESS_LOG: /dev/stdout
      KONG_ADMIN_ERROR_LOG: /dev/stderr
      KONG_ADMIN_LISTEN: "0.0.0.0:8001"
      KONG_CASSANDRA_CONTACT_POINTS: kong-db
      KONG_DATABASE: postgres
      KONG_PG_DATABASE: kong
      KONG_PG_HOST: 172.18.1.5
      KONG_PG_USER: ${LC_KONG_PG_USERNAME}
      KONG_PG_PASSWORD: ${LC_KONG_PG_PASSWORD}
      # There is an issue with the password file for this command https://github.com/Kong/docker-kong/issues/91
      # KONG_PG_PASSWORD_FILE: /run/secrets/kong_postgres_password
      KONG_PROXY_ACCESS_LOG: /dev/stdout
      KONG_PROXY_ERROR_LOG: /dev/stderr
      KONG_DNS_RESOLVER: ${CONSUL_DNS_SERVICE_IPV4}:8600 # default for consul
      KONG_PLUGINS: bundled,oidc
      # use when debugging Kong behaviour:
      #KONG_LOG_LEVEL: debug
      TZ: Europe/Vienna
    networks:
      api-management-net:
        ipv4_address: ${KONG_API_GATEWAY_SERVICE_IPV4}
      kong-net:
        ipv4_address: 172.18.1.4
      management-uis:
    healthcheck:
      test: ["CMD", "kong", "health"]
      interval: 10s
      timeout: 10s
      retries: 10
    restart: on-failure

  kong-db:
    image: "${POSTGRES_DOCKER_TAG:-postgres:9.5}"
    environment:
      POSTGRES_DB: kong
      POSTGRES_USER: ${LC_KONG_PG_USERNAME}
      POSTGRES_PASSWORD: ${LC_KONG_PG_PASSWORD}
    healthcheck:
      test: ["CMD", "pg_isready", "-U", "${KONG_PG_USER:-kong}"]
      interval: 30s
      timeout: 30s
      retries: 3
    restart: on-failure
    stdin_open: true
    tty: true
    networks:
      kong-net:
        ipv4_address: 172.18.1.5
    volumes:
      - kong_data:/var/lib/postgresql/data

  konga:
    image: "${KONGA_DOCKER_TAG:-pantsel/konga:0.14.7}"
    depends_on:
      - kong
    networks:
      kong-net:
        ipv4_address: 172.18.1.6
      management-uis:
    environment:
      DB_ADAPTER: postgres
      DB_HOST: 172.18.1.5
      DB_PORT: 5432
      DB_USER: ${LC_KONG_PG_USERNAME}
      DB_PASSWORD: ${LC_KONG_PG_PASSWORD}
      DB_DATABASE: kong
      NODE_ENV: development

  consul:
    image: "${CONSUL_DOCKER_TAG:-consul:latest}"
    networks:
      api-management-net:
        ipv4_address: ${CONSUL_DNS_SERVICE_IPV4}
      management-uis:
    environment:
      TZ: Europe/Vienna
    volumes:
      - consul_data:/consul/data
    command:
      - "agent"
      - "-server"
      - "-client"
      - "0.0.0.0"
        # - '-advertise={{ GetInterfaceIP "eth0" }}'
        # This does not work thus use bind and regular expressions to find the correct interface
      # The bind interface will be automatically advertised
      - "-bind"
      - "${CONSUL_DNS_SERVICE_IPV4}"
        # Or try to bind only a specific one
      # - '{{ GetAllInterfaces | include "name" "^eth" | include "flags" "forwardable|up" | attr "address" }}'
      - "-recursor"
      - "1.0.0.1"
      - "-bootstrap"
      - "-ui"
      - "-dev"
    healthcheck:
      test:
        [
          "CMD-SHELL",
          "curl -I -s -L http://${CONSUL_DNS_SERVICE_IPV4}:8500 || exit 1",
        ]
      interval: 5s
      retries: 10
    restart: on-failure
    dns:
      - ${CONSUL_DNS_SERVICE_IPV4}

  # Mongo Database
  mongo-db:
    image: "${MONGO_DOCKER_TAG:-mongo:latest}"
    environment:
      MONGO_INITDB_ROOT_USERNAME: ${LC_MONGO_USERNAME}
      MONGO_INITDB_ROOT_PASSWORD: ${LC_MONGO_PASSWORD}
    networks:
      mongo-db-net:
    volumes:
      - tg_mongo_data:/data/db
    restart: on-failure

  # Mongo Web UI
  mongo-ui:
    image: mongo-express
    depends_on:
      - mongo-db
    networks:
      mongo-db-net:
      management-uis:
    environment:
      ME_CONFIG_MONGODB_ADMINUSERNAME: ${LC_MONGO_USERNAME}
      ME_CONFIG_MONGODB_ADMINPASSWORD: ${LC_MONGO_PASSWORD}
      ME_CONFIG_MONGODB_SERVER: mongo-db

  # User Service
  tg-user-service:
    image: registry.gitlab.com/tugood/ss20_ase_inso_02/tg-user-service:dev
    environment:
      MONGO_HOST: mongo-db
      MONGO_DB: userservice-db
      MONGO_USER: ${LC_MONGO_USERNAME}
      MONGO_PWD: ${LC_MONGO_PASSWORD}
      CONSUL_DNS_SERVICE_IPV4: ${CONSUL_DNS_SERVICE_IPV4}

      # Keycloak Info
      KEYCLOAK_USERSERVICE_URL:  ${LC_KEYCLOAK_USERSERVICE_URL}
      KEYCLOAK_USERSERVICE_REALM:  ${LC_KEYCLOAK_USERSERVICE_REALM}
      KEYCLOAK_USERSERVICE_CLIENTID:  ${LC_KEYCLOAK_USERSERVICE_CLIENTID}
      KEYCLOAK_USERSERVICE_SECRET:  ${LC_KEYCLOAK_USERSERVICE_SECRET}
      KEYCLOAK_USERSERVICE_USER_REALM:  ${LC_KEYCLOAK_USERSERVICE_USER_REALM}

      TZ: Europe/Vienna
      TG_EMAIL_USERNAME: ${LC_TG_EMAIL_USERNAME}
      TG_EMAIL_PASSWORD: ${LC_TG_EMAIL_PASSWORD}
    networks:
      api-management-net:
        ipv4_address: ${TG_USER_SERVICE_IPV4}
      mongo-db-net:
    restart: on-failure
    depends_on:
      - kong
      - consul
      - mongo-db

  # Notification service
  tg-notification-service:
    image: registry.gitlab.com/tugood/ss20_ase_inso_02/tg-notification-service:dev
    environment:
      MONGO_HOST: mongo-db
      MONGO_DB: notificationservice-db
      MONGO_USER: ${LC_MONGO_USERNAME}
      MONGO_PWD: ${LC_MONGO_PASSWORD}
      CONSUL_DNS_SERVICE_IPV4: ${CONSUL_DNS_SERVICE_IPV4}
      WEBPUSH_VAPID_PUBLIC_KEY: ${LC_NOTIFICATIONSERVICE_VAPID_PUBLIC_KEY}
      WEBPUSH_VAPID_PRIVATE_KEY: ${LC_NOTIFICATIONSERVICE_VAPID_PRIVATE_KEY}
      TZ: Europe/Vienna
    networks:
      api-management-net:
        ipv4_address: ${TG_NOTIFICATION_SERVICE_IPV4}
      mongo-db-net:
    restart: on-failure
    depends_on:
      - kong
      - consul
      - mongo-db

  # Request matching service
  tg-request-matching-service:
    image: registry.gitlab.com/tugood/ss20_ase_inso_02/tg-request-matching-service:dev
    environment:
      MONGO_HOST: mongo-db
      MONGO_DB: requestservice-db
      MONGO_USER: ${LC_MONGO_USERNAME}
      MONGO_PWD: ${LC_MONGO_PASSWORD}
      CONSUL_DNS_SERVICE_IPV4: ${CONSUL_DNS_SERVICE_IPV4}
      TZ: Europe/Vienna
      RATELIMIT_MAX_CREATION_LIMIT: 3
      RATELIMIT_MAX_MATCH_LIMIT: 3
    networks:
      api-management-net:
        ipv4_address: ${TG_REQUEST_MATCHING_SERVICE_IPV4}
      mongo-db-net:
    restart: on-failure
    depends_on:
      - mongo-db
      - kong
      - consul

  # Karma service
  tg-karma-service:
    image: registry.gitlab.com/tugood/ss20_ase_inso_02/tg-karma-service:dev
    environment:
      MONGO_HOST: mongo-db
      MONGO_DB: karmaservice-db
      MONGO_USER: ${LC_MONGO_USERNAME}
      MONGO_PWD: ${LC_MONGO_PASSWORD}
      CONSUL_DNS_SERVICE_IPV4: ${CONSUL_DNS_SERVICE_IPV4}
      TZ: Europe/Vienna
    networks:
      api-management-net:
        ipv4_address: ${TG_KARMA_SERVICE_IPV4}
      mongo-db-net:
    restart: on-failure
    depends_on:
      - kong
      - consul
      - mongo-db

  tg-frontend:
    image: registry.gitlab.com/tugood/ss20_ase_inso_02/tg-frontend:dev
    environment:
      WEBPUSH_VAPID_PUBLIC_KEY: ${LC_NOTIFICATIONSERVICE_VAPID_PUBLIC_KEY}
      TZ: Europe/Vienna
    networks:
      api-management-net:
        ipv4_address: ${TG_FRONTEND_IPV4}
      management-uis:
    restart: on-failure
    depends_on:
      - kong
      - consul
    restart: unless-stopped

  tg-test-service:
    image: registry.gitlab.com/tugood/ss20_ase_inso_02/tg-test-service:dev
    environment:
      PORT: 3000
      LC_USERSERVICE_SECRET: ${LC_KEYCLOAK_USERSERVICE_SECRET}
      CONSUL_HOST: ${CONSUL_DNS_SERVICE_IPV4}
      CONSUL_PORT: 8600
      USER_SERVICE: "http://tg-user-service:8080"
      REQUEST_SERVICE: "http://tg-request-matching-service:8080"
      NOTIFICATION_SERVICE: "http://tg-notification-service:8080"
      TZ: Europe/Vienna
    networks:
      api-management-net:
        ipv4_address: ${TG_TEST_SERVICE_IPV4}
    restart: on-failure
    depends_on:
      - kong
      - consul
