#!/bin/bash

export $(cat .env | sed 's/#.*//g' | xargs)
export VAULT_ADDR=$LC_VAULT_ADDR


echo "Running for droplet: $devDroplet"

cd ..
if [ -f "vault-bin" ]; then
    echo "---------------vault cli already exists---------------"
else
    echo "---------------Installing vault cli---------------"
    mkdir -p tmp
    cd tmp
    wget https://releases.hashicorp.com/vault/1.4.0/vault_1.4.0_linux_amd64.zip
    unzip vault_1.4.0_linux_amd64.zip
    chmod +x vault
    mv vault ../vault-bin
    cd ..
    rm -rf tmp
fi
echo "---------------Logging into vault---------------"
./vault-bin login token=$VAULT_TOKEN
echo "---------------Getting secrets from the vault $VAULT_ADDR---------------"
export LC_KEYCLOAK_KONG_SECRET="$(./vault-bin kv get -field=client_secret secret/keycloak/kong)"
export LC_KEYCLOAK_USERSERVICE_SECRET="$(./vault-bin kv get -field=client_secret secret/keycloak/admin-userservice)"
export LC_USERSERVICE_SECRET="$(./vault-bin kv get -field=client_secret secret/keycloak/admin-userservice)"
export LC_KEYCLOAK_DB_USERNAME="$(./vault-bin kv get -field=username secret/keycloak/db)"
export LC_KEYCLOAK_DB_PASSWORD="$(./vault-bin kv get -field=password secret/keycloak/db)"
export LC_KEYCLOAK_USERNAME="$(./vault-bin kv get -field=username secret/keycloak/admin)"
export LC_KEYCLOAK_PASSWORD="$(./vault-bin kv get -field=password secret/keycloak/admin)"
export LC_USERSERVICE_MONGO_USERNAME="$(./vault-bin kv get -field=username secret/microservice/tg-user-service/mongo)"
export LC_USERSERVICE_MONGO_PASSWORD="$(./vault-bin kv get -field=password secret/microservice/tg-user-service/mongo)"
export LC_NOTIFICATIONSERVICE_MONGO_USERNAME="$(./vault-bin kv get -field=username secret/microservice/tg-notification-service/mongo)"
export LC_NOTIFICATIONSERVICE_MONGO_PASSWORD="$(./vault-bin kv get -field=password secret/microservice/tg-notification-service/mongo)"
export LC_NOTIFICATIONSERVICE_VAPID_PUBLIC_KEY="$(./vault-bin kv get -field=public_key /secret/microservice/tg-notification-service/vapid)"
export LC_NOTIFICATIONSERVICE_VAPID_PRIVATE_KEY="$(./vault-bin kv get -field=private_key secret/microservice/tg-notification-service/vapid)"
export LC_KONG_PG_USERNAME="$(./vault-bin kv get -field=username secret/kong/pg)"
export LC_KONG_PG_PASSWORD="$(./vault-bin kv get -field=password secret/kong/pg)"
echo "---------------Disabling ufw firewall---------------"
ssh root@$devDroplet 'ufw disable'
echo "---------------Creating gitlab-ci user---------------"
ssh root@$devDroplet 'useradd -m gitlab-ci'
echo "---------------Creating dev folder---------------"
ssh root@$devDroplet 'mkdir -p /home/gitlab-ci/dev'
echo "---------------Adding gitlab-ci user to docker group---------------"
ssh root@$devDroplet 'groupadd docker'
ssh root@$devDroplet 'usermod -aG docker gitlab-ci'
echo "---------------Adding gitlab-ci ssh key---------------"
ssh root@$devDroplet 'mkdir -p /home/gitlab-ci/.ssh'
export LC_SSH_PUBLIC_KEY="ssh-rsa $LC_SSH_PUBLIC_KEY"
ssh -o SendEnv=LC_SSH_PUBLIC_KEY root@$devDroplet 'echo $LC_SSH_PUBLIC_KEY >> /home/gitlab-ci/.ssh/authorized_keys'
ssh root@$devDroplet 'chown -R gitlab-ci:gitlab-ci /home/gitlab-ci/.ssh'
ssh root@$devDroplet 'chmod 700 /home/gitlab-ci/.ssh'
ssh root@$devDroplet 'chmod 600 /home/gitlab-ci/.ssh/authorized_keys'
echo "---------------Updating config files---------------"
rsync -avuz docker-compose-config root@$devDroplet:/home/gitlab-ci/dev
echo "---------------Injecting secret variable---------------"
ssh -o SendEnv=LC_KEYCLOAK_KONG_SECRET root@$devDroplet 'sed -i "s/LC_KEYCLOAK_KONG_SECRET/$LC_KEYCLOAK_KONG_SECRET/g" /home/gitlab-ci/dev/docker-compose-config/kong/kong.yml'
echo "---------------Updating docker-compose file---------------"
rsync -avuz docker-compose.yml root@$devDroplet:/home/gitlab-ci/dev
echo "---------------Updating .env file---------------"
rsync -avuz .env root@$devDroplet:/home/gitlab-ci/dev
echo "---------------Updating dev permissions---------------"
ssh root@$devDroplet 'chown -R gitlab-ci:gitlab-ci /home/gitlab-ci/dev'
echo "---------------Running docker-compose pull---------------"
ssh -o SendEnv=LC_* root@$devDroplet 'cd /home/gitlab-ci/dev && docker-compose pull'
echo "---------------Running docker-compose up---------------"
ssh -o SendEnv=LC_* root@$devDroplet 'cd /home/gitlab-ci/dev && docker-compose up -d'
echo "---------------Getting pritunl password---------------"
ssh root@$devDroplet 'docker exec ase_tugood_pritunl-zero_1 pritunl-zero default-password'
