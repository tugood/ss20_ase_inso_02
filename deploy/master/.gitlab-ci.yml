stages:
  - update
  - test
  - release
  - deploy

.default:master:deploy:
  image: instrumentisto/rsync-ssh:latest
  before_script:
    # Start agent
    - eval $(ssh-agent -s)
    # Inject the remote's private key
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    # Append keyscan output into known hosts
    - ssh-keyscan $SERVER_MASTER_HOSTNAME >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    # Vault Secrets
    - wget https://releases.hashicorp.com/vault/1.4.0/vault_1.4.0_linux_amd64.zip
    - unzip vault_1.4.0_linux_amd64.zip
    - chmod +x vault
    - mv vault /usr/bin/
    - export VAULT_ADDR=https://vault.dev.tugood.team
    - export VAULT_TOKEN="$(vault write -field=token auth/jwt/login role=tugood-ci jwt=$CI_JOB_JWT)"
    - export LC_KEYCLOAK_KONG_SECRET="$(vault kv get -field=client_secret secret/keycloak/kong)"
    - export LC_KEYCLOAK_USERSERVICE_SECRET="$(vault kv get -field=client_secret secret/keycloak/admin-userservice)"
    - export LC_USERSERVICE_SECRET="$(vault kv get -field=client_secret secret/keycloak/admin-userservice)"
    - export LC_KEYCLOAK_DB_USERNAME="$(vault kv get -field=username secret/keycloak/db)"
    - export LC_KEYCLOAK_DB_PASSWORD="$(vault kv get -field=password secret/keycloak/db)"
    - export LC_KEYCLOAK_USERNAME="$(vault kv get -field=username secret/keycloak/admin)"
    - export LC_KEYCLOAK_PASSWORD="$(vault kv get -field=password secret/keycloak/admin)"
    - export LC_MONGO_USERNAME="$(vault kv get -field=username secret/microservice/mongo)"
    - export LC_MONGO_PASSWORD="$(vault kv get -field=password secret/microservice/mongo)"
    - export LC_NOTIFICATIONSERVICE_VAPID_PUBLIC_KEY="$(vault kv get -field=public_key /secret/microservice/tg-notification-service/vapid)"
    - export LC_NOTIFICATIONSERVICE_VAPID_PRIVATE_KEY="$(vault kv get -field=private_key secret/microservice/tg-notification-service/vapid)"
    - export LC_KONG_PG_USERNAME="$(vault kv get -field=username secret/kong/pg)"
    - export LC_KONG_PG_PASSWORD="$(vault kv get -field=password secret/kong/pg)"
    - export LC_TG_EMAIL_USERNAME="$(vault kv get -field=username secret/credentials/tugood-gmail)"
    - export LC_TG_EMAIL_PASSWORD="$(vault kv get -field=password secret/credentials/tugood-gmail)"
  only:
    refs:
      - main

update:master:compose:
  stage: update
  extends: .default:master:deploy
  only:
    changes:
      - deploy/master/docker-compose.yml
  script:
    # copy the docker-compose to the droplet
    - rsync -avuz deploy/master/docker-compose.yml $SERVER_USER@$SERVER_MASTER_HOSTNAME:~/

update:master:env:
  stage: update
  extends: .default:master:deploy
  only:
    changes:
      - deploy/master/.env
  script:
    # copy the deploy folder to the droplet
    - rsync -avuz deploy/master/.env $SERVER_USER@$SERVER_MASTER_HOSTNAME:~/

update:master:config:
  stage: update
  extends: .default:master:deploy
  script:
    # copy the deploy folder to the droplet
    - rsync -avuz deploy/master/docker-compose-config $SERVER_USER@$SERVER_MASTER_HOSTNAME:~/
    # injecting the client secret from the vault
    - ssh -o SendEnv="LC_KEYCLOAK_KONG_SECRET" $SERVER_USER@$SERVER_MASTER_HOSTNAME 'sed -i "s/LC_KEYCLOAK_KONG_SECRET/$LC_KEYCLOAK_KONG_SECRET/g" docker-compose-config/kong/kong.yml'

deploy:master:
  stage: deploy
  extends: .default:master:deploy
  retry: 2
  script:
    # login to the gitlab container registry
    # pull the latest image from the container registry and then spin up the containers
    # prune docker so that old images don't slowly fill up the hdd
    - ssh -o SendEnv=LC_* $SERVER_USER@$SERVER_MASTER_HOSTNAME "docker login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY} && docker-compose pull --ignore-pull-failures && docker-compose up -d && docker image prune -f"
