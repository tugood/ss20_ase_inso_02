#!/bin/bash

echo "---------------Getting secrets from the vault---------------"
export VAULT_ADDR=https://vault.dev.tugood.team
export LC_KEYCLOAK_KONG_SECRET="$(vault kv get -field=client_secret secret/keycloak/kong)"
export LC_KEYCLOAK_USERSERVICE_SECRET="$(vault kv get -field=client_secret secret/keycloak/admin-userservice)"
export LC_USERSERVICE_SECRET="$(vault kv get -field=client_secret secret/keycloak/admin-userservice)"
export LC_KEYCLOAK_DB_USERNAME="$(vault kv get -field=username secret/keycloak/db)"
export LC_KEYCLOAK_DB_PASSWORD="$(vault kv get -field=password secret/keycloak/db)"
export LC_KEYCLOAK_USERNAME="$(vault kv get -field=username secret/keycloak/admin)"
export LC_KEYCLOAK_PASSWORD="$(vault kv get -field=password secret/keycloak/admin)"
export LC_MONGO_USERNAME="$(vault kv get -field=username secret/microservice/mongo)"
export LC_MONGO_PASSWORD="$(vault kv get -field=password secret/microservice/mongo)"
export LC_NOTIFICATIONSERVICE_VAPID_PUBLIC_KEY="$(vault kv get -field=public_key /secret/microservice/tg-notification-service/vapid)"
export LC_NOTIFICATIONSERVICE_VAPID_PRIVATE_KEY="$(vault kv get -field=private_key secret/microservice/tg-notification-service/vapid)"
export LC_KONG_PG_USERNAME="$(vault kv get -field=username secret/kong/pg)"
export LC_KONG_PG_PASSWORD="$(vault kv get -field=password secret/kong/pg)"
echo "---------------Updating config files---------------"
rsync -avuz docker-compose-config root@167.172.162.126:/home/gitlab-ci
echo "---------------Injecting secret variable---------------"
ssh -o SendEnv=LC_KEYCLOAK_KONG_SECRET root@167.172.162.126 'sed -i "s/LC_KEYCLOAK_KONG_SECRET/$LC_KEYCLOAK_KONG_SECRET/g" /home/gitlab-ci/docker-compose-config/kong/kong.yml'
echo "---------------Updating docker-compose file---------------"
rsync -avuz docker-compose.yml root@167.172.162.126:/home/gitlab-ci
echo "---------------Updating .env file---------------"
rsync -avuz .env root@64.225.96.119:/home/gitlab-ci
echo "---------------Running docker-compose pull---------------"
ssh -o SendEnv=LC_* root@167.172.162.126 'cd /home/gitlab-ci && docker-compose up -d'
echo "---------------Running docker-compose up---------------"
ssh -o SendEnv=LC_* root@167.172.162.126 'cd /home/gitlab-ci && docker-compose up -d'
