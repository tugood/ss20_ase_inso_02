# Must be in the root directory since it needs the same env file as the docker-compose

FROM ${KONG_DOCKER_TAG:-kong:latest}

LABEL description="Kong_OIDC"

USER root
RUN apk update && apk add git unzip luarocks
RUN luarocks install kong-oidc

USER ${KONG_USER:-kong}
