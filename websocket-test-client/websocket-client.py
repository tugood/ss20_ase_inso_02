import asyncio
import websockets
import os
import threading


server = os.getenv("BASE_URL")
if server is None:
    server = 'localhost:8087'

thread_size = os.getenv("THREADS")
if thread_size is None:
    thread_size = 5000
else: 
    thread_size = int(thread_size)

is_secure = os.getenv("SECURE")
if is_secure is None: 
    is_secure = "True"

is_secure = is_secure == 'True'
if is_secure: 
    is_secure = "wss"
else:
    is_secure = "ws"

protocol = f"{is_secure}://"

url = protocol + server + '/socket-notifications/test-notifications-emitter'


class W_S_Client_Worker (threading.Thread):
    thread_nr = 0

    def __init__(self, thread_nr):
        threading.Thread.__init__(self)
        self.thread_nr = thread_nr

    async def testSocket(self, uri):
        async with websockets.connect(uri) as websocket:
            while(True):
                await websocket.send("Hello world!")
                a = await websocket.recv()
                print(f"thread {self.thread_nr} reports {a}")

    def run(self):
        asyncio.new_event_loop().run_until_complete(self.testSocket(url))


if __name__ == "__main__":
    print(f"connect to url {url}")
    for x in range(0, thread_size):
        W_S_Client_Worker(x).start()