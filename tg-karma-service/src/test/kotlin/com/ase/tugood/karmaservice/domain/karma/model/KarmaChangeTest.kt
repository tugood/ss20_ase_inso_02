package com.ase.tugood.karmaservice.domain.karma.model

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import java.time.Instant
import java.util.*

@SpringBootTest
class KarmaChangeTest {
    @Test
    fun `create object`() {
        val karmaChange = KarmaChange(
            id = UUID.randomUUID().toString(),
            type = KarmaChangeType.INTERNAL_FINISH_REQUEST,
            userId = "TEst id",
            timestamp = Instant.now(),
            value = 100,
            thirdPartyClient = "clientId"
        )

        Assertions.assertNotNull(karmaChange)
    }

    @Test
    fun `edit object`() {
        val karmaChange = KarmaChange(
            id = UUID.randomUUID().toString(),
            type = KarmaChangeType.INTERNAL_FINISH_REQUEST,
            userId = "TEst id",
            timestamp = Instant.now(),
            value = 100,
            thirdPartyClient = "clientId"
        )

        karmaChange.id = UUID.randomUUID().toString()
        karmaChange.timestamp = Instant.now()
        karmaChange.type = KarmaChangeType.INTERNAL_CANCEL_REQUEST
        karmaChange.userId = "Another id"
        karmaChange.value = 500
    }
}
