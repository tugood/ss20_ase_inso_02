package com.ase.tugood.karmaservice.infrastructure.web.model.request

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class UserResourceTest {

    private lateinit var userInfo: UserInfo

    @BeforeEach
    fun initTest() {
    }

    @Test
    fun `test user info object class`() {
        userInfo = UserInfo(
            id = "Herbert",
            username = "Herbert",
            email = "herbert@email.com",
            email_verified = true,
            family_name = "Herbert",
            given_name = "Herbert",
            locale = "en",
            name = "herbert",
            preferred_username = "herbert",
            sub = "herbert"
        )

        Assertions.assertNotNull(userInfo)
        Assertions.assertEquals(userInfo.id, "Herbert")
        Assertions.assertEquals(userInfo.username, "Herbert")
        Assertions.assertEquals(userInfo.email, "herbert@email.com")
        Assertions.assertEquals(userInfo.email_verified, true)
        Assertions.assertEquals(userInfo.family_name, "Herbert")
        Assertions.assertEquals(userInfo.given_name, "Herbert")
        Assertions.assertEquals(userInfo.locale, "en")
        Assertions.assertEquals(userInfo.name, "herbert")
        Assertions.assertEquals(userInfo.preferred_username, "herbert")
        Assertions.assertEquals(userInfo.sub, "herbert")
    }

    @Test
    fun `test user info default constructor`() {
        userInfo = UserInfo(
            id = "Herbert",
            email_verified = true
        )

        Assertions.assertNotNull(userInfo)
        Assertions.assertNotNull(userInfo.id)
        Assertions.assertNotNull(userInfo.email_verified)
    }


}
