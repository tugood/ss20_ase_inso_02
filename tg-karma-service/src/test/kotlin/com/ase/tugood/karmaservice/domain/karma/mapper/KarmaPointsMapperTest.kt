package com.ase.tugood.karmaservice.domain.karma.mapper

import com.ase.tugood.karmaservice.infrastructure.web.model.request.InternalKarmaChangeType
import com.ase.tugood.karmaservice.infrastructure.web.model.request.InternalKarmaRequestDTO
import com.ase.tugood.karmaservice.infrastructure.web.model.request.RatingDTO
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class KarmaPointsMapperTest {

    private val mapper: KarmaPointsMapper = KarmaPointsMapper()

    private lateinit var internalKarmaRequestDTO: InternalKarmaRequestDTO


    @Test
    internal fun cancel() {

        internalKarmaRequestDTO = InternalKarmaRequestDTO("any", InternalKarmaChangeType.CANCEL_REQUEST)

        assertThat(mapper.scoreCancelRequest(internalKarmaRequestDTO)).isEqualTo(-150)
    }

    @Test
    internal fun finish() {

        internalKarmaRequestDTO = InternalKarmaRequestDTO("any", InternalKarmaChangeType.FINISH_REQUEST)

        assertThat(mapper.scoreFinishRequest(internalKarmaRequestDTO)).isEqualTo(50)
    }


    @Test
    internal fun finishWithGoodRating() {

        internalKarmaRequestDTO = InternalKarmaRequestDTO("any", InternalKarmaChangeType.FINISH_REQUEST, ratingDTO = RatingDTO(rating = 4))

        assertThat(mapper.scoreFinishRequest(internalKarmaRequestDTO)).isEqualTo(100)
    }


    @Test
    internal fun finishWithExcellentRating() {

        internalKarmaRequestDTO = InternalKarmaRequestDTO("any", InternalKarmaChangeType.FINISH_REQUEST, ratingDTO = RatingDTO(rating = 5))

        assertThat(mapper.scoreFinishRequest(internalKarmaRequestDTO)).isEqualTo(150)
    }

}

