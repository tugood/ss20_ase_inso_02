package com.ase.tugood.karmaservice

import com.ase.tugood.karmaservice.infrastructure.web.model.request.UserInfo
import java.util.*

class TestUserInfoBuilder {
    companion object {
        val defaultId: String = UUID.randomUUID().toString()
        const val defaultFirstName = "Karl"
        const val defaultLastName = "Ludwig"
        const val defaultEmail = "karl@ludwig.at"
        const val defaultLangKey = "en"
        val updatedId: String = UUID.randomUUID().toString()
        const val updatedFirstName = "Andi"
        const val updatedLastName = "Admin"
        const val updatedEmail = "admin@admin.at"
        const val updatedLangKey = "en"
    }

    fun validDefaultUserInfo() = UserInfo("angular", defaultId, defaultLangKey, "$defaultFirstName $defaultLastName", defaultEmail, defaultEmail, defaultFirstName, defaultId, defaultEmail, true, defaultLastName)
    fun validUpdatedUserInfo() = UserInfo("angular", updatedId, updatedLangKey, "$updatedFirstName $updatedLastName", updatedEmail, updatedEmail, updatedFirstName, updatedId, updatedEmail, true, updatedLastName)
}
