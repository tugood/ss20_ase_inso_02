package com.ase.tugood.karmaservice.infrastructure.web.rest.controller

import com.ase.tugood.karmaservice.application.NotificationService
import com.ase.tugood.karmaservice.application.data.KarmaChangeDTO
import com.ase.tugood.karmaservice.domain.karma.model.KarmaChangeType
import com.ase.tugood.karmaservice.infrastructure.web.model.request.InternalKarmaChangeType
import com.ase.tugood.karmaservice.infrastructure.web.model.request.InternalKarmaRequestDTO
import com.ase.tugood.karmaservice.infrastructure.web.model.request.RatingDTO
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.verifyAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Mono

@AutoConfigureWebTestClient
@SpringBootTest
internal class InternalKarmaControllerTest {


    @Autowired
    private lateinit var webTestClient: WebTestClient

    private lateinit var cancelKarmaRequestDTO: InternalKarmaRequestDTO
    private lateinit var finishKarmaRequestDTO: InternalKarmaRequestDTO

    @MockkBean
    private lateinit var notificationService: NotificationService

    @BeforeEach
    internal fun setUp() {

        cancelKarmaRequestDTO = InternalKarmaRequestDTO(userId = "userId", type = InternalKarmaChangeType.CANCEL_REQUEST)
        finishKarmaRequestDTO = InternalKarmaRequestDTO(userId = "userId", type = InternalKarmaChangeType.FINISH_REQUEST, helperId = "helperId", ratingDTO = RatingDTO(rating = 4, comment = "1"))

        every {
            notificationService.notify("userId", any(), any(), any())
        } answers {
            Mono.empty()
        }

        every {
            notificationService.notify("helperId", any(), any(), any())
        } answers {
            Mono.empty()
        }
    }

    @Test
    internal fun `POST Cancel karma change`() {

        webTestClient.post().uri("/internal/karma")
            .body(Mono.just(cancelKarmaRequestDTO), KarmaChangeDTO::class.java)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("[0].value").isEqualTo(-150)
            .jsonPath("[0].id").exists()
            .jsonPath("[0].type").isEqualTo(KarmaChangeType.INTERNAL_CANCEL_REQUEST.toString())

        verifyAll {
            notificationService.notify("userId", any(), any(), any())
        }
    }

    @Test
    internal fun `POST Finish karma change`() {

        webTestClient.post().uri("/internal/karma")
            .body(Mono.just(finishKarmaRequestDTO), KarmaChangeDTO::class.java)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("[0].id").exists()
            .jsonPath("[0].value").isEqualTo(30)
            .jsonPath("[0].type").isEqualTo(KarmaChangeType.INTERNAL_COMPLETE_RATING.toString())
            .jsonPath("[1].id").exists()
            .jsonPath("[1].value").isEqualTo(100)
            .jsonPath("[1].type").isEqualTo(KarmaChangeType.INTERNAL_FINISH_REQUEST.toString())


        verifyAll {
            notificationService.notify("userId", any(), any(), any())
            notificationService.notify("helperId", any(), any(), any())
        }

    }
}
