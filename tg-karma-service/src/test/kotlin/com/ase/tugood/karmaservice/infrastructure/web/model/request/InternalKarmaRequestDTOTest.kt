package com.ase.tugood.karmaservice.infrastructure.web.model.request

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import java.util.*

@SpringBootTest
class InternalKarmaRequestDTOTest {

    @BeforeEach
    fun setup() {
    }

    @Test
    fun `test default constructor`() {
        val internalKarmaRequestDTO = InternalKarmaRequestDTO(
            UUID.randomUUID().toString(),
            InternalKarmaChangeType.FINISH_REQUEST
        )

        Assertions.assertNotNull(internalKarmaRequestDTO)
        Assertions.assertNotNull(internalKarmaRequestDTO.type)
        Assertions.assertNotNull(internalKarmaRequestDTO.userId)
        Assertions.assertNull(internalKarmaRequestDTO.helperId)
        Assertions.assertNull(internalKarmaRequestDTO.ratingDTO)
    }

    @Test
    fun `test full constructor`() {
        val internalKarmaRequestDTO = InternalKarmaRequestDTO(
            UUID.randomUUID().toString(),
            InternalKarmaChangeType.FINISH_REQUEST,
            helperId = "helperId",
            ratingDTO = RatingDTO(rating = 5, comment = "100")
        )

        Assertions.assertNotNull(internalKarmaRequestDTO)
        Assertions.assertNotNull(internalKarmaRequestDTO.type)
        Assertions.assertNotNull(internalKarmaRequestDTO.userId)
        Assertions.assertNotNull(internalKarmaRequestDTO.helperId)
        Assertions.assertNotNull(internalKarmaRequestDTO.ratingDTO)
    }
}
