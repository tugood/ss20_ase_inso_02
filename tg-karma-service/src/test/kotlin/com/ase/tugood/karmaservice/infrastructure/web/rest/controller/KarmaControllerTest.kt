package com.ase.tugood.karmaservice.infrastructure.web.rest.controller

import com.ase.tugood.karmaservice.TestKarmaChangeBuilder
import com.ase.tugood.karmaservice.TestUserInfoBuilder
import com.ase.tugood.karmaservice.application.NotificationService
import com.ase.tugood.karmaservice.domain.karma.mapper.KarmaMapper
import com.ase.tugood.karmaservice.domain.karma.mapper.KarmaPointsMapper
import com.ase.tugood.karmaservice.infrastructure.persistence.KarmaChangeRepository
import com.ase.tugood.karmaservice.infrastructure.web.model.request.ThirdPartyKarmaRequestDTO
import com.ase.tugood.karmaservice.infrastructure.web.model.request.UserInfo
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.ninjasquad.springmockk.MockkBean
import com.ninjasquad.springmockk.SpykBean
import io.mockk.every
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Mono
import java.util.*

@AutoConfigureWebTestClient
@SpringBootTest
internal class KarmaControllerTest {

    @Autowired
    private lateinit var webTestClient: WebTestClient

    private lateinit var userInfo: UserInfo
    private lateinit var karmaMapper: KarmaMapper

    @SpykBean
    private lateinit var karmaChangeRepository: KarmaChangeRepository

    @MockkBean
    private lateinit var notificationService: NotificationService

    @BeforeEach
    fun initTest() {
        userInfo = TestUserInfoBuilder().validDefaultUserInfo()

        karmaMapper = KarmaMapper(karmaPointsMapper = KarmaPointsMapper())

        karmaChangeRepository.deleteAll().block()

        every {
            notificationService.notify(userInfo.id, any(), any(), any())
        } answers {
            Mono.empty()
        }
    }


    @Test
    fun `GET karma by userinfo`() {
        karmaChangeRepository.insert(TestKarmaChangeBuilder().validDefaultKarmaChange(userInfo.id))
            .then(karmaChangeRepository.insert(TestKarmaChangeBuilder().validUpdatedKarmaChange(userInfo.id)))
            .then(karmaChangeRepository.insert(TestKarmaChangeBuilder().validDefaultKarmaChange(userInfo.id)))
            .block()

        val expectedSum = TestKarmaChangeBuilder().defaultValue * 2 + TestKarmaChangeBuilder().updatedValue

        webTestClient.get().uri("/api/karma")
            .header("X-Userinfo", Base64.getEncoder().encodeToString(jacksonObjectMapper().writeValueAsBytes(userInfo)))
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("karmaPoints").isEqualTo(expectedSum)
    }

    @Test
    fun `third party karma transaction`() {
        karmaChangeRepository.insert(TestKarmaChangeBuilder().validDefaultKarmaChange(userInfo.id))
            .then(karmaChangeRepository.insert(TestKarmaChangeBuilder().validDefaultKarmaChange(userInfo.id)))
            .block()

        val transaction = ThirdPartyKarmaRequestDTO(
            requestId = UUID.randomUUID(),
            value = TestKarmaChangeBuilder().defaultValue
        )

        webTestClient.post().uri("/api/karma")
            .header("X-Userinfo", Base64.getEncoder().encodeToString(jacksonObjectMapper().writeValueAsBytes(userInfo)))
            .header("azp", "clientId")
            .body(Mono.just(transaction), ThirdPartyKarmaRequestDTO::class.java)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("karmaPoints").isEqualTo(TestKarmaChangeBuilder().defaultValue)
    }

    @Test
    fun `third party karma transaction should fail`() {
        val transaction = ThirdPartyKarmaRequestDTO(
            requestId = UUID.randomUUID(),
            value = 100
        )

        webTestClient.post().uri("/api/karma")
            .header("X-Userinfo", Base64.getEncoder().encodeToString(jacksonObjectMapper().writeValueAsBytes(userInfo)))
            .body(Mono.just(transaction), ThirdPartyKarmaRequestDTO::class.java)
            .exchange()
            .expectStatus().is4xxClientError
    }

    @Test
    fun `transaction with same id should fail`() {
        var result = karmaChangeRepository.insert(TestKarmaChangeBuilder().validDefaultKarmaChange(userInfo.id)).block()

        val transaction = ThirdPartyKarmaRequestDTO(
            requestId = UUID.fromString(result!!.id),
            value = 99
        )

        webTestClient.post().uri("/api/karma")
            .header("X-Userinfo", Base64.getEncoder().encodeToString(jacksonObjectMapper().writeValueAsBytes(userInfo)))
            .body(Mono.just(transaction), ThirdPartyKarmaRequestDTO::class.java)
            .exchange()
            .expectStatus().is4xxClientError
    }
}
