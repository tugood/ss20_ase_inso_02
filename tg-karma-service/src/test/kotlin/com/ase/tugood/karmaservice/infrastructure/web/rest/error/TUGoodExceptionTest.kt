package com.ase.tugood.karmaservice.infrastructure.web.rest.error

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus

@SpringBootTest
class TUGoodExceptionTest {
    @Test
    fun `create exception`() {
        val tuGoodException = TUGoodException(
            message = "Test message",
            type = "Test type",
            status = HttpStatus.OK,
            cause = TUGoodException(
                message = "Test message",
                type = "Test type",
                status = HttpStatus.OK,
                cause = Throwable()
            )
        )

        Assertions.assertNotNull(tuGoodException)
        Assertions.assertNotNull(tuGoodException.error)
        Assertions.assertNotNull(tuGoodException.status)
    }

    @Test
    fun `create exception with empty cause`() {
        val tuGoodException = TUGoodException(
            message = "Test message",
            type = "Test type",
            status = HttpStatus.OK,
            cause = null
        )

        Assertions.assertNotNull(tuGoodException)
        Assertions.assertNotNull(tuGoodException.error)
        Assertions.assertNotNull(tuGoodException.status)
    }
}
