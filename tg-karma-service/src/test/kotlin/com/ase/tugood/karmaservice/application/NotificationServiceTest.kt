package com.ase.tugood.karmaservice.application


import com.ase.tugood.karmaservice.TestUserInfoBuilder
import com.ase.tugood.karmaservice.infrastructure.web.client.NotificationServiceClient
import com.ase.tugood.karmaservice.infrastructure.web.model.request.UserInfo
import com.ase.tugood.notificationservice.application.data.user.UserDTO
import com.ase.tugood.requestservice.application.data.notification.ServiceNotificationDTO
import com.ase.tugood.requestservice.configuration.WireMockInitializer
import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock.*
import com.google.gson.Gson
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.slot
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.cloud.client.discovery.DiscoveryClient
import org.springframework.context.ApplicationContext
import org.springframework.context.MessageSource
import org.springframework.core.env.get
import org.springframework.test.context.ContextConfiguration
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.net.URI
import java.util.*


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(MockKExtension::class)
@ContextConfiguration(initializers = [WireMockInitializer::class])
internal class NotificationServiceTest {

    @Autowired
    private lateinit var context: ApplicationContext

    private lateinit var notificationService: NotificationService

    @Autowired
    private lateinit var notificationServiceClient: NotificationServiceClient

    @MockK
    private lateinit var mockUserService: UserService

    @MockK
    private lateinit var mockMessageSource: MessageSource

    @MockkBean
    private lateinit var discoveryClient: DiscoveryClient

    private lateinit var serviceNotificationDTO: ServiceNotificationDTO

    private lateinit var userInfo: UserInfo

    private lateinit var userDTO: UserDTO

    /**
     * @see com.ase.tugood.requestservice.configuration.WireMockInitializer#initialize()
     */
    @Autowired
    @Qualifier("wireMockServer")
    private lateinit var wireMockServer: WireMockServer

    @BeforeEach
    fun setUp() {
        notificationService = NotificationService(userService = mockUserService, notificationServiceClient = notificationServiceClient, messageSource = mockMessageSource)
        userInfo = TestUserInfoBuilder().validDefaultUserInfo()
        userDTO = UserDTO(id = userInfo.id)
        serviceNotificationDTO = ServiceNotificationDTO(userId = userInfo.id, notificationType = ServiceNotificationDTO.NotificationType.PUSH)


        val serviceIdSlot = slot<String>()
        every { discoveryClient.getInstances(capture(serviceIdSlot)) } answers {
            val serviceURIString = context.environment[serviceIdSlot.captured]

            listOf(mockk() {
                every { serviceId } returns serviceIdSlot.captured
                every { uri } returns if (serviceURIString != null) URI.create(serviceURIString) else null
            })
        }
    }

    @AfterEach
    fun afterEach() {
        wireMockServer.resetAll()
    }

    @Test
    fun `test notify user should query user service and then send push notification`() {

        val newServiceNotificationDTO = ServiceNotificationDTO().apply {
            this.userId = userDTO.id
            this.userEmail = userDTO.email
            this.notificationType = ServiceNotificationDTO.NotificationType.PUSH
            this.title = serviceNotificationDTO.title
            this.body = serviceNotificationDTO.body
        }

        // just return something with ok
        this.wireMockServer.stubFor(post(urlPathEqualTo("/internal/notifications/notify"))
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBody("\"testing-library\": \"WireMock\"")))

        every {
            mockUserService.getUserForId(any())
        } answers {
            Mono.just(userDTO.apply {
                langKey = Locale.ENGLISH.toLanguageTag()
            })
        }

        val titleSource = "notification.does.not.matter.title"
        val bodySource = "notification.does.not.matter.body"
        every { mockMessageSource.getMessage(titleSource, any(), any()) } answers { newServiceNotificationDTO.title }
        every { mockMessageSource.getMessage(bodySource, any(), any()) } answers { newServiceNotificationDTO.body }

        val notify = notificationService.notify(userDTO.id,
            Pair(titleSource, null),
            Pair(bodySource, null),
            ServiceNotificationDTO.NotificationType.PUSH)

        StepVerifier
            .create<Void>(notify)
            .expectNext()
            .expectComplete()
            .verify()

        val json = Gson().toJson(newServiceNotificationDTO)

        this.wireMockServer.verify(postRequestedFor(urlEqualTo("/internal/notifications/notify"))
            .withHeader("Content-Type", equalTo("application/json"))
            .withRequestBody(equalToJson(json, true, true)))
    }

    @Test
    fun `test notify user with message parameters should query user service and then send push notification with replaced strings`() {

        val newServiceNotificationDTO = ServiceNotificationDTO().apply {
            this.userId = userDTO.id
            this.userEmail = userDTO.email
            this.notificationType = ServiceNotificationDTO.NotificationType.PUSH
            this.title = "Nobody expects the spanish inquisition"
            this.body = "Tis but a scratch. King Arthur: A scratch? Your arm's off!"
        }

        // just return something with ok
        this.wireMockServer.stubFor(post(urlPathEqualTo("/internal/notifications/notify"))
            .willReturn(aResponse()
                .withStatus(200)
                .withHeader("Content-Type", "application/json")
                .withBody("\"testing-library\": \"WireMock\"")))

        every {
            mockUserService.getUserForId(any())
        } answers {
            Mono.just(userDTO.apply {
                langKey = Locale.ENGLISH.toLanguageTag()
            })
        }

        val titleToReplace = "Nobody expects the %s"
        val bodyToReplace = "Tis but a scratch. %s %s"
        val titleSource = "notification.does.not.matter.title"
        val bodySource = "notification.does.not.matter.body"
        every { mockMessageSource.getMessage(titleSource, any(), any()) } answers { titleToReplace }
        every { mockMessageSource.getMessage(bodySource, any(), any()) } answers { bodyToReplace }

        val notify = notificationService.notify(userDTO.id,
            Pair(titleSource, arrayOf("spanish inquisition")),
            Pair(bodySource, arrayOf("King Arthur:", "A scratch? Your arm's off!")),
            ServiceNotificationDTO.NotificationType.PUSH)

        StepVerifier
            .create<Void>(notify)
            .expectNext()
            .expectComplete()
            .verify()

        val json = Gson().toJson(newServiceNotificationDTO)

        this.wireMockServer.verify(postRequestedFor(urlEqualTo("/internal/notifications/notify"))
            .withHeader("Content-Type", equalTo("application/json"))
            .withRequestBody(equalToJson(json, true, true)))
    }
}
