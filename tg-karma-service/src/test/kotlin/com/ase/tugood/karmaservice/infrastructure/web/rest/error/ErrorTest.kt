package com.ase.tugood.karmaservice.infrastructure.web.rest.error

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class ErrorTest {
    @BeforeEach
    fun setup() {
    }

    @Test
    fun `init default error object`() {
        val error = Error(
            message = "Test error",
            type = "Test type"
        )

        Assertions.assertNotNull(error)
        Assertions.assertNotNull(error.message)
        Assertions.assertNotNull(error.type)
    }

    @Test
    fun `init complete error object`() {
        val error = Error(
            message = "Test error",
            type = "Test type",
            code = 101,
            cause = mutableListOf()
        )

        Assertions.assertNotNull(error)
        Assertions.assertNotNull(error.message)
        Assertions.assertNotNull(error.type)
        Assertions.assertNotNull(error.code)
        Assertions.assertNotNull(error.cause)
    }
}
