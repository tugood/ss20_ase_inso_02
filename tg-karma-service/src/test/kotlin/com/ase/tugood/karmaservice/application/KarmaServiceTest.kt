package com.ase.tugood.karmaservice.application

import com.ase.tugood.karmaservice.TestUserInfoBuilder
import com.ase.tugood.karmaservice.application.data.KarmaChangeDTO
import com.ase.tugood.karmaservice.application.data.KarmaSummaryDTO
import com.ase.tugood.karmaservice.domain.karma.mapper.KarmaMapper
import com.ase.tugood.karmaservice.domain.karma.mapper.KarmaPointsMapper
import com.ase.tugood.karmaservice.domain.karma.model.KarmaChange
import com.ase.tugood.karmaservice.domain.karma.model.KarmaChangeType
import com.ase.tugood.karmaservice.infrastructure.persistence.KarmaChangeRepository
import com.ase.tugood.karmaservice.infrastructure.web.model.request.*
import com.ase.tugood.karmaservice.infrastructure.web.rest.error.TUGoodException
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.verifyAll
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.lang.Thread.sleep
import java.time.Instant
import java.util.*
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@SpringBootTest
internal class KarmaServiceTest {
    @Autowired
    private lateinit var karmaService: KarmaService

    private lateinit var karmaMapper: KarmaMapper

    private lateinit var userInfo: UserInfo
    private lateinit var internalFinishKarmaRequestDTO: InternalKarmaRequestDTO
    private lateinit var internalCancelKarmaRequestDTO: InternalKarmaRequestDTO
    private lateinit var cancelKarmaChangeDTO: KarmaChangeDTO
    private lateinit var finishKarmaChangeDTO: KarmaChangeDTO
    private lateinit var rateKarmaChangeDTO: KarmaChangeDTO

    @Autowired
    private lateinit var karmaChangeRepository: KarmaChangeRepository

    @MockkBean
    private lateinit var notificationService: NotificationService

    @BeforeEach
    fun setup() {
        userInfo = TestUserInfoBuilder().validDefaultUserInfo()
        internalFinishKarmaRequestDTO = InternalKarmaRequestDTO(userId = userInfo.id,
            type = InternalKarmaChangeType.FINISH_REQUEST,
            ratingDTO = RatingDTO(rating = 4, comment = "a comment"),
            helperId = "helperId"
        )
        internalCancelKarmaRequestDTO = InternalKarmaRequestDTO(
            userId = userInfo.id,
            type = InternalKarmaChangeType.CANCEL_REQUEST
        )

        cancelKarmaChangeDTO = KarmaChangeDTO(id = UUID.randomUUID().toString(), timestamp = Instant.now(), value = -150, type = KarmaChangeType.INTERNAL_CANCEL_REQUEST, thirdPartyClient = "clientId")
        finishKarmaChangeDTO = KarmaChangeDTO(id = UUID.randomUUID().toString(), timestamp = Instant.now(), value = +100, type = KarmaChangeType.INTERNAL_FINISH_REQUEST, thirdPartyClient = "clientId")
        rateKarmaChangeDTO = KarmaChangeDTO(id = UUID.randomUUID().toString(), timestamp = Instant.now(), value = +30, type = KarmaChangeType.INTERNAL_COMPLETE_RATING, thirdPartyClient = "clientId")

        karmaMapper = KarmaMapper(karmaPointsMapper = KarmaPointsMapper())

        karmaChangeRepository.deleteAll().block()

        every {
            notificationService.notify(userInfo.id, any(), any(), any())
        } answers {
            Mono.empty()
        }

        every {
            notificationService.notify("helperId", any(), any(), any())
        } answers {
            Mono.empty()
        }
    }

    @Test
    fun `test internal karma changes with finishing task`() {
        val karmaChanges = karmaService.internalKarmaChange(internalFinishKarmaRequestDTO)

        Assertions.assertNotNull(karmaChanges)
        Assertions.assertEquals(2, karmaChanges!!.size)

        sleep(1000)

        val change1 = karmaChangeRepository.findById(karmaChanges[0].id).block()
        val change2 = karmaChangeRepository.findById(karmaChanges[1].id).block()

        Assertions.assertNotNull(change1)
        Assertions.assertNotNull(change2)
        Assertions.assertEquals(rateKarmaChangeDTO.value, change1?.value)
        Assertions.assertEquals(finishKarmaChangeDTO.value, change2?.value)
        Assertions.assertEquals(internalFinishKarmaRequestDTO.userId, change1?.userId)
        Assertions.assertEquals(internalFinishKarmaRequestDTO.helperId, change2?.userId)

        verifyAll {
            notificationService.notify(userInfo.id, any(), any(), any())
            notificationService.notify("helperId", any(), any(), any())
        }
    }

    @Test
    fun `test internal karma changes with cancelling task`() {
        val karmaChanges = karmaService.internalKarmaChange(internalCancelKarmaRequestDTO)

        Assertions.assertNotNull(karmaChanges)
        Assertions.assertEquals(1, karmaChanges!!.size)

        val change = karmaChanges[0]

        Assertions.assertEquals(cancelKarmaChangeDTO.value, change.value)


        verifyAll {
            notificationService.notify(userInfo.id, any(), any(), any())
        }
    }

    @Test
    fun `test third party karma change with notification`() {

        karmaChangeRepository.insert(KarmaChange(id = "111", userId = userInfo.id, value = 200, timestamp = Instant.now(), type = KarmaChangeType.INTERNAL_FINISH_REQUEST, thirdPartyClient = null)).block()

        val karmaChange = karmaService.thirdPartyKarmaChange(ThirdPartyKarmaRequestDTO(100, UUID.randomUUID()), userInfo, "clientId").block()

        Assertions.assertNotNull(karmaChange)

        Assertions.assertEquals(100, karmaChange?.karmaPoints)

        verifyAll {
            notificationService.notify(userInfo.id, any(), any(), any())
        }
    }

    @Test
    fun `test third party karma change with too little points`() {

        assertThrows<TUGoodException> {
            karmaService.thirdPartyKarmaChange(ThirdPartyKarmaRequestDTO(100, UUID.randomUUID()), userInfo, "clientId").block()

        }
    }

    @Test
    fun `test getKarmaSummary`() {
        val res1 = karmaService.internalKarmaChange(internalCancelKarmaRequestDTO)
        val res2 = karmaService.internalKarmaChange(internalFinishKarmaRequestDTO)

        sleep(1000)

        val summaryDto = karmaService.getKarmaSummary(userInfo = userInfo)

        StepVerifier
            .create<KarmaSummaryDTO>(summaryDto)
            .consumeNextWith { dto ->
                assertNotNull(dto)
                assertThat(dto.karmaChangeHistory).hasSize(2)
                assertEquals(cancelKarmaChangeDTO.value + rateKarmaChangeDTO.value, dto.karmaPoints)
            }
            .expectComplete()
            .verify()


        val summaryDtoHelper = karmaService.getKarmaSummary(UserInfo(id = "helperId", email_verified = false))

        StepVerifier
            .create<KarmaSummaryDTO>(summaryDtoHelper)
            .consumeNextWith { dto ->
                assertNotNull(dto)
                assertThat(dto.karmaChangeHistory).hasSize(1)
                assertEquals(finishKarmaChangeDTO.value, dto.karmaPoints)
            }
            .expectComplete()
            .verify()
    }
}
