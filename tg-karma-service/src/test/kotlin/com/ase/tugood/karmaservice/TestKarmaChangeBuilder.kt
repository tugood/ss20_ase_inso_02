package com.ase.tugood.karmaservice

import com.ase.tugood.karmaservice.application.data.KarmaChangeDTO
import com.ase.tugood.karmaservice.domain.karma.model.KarmaChange
import com.ase.tugood.karmaservice.domain.karma.model.KarmaChangeType
import com.ase.tugood.karmaservice.infrastructure.web.model.request.InternalKarmaChangeType
import com.ase.tugood.karmaservice.infrastructure.web.model.request.InternalKarmaRequestDTO
import com.ase.tugood.karmaservice.infrastructure.web.model.request.RatingDTO
import java.time.Instant
import java.util.*

class TestKarmaChangeBuilder {
    val defaultId = UUID.randomUUID().toString()
    val defaultUserId: String = UUID.fromString("850ed933-4c73-425f-8963-6a852b482803").toString()
    val defaultTimestamp: Instant = Instant.parse("2020-05-27T15:37:12.385Z")
    val defaultValue: Int = 200
    val defaultType = KarmaChangeType.INTERNAL_FINISH_REQUEST
    val defaultInternalType = InternalKarmaChangeType.FINISH_REQUEST

    val updatedId = UUID.randomUUID().toString()
    val updatedUserId: String = UUID.fromString("850ed933-4c73-425f-8963-6a852b482803").toString()
    val updatedTimestamp: Instant = Instant.parse("2020-05-27T15:38:12.385Z")
    val updatedValue: Int = -100
    val updatedType = KarmaChangeType.THIRD_PARTY_TRANSACTION
    val updatedInternalType = InternalKarmaChangeType.CANCEL_REQUEST

    fun validDefaultKarmaChange(userId: String) = KarmaChange(defaultId, userId, defaultValue, defaultTimestamp, defaultType, null)
    fun validDefaultKarmaChangeDTO() = KarmaChangeDTO(defaultId, defaultValue, defaultTimestamp, defaultType, null)
    fun validUpdatedKarmaChange(userId: String) = KarmaChange(updatedId, userId, updatedValue, updatedTimestamp, updatedType, "clientId")
    fun validUpdatedKarmaChangeDTO() = KarmaChangeDTO(updatedId, updatedValue, updatedTimestamp, updatedType, "clientId")

    fun validDefaultInternalKarmaRequestDTO() = InternalKarmaRequestDTO(defaultUserId, defaultInternalType, updatedUserId, RatingDTO(rating = 5))
    fun validDefaultInternalKarmaRequestDTO(userId: String) = InternalKarmaRequestDTO(userId, defaultInternalType, defaultUserId)

    fun validUpdatedInternalKarmaRequestDTO() = InternalKarmaRequestDTO(updatedUserId, updatedInternalType)
    fun validUpdatedInternalKarmaRequestDTO(userId: String) = InternalKarmaRequestDTO(userId, updatedInternalType)
}
