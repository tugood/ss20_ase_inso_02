package com.ase.tugood.karmaservice.infrastructure.web.client

import com.ase.tugood.requestservice.application.data.notification.ServiceNotificationDTO
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.ClientResponse
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono


@Component
class NotificationServiceClient {

    val logger: Logger = LoggerFactory.getLogger(javaClass)

    @Autowired
    private val webClientBuilder: WebClient.Builder? = null

    @Autowired
    private val discoveryClientController: DiscoveryClientController? = null

    fun notify(serviceNotificationDTO: ServiceNotificationDTO): Mono<String> {
        logger.info("notify user: id={}", serviceNotificationDTO.userId)

        if (discoveryClientController == null) {
            throw RuntimeException("Cannot initialize notification service client")
        }

        return discoveryClientController
            .notificationService()
            .flatMap { url ->
                webClientBuilder!!.build()
                    .post().uri { builder ->
                        url.resolve(builder.path("/internal/notifications/notify").build())
                    }
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(BodyInserters.fromValue(serviceNotificationDTO))
                    .exchange()
            }
            .flatMap { t: ClientResponse? ->
                t?.bodyToMono(String::class.java)
            }
            .switchIfEmpty(Mono.empty())
    }

}
