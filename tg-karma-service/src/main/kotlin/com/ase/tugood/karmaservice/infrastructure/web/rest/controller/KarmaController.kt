package com.ase.tugood.karmaservice.infrastructure.web.rest.controller

import com.ase.tugood.karmaservice.application.KarmaService
import com.ase.tugood.karmaservice.application.data.KarmaSummaryDTO
import com.ase.tugood.karmaservice.infrastructure.web.model.request.ThirdPartyKarmaRequestDTO
import com.ase.tugood.karmaservice.infrastructure.web.model.request.UserInfo
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.swagger.v3.oas.annotations.ExternalDocumentation
import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.info.Contact
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.servers.Server
import io.swagger.v3.oas.annotations.servers.ServerVariable
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import java.util.*


@RestController
@RequestMapping("/api/karma")
@OpenAPIDefinition(
    info = Info(title = "TUgood Third Party Karma Service",
        description = "This is the third party API for the Karma Service.",
        contact = Contact(email = "e51824287@student.tuwien.ac.at"), version = "1.0.0"),
    externalDocs = ExternalDocumentation(description = "Project Repo",
        url = "https://reset.inso.tuwien.ac.at/repo/2020ss-ASE-group/ss20_ase_inso_02/-/tree/master/tg-karma-service"),
    servers = [
        Server(url = "http://127.0.0.1:{port}", variables = [ServerVariable(name = "port", defaultValue = "8085")])
    ]
)
class KarmaController(@Autowired private val karmaService: KarmaService) {
    private val logger: Logger = LoggerFactory.getLogger(javaClass)

    @Operation(
        description = "Retrieve karma points of user with history",
        method = "getKarma",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "Karma point summary returned", content = [Content(schema = Schema(implementation = KarmaSummaryDTO::class))])])
    @RequestMapping(
        value = [""],
        produces = ["application/json"],
        method = [RequestMethod.GET])
    fun getKarma(
        @RequestHeader("X-Userinfo") userInfo: String
    ): Mono<ResponseEntity<KarmaSummaryDTO>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        return karmaService.getKarmaSummary(info)
            .map { ResponseEntity.ok(it) }
    }

    @Operation(
        description = "Subtract karma points of a user account",
        method = "createTransaction",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "406", description = "The subtraction of the value would lead to a negative sum of karma points", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "Karma point summary returned", content = [Content(schema = Schema(implementation = KarmaSummaryDTO::class))])])
    @RequestMapping(
        value = [""],
        produces = ["application/json"],
        method = [RequestMethod.POST])
    fun createTransaction(
        @RequestHeader("X-Userinfo") userInfo: String,
        @RequestBody newTransactionThirdParty: ThirdPartyKarmaRequestDTO
    ): Mono<ResponseEntity<KarmaSummaryDTO>> {
        val info = jacksonObjectMapper().readValue<UserInfo>(String(Base64.getDecoder().decode(userInfo)))
        return karmaService.thirdPartyKarmaChange(newTransactionThirdParty, info, info.azp).map {
            ResponseEntity.ok(it)
        }
    }
}
