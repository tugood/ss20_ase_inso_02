package com.ase.tugood.notificationservice.application.data.user

import com.fasterxml.jackson.annotation.JsonIgnore
import java.time.Instant
import javax.validation.constraints.Email
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

data class UserDTO(

    var id: String = "",

    @field:Size(max = 50)
    @NotNull
    var firstName: String? = null,

    @field:Size(max = 50)
    @NotNull
    var lastName: String? = null,

    @field:Email
    @field:Size(min = 5, max = 254)
    @NotNull
    var email: String? = null,

    @field:Size(min = 4, max = 20)
    var phoneNr: String? = null,

    @field:Size(min = 2, max = 10)
    var langKey: String? = null,

    var description: String? = null,

    var hasImage: Boolean = false,

    var userState: UserState? = null,

    var distance: Long? = null,

    var avgRating: Double? = null,

    @JsonIgnore
    var createdDate: Instant? = null,

    @JsonIgnore
    var lastModifiedDate: Instant? = null
)
