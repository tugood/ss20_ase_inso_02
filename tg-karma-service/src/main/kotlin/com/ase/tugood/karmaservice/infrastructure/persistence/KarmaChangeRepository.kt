package com.ase.tugood.karmaservice.infrastructure.persistence

import com.ase.tugood.karmaservice.domain.karma.model.KarmaChange
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux

@Repository
interface KarmaChangeRepository : ReactiveMongoRepository<KarmaChange, String> {
    fun findAllByUserId(userId: String): Flux<KarmaChange>
}
