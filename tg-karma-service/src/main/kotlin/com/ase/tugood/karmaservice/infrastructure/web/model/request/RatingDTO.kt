package com.ase.tugood.karmaservice.infrastructure.web.model.request

import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.Size

data class RatingDTO(
    @field:Min(value = 0)
    @field:Max(value = 5)
    @JsonProperty("rating")
    var rating: Int? = null,

    @field:Size(min = 0, max = 2500)
    @JsonProperty("comment")
    var comment: String? = null
)
