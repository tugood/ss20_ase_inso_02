package com.ase.tugood.karmaservice.domain.karma.mapper

import com.ase.tugood.karmaservice.infrastructure.web.model.request.InternalKarmaRequestDTO
import org.springframework.stereotype.Component

@Component
class KarmaPointsMapper {

    fun scoreFinishRequest(internalRequest: InternalKarmaRequestDTO): Int {

        var basePoints = +50

        if (internalRequest.ratingDTO != null) {
            if (internalRequest.ratingDTO.rating == 4) {
                basePoints += 50
            } else if (internalRequest.ratingDTO.rating == 5) {
                basePoints += 100
            }
        }

        return basePoints
    }


    fun scoreCancelRequest(internalRequest: InternalKarmaRequestDTO): Int {
        return -150
    }

    fun scoreCompleteRating(internalRequest: InternalKarmaRequestDTO): Int {

        var basePoints = 0

        if (internalRequest.ratingDTO?.comment != null) {
            basePoints += 30
        }

        return basePoints
    }

}
