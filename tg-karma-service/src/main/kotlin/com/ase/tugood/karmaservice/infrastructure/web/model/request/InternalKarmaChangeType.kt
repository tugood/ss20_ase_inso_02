package com.ase.tugood.karmaservice.infrastructure.web.model.request

enum class InternalKarmaChangeType {
    CANCEL_REQUEST,
    FINISH_REQUEST,
}
