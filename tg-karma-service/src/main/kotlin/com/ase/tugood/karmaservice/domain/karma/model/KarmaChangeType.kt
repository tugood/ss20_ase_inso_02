package com.ase.tugood.karmaservice.domain.karma.model

enum class KarmaChangeType {
    THIRD_PARTY_TRANSACTION,
    INTERNAL_CANCEL_REQUEST,
    INTERNAL_FINISH_REQUEST,
    INTERNAL_COMPLETE_RATING
}
