package com.ase.tugood.karmaservice.infrastructure.web.rest.controller

import com.ase.tugood.karmaservice.infrastructure.web.rest.error.Error
import com.ase.tugood.karmaservice.infrastructure.web.rest.error.TUGoodException
import org.slf4j.LoggerFactory
import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono

@Order(Ordered.HIGHEST_PRECEDENCE)
@RestControllerAdvice
@ControllerAdvice
class ExceptionTranslator {
    private val logger = LoggerFactory.getLogger(javaClass)

    @ExceptionHandler(TUGoodException::class)
    fun handleTUGoodExceptions(
        ex: TUGoodException,
        request: ServerWebExchange
    ): Mono<ResponseEntity<Error>> {
        logger.info("The request: ${request.logPrefix} on the path: ${request.request.path} " +
            "failed  with the error: ${ex.message}")
        return Mono.just(ResponseEntity(ex.error, ex.status))
    }
}
