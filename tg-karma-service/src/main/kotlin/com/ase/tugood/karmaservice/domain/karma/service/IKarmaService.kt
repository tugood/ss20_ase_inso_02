package com.ase.tugood.karmaservice.domain.karma.service

import com.ase.tugood.karmaservice.application.data.KarmaChangeDTO
import com.ase.tugood.karmaservice.application.data.KarmaSummaryDTO
import com.ase.tugood.karmaservice.infrastructure.web.model.request.InternalKarmaRequestDTO
import com.ase.tugood.karmaservice.infrastructure.web.model.request.ThirdPartyKarmaRequestDTO
import com.ase.tugood.karmaservice.infrastructure.web.model.request.UserInfo
import reactor.core.publisher.Mono

interface IKarmaService {

    /**
     * The aim of this function is to persist a karma change that has been triggered TUgood internally.
     * Additionally, a notification for the corresponding user is triggered.
     *
     * @param internalKarmaRequestDTO the data corresponding to the internal karma change
     * @return a list of all persisted karma change DTOs
     */
    fun internalKarmaChange(internalKarmaRequestDTO: InternalKarmaRequestDTO): List<KarmaChangeDTO>

    /**
     * The aim of this function is to retrieve a list of all karma summary instances per specified user.
     *
     * @param userInfo the userInfo of the user we want to retrieve the karma summaries of
     * @return a list of karma summary DTOs for the specified user
     */
    fun getKarmaSummary(userInfo: UserInfo): Mono<KarmaSummaryDTO>

    /**
     * The aim of this function is to persist karma changes that have been triggered by third party entities.
     *
     * @param thirdPartyKarmaRequestDTO DTO that specifies the amount of karma spent in a transaction
     * @param userInfo the userInfo of the user that has spent the karma points at the third party entity
     * @param clientId the id of the third party entity that triggered this karma change
     * @return the karma summary DTO for the specified user after persisting this third party karma change
     */
    fun thirdPartyKarmaChange(thirdPartyKarmaRequestDTO: ThirdPartyKarmaRequestDTO, userInfo: UserInfo, clientId: String): Mono<KarmaSummaryDTO>

}
