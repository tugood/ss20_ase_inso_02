package com.ase.tugood.karmaservice.application.data

data class KarmaSummaryDTO(
    val userId: String,
    val karmaPoints: Int,
    val karmaChangeHistory: List<KarmaChangeDTO>
)
