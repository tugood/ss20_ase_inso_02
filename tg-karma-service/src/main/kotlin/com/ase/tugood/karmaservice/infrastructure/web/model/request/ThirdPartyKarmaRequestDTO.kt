package com.ase.tugood.karmaservice.infrastructure.web.model.request

import java.util.*
import javax.validation.constraints.Min

data class ThirdPartyKarmaRequestDTO(
    @Min(0)
    val value: Int,
    val requestId: UUID
)
