package com.ase.tugood.karmaservice.domain.karma.mapper

import com.ase.tugood.karmaservice.application.data.KarmaChangeDTO
import com.ase.tugood.karmaservice.application.data.KarmaSummaryDTO
import com.ase.tugood.karmaservice.domain.karma.model.KarmaChange
import com.ase.tugood.karmaservice.domain.karma.model.KarmaChangeType
import com.ase.tugood.karmaservice.infrastructure.web.model.request.InternalKarmaChangeType
import com.ase.tugood.karmaservice.infrastructure.web.model.request.InternalKarmaRequestDTO
import com.ase.tugood.karmaservice.infrastructure.web.model.request.ThirdPartyKarmaRequestDTO
import com.ase.tugood.karmaservice.infrastructure.web.model.request.UserInfo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import java.time.Instant
import java.util.*

@Component
class KarmaMapper(@Autowired private val karmaPointsMapper: KarmaPointsMapper) {
    companion object {
        const val START_KARMA = 0
    }

    fun toDto(karmaChange: KarmaChange): KarmaChangeDTO {
        return KarmaChangeDTO(
            id = karmaChange.id,
            value = karmaChange.value,
            timestamp = karmaChange.timestamp,
            type = karmaChange.type,
            thirdPartyClient = karmaChange.thirdPartyClient
        )
    }

    fun toSummaryDto(karmaChangeDTOs: List<KarmaChangeDTO>, userInfo: UserInfo): KarmaSummaryDTO {
        val currentValue = START_KARMA + karmaChangeDTOs.sumBy { it.value }

        return KarmaSummaryDTO(userId = userInfo.id, karmaChangeHistory = karmaChangeDTOs, karmaPoints = currentValue)
    }

    fun toEntities(internalKarmaRequestDTO: InternalKarmaRequestDTO): List<KarmaChange> {

        val list = ArrayList<KarmaChange>()

        when (internalKarmaRequestDTO.type) {
            InternalKarmaChangeType.CANCEL_REQUEST -> {
                list.add(KarmaChange(id = UUID.randomUUID().toString(),
                    timestamp = Instant.now(),
                    type = KarmaChangeType.INTERNAL_CANCEL_REQUEST,
                    userId = internalKarmaRequestDTO.userId,
                    value = karmaPointsMapper.scoreCancelRequest(internalKarmaRequestDTO),
                    thirdPartyClient = null
                ))
            }
            InternalKarmaChangeType.FINISH_REQUEST -> {

                val instant = Instant.now()

                list.add(KarmaChange(id = UUID.randomUUID().toString(),
                    timestamp = instant,
                    type = KarmaChangeType.INTERNAL_COMPLETE_RATING,
                    userId = internalKarmaRequestDTO.userId,
                    value = karmaPointsMapper.scoreCompleteRating(internalKarmaRequestDTO),
                    thirdPartyClient = null
                ))

                if (internalKarmaRequestDTO.helperId != null) {
                    list.add(KarmaChange(id = UUID.randomUUID().toString(),
                        timestamp = instant.plusMillis(20),
                        type = KarmaChangeType.INTERNAL_FINISH_REQUEST,
                        userId = internalKarmaRequestDTO.helperId,
                        value = karmaPointsMapper.scoreFinishRequest(internalKarmaRequestDTO),
                        thirdPartyClient = null
                    ))
                }

            }
        }

        return list
    }

    fun toEntity(thirdPartyKarmaRequestDTO: ThirdPartyKarmaRequestDTO, userInfo: UserInfo, clientId: String): KarmaChange {
        return KarmaChange(
            id = thirdPartyKarmaRequestDTO.requestId.toString(),
            timestamp = Instant.now(),
            type = KarmaChangeType.THIRD_PARTY_TRANSACTION,
            userId = userInfo.id,
            value = -thirdPartyKarmaRequestDTO.value,
            thirdPartyClient = clientId
        )
    }
}
