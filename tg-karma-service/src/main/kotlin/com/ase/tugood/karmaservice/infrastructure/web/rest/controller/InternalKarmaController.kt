package com.ase.tugood.karmaservice.infrastructure.web.rest.controller

import com.ase.tugood.karmaservice.application.KarmaService
import com.ase.tugood.karmaservice.application.data.KarmaChangeDTO
import com.ase.tugood.karmaservice.application.data.KarmaSummaryDTO
import com.ase.tugood.karmaservice.infrastructure.web.model.request.InternalKarmaRequestDTO
import io.swagger.v3.oas.annotations.ExternalDocumentation
import io.swagger.v3.oas.annotations.OpenAPIDefinition
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.info.Contact
import io.swagger.v3.oas.annotations.info.Info
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.servers.Server
import io.swagger.v3.oas.annotations.servers.ServerVariable
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/internal/karma")
@OpenAPIDefinition(
    info = Info(title = "TUgood Internal Karma Service",
        description = "This is the internal API for the Karma Service.",
        contact = Contact(email = "e51824287@student.tuwien.ac.at"), version = "1.0.0"),
    externalDocs = ExternalDocumentation(description = "Project Repo",
        url = "https://reset.inso.tuwien.ac.at/repo/2020ss-ASE-group/ss20_ase_inso_02/-/tree/master/tg-karma-service"),
    servers = [
        Server(url = "http://127.0.0.1:{port}", variables = [ServerVariable(name = "port", defaultValue = "8085")])
    ]
)
class InternalKarmaController(@Autowired private val karmaService: KarmaService) {

    @Operation(
        description = "Add or subtract karma points from a user account.",
        method = "changeKarma",
        responses = [
            ApiResponse(responseCode = "400", description = "The JSON is not valid", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "401", description = "The request requires an user authentication", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "404", description = "The resource could not be found", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "500", description = "An unexpected error occurred.", content = [Content(schema = Schema(implementation = Error::class))]),
            ApiResponse(responseCode = "200", description = "List of all karma point records", content = [Content(schema = Schema(implementation = Array<KarmaChangeDTO>::class))])]
    )
    @RequestMapping(
        value = [""],
        produces = ["application/json"],
        method = [RequestMethod.POST])
    fun changeKarma(
        @RequestBody requestDTO: InternalKarmaRequestDTO
    ): ResponseEntity<List<KarmaChangeDTO>> {
        return ResponseEntity.ok(karmaService.internalKarmaChange(internalKarmaRequestDTO = requestDTO))
    }
}
