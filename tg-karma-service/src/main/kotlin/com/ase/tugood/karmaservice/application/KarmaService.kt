package com.ase.tugood.karmaservice.application

import com.ase.tugood.karmaservice.application.data.KarmaChangeDTO
import com.ase.tugood.karmaservice.application.data.KarmaSummaryDTO
import com.ase.tugood.karmaservice.domain.karma.mapper.KarmaMapper
import com.ase.tugood.karmaservice.domain.karma.model.KarmaChange
import com.ase.tugood.karmaservice.domain.karma.model.KarmaChangeType
import com.ase.tugood.karmaservice.domain.karma.service.IKarmaService
import com.ase.tugood.karmaservice.infrastructure.persistence.KarmaChangeRepository
import com.ase.tugood.karmaservice.infrastructure.web.model.request.InternalKarmaRequestDTO
import com.ase.tugood.karmaservice.infrastructure.web.model.request.ThirdPartyKarmaRequestDTO
import com.ase.tugood.karmaservice.infrastructure.web.model.request.UserInfo
import com.ase.tugood.karmaservice.infrastructure.web.rest.error.TUGoodException
import com.ase.tugood.requestservice.application.data.notification.ServiceNotificationDTO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class KarmaService(@Autowired private val karmaMapper: KarmaMapper,
                   @Autowired private val karmaChangeRepository: KarmaChangeRepository,
                   @Autowired private val notificationService: NotificationService) : IKarmaService {

    override fun internalKarmaChange(internalKarmaRequestDTO: InternalKarmaRequestDTO): List<KarmaChangeDTO> {

        return karmaMapper.toEntities(internalKarmaRequestDTO)
            .map { karmaChange ->
                karmaChangeRepository
                    .insert(karmaChange).subscribe()

                notifyUser(karmaChange)

                karmaMapper.toDto(karmaChange)
            }

    }

    override fun getKarmaSummary(userInfo: UserInfo): Mono<KarmaSummaryDTO> {
        return karmaChangeRepository
            .findAllByUserId(userId = userInfo.id)
            .map { sub -> karmaMapper.toDto(sub) }
            .collectList()
            .map { list ->
                karmaMapper.toSummaryDto(list, userInfo)
            }

    }

    override fun thirdPartyKarmaChange(thirdPartyKarmaRequestDTO: ThirdPartyKarmaRequestDTO, userInfo: UserInfo, clientId: String): Mono<KarmaSummaryDTO> {

        return doesRequestExist(thirdPartyKarmaRequestDTO, userInfo)
            .flatMap { requestExists ->
                val summary = getKarmaSummary(userInfo)

                if (!requestExists)
                    summary.map { karmaSummary ->
                        storeKarmaChange(karmaSummary, thirdPartyKarmaRequestDTO, userInfo, clientId)
                    }
                else
                    summary
            }

    }


    /**
     * The aim of this function is to check if the specified third party karma request already exists.
     *
     * @param thirdPartyKarmaRequestDTO the third party karma request to be checked
     * @param userInfo the user that is involved in the  third party karma request
     * @return data stream of true if the request already exists, false otherwise
     * @throws TUGoodException if the third party karma request already exists with different values
     */
    private fun doesRequestExist(thirdPartyKarmaRequestDTO: ThirdPartyKarmaRequestDTO, userInfo: UserInfo): Mono<Boolean> {
        return karmaChangeRepository.findById(thirdPartyKarmaRequestDTO.requestId.toString())
            .map {
                if (thirdPartyKarmaRequestDTO.value != it.value || userInfo.id != it.userId)
                    throw TUGoodException("Request id already exists with different values",
                        status = HttpStatus.NOT_ACCEPTABLE,
                        cause = null,
                        type = "ThirdPartyKarmaChange"
                    )
                else
                    true
            }.defaultIfEmpty(false)
    }

    /**
     * The aim of this function is to store the karma change if there is enough karma to spend. The user is
     * notified if karma has been spent. A NOT_ACCEPTABLE response is triggered, if the user does not have
     * enough karma points left.
     *
     * @param karmaSummary the karma summary DTO of the specified user
     * @param thirdPartyKarmaRequestDTO the third party karma request to spend karma via a third party entity
     * @param userInfo the userInfo of the corresponding user
     * @param clientId the id of the third party entity
     * @return the karma summary DTO after the change request has been processed
     * @throws TUGoodException if the specified user does not have enough karma points to spend
     */
    private fun storeKarmaChange(karmaSummary: KarmaSummaryDTO, thirdPartyKarmaRequestDTO: ThirdPartyKarmaRequestDTO, userInfo: UserInfo, clientId: String): KarmaSummaryDTO {

        if (karmaSummary.karmaPoints - thirdPartyKarmaRequestDTO.value < 0) {

            throw TUGoodException("Not enough karma points available",
                status = HttpStatus.NOT_ACCEPTABLE,
                cause = null,
                type = "ThirdPartyKarmaChange"
            )
        }

        val karmaChange = karmaMapper.toEntity(thirdPartyKarmaRequestDTO, userInfo, clientId)

        karmaChangeRepository
            .insert(karmaChange).subscribe()

        notifyUser(karmaChange)

        return KarmaSummaryDTO(
            userId = karmaSummary.userId,
            karmaPoints = karmaSummary.karmaPoints - thirdPartyKarmaRequestDTO.value,
            karmaChangeHistory = karmaSummary.karmaChangeHistory + KarmaChangeDTO(
                id = karmaChange.id,
                type = KarmaChangeType.THIRD_PARTY_TRANSACTION,
                value = -thirdPartyKarmaRequestDTO.value,
                timestamp = karmaChange.timestamp,
                thirdPartyClient = clientId
            )
        ) // instead of accessing the database again return it directly

    }

    /**
     * The aim of this function is to trigger a notification for a specified karma change.
     *
     * @param karmaChange the karma change instance about which we want to notify
     */
    private fun notifyUser(karmaChange: KarmaChange) {

        val karmaValueString = if (karmaChange.value >= 0) "+" + karmaChange.value else "-" + karmaChange.value

        val karmaMessageBodyTag = when (karmaChange.type) {
            KarmaChangeType.INTERNAL_CANCEL_REQUEST -> "notification.karma.cancel.body"
            KarmaChangeType.INTERNAL_COMPLETE_RATING -> "notification.karma.rate.body"
            KarmaChangeType.INTERNAL_FINISH_REQUEST -> "notification.karma.finish.body"
            KarmaChangeType.THIRD_PARTY_TRANSACTION -> "notification.karma.thirdparty.body"
        }

        val karmaMessageArguments = if (karmaChange.thirdPartyClient != null)
            arrayOf(karmaValueString, karmaChange.thirdPartyClient)
        else arrayOf(karmaValueString)

        notificationService.notify(karmaChange.userId,
            Pair("notification.karma.title", arrayOf(karmaValueString)),
            Pair(karmaMessageBodyTag, karmaMessageArguments),
            ServiceNotificationDTO.NotificationType.BOTH).ignoreElement().subscribe()
    }
}
