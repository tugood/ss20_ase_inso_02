package com.ase.tugood.karmaservice.infrastructure.web.rest.util

import com.fasterxml.jackson.databind.annotation.JsonSerialize

@JsonSerialize
class EmptyJsonResponse
