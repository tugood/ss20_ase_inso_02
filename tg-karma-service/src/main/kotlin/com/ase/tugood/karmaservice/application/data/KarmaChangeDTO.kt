package com.ase.tugood.karmaservice.application.data

import com.ase.tugood.karmaservice.domain.karma.model.KarmaChangeType
import java.time.Instant

data class KarmaChangeDTO(
    val id: String,
    val value: Int,
    val timestamp: Instant,
    val type: KarmaChangeType,
    val thirdPartyClient: String?
)
