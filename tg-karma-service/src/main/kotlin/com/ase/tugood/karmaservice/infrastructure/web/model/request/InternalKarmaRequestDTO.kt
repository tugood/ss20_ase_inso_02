package com.ase.tugood.karmaservice.infrastructure.web.model.request

data class InternalKarmaRequestDTO(
    val userId: String,
    val type: InternalKarmaChangeType,
    val helperId: String? = null,
    val ratingDTO: RatingDTO? = null,
    val matchingScore: Long? = null
)
