package com.ase.tugood.karmaservice.infrastructure.web.model.request

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonIgnoreProperties(ignoreUnknown = true)
data class UserInfo(
    @JsonProperty("azp")
    val azp: String = "angular",

    @JsonProperty("id")
    val id: String,

    @JsonProperty("locale")
    val locale: String? = null,

    @JsonProperty("name")
    val name: String? = null,

    @JsonProperty("username")
    val username: String? = null,

    @JsonProperty("email")
    val email: String? = null,

    @JsonProperty("given_name")
    val given_name: String? = null,

    @JsonProperty("sub")
    val sub: String? = null,

    @JsonProperty("preferred_username")
    val preferred_username: String? = null,

    @JsonProperty("email_verified")
    val email_verified: Boolean,

    @JsonProperty("family_name")
    val family_name: String? = null
) : Serializable
