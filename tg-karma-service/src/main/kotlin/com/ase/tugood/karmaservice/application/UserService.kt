package com.ase.tugood.karmaservice.application

import com.ase.tugood.karmaservice.infrastructure.web.client.UserServiceClient
import com.ase.tugood.notificationservice.application.data.user.UserDTO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class UserService(@Autowired private val userServiceClient: UserServiceClient) {

    /**
     * The aim of this function is to get the user DTO by userId from tg-user-service.
     *
     * @param userId the id of the user we want to get the DTO from.
     * @return a data stream holding the retrieved user DTO.
     */
    fun getUserForId(userId: String): Mono<UserDTO> {
        return userServiceClient
            .getUserData(userId)
    }
}
