package com.ase.tugood.karmaservice

import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories
import java.security.Security


@SpringBootApplication
@EnableReactiveMongoRepositories
class MainApplication

fun main(args: Array<String>) {
    Security.addProvider(BouncyCastleProvider())

    runApplication<MainApplication>(*args)
}
