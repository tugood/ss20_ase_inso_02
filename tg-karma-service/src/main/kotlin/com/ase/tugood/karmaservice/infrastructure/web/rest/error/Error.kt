package com.ase.tugood.karmaservice.infrastructure.web.rest.error

import com.fasterxml.jackson.annotation.JsonProperty

/**
 *
 * @param message
 * @param type
 * @param code
 * @param errors
 */
data class Error(

    @JsonProperty("message")
    val message: String,

    @JsonProperty("type")
    val type: String,

    @Deprecated(message = "")
    @JsonProperty("code")
    val code: Int? = null,

    @JsonProperty("cause")
    val cause: MutableList<Error?> = mutableListOf()
)

