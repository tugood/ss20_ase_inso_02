package com.ase.tugood.karmaservice.infrastructure.web.rest.error

import org.springframework.http.HttpStatus

open class TUGoodException(message: String,
                           val status: HttpStatus,
                           type: String,
                           cause: Throwable?) : RuntimeException(message, cause) {

    val error: Error = Error(message = message, code = status.value(), type = type)

    init {
        if (cause != null)
            if (cause is TUGoodException) {
                error.cause.add(cause.error)
            }
    }
}
