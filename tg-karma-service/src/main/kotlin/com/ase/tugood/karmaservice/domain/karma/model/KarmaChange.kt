package com.ase.tugood.karmaservice.domain.karma.model

import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import java.time.Instant
import javax.validation.constraints.NotNull

@Document(collection = "tugood_karma_change")
data class KarmaChange @JvmOverloads constructor(
    @Id
    var id: String,

    @NotNull
    @Field("userId")
    var userId: String,

    @NotNull
    @Field("value")
    var value: Int,

    @NotNull
    @Field("timestamp")
    @CreatedDate
    var timestamp: Instant,

    @NotNull
    @Field("type")
    var type: KarmaChangeType,

    @Field("thirdPartyClient")
    var thirdPartyClient: String?
)
