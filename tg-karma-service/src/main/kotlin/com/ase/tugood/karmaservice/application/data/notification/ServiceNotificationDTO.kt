package com.ase.tugood.requestservice.application.data.notification

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import javax.validation.constraints.NotNull

@JsonInclude(JsonInclude.Include.NON_NULL)
data class ServiceNotificationDTO(

    @NotNull
    @JsonProperty("title")
    var title: String = "",

    @JsonProperty("badge")
    var badge: String? = null,

    @NotNull
    @JsonProperty("body")
    var body: String = "",

    @JsonProperty("icon")
    var icon: String? = null,

    @NotNull
    @JsonProperty("userId")
    var userId: String = "",

    @NotNull
    @JsonProperty("userEmail")
    var userEmail: String? = null,

    @NotNull
    @JsonProperty("tag")
    var tag: String = "request",

    @NotNull
    @JsonProperty("notificationType")
    var notificationType: NotificationType = NotificationType.PLAIN_OLD
) {
    enum class NotificationType {
        PUSH,
        PLAIN_OLD,
        BOTH // if not online send a push notification
    }
}
